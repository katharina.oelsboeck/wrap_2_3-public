POISSON POINT PROCESS
# trials, dim, window size,     lambda, periodic
     100,   3,          18,          1,        0

TRIAL, 0
TIME FOR NEWLY COMPUTING COMPLEXES, 5.33597
Elapsed time: 5.58693
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.14661
TIME FOR NEWLY COMPUTING COMPLEXES, 5.06791
Elapsed time: 5.29919
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.2955
TIME FOR NEWLY COMPUTING COMPLEXES, 5.15907
Elapsed time: 5.49413

TRIAL, 1
TIME FOR NEWLY COMPUTING COMPLEXES, 5.09879
Elapsed time: 5.33
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.26365
TIME FOR NEWLY COMPUTING COMPLEXES, 5.01534
Elapsed time: 5.25709
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.10699
TIME FOR NEWLY COMPUTING COMPLEXES, 5.3179
Elapsed time: 5.54832

TRIAL, 2
TIME FOR NEWLY COMPUTING COMPLEXES, 5.07786
Elapsed time: 5.36679
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.72251
TIME FOR NEWLY COMPUTING COMPLEXES, 5.51734
Elapsed time: 5.75441
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.908145
TIME FOR NEWLY COMPUTING COMPLEXES, 5.16338
Elapsed time: 5.40394

TRIAL, 3
TIME FOR NEWLY COMPUTING COMPLEXES, 6.35366
Elapsed time: 6.62881
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.59819
TIME FOR NEWLY COMPUTING COMPLEXES, 5.24358
Elapsed time: 5.48441
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.21818
TIME FOR NEWLY COMPUTING COMPLEXES, 5.26934
Elapsed time: 5.51648

TRIAL, 4
TIME FOR NEWLY COMPUTING COMPLEXES, 5.09788
Elapsed time: 5.33935
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.42913
TIME FOR NEWLY COMPUTING COMPLEXES, 4.93132
Elapsed time: 5.18376
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.90307
TIME FOR NEWLY COMPUTING COMPLEXES, 4.92884
Elapsed time: 5.15898

TRIAL, 5
TIME FOR NEWLY COMPUTING COMPLEXES, 4.86674
Elapsed time: 5.10366
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.92277
TIME FOR NEWLY COMPUTING COMPLEXES, 5.02446
Elapsed time: 5.29883
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.86566
TIME FOR NEWLY COMPUTING COMPLEXES, 5.12645
Elapsed time: 5.36409

TRIAL, 6
TIME FOR NEWLY COMPUTING COMPLEXES, 4.77535
Elapsed time: 5.01184
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.14025
TIME FOR NEWLY COMPUTING COMPLEXES, 5.012
Elapsed time: 5.25522
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.61784
TIME FOR NEWLY COMPUTING COMPLEXES, 4.96552
Elapsed time: 5.21821

TRIAL, 7
TIME FOR NEWLY COMPUTING COMPLEXES, 4.91394
Elapsed time: 5.1438
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.62536
TIME FOR NEWLY COMPUTING COMPLEXES, 4.90219
Elapsed time: 5.13493
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.03063
TIME FOR NEWLY COMPUTING COMPLEXES, 5.0478
Elapsed time: 5.28902

TRIAL, 8
TIME FOR NEWLY COMPUTING COMPLEXES, 4.99681
Elapsed time: 5.2387
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.732991
TIME FOR NEWLY COMPUTING COMPLEXES, 4.97518
Elapsed time: 5.25092
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.90204
TIME FOR NEWLY COMPUTING COMPLEXES, 5.0962
Elapsed time: 5.34232

TRIAL, 9
TIME FOR NEWLY COMPUTING COMPLEXES, 4.89113
Elapsed time: 5.13201
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.47472
TIME FOR NEWLY COMPUTING COMPLEXES, 4.97262
Elapsed time: 5.21374
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.12569
TIME FOR NEWLY COMPUTING COMPLEXES, 4.88279
Elapsed time: 5.13241

TRIAL, 10
TIME FOR NEWLY COMPUTING COMPLEXES, 4.88278
Elapsed time: 5.13861
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.15854
TIME FOR NEWLY COMPUTING COMPLEXES, 5.03397
Elapsed time: 5.28506
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.05583
TIME FOR NEWLY COMPUTING COMPLEXES, 4.94902
Elapsed time: 5.18747

TRIAL, 11
TIME FOR NEWLY COMPUTING COMPLEXES, 4.91481
Elapsed time: 5.16605
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.49083
TIME FOR NEWLY COMPUTING COMPLEXES, 5.06376
Elapsed time: 5.2995
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.27797
TIME FOR NEWLY COMPUTING COMPLEXES, 5.03711
Elapsed time: 5.2934

TRIAL, 12
TIME FOR NEWLY COMPUTING COMPLEXES, 5.23679
Elapsed time: 5.47683
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.10234
TIME FOR NEWLY COMPUTING COMPLEXES, 4.84474
Elapsed time: 5.08309
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.877957
TIME FOR NEWLY COMPUTING COMPLEXES, 5.38153
Elapsed time: 5.66881

TRIAL, 13
TIME FOR NEWLY COMPUTING COMPLEXES, 5.11426
Elapsed time: 5.36128
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.31255
TIME FOR NEWLY COMPUTING COMPLEXES, 5.10606
Elapsed time: 5.41898
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.650284
TIME FOR NEWLY COMPUTING COMPLEXES, 5.03272
Elapsed time: 5.28014

TRIAL, 14
TIME FOR NEWLY COMPUTING COMPLEXES, 5.12748
Elapsed time: 5.37883
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.55974
TIME FOR NEWLY COMPUTING COMPLEXES, 4.94376
Elapsed time: 5.20548
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.10958
TIME FOR NEWLY COMPUTING COMPLEXES, 5.27399
Elapsed time: 5.52504

TRIAL, 15
TIME FOR NEWLY COMPUTING COMPLEXES, 5.0649
Elapsed time: 5.31271
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.41523
TIME FOR NEWLY COMPUTING COMPLEXES, 5.37119
Elapsed time: 5.64084
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.63979
TIME FOR NEWLY COMPUTING COMPLEXES, 5.23444
Elapsed time: 5.48834

TRIAL, 16
TIME FOR NEWLY COMPUTING COMPLEXES, 5.01558
Elapsed time: 5.26321
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.40648
TIME FOR NEWLY COMPUTING COMPLEXES, 5.05578
Elapsed time: 5.31649
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.96628
TIME FOR NEWLY COMPUTING COMPLEXES, 4.81153
Elapsed time: 5.22943

TRIAL, 17
TIME FOR NEWLY COMPUTING COMPLEXES, 5.38257
Elapsed time: 5.6351
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.3908
TIME FOR NEWLY COMPUTING COMPLEXES, 4.72424
Elapsed time: 5.20126
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.19721
TIME FOR NEWLY COMPUTING COMPLEXES, 4.78551
Elapsed time: 5.19624

TRIAL, 18
TIME FOR NEWLY COMPUTING COMPLEXES, 4.93987
Elapsed time: 5.23527
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.53429
TIME FOR NEWLY COMPUTING COMPLEXES, 6.64106
Elapsed time: 6.91311
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 3.42181
TIME FOR NEWLY COMPUTING COMPLEXES, 9.27951
Elapsed time: 9.67074

TRIAL, 19
TIME FOR NEWLY COMPUTING COMPLEXES, 6.0272
Elapsed time: 6.27022
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.57508
TIME FOR NEWLY COMPUTING COMPLEXES, 6.39513
Elapsed time: 6.71105
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.17077
TIME FOR NEWLY COMPUTING COMPLEXES, 5.59465
Elapsed time: 5.86179

TRIAL, 20
TIME FOR NEWLY COMPUTING COMPLEXES, 5.66135
Elapsed time: 6.00753
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.49676
TIME FOR NEWLY COMPUTING COMPLEXES, 5.44015
Elapsed time: 5.73231
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.50623
TIME FOR NEWLY COMPUTING COMPLEXES, 5.12421
Elapsed time: 5.39459

TRIAL, 21
TIME FOR NEWLY COMPUTING COMPLEXES, 5.25067
Elapsed time: 5.50726
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.94579
TIME FOR NEWLY COMPUTING COMPLEXES, 5.04673
Elapsed time: 5.29868
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.54309
TIME FOR NEWLY COMPUTING COMPLEXES, 5.37858
Elapsed time: 5.89129

TRIAL, 22
TIME FOR NEWLY COMPUTING COMPLEXES, 5.37174
Elapsed time: 5.62607
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.964964
TIME FOR NEWLY COMPUTING COMPLEXES, 5.18225
Elapsed time: 5.4401
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.86893
TIME FOR NEWLY COMPUTING COMPLEXES, 5.94818
Elapsed time: 6.44198

TRIAL, 23
TIME FOR NEWLY COMPUTING COMPLEXES, 5.02823
Elapsed time: 5.28215
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.55558
TIME FOR NEWLY COMPUTING COMPLEXES, 5.19891
Elapsed time: 5.45037
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.1587
TIME FOR NEWLY COMPUTING COMPLEXES, 4.89943
Elapsed time: 5.18626

TRIAL, 24
TIME FOR NEWLY COMPUTING COMPLEXES, 6.05913
Elapsed time: 6.36029
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.26003
TIME FOR NEWLY COMPUTING COMPLEXES, 5.95575
Elapsed time: 6.24878
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.26887
TIME FOR NEWLY COMPUTING COMPLEXES, 5.58194
Elapsed time: 6.02781

TRIAL, 25
TIME FOR NEWLY COMPUTING COMPLEXES, 5.32293
Elapsed time: 5.58438
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.26299
TIME FOR NEWLY COMPUTING COMPLEXES, 5.49261
Elapsed time: 5.75688
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.5127
TIME FOR NEWLY COMPUTING COMPLEXES, 5.76929
Elapsed time: 6.36644

TRIAL, 26
TIME FOR NEWLY COMPUTING COMPLEXES, 8.55318
Elapsed time: 9.07199
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.92721
TIME FOR NEWLY COMPUTING COMPLEXES, 6.10094
Elapsed time: 6.77655
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.30027
TIME FOR NEWLY COMPUTING COMPLEXES, 5.43596
Elapsed time: 5.7404

TRIAL, 27
TIME FOR NEWLY COMPUTING COMPLEXES, 5.74131
Elapsed time: 6.00315
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.217
TIME FOR NEWLY COMPUTING COMPLEXES, 7.05154
Elapsed time: 7.61422
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.86326
TIME FOR NEWLY COMPUTING COMPLEXES, 9.24848
Elapsed time: 9.81491

TRIAL, 28
TIME FOR NEWLY COMPUTING COMPLEXES, 6.25932
Elapsed time: 6.51577
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.53133
TIME FOR NEWLY COMPUTING COMPLEXES, 5.92101
Elapsed time: 6.1857
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.68184
TIME FOR NEWLY COMPUTING COMPLEXES, 6.01419
Elapsed time: 6.29323

TRIAL, 29
TIME FOR NEWLY COMPUTING COMPLEXES, 5.37106
Elapsed time: 5.72085
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.92934
TIME FOR NEWLY COMPUTING COMPLEXES, 6.13514
Elapsed time: 6.51197
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.2778
TIME FOR NEWLY COMPUTING COMPLEXES, 4.93603
Elapsed time: 5.18671

TRIAL, 30
TIME FOR NEWLY COMPUTING COMPLEXES, 4.65556
Elapsed time: 4.9
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.2557
TIME FOR NEWLY COMPUTING COMPLEXES, 5.22854
Elapsed time: 5.58925
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.06512
TIME FOR NEWLY COMPUTING COMPLEXES, 4.82939
Elapsed time: 5.19265

TRIAL, 31
TIME FOR NEWLY COMPUTING COMPLEXES, 5.50329
Elapsed time: 5.77097
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.2055
TIME FOR NEWLY COMPUTING COMPLEXES, 4.97863
Elapsed time: 5.35723
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.0855
TIME FOR NEWLY COMPUTING COMPLEXES, 5.13362
Elapsed time: 5.58675

TRIAL, 32
TIME FOR NEWLY COMPUTING COMPLEXES, 4.88386
Elapsed time: 5.11913
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.57615
TIME FOR NEWLY COMPUTING COMPLEXES, 7.46923
Elapsed time: 7.82801
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.29967
TIME FOR NEWLY COMPUTING COMPLEXES, 5.51741
Elapsed time: 5.94895

TRIAL, 33
TIME FOR NEWLY COMPUTING COMPLEXES, 4.7774
Elapsed time: 5.00734
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.691997
TIME FOR NEWLY COMPUTING COMPLEXES, 5.58175
Elapsed time: 5.82335
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.17117
TIME FOR NEWLY COMPUTING COMPLEXES, 4.90688
Elapsed time: 5.35347

TRIAL, 34
TIME FOR NEWLY COMPUTING COMPLEXES, 5.05466
Elapsed time: 5.31198
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 3.55472
TIME FOR NEWLY COMPUTING COMPLEXES, 6.75604
Elapsed time: 7.04287
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.24856
TIME FOR NEWLY COMPUTING COMPLEXES, 8.47431
Elapsed time: 9.04369

TRIAL, 35
TIME FOR NEWLY COMPUTING COMPLEXES, 6.81028
Elapsed time: 7.06641
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.54294
TIME FOR NEWLY COMPUTING COMPLEXES, 4.81535
Elapsed time: 5.07312
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.94405
TIME FOR NEWLY COMPUTING COMPLEXES, 4.847
Elapsed time: 5.09237

TRIAL, 36
TIME FOR NEWLY COMPUTING COMPLEXES, 5.43502
Elapsed time: 5.73616
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.55118
TIME FOR NEWLY COMPUTING COMPLEXES, 5.34541
Elapsed time: 5.63672
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.51348
TIME FOR NEWLY COMPUTING COMPLEXES, 5.68914
Elapsed time: 6.10826

TRIAL, 37
TIME FOR NEWLY COMPUTING COMPLEXES, 6.18267
Elapsed time: 6.42771
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.54706
TIME FOR NEWLY COMPUTING COMPLEXES, 5.43226
Elapsed time: 5.99268
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.13221
TIME FOR NEWLY COMPUTING COMPLEXES, 5.31878
Elapsed time: 5.73856

TRIAL, 38
TIME FOR NEWLY COMPUTING COMPLEXES, 5.32908
Elapsed time: 5.57479
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.95468
TIME FOR NEWLY COMPUTING COMPLEXES, 5.50842
Elapsed time: 5.76295
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.09772
TIME FOR NEWLY COMPUTING COMPLEXES, 5.43822
Elapsed time: 5.87947

TRIAL, 39
TIME FOR NEWLY COMPUTING COMPLEXES, 5.49055
Elapsed time: 5.79192
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.963472
TIME FOR NEWLY COMPUTING COMPLEXES, 5.41683
Elapsed time: 5.86599
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.02922
TIME FOR NEWLY COMPUTING COMPLEXES, 5.20821
Elapsed time: 5.48875

TRIAL, 40
TIME FOR NEWLY COMPUTING COMPLEXES, 5.21772
Elapsed time: 5.48125
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.52582
TIME FOR NEWLY COMPUTING COMPLEXES, 6.1972
Elapsed time: 6.5001
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.19962
TIME FOR NEWLY COMPUTING COMPLEXES, 5.47847
Elapsed time: 5.95426

TRIAL, 41
TIME FOR NEWLY COMPUTING COMPLEXES, 5.81932
Elapsed time: 6.11103
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.43331
TIME FOR NEWLY COMPUTING COMPLEXES, 5.4984
Elapsed time: 5.91996
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.49651
TIME FOR NEWLY COMPUTING COMPLEXES, 6.31094
Elapsed time: 6.60455

TRIAL, 42
TIME FOR NEWLY COMPUTING COMPLEXES, 5.10426
Elapsed time: 5.40911
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.38399
TIME FOR NEWLY COMPUTING COMPLEXES, 5.22111
Elapsed time: 5.45298
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.20319
TIME FOR NEWLY COMPUTING COMPLEXES, 5.35294
Elapsed time: 5.80793

TRIAL, 43
TIME FOR NEWLY COMPUTING COMPLEXES, 4.76996
Elapsed time: 5.00376
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.50766
TIME FOR NEWLY COMPUTING COMPLEXES, 6.10613
Elapsed time: 6.42538
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.17579
TIME FOR NEWLY COMPUTING COMPLEXES, 4.84271
Elapsed time: 5.08113

TRIAL, 44
TIME FOR NEWLY COMPUTING COMPLEXES, 5.38012
Elapsed time: 5.60503
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.51097
TIME FOR NEWLY COMPUTING COMPLEXES, 5.09793
Elapsed time: 5.3414
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.10426
TIME FOR NEWLY COMPUTING COMPLEXES, 4.60835
Elapsed time: 4.83867

TRIAL, 45
TIME FOR NEWLY COMPUTING COMPLEXES, 4.73453
Elapsed time: 4.98363
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.97334
TIME FOR NEWLY COMPUTING COMPLEXES, 4.67506
Elapsed time: 4.91651
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.4339
TIME FOR NEWLY COMPUTING COMPLEXES, 4.58774
Elapsed time: 4.8203

TRIAL, 46
TIME FOR NEWLY COMPUTING COMPLEXES, 6.02059
Elapsed time: 6.28116
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.95551
TIME FOR NEWLY COMPUTING COMPLEXES, 4.9248
Elapsed time: 5.16971
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.67432
TIME FOR NEWLY COMPUTING COMPLEXES, 5.13198
Elapsed time: 5.38134

TRIAL, 47
TIME FOR NEWLY COMPUTING COMPLEXES, 6.16591
Elapsed time: 6.46508
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.42415
TIME FOR NEWLY COMPUTING COMPLEXES, 6.13708
Elapsed time: 6.41613
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.73922
TIME FOR NEWLY COMPUTING COMPLEXES, 6.41416
Elapsed time: 6.9132

TRIAL, 48
TIME FOR NEWLY COMPUTING COMPLEXES, 6.16761
Elapsed time: 6.43968
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.34336
TIME FOR NEWLY COMPUTING COMPLEXES, 5.09591
Elapsed time: 5.33737
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.42456
TIME FOR NEWLY COMPUTING COMPLEXES, 5.88919
Elapsed time: 6.12616

TRIAL, 49
TIME FOR NEWLY COMPUTING COMPLEXES, 5.19809
Elapsed time: 5.49328
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.71011
TIME FOR NEWLY COMPUTING COMPLEXES, 5.54197
Elapsed time: 5.99373
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.99069
TIME FOR NEWLY COMPUTING COMPLEXES, 5.16319
Elapsed time: 5.4259

TRIAL, 50
TIME FOR NEWLY COMPUTING COMPLEXES, 5.27929
Elapsed time: 5.5451
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.34618
TIME FOR NEWLY COMPUTING COMPLEXES, 5.39397
Elapsed time: 5.64219
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.16138
TIME FOR NEWLY COMPUTING COMPLEXES, 5.64586
Elapsed time: 5.89769

TRIAL, 51
TIME FOR NEWLY COMPUTING COMPLEXES, 5.11215
Elapsed time: 5.37241
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.20721
TIME FOR NEWLY COMPUTING COMPLEXES, 5.49639
Elapsed time: 5.78279
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.48016
TIME FOR NEWLY COMPUTING COMPLEXES, 5.33987
Elapsed time: 5.61915

TRIAL, 52
TIME FOR NEWLY COMPUTING COMPLEXES, 4.88851
Elapsed time: 5.16237
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.04439
TIME FOR NEWLY COMPUTING COMPLEXES, 5.06052
Elapsed time: 5.44977
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.78912
TIME FOR NEWLY COMPUTING COMPLEXES, 5.07782
Elapsed time: 5.34278

TRIAL, 53
TIME FOR NEWLY COMPUTING COMPLEXES, 5.42135
Elapsed time: 5.67946
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.63083
TIME FOR NEWLY COMPUTING COMPLEXES, 5.45832
Elapsed time: 5.7217
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.08388
TIME FOR NEWLY COMPUTING COMPLEXES, 5.36299
Elapsed time: 5.68737

TRIAL, 54
TIME FOR NEWLY COMPUTING COMPLEXES, 4.84107
Elapsed time: 5.11555
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.03137
TIME FOR NEWLY COMPUTING COMPLEXES, 5.30312
Elapsed time: 5.56129
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.52748
TIME FOR NEWLY COMPUTING COMPLEXES, 4.91446
Elapsed time: 5.1862

TRIAL, 55
TIME FOR NEWLY COMPUTING COMPLEXES, 5.39383
Elapsed time: 5.66025
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.28965
TIME FOR NEWLY COMPUTING COMPLEXES, 5.2362
Elapsed time: 5.48565
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.28571
TIME FOR NEWLY COMPUTING COMPLEXES, 5.91659
Elapsed time: 6.37302

TRIAL, 56
TIME FOR NEWLY COMPUTING COMPLEXES, 6.60251
Elapsed time: 6.88021
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.3744
TIME FOR NEWLY COMPUTING COMPLEXES, 6.43612
Elapsed time: 6.75497
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.74006
TIME FOR NEWLY COMPUTING COMPLEXES, 6.45101
Elapsed time: 6.95726

TRIAL, 57
TIME FOR NEWLY COMPUTING COMPLEXES, 5.69153
Elapsed time: 6.02622
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.729443
TIME FOR NEWLY COMPUTING COMPLEXES, 5.68429
Elapsed time: 6.06065
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.79017
TIME FOR NEWLY COMPUTING COMPLEXES, 6.36789
Elapsed time: 6.65806

TRIAL, 58
TIME FOR NEWLY COMPUTING COMPLEXES, 6.42476
Elapsed time: 6.70664
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.47306
TIME FOR NEWLY COMPUTING COMPLEXES, 6.69349
Elapsed time: 7.20662
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.92128
TIME FOR NEWLY COMPUTING COMPLEXES, 6.0794
Elapsed time: 6.60636

TRIAL, 59
TIME FOR NEWLY COMPUTING COMPLEXES, 5.66059
Elapsed time: 5.95347
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.073
TIME FOR NEWLY COMPUTING COMPLEXES, 6.21887
Elapsed time: 6.52108
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.85433
TIME FOR NEWLY COMPUTING COMPLEXES, 6.62785
Elapsed time: 6.93529

TRIAL, 60
TIME FOR NEWLY COMPUTING COMPLEXES, 6.42749
Elapsed time: 6.75257
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.836376
TIME FOR NEWLY COMPUTING COMPLEXES, 6.33949
Elapsed time: 6.71396
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.41993
TIME FOR NEWLY COMPUTING COMPLEXES, 5.00831
Elapsed time: 5.47269

TRIAL, 61
TIME FOR NEWLY COMPUTING COMPLEXES, 4.95017
Elapsed time: 5.20997
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.949739
TIME FOR NEWLY COMPUTING COMPLEXES, 5.8077
Elapsed time: 6.2516
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.606327
TIME FOR NEWLY COMPUTING COMPLEXES, 5.02505
Elapsed time: 5.47464

TRIAL, 62
TIME FOR NEWLY COMPUTING COMPLEXES, 5.15158
Elapsed time: 5.40327
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.35319
TIME FOR NEWLY COMPUTING COMPLEXES, 4.9855
Elapsed time: 5.42029
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.10134
TIME FOR NEWLY COMPUTING COMPLEXES, 5.36371
Elapsed time: 5.81538

TRIAL, 63
TIME FOR NEWLY COMPUTING COMPLEXES, 5.27986
Elapsed time: 5.54177
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.69173
TIME FOR NEWLY COMPUTING COMPLEXES, 5.42431
Elapsed time: 5.74539
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.18857
TIME FOR NEWLY COMPUTING COMPLEXES, 5.77683
Elapsed time: 6.03424

TRIAL, 64
TIME FOR NEWLY COMPUTING COMPLEXES, 5.19585
Elapsed time: 5.47178
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.86129
TIME FOR NEWLY COMPUTING COMPLEXES, 6.22469
Elapsed time: 6.58009
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.49686
TIME FOR NEWLY COMPUTING COMPLEXES, 6.35935
Elapsed time: 6.90618

TRIAL, 65
TIME FOR NEWLY COMPUTING COMPLEXES, 6.45095
Elapsed time: 6.75913
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.53574
TIME FOR NEWLY COMPUTING COMPLEXES, 6.25947
Elapsed time: 6.60001
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.55467
TIME FOR NEWLY COMPUTING COMPLEXES, 5.18035
Elapsed time: 5.42973

TRIAL, 66
TIME FOR NEWLY COMPUTING COMPLEXES, 4.79177
Elapsed time: 5.03721
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.304492
TIME FOR NEWLY COMPUTING COMPLEXES, 4.88923
Elapsed time: 5.24447
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.16528
TIME FOR NEWLY COMPUTING COMPLEXES, 4.90514
Elapsed time: 5.3473

TRIAL, 67
TIME FOR NEWLY COMPUTING COMPLEXES, 5.4338
Elapsed time: 5.69647
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.12192
TIME FOR NEWLY COMPUTING COMPLEXES, 4.95167
Elapsed time: 5.21668
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.61179
TIME FOR NEWLY COMPUTING COMPLEXES, 4.97649
Elapsed time: 5.24099

TRIAL, 68
TIME FOR NEWLY COMPUTING COMPLEXES, 4.86996
Elapsed time: 5.13753
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.66928
TIME FOR NEWLY COMPUTING COMPLEXES, 5.30945
Elapsed time: 5.60286
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.11116
TIME FOR NEWLY COMPUTING COMPLEXES, 5.40023
Elapsed time: 5.69008

TRIAL, 69
TIME FOR NEWLY COMPUTING COMPLEXES, 5.47508
Elapsed time: 5.79554
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.74847
TIME FOR NEWLY COMPUTING COMPLEXES, 5.04238
Elapsed time: 5.30042
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.71982
TIME FOR NEWLY COMPUTING COMPLEXES, 4.86549
Elapsed time: 5.1302

TRIAL, 70
TIME FOR NEWLY COMPUTING COMPLEXES, 4.91923
Elapsed time: 5.18503
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.24568
TIME FOR NEWLY COMPUTING COMPLEXES, 5.04932
Elapsed time: 5.31652
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.46768
TIME FOR NEWLY COMPUTING COMPLEXES, 5.0196
Elapsed time: 5.27403

TRIAL, 71
TIME FOR NEWLY COMPUTING COMPLEXES, 5.10754
Elapsed time: 5.37994
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.24857
TIME FOR NEWLY COMPUTING COMPLEXES, 5.10657
Elapsed time: 5.37092
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.22543
TIME FOR NEWLY COMPUTING COMPLEXES, 4.93194
Elapsed time: 5.19213

TRIAL, 72
TIME FOR NEWLY COMPUTING COMPLEXES, 4.93624
Elapsed time: 5.1836
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.4618
TIME FOR NEWLY COMPUTING COMPLEXES, 4.97934
Elapsed time: 5.2463
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.04825
TIME FOR NEWLY COMPUTING COMPLEXES, 4.94309
Elapsed time: 5.19458

TRIAL, 73
TIME FOR NEWLY COMPUTING COMPLEXES, 4.89813
Elapsed time: 5.15658
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.20415
TIME FOR NEWLY COMPUTING COMPLEXES, 5.01792
Elapsed time: 5.27675
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.43042
TIME FOR NEWLY COMPUTING COMPLEXES, 5.31373
Elapsed time: 5.58139

TRIAL, 74
TIME FOR NEWLY COMPUTING COMPLEXES, 5.64203
Elapsed time: 5.92362
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.95709
TIME FOR NEWLY COMPUTING COMPLEXES, 6.04464
Elapsed time: 6.53072
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.02083
TIME FOR NEWLY COMPUTING COMPLEXES, 6.23293
Elapsed time: 6.70909

TRIAL, 75
TIME FOR NEWLY COMPUTING COMPLEXES, 6.45657
Elapsed time: 6.77306
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.81736
TIME FOR NEWLY COMPUTING COMPLEXES, 6.10584
Elapsed time: 6.56521
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.60796
TIME FOR NEWLY COMPUTING COMPLEXES, 5.76782
Elapsed time: 6.23704

TRIAL, 76
TIME FOR NEWLY COMPUTING COMPLEXES, 6.23578
Elapsed time: 6.55098
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.49228
TIME FOR NEWLY COMPUTING COMPLEXES, 6.19117
Elapsed time: 6.46512
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.951155
TIME FOR NEWLY COMPUTING COMPLEXES, 5.54235
Elapsed time: 6.04547

TRIAL, 77
TIME FOR NEWLY COMPUTING COMPLEXES, 5.39214
Elapsed time: 5.66283
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.497298
TIME FOR NEWLY COMPUTING COMPLEXES, 5.5361
Elapsed time: 5.80842
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.01461
TIME FOR NEWLY COMPUTING COMPLEXES, 5.54223
Elapsed time: 6.12471

TRIAL, 78
TIME FOR NEWLY COMPUTING COMPLEXES, 5.47214
Elapsed time: 5.74952
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.71162
TIME FOR NEWLY COMPUTING COMPLEXES, 5.64686
Elapsed time: 6.14129
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.49409
TIME FOR NEWLY COMPUTING COMPLEXES, 6.61592
Elapsed time: 6.96444

TRIAL, 79
TIME FOR NEWLY COMPUTING COMPLEXES, 6.36258
Elapsed time: 6.68541
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.13148
TIME FOR NEWLY COMPUTING COMPLEXES, 6.6776
Elapsed time: 6.98456
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.3303
TIME FOR NEWLY COMPUTING COMPLEXES, 5.21114
Elapsed time: 5.58099

TRIAL, 80
TIME FOR NEWLY COMPUTING COMPLEXES, 5.51232
Elapsed time: 5.78303
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.986224
TIME FOR NEWLY COMPUTING COMPLEXES, 5.48553
Elapsed time: 5.74838
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.53847
TIME FOR NEWLY COMPUTING COMPLEXES, 5.21293
Elapsed time: 5.68044

TRIAL, 81
TIME FOR NEWLY COMPUTING COMPLEXES, 5.13527
Elapsed time: 5.40783
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.908267
TIME FOR NEWLY COMPUTING COMPLEXES, 5.05871
Elapsed time: 5.49681
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.10395
TIME FOR NEWLY COMPUTING COMPLEXES, 5.07967
Elapsed time: 5.53008

TRIAL, 82
TIME FOR NEWLY COMPUTING COMPLEXES, 4.92166
Elapsed time: 5.19368
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.97083
TIME FOR NEWLY COMPUTING COMPLEXES, 5.12385
Elapsed time: 5.48109
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.809836
TIME FOR NEWLY COMPUTING COMPLEXES, 4.48447
Elapsed time: 4.90346

TRIAL, 83
TIME FOR NEWLY COMPUTING COMPLEXES, 4.61038
Elapsed time: 4.84308
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.19904
TIME FOR NEWLY COMPUTING COMPLEXES, 5.03334
Elapsed time: 5.28681
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.43107
TIME FOR NEWLY COMPUTING COMPLEXES, 4.73939
Elapsed time: 5.14861

TRIAL, 84
TIME FOR NEWLY COMPUTING COMPLEXES, 4.82007
Elapsed time: 5.07255
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.32463
TIME FOR NEWLY COMPUTING COMPLEXES, 4.91378
Elapsed time: 5.16075
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.21538
TIME FOR NEWLY COMPUTING COMPLEXES, 5.10797
Elapsed time: 5.5307

TRIAL, 85
TIME FOR NEWLY COMPUTING COMPLEXES, 4.95417
Elapsed time: 5.20451
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.35689
TIME FOR NEWLY COMPUTING COMPLEXES, 4.68054
Elapsed time: 4.93086
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.46193
TIME FOR NEWLY COMPUTING COMPLEXES, 4.57619
Elapsed time: 4.96285

TRIAL, 86
TIME FOR NEWLY COMPUTING COMPLEXES, 4.6436
Elapsed time: 4.89404
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.07967
TIME FOR NEWLY COMPUTING COMPLEXES, 4.63132
Elapsed time: 5.05083
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.65419
TIME FOR NEWLY COMPUTING COMPLEXES, 5.47681
Elapsed time: 5.83742

TRIAL, 87
TIME FOR NEWLY COMPUTING COMPLEXES, 5.48384
Elapsed time: 5.7555
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.609087
TIME FOR NEWLY COMPUTING COMPLEXES, 4.88323
Elapsed time: 5.3041
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.753266
TIME FOR NEWLY COMPUTING COMPLEXES, 4.88763
Elapsed time: 5.47295

TRIAL, 88
TIME FOR NEWLY COMPUTING COMPLEXES, 6.1407
Elapsed time: 6.41873
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.35725
TIME FOR NEWLY COMPUTING COMPLEXES, 5.99014
Elapsed time: 6.47519
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.41012
TIME FOR NEWLY COMPUTING COMPLEXES, 6.13557
Elapsed time: 6.62321

TRIAL, 89
TIME FOR NEWLY COMPUTING COMPLEXES, 6.1255
Elapsed time: 6.43803
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.14737
TIME FOR NEWLY COMPUTING COMPLEXES, 4.96851
Elapsed time: 5.37548
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.18946
TIME FOR NEWLY COMPUTING COMPLEXES, 4.62666
Elapsed time: 5.03703

TRIAL, 90
TIME FOR NEWLY COMPUTING COMPLEXES, 4.68217
Elapsed time: 4.94734
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.3316
TIME FOR NEWLY COMPUTING COMPLEXES, 4.98444
Elapsed time: 5.32309
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.04405
TIME FOR NEWLY COMPUTING COMPLEXES, 5.03041
Elapsed time: 5.512

TRIAL, 91
TIME FOR NEWLY COMPUTING COMPLEXES, 5.47873
Elapsed time: 5.79307
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.2987
TIME FOR NEWLY COMPUTING COMPLEXES, 5.45738
Elapsed time: 5.90788
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.27126
TIME FOR NEWLY COMPUTING COMPLEXES, 5.74605
Elapsed time: 6.15418

TRIAL, 92
TIME FOR NEWLY COMPUTING COMPLEXES, 5.20575
Elapsed time: 5.49121
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.11989
TIME FOR NEWLY COMPUTING COMPLEXES, 5.38149
Elapsed time: 5.68253
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.5176
TIME FOR NEWLY COMPUTING COMPLEXES, 5.40944
Elapsed time: 5.88407

TRIAL, 93
TIME FOR NEWLY COMPUTING COMPLEXES, 5.67293
Elapsed time: 5.99264
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.43337
TIME FOR NEWLY COMPUTING COMPLEXES, 6.56074
Elapsed time: 7.05245
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 2.03818
TIME FOR NEWLY COMPUTING COMPLEXES, 6.38438
Elapsed time: 6.91833

TRIAL, 94
TIME FOR NEWLY COMPUTING COMPLEXES, 6.77703
Elapsed time: 7.17317
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.35021
TIME FOR NEWLY COMPUTING COMPLEXES, 6.26802
Elapsed time: 6.54062
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.25751
TIME FOR NEWLY COMPUTING COMPLEXES, 5.50869
Elapsed time: 5.98946

TRIAL, 95
TIME FOR NEWLY COMPUTING COMPLEXES, 5.34398
Elapsed time: 5.61365
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.5377
TIME FOR NEWLY COMPUTING COMPLEXES, 5.5187
Elapsed time: 5.80305
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.60964
TIME FOR NEWLY COMPUTING COMPLEXES, 5.04493
Elapsed time: 5.49791

TRIAL, 96
TIME FOR NEWLY COMPUTING COMPLEXES, 5.15931
Elapsed time: 5.43939
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.27029
TIME FOR NEWLY COMPUTING COMPLEXES, 5.05273
Elapsed time: 5.49396
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.387418
TIME FOR NEWLY COMPUTING COMPLEXES, 4.97061
Elapsed time: 5.34901

TRIAL, 97
TIME FOR NEWLY COMPUTING COMPLEXES, 5.40584
Elapsed time: 5.69861
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 2.31657
TIME FOR NEWLY COMPUTING COMPLEXES, 5.25545
Elapsed time: 5.54937
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.6479
TIME FOR NEWLY COMPUTING COMPLEXES, 5.60843
Elapsed time: 5.98274

TRIAL, 98
TIME FOR NEWLY COMPUTING COMPLEXES, 5.24923
Elapsed time: 5.52055
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.66092
TIME FOR NEWLY COMPUTING COMPLEXES, 5.15981
Elapsed time: 5.67774
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.33259
TIME FOR NEWLY COMPUTING COMPLEXES, 5.42217
Elapsed time: 5.89646

TRIAL, 99
TIME FOR NEWLY COMPUTING COMPLEXES, 5.13514
Elapsed time: 5.40591
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.43513
TIME FOR NEWLY COMPUTING COMPLEXES, 5.49975
Elapsed time: 5.94536
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 1.72806
TIME FOR NEWLY COMPUTING COMPLEXES, 5.68379
Elapsed time: 6.13816
