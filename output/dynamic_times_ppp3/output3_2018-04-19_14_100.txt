POISSON POINT PROCESS
# trials, dim, window size,     lambda, periodic
     100,   3,          14,          1,        0

TRIAL, 0
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80492
Elapsed time: 1.89471
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.402002
TIME FOR NEWLY COMPUTING COMPLEXES, 1.91995
Elapsed time: 2.02837
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.635757
TIME FOR NEWLY COMPUTING COMPLEXES, 1.85297
Elapsed time: 1.93879

TRIAL, 1
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78724
Elapsed time: 1.87506
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.479492
TIME FOR NEWLY COMPUTING COMPLEXES, 1.74428
Elapsed time: 1.84513
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.565488
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78969
Elapsed time: 1.87761

TRIAL, 2
TIME FOR NEWLY COMPUTING COMPLEXES, 1.90715
Elapsed time: 1.9963
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.432794
TIME FOR NEWLY COMPUTING COMPLEXES, 1.96107
Elapsed time: 2.04769
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.440295
TIME FOR NEWLY COMPUTING COMPLEXES, 2.24205
Elapsed time: 2.36814

TRIAL, 3
TIME FOR NEWLY COMPUTING COMPLEXES, 1.88118
Elapsed time: 1.97333
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.809918
TIME FOR NEWLY COMPUTING COMPLEXES, 2.10467
Elapsed time: 2.19442
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.579127
TIME FOR NEWLY COMPUTING COMPLEXES, 2.4069
Elapsed time: 2.56016

TRIAL, 4
TIME FOR NEWLY COMPUTING COMPLEXES, 2.03932
Elapsed time: 2.13661
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.45217
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80277
Elapsed time: 1.94047
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.412872
TIME FOR NEWLY COMPUTING COMPLEXES, 1.70955
Elapsed time: 1.83047

TRIAL, 5
TIME FOR NEWLY COMPUTING COMPLEXES, 1.77355
Elapsed time: 1.85439
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.991658
TIME FOR NEWLY COMPUTING COMPLEXES, 1.7766
Elapsed time: 1.88246
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.871882
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80962
Elapsed time: 1.89595

TRIAL, 6
TIME FOR NEWLY COMPUTING COMPLEXES, 1.90658
Elapsed time: 1.9902
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.554374
TIME FOR NEWLY COMPUTING COMPLEXES, 1.86856
Elapsed time: 1.96235
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.437636
TIME FOR NEWLY COMPUTING COMPLEXES, 2.39048
Elapsed time: 2.47637

TRIAL, 7
TIME FOR NEWLY COMPUTING COMPLEXES, 1.75838
Elapsed time: 1.84828
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.698717
TIME FOR NEWLY COMPUTING COMPLEXES, 1.81555
Elapsed time: 1.89553
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.457792
TIME FOR NEWLY COMPUTING COMPLEXES, 1.76518
Elapsed time: 1.84516

TRIAL, 8
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80348
Elapsed time: 1.88909
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.425772
TIME FOR NEWLY COMPUTING COMPLEXES, 1.74606
Elapsed time: 1.83131
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.525761
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78648
Elapsed time: 1.86844

TRIAL, 9
TIME FOR NEWLY COMPUTING COMPLEXES, 1.79064
Elapsed time: 1.87231
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.551981
TIME FOR NEWLY COMPUTING COMPLEXES, 1.90531
Elapsed time: 2.00077
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.778672
TIME FOR NEWLY COMPUTING COMPLEXES, 1.988
Elapsed time: 2.09411

TRIAL, 10
TIME FOR NEWLY COMPUTING COMPLEXES, 2.00741
Elapsed time: 2.1018
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.493087
TIME FOR NEWLY COMPUTING COMPLEXES, 1.86047
Elapsed time: 1.95236
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.745194
TIME FOR NEWLY COMPUTING COMPLEXES, 1.81412
Elapsed time: 1.93957

TRIAL, 11
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80988
Elapsed time: 1.89412
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.50083
TIME FOR NEWLY COMPUTING COMPLEXES, 1.85761
Elapsed time: 1.95261
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.328663
TIME FOR NEWLY COMPUTING COMPLEXES, 1.79727
Elapsed time: 1.88953

TRIAL, 12
TIME FOR NEWLY COMPUTING COMPLEXES, 1.88868
Elapsed time: 1.98632
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.623431
TIME FOR NEWLY COMPUTING COMPLEXES, 1.89613
Elapsed time: 1.98852
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.482798
TIME FOR NEWLY COMPUTING COMPLEXES, 1.87911
Elapsed time: 1.9621

TRIAL, 13
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83947
Elapsed time: 1.92565
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.514724
TIME FOR NEWLY COMPUTING COMPLEXES, 1.81092
Elapsed time: 1.94032
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.519948
TIME FOR NEWLY COMPUTING COMPLEXES, 1.81829
Elapsed time: 1.91039

TRIAL, 14
TIME FOR NEWLY COMPUTING COMPLEXES, 1.77823
Elapsed time: 1.8653
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.672183
TIME FOR NEWLY COMPUTING COMPLEXES, 1.86716
Elapsed time: 1.95232
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.282237
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83355
Elapsed time: 1.91994

TRIAL, 15
TIME FOR NEWLY COMPUTING COMPLEXES, 1.8071
Elapsed time: 1.90333
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.396225
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80847
Elapsed time: 1.89975
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.397235
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83296
Elapsed time: 1.92211

TRIAL, 16
TIME FOR NEWLY COMPUTING COMPLEXES, 1.85635
Elapsed time: 1.94525
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.431076
TIME FOR NEWLY COMPUTING COMPLEXES, 1.8551
Elapsed time: 1.94793
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.416162
TIME FOR NEWLY COMPUTING COMPLEXES, 1.84465
Elapsed time: 1.93967

TRIAL, 17
TIME FOR NEWLY COMPUTING COMPLEXES, 1.8248
Elapsed time: 1.91182
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.331148
TIME FOR NEWLY COMPUTING COMPLEXES, 1.87084
Elapsed time: 2.02925
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.483453
TIME FOR NEWLY COMPUTING COMPLEXES, 1.82991
Elapsed time: 1.918

TRIAL, 18
TIME FOR NEWLY COMPUTING COMPLEXES, 1.87194
Elapsed time: 1.95613
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.615683
TIME FOR NEWLY COMPUTING COMPLEXES, 1.82715
Elapsed time: 1.94636
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.448771
TIME FOR NEWLY COMPUTING COMPLEXES, 1.79347
Elapsed time: 1.88396

TRIAL, 19
TIME FOR NEWLY COMPUTING COMPLEXES, 1.84685
Elapsed time: 1.93984
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.68625
TIME FOR NEWLY COMPUTING COMPLEXES, 1.82118
Elapsed time: 1.90648
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.68878
TIME FOR NEWLY COMPUTING COMPLEXES, 1.81208
Elapsed time: 1.94349

TRIAL, 20
TIME FOR NEWLY COMPUTING COMPLEXES, 1.85206
Elapsed time: 1.94074
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.445711
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83673
Elapsed time: 1.9221
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.45268
TIME FOR NEWLY COMPUTING COMPLEXES, 1.97462
Elapsed time: 2.09892

TRIAL, 21
TIME FOR NEWLY COMPUTING COMPLEXES, 1.89658
Elapsed time: 2.00691
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.752614
TIME FOR NEWLY COMPUTING COMPLEXES, 1.95735
Elapsed time: 2.07284
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.525744
TIME FOR NEWLY COMPUTING COMPLEXES, 2.25443
Elapsed time: 2.415

TRIAL, 22
TIME FOR NEWLY COMPUTING COMPLEXES, 1.92517
Elapsed time: 2.01053
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.269771
TIME FOR NEWLY COMPUTING COMPLEXES, 1.91131
Elapsed time: 1.99658
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.251905
TIME FOR NEWLY COMPUTING COMPLEXES, 1.97726
Elapsed time: 2.11198

TRIAL, 23
TIME FOR NEWLY COMPUTING COMPLEXES, 1.90207
Elapsed time: 1.98831
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.659181
TIME FOR NEWLY COMPUTING COMPLEXES, 1.86679
Elapsed time: 2.0301
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.768739
TIME FOR NEWLY COMPUTING COMPLEXES, 1.96979
Elapsed time: 2.0486

TRIAL, 24
TIME FOR NEWLY COMPUTING COMPLEXES, 1.77339
Elapsed time: 1.85137
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.407834
TIME FOR NEWLY COMPUTING COMPLEXES, 1.82216
Elapsed time: 1.9045
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.473468
TIME FOR NEWLY COMPUTING COMPLEXES, 1.89424
Elapsed time: 1.98808

TRIAL, 25
TIME FOR NEWLY COMPUTING COMPLEXES, 1.85262
Elapsed time: 1.93395
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.368868
TIME FOR NEWLY COMPUTING COMPLEXES, 1.87642
Elapsed time: 1.96749
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.508811
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83763
Elapsed time: 1.92001

TRIAL, 26
TIME FOR NEWLY COMPUTING COMPLEXES, 1.79542
Elapsed time: 1.87663
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.718878
TIME FOR NEWLY COMPUTING COMPLEXES, 1.84761
Elapsed time: 1.93077
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.161569
TIME FOR NEWLY COMPUTING COMPLEXES, 1.9596
Elapsed time: 2.11242

TRIAL, 27
TIME FOR NEWLY COMPUTING COMPLEXES, 1.85527
Elapsed time: 1.9404
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.302259
TIME FOR NEWLY COMPUTING COMPLEXES, 2.46796
Elapsed time: 2.6266
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.632153
TIME FOR NEWLY COMPUTING COMPLEXES, 2.57165
Elapsed time: 2.6575

TRIAL, 28
TIME FOR NEWLY COMPUTING COMPLEXES, 1.90263
Elapsed time: 1.9894
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.672056
TIME FOR NEWLY COMPUTING COMPLEXES, 1.74497
Elapsed time: 1.85676
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.443686
TIME FOR NEWLY COMPUTING COMPLEXES, 1.79149
Elapsed time: 1.91208

TRIAL, 29
TIME FOR NEWLY COMPUTING COMPLEXES, 1.68923
Elapsed time: 1.77026
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.362696
TIME FOR NEWLY COMPUTING COMPLEXES, 1.67127
Elapsed time: 1.81526
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.107208
TIME FOR NEWLY COMPUTING COMPLEXES, 1.69797
Elapsed time: 1.78485

TRIAL, 30
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78395
Elapsed time: 1.88173
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.452298
TIME FOR NEWLY COMPUTING COMPLEXES, 1.73696
Elapsed time: 1.82181
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.354678
TIME FOR NEWLY COMPUTING COMPLEXES, 1.7065
Elapsed time: 1.82795

TRIAL, 31
TIME FOR NEWLY COMPUTING COMPLEXES, 1.76321
Elapsed time: 1.85083
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.494351
TIME FOR NEWLY COMPUTING COMPLEXES, 1.75844
Elapsed time: 1.84169
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.225072
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80241
Elapsed time: 1.89078

TRIAL, 32
TIME FOR NEWLY COMPUTING COMPLEXES, 1.76599
Elapsed time: 1.84758
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.489213
TIME FOR NEWLY COMPUTING COMPLEXES, 1.77538
Elapsed time: 1.85805
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.423852
TIME FOR NEWLY COMPUTING COMPLEXES, 1.77352
Elapsed time: 1.89617

TRIAL, 33
TIME FOR NEWLY COMPUTING COMPLEXES, 1.71649
Elapsed time: 1.812
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.194579
TIME FOR NEWLY COMPUTING COMPLEXES, 1.75097
Elapsed time: 1.89613
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.443463
TIME FOR NEWLY COMPUTING COMPLEXES, 1.75429
Elapsed time: 1.83836

TRIAL, 34
TIME FOR NEWLY COMPUTING COMPLEXES, 1.91952
Elapsed time: 2.01671
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.676961
TIME FOR NEWLY COMPUTING COMPLEXES, 1.89206
Elapsed time: 2.02258
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.421692
TIME FOR NEWLY COMPUTING COMPLEXES, 1.95687
Elapsed time: 2.06246

TRIAL, 35
TIME FOR NEWLY COMPUTING COMPLEXES, 1.90576
Elapsed time: 1.99074
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.377602
TIME FOR NEWLY COMPUTING COMPLEXES, 1.74052
Elapsed time: 1.8285
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.498459
TIME FOR NEWLY COMPUTING COMPLEXES, 2.06648
Elapsed time: 2.1493

TRIAL, 36
TIME FOR NEWLY COMPUTING COMPLEXES, 1.89246
Elapsed time: 1.98471
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.478699
TIME FOR NEWLY COMPUTING COMPLEXES, 1.84442
Elapsed time: 1.93176
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.360682
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83088
Elapsed time: 1.9174

TRIAL, 37
TIME FOR NEWLY COMPUTING COMPLEXES, 1.8672
Elapsed time: 1.95299
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.729055
TIME FOR NEWLY COMPUTING COMPLEXES, 1.93348
Elapsed time: 2.01408
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.700776
TIME FOR NEWLY COMPUTING COMPLEXES, 1.88149
Elapsed time: 2.00577

TRIAL, 38
TIME FOR NEWLY COMPUTING COMPLEXES, 1.86579
Elapsed time: 1.94974
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.543965
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78679
Elapsed time: 1.87543
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.411355
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80907
Elapsed time: 1.93808

TRIAL, 39
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78975
Elapsed time: 1.87158
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.548446
TIME FOR NEWLY COMPUTING COMPLEXES, 1.77866
Elapsed time: 1.85688
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.189792
TIME FOR NEWLY COMPUTING COMPLEXES, 2.47816
Elapsed time: 2.56797

TRIAL, 40
TIME FOR NEWLY COMPUTING COMPLEXES, 2.60468
Elapsed time: 2.70892
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.421905
TIME FOR NEWLY COMPUTING COMPLEXES, 2.75569
Elapsed time: 2.84618
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.476176
TIME FOR NEWLY COMPUTING COMPLEXES, 1.87082
Elapsed time: 2.0094

TRIAL, 41
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83251
Elapsed time: 1.9138
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.405935
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78915
Elapsed time: 1.88136
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.457516
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80909
Elapsed time: 1.89644

TRIAL, 42
TIME FOR NEWLY COMPUTING COMPLEXES, 2.1903
Elapsed time: 2.28872
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.483732
TIME FOR NEWLY COMPUTING COMPLEXES, 1.85706
Elapsed time: 1.93682
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.53728
TIME FOR NEWLY COMPUTING COMPLEXES, 1.86525
Elapsed time: 1.94654

TRIAL, 43
TIME FOR NEWLY COMPUTING COMPLEXES, 1.89427
Elapsed time: 1.97744
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.542855
TIME FOR NEWLY COMPUTING COMPLEXES, 1.7756
Elapsed time: 1.88755
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.296463
TIME FOR NEWLY COMPUTING COMPLEXES, 1.96297
Elapsed time: 2.11509

TRIAL, 44
TIME FOR NEWLY COMPUTING COMPLEXES, 1.81747
Elapsed time: 1.90414
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.367841
TIME FOR NEWLY COMPUTING COMPLEXES, 1.71369
Elapsed time: 1.80511
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.479945
TIME FOR NEWLY COMPUTING COMPLEXES, 1.82664
Elapsed time: 1.91051

TRIAL, 45
TIME FOR NEWLY COMPUTING COMPLEXES, 2.17618
Elapsed time: 2.2616
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.198885
TIME FOR NEWLY COMPUTING COMPLEXES, 1.95399
Elapsed time: 2.03563
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.243474
TIME FOR NEWLY COMPUTING COMPLEXES, 2.38155
Elapsed time: 2.48911

TRIAL, 46
TIME FOR NEWLY COMPUTING COMPLEXES, 2.4508
Elapsed time: 2.65743
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.61907
TIME FOR NEWLY COMPUTING COMPLEXES, 1.79162
Elapsed time: 1.87135
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.155629
TIME FOR NEWLY COMPUTING COMPLEXES, 1.70329
Elapsed time: 1.82273

TRIAL, 47
TIME FOR NEWLY COMPUTING COMPLEXES, 1.75781
Elapsed time: 1.83876
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.462366
TIME FOR NEWLY COMPUTING COMPLEXES, 1.75199
Elapsed time: 1.83233
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.409824
TIME FOR NEWLY COMPUTING COMPLEXES, 1.754
Elapsed time: 1.87767

TRIAL, 48
TIME FOR NEWLY COMPUTING COMPLEXES, 1.82649
Elapsed time: 1.9166
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.439039
TIME FOR NEWLY COMPUTING COMPLEXES, 2.08866
Elapsed time: 2.17991
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.51348
TIME FOR NEWLY COMPUTING COMPLEXES, 1.87058
Elapsed time: 1.97522

TRIAL, 49
TIME FOR NEWLY COMPUTING COMPLEXES, 1.76228
Elapsed time: 1.84385
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.459455
TIME FOR NEWLY COMPUTING COMPLEXES, 1.84082
Elapsed time: 1.96772
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.520355
TIME FOR NEWLY COMPUTING COMPLEXES, 2.2207
Elapsed time: 2.35905

TRIAL, 50
TIME FOR NEWLY COMPUTING COMPLEXES, 2.7885
Elapsed time: 2.89026
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.711087
TIME FOR NEWLY COMPUTING COMPLEXES, 2.02017
Elapsed time: 2.15533
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.435932
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80676
Elapsed time: 1.88748

TRIAL, 51
TIME FOR NEWLY COMPUTING COMPLEXES, 1.74119
Elapsed time: 1.83548
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.375584
TIME FOR NEWLY COMPUTING COMPLEXES, 1.75404
Elapsed time: 1.83923
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.503199
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78721
Elapsed time: 1.87661

TRIAL, 52
TIME FOR NEWLY COMPUTING COMPLEXES, 2.05562
Elapsed time: 2.13566
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.767587
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83809
Elapsed time: 1.92978
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.155446
TIME FOR NEWLY COMPUTING COMPLEXES, 1.82038
Elapsed time: 1.90454

TRIAL, 53
TIME FOR NEWLY COMPUTING COMPLEXES, 1.70795
Elapsed time: 1.79222
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.466766
TIME FOR NEWLY COMPUTING COMPLEXES, 1.90136
Elapsed time: 1.98435
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.315174
TIME FOR NEWLY COMPUTING COMPLEXES, 1.90058
Elapsed time: 1.99095

TRIAL, 54
TIME FOR NEWLY COMPUTING COMPLEXES, 2.19493
Elapsed time: 2.29387
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.987084
TIME FOR NEWLY COMPUTING COMPLEXES, 2.64486
Elapsed time: 2.73851
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.51968
TIME FOR NEWLY COMPUTING COMPLEXES, 2.00666
Elapsed time: 2.09899

TRIAL, 55
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80109
Elapsed time: 1.90782
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.594276
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78785
Elapsed time: 1.87988
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.612128
TIME FOR NEWLY COMPUTING COMPLEXES, 1.81619
Elapsed time: 1.94044

TRIAL, 56
TIME FOR NEWLY COMPUTING COMPLEXES, 1.77021
Elapsed time: 1.86417
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.560282
TIME FOR NEWLY COMPUTING COMPLEXES, 1.82399
Elapsed time: 1.9415
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.187137
TIME FOR NEWLY COMPUTING COMPLEXES, 2.20219
Elapsed time: 2.30426

TRIAL, 57
TIME FOR NEWLY COMPUTING COMPLEXES, 2.12723
Elapsed time: 2.22354
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.423092
TIME FOR NEWLY COMPUTING COMPLEXES, 2.00315
Elapsed time: 2.09447
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.473015
TIME FOR NEWLY COMPUTING COMPLEXES, 1.99173
Elapsed time: 2.11011

TRIAL, 58
TIME FOR NEWLY COMPUTING COMPLEXES, 2.01962
Elapsed time: 2.11702
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.495253
TIME FOR NEWLY COMPUTING COMPLEXES, 1.88896
Elapsed time: 2.02388
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.304525
TIME FOR NEWLY COMPUTING COMPLEXES, 1.8991
Elapsed time: 1.99447

TRIAL, 59
TIME FOR NEWLY COMPUTING COMPLEXES, 2.02599
Elapsed time: 2.11748
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.862333
TIME FOR NEWLY COMPUTING COMPLEXES, 1.98197
Elapsed time: 2.06949
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.527064
TIME FOR NEWLY COMPUTING COMPLEXES, 1.95069
Elapsed time: 2.07939

TRIAL, 60
TIME FOR NEWLY COMPUTING COMPLEXES, 1.86147
Elapsed time: 1.96491
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.775466
TIME FOR NEWLY COMPUTING COMPLEXES, 1.85035
Elapsed time: 1.94022
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.559131
TIME FOR NEWLY COMPUTING COMPLEXES, 1.93031
Elapsed time: 2.02286

TRIAL, 61
TIME FOR NEWLY COMPUTING COMPLEXES, 2.02391
Elapsed time: 2.11662
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.371403
TIME FOR NEWLY COMPUTING COMPLEXES, 1.92198
Elapsed time: 2.00788
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.514483
TIME FOR NEWLY COMPUTING COMPLEXES, 1.73564
Elapsed time: 1.8214

TRIAL, 62
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78004
Elapsed time: 1.86533
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.561991
TIME FOR NEWLY COMPUTING COMPLEXES, 1.8266
Elapsed time: 1.91219
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.498368
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78808
Elapsed time: 1.87856

TRIAL, 63
TIME FOR NEWLY COMPUTING COMPLEXES, 1.79781
Elapsed time: 1.88159
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.441157
TIME FOR NEWLY COMPUTING COMPLEXES, 1.75358
Elapsed time: 1.83504
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.430902
TIME FOR NEWLY COMPUTING COMPLEXES, 1.86858
Elapsed time: 1.96504

TRIAL, 64
TIME FOR NEWLY COMPUTING COMPLEXES, 1.8576
Elapsed time: 1.94783
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.650207
TIME FOR NEWLY COMPUTING COMPLEXES, 1.98891
Elapsed time: 2.07814
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.518115
TIME FOR NEWLY COMPUTING COMPLEXES, 1.89535
Elapsed time: 1.9929

TRIAL, 65
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78942
Elapsed time: 1.88149
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.673984
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80124
Elapsed time: 1.88196
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.428177
TIME FOR NEWLY COMPUTING COMPLEXES, 1.75722
Elapsed time: 1.89577

TRIAL, 66
TIME FOR NEWLY COMPUTING COMPLEXES, 1.8369
Elapsed time: 1.93918
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.464173
TIME FOR NEWLY COMPUTING COMPLEXES, 1.91202
Elapsed time: 2.04657
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.510448
TIME FOR NEWLY COMPUTING COMPLEXES, 1.81133
Elapsed time: 1.90064

TRIAL, 67
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78054
Elapsed time: 1.87113
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.192304
TIME FOR NEWLY COMPUTING COMPLEXES, 1.94222
Elapsed time: 2.06888
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.539136
TIME FOR NEWLY COMPUTING COMPLEXES, 1.91351
Elapsed time: 2.00881

TRIAL, 68
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78565
Elapsed time: 1.88405
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.51482
TIME FOR NEWLY COMPUTING COMPLEXES, 1.87005
Elapsed time: 1.96315
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.340661
TIME FOR NEWLY COMPUTING COMPLEXES, 1.79485
Elapsed time: 1.87562

TRIAL, 69
TIME FOR NEWLY COMPUTING COMPLEXES, 1.9316
Elapsed time: 2.02053
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.787524
TIME FOR NEWLY COMPUTING COMPLEXES, 1.98161
Elapsed time: 2.10368
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.427439
TIME FOR NEWLY COMPUTING COMPLEXES, 1.89907
Elapsed time: 2.0197

TRIAL, 70
TIME FOR NEWLY COMPUTING COMPLEXES, 1.84731
Elapsed time: 1.93429
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.540065
TIME FOR NEWLY COMPUTING COMPLEXES, 2.03408
Elapsed time: 2.12749
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.359752
TIME FOR NEWLY COMPUTING COMPLEXES, 1.951
Elapsed time: 2.04238

TRIAL, 71
TIME FOR NEWLY COMPUTING COMPLEXES, 1.94202
Elapsed time: 2.02792
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.821281
TIME FOR NEWLY COMPUTING COMPLEXES, 1.86682
Elapsed time: 1.94975
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.323938
TIME FOR NEWLY COMPUTING COMPLEXES, 1.79284
Elapsed time: 1.95271

TRIAL, 72
TIME FOR NEWLY COMPUTING COMPLEXES, 1.79228
Elapsed time: 1.87985
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.325809
TIME FOR NEWLY COMPUTING COMPLEXES, 2.00316
Elapsed time: 2.13779
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.531489
TIME FOR NEWLY COMPUTING COMPLEXES, 2.08118
Elapsed time: 2.20383

TRIAL, 73
TIME FOR NEWLY COMPUTING COMPLEXES, 2.16801
Elapsed time: 2.2887
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 1.14643
TIME FOR NEWLY COMPUTING COMPLEXES, 2.85957
Elapsed time: 2.95055
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.453941
TIME FOR NEWLY COMPUTING COMPLEXES, 2.13734
Elapsed time: 2.2303

TRIAL, 74
TIME FOR NEWLY COMPUTING COMPLEXES, 1.99992
Elapsed time: 2.10426
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.709202
TIME FOR NEWLY COMPUTING COMPLEXES, 2.16817
Elapsed time: 2.27803
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.70488
TIME FOR NEWLY COMPUTING COMPLEXES, 2.44172
Elapsed time: 2.54101

TRIAL, 75
TIME FOR NEWLY COMPUTING COMPLEXES, 2.00427
Elapsed time: 2.09569
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.295849
TIME FOR NEWLY COMPUTING COMPLEXES, 1.90717
Elapsed time: 2.05773
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.233719
TIME FOR NEWLY COMPUTING COMPLEXES, 1.99401
Elapsed time: 2.1651

TRIAL, 76
TIME FOR NEWLY COMPUTING COMPLEXES, 1.94834
Elapsed time: 2.05291
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.588044
TIME FOR NEWLY COMPUTING COMPLEXES, 1.97252
Elapsed time: 2.06074
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.438242
TIME FOR NEWLY COMPUTING COMPLEXES, 1.79959
Elapsed time: 1.92874

TRIAL, 77
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80985
Elapsed time: 1.90136
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.479091
TIME FOR NEWLY COMPUTING COMPLEXES, 1.78503
Elapsed time: 1.87483
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.389828
TIME FOR NEWLY COMPUTING COMPLEXES, 1.93797
Elapsed time: 2.06572

TRIAL, 78
TIME FOR NEWLY COMPUTING COMPLEXES, 1.85633
Elapsed time: 2.01231
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.625188
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83881
Elapsed time: 1.93269
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.28742
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80935
Elapsed time: 1.9012

TRIAL, 79
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83456
Elapsed time: 1.93055
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.442861
TIME FOR NEWLY COMPUTING COMPLEXES, 1.88545
Elapsed time: 1.98119
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.530594
TIME FOR NEWLY COMPUTING COMPLEXES, 1.82795
Elapsed time: 1.96289

TRIAL, 80
TIME FOR NEWLY COMPUTING COMPLEXES, 1.77632
Elapsed time: 1.86735
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.354234
TIME FOR NEWLY COMPUTING COMPLEXES, 1.8177
Elapsed time: 1.89979
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.465602
TIME FOR NEWLY COMPUTING COMPLEXES, 2.16901
Elapsed time: 2.30266

TRIAL, 81
TIME FOR NEWLY COMPUTING COMPLEXES, 2.20056
Elapsed time: 2.30918
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.462022
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83908
Elapsed time: 1.96974
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.17261
TIME FOR NEWLY COMPUTING COMPLEXES, 1.80741
Elapsed time: 1.89959

TRIAL, 82
TIME FOR NEWLY COMPUTING COMPLEXES, 1.81108
Elapsed time: 1.90253
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.545428
TIME FOR NEWLY COMPUTING COMPLEXES, 2.41066
Elapsed time: 2.53053
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.306663
TIME FOR NEWLY COMPUTING COMPLEXES, 2.82419
Elapsed time: 2.92917

TRIAL, 83
TIME FOR NEWLY COMPUTING COMPLEXES, 1.88799
Elapsed time: 1.97786
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.664403
TIME FOR NEWLY COMPUTING COMPLEXES, 1.85621
Elapsed time: 1.94279
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.431533
TIME FOR NEWLY COMPUTING COMPLEXES, 2.28472
Elapsed time: 2.43714

TRIAL, 84
TIME FOR NEWLY COMPUTING COMPLEXES, 2.55658
Elapsed time: 2.71446
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.994991
TIME FOR NEWLY COMPUTING COMPLEXES, 1.89428
Elapsed time: 2.03502
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.985073
TIME FOR NEWLY COMPUTING COMPLEXES, 2.139
Elapsed time: 2.26522

TRIAL, 85
TIME FOR NEWLY COMPUTING COMPLEXES, 2.06448
Elapsed time: 2.15932
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.93922
TIME FOR NEWLY COMPUTING COMPLEXES, 2.13185
Elapsed time: 2.27956
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.828779
TIME FOR NEWLY COMPUTING COMPLEXES, 2.03727
Elapsed time: 2.17365

TRIAL, 86
TIME FOR NEWLY COMPUTING COMPLEXES, 2.29407
Elapsed time: 2.41885
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.662845
TIME FOR NEWLY COMPUTING COMPLEXES, 2.17197
Elapsed time: 2.27309
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.690759
TIME FOR NEWLY COMPUTING COMPLEXES, 2.75974
Elapsed time: 2.85847

TRIAL, 87
TIME FOR NEWLY COMPUTING COMPLEXES, 1.9507
Elapsed time: 2.06651
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.68093
TIME FOR NEWLY COMPUTING COMPLEXES, 1.96704
Elapsed time: 2.10197
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.573984
TIME FOR NEWLY COMPUTING COMPLEXES, 2.0341
Elapsed time: 2.13039

TRIAL, 88
TIME FOR NEWLY COMPUTING COMPLEXES, 2.14266
Elapsed time: 2.24347
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.793001
TIME FOR NEWLY COMPUTING COMPLEXES, 1.9644
Elapsed time: 2.09935
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.415339
TIME FOR NEWLY COMPUTING COMPLEXES, 1.98339
Elapsed time: 2.07178

TRIAL, 89
TIME FOR NEWLY COMPUTING COMPLEXES, 2.35941
Elapsed time: 2.5093
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.491553
TIME FOR NEWLY COMPUTING COMPLEXES, 2.02482
Elapsed time: 2.1171
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.775482
TIME FOR NEWLY COMPUTING COMPLEXES, 2.19423
Elapsed time: 2.32847

TRIAL, 90
TIME FOR NEWLY COMPUTING COMPLEXES, 2.08521
Elapsed time: 2.20359
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.577828
TIME FOR NEWLY COMPUTING COMPLEXES, 2.05806
Elapsed time: 2.19557
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.660157
TIME FOR NEWLY COMPUTING COMPLEXES, 2.01849
Elapsed time: 2.11348

TRIAL, 91
TIME FOR NEWLY COMPUTING COMPLEXES, 1.95609
Elapsed time: 2.05814
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.520166
TIME FOR NEWLY COMPUTING COMPLEXES, 1.9479
Elapsed time: 2.0512
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.631882
TIME FOR NEWLY COMPUTING COMPLEXES, 1.8452
Elapsed time: 1.94184

TRIAL, 92
TIME FOR NEWLY COMPUTING COMPLEXES, 1.882
Elapsed time: 1.97696
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.497465
TIME FOR NEWLY COMPUTING COMPLEXES, 1.90187
Elapsed time: 1.99289
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.525378
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83388
Elapsed time: 1.922

TRIAL, 93
TIME FOR NEWLY COMPUTING COMPLEXES, 1.8498
Elapsed time: 1.94168
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.313447
TIME FOR NEWLY COMPUTING COMPLEXES, 1.96494
Elapsed time: 2.06645
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.555311
TIME FOR NEWLY COMPUTING COMPLEXES, 1.89037
Elapsed time: 1.98279

TRIAL, 94
TIME FOR NEWLY COMPUTING COMPLEXES, 1.98615
Elapsed time: 2.08773
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.490733
TIME FOR NEWLY COMPUTING COMPLEXES, 1.99105
Elapsed time: 2.09515
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.181796
TIME FOR NEWLY COMPUTING COMPLEXES, 2.22611
Elapsed time: 2.31908

TRIAL, 95
TIME FOR NEWLY COMPUTING COMPLEXES, 1.89898
Elapsed time: 2.00058
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.410467
TIME FOR NEWLY COMPUTING COMPLEXES, 2.61075
Elapsed time: 2.73317
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.337171
TIME FOR NEWLY COMPUTING COMPLEXES, 2.60857
Elapsed time: 2.68814

TRIAL, 96
TIME FOR NEWLY COMPUTING COMPLEXES, 1.98456
Elapsed time: 2.08983
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.27156
TIME FOR NEWLY COMPUTING COMPLEXES, 1.89665
Elapsed time: 1.98221
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.590598
TIME FOR NEWLY COMPUTING COMPLEXES, 1.88817
Elapsed time: 1.99024

TRIAL, 97
TIME FOR NEWLY COMPUTING COMPLEXES, 1.85315
Elapsed time: 1.95255
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.337466
TIME FOR NEWLY COMPUTING COMPLEXES, 2.22221
Elapsed time: 2.31351
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.604764
TIME FOR NEWLY COMPUTING COMPLEXES, 2.14039
Elapsed time: 2.23208

TRIAL, 98
TIME FOR NEWLY COMPUTING COMPLEXES, 1.99325
Elapsed time: 2.08349
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.628934
TIME FOR NEWLY COMPUTING COMPLEXES, 2.4447
Elapsed time: 2.56304
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.775007
TIME FOR NEWLY COMPUTING COMPLEXES, 2.44727
Elapsed time: 2.54085

TRIAL, 99
TIME FOR NEWLY COMPUTING COMPLEXES, 1.90344
Elapsed time: 1.9937
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.353483
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83404
Elapsed time: 1.92971
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.353959
TIME FOR NEWLY COMPUTING COMPLEXES, 1.90004
Elapsed time: 2.01079
