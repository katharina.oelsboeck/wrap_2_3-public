//persistence.h
//author: koelsboe

#ifndef PERSISTENCE_H
#define PERSISTENCE_H

#include "basics.h"

class PersistencePair{
    private:
        int birth_index_filtration; //filtration index of birth simplex
        int death_index_filtration; //filtration index of death simplex (-1 if infinite persistence)
        int dim;
        
    public:
        //constructors
        PersistencePair(){} //empty
        PersistencePair(int dim_, int birth_index_, int death_index_):dim(dim_),birth_index_filtration(birth_index_),death_index_filtration(death_index_){} //finite pair
        PersistencePair(int dim_, int birth_index_):dim(dim_),birth_index_filtration(birth_index_){death_index_filtration=-1;} //infinite pair
        
        int getDim(){return dim;}
        bool isInfinite(){return death_index_filtration<0;}
        bool hasZeroPersistence(Filtration* filtration){return (getBirth2(filtration)==getDeath2(filtration));}
        //get indices in filtration (same index as in boundary matrix)
        int getBirthIndexFiltration() const {return birth_index_filtration;}
        int getDeathIndexFiltration() const {return death_index_filtration;}
        exact getBirth2(Filtration* filtration){
            return (filtration->getElement(birth_index_filtration)).first;
        }
        exact getDeath2(Filtration* filtration){
            if(isInfinite()){
                return -1;
            }else{
                return (filtration->getElement(death_index_filtration)).first;
            }
        }
        //get sqrt of corresponding filtration values (for unweighted points)
        double getBirth_double(Filtration* filtration) {if(getBirth2(filtration)>=0){return std::sqrt(CGAL::to_double(getBirth2(filtration)));}else{return -1;}}
        double getDeath_double(Filtration* filtration) {if(isInfinite()||getDeath2(filtration)<0){return -1;}else{return std::sqrt(CGAL::to_double(getDeath2(filtration)));}}
        //get corresponding filtration values (for weighted points)
        double getBirth2_double(Filtration* filtration){return CGAL::to_double(getBirth2(filtration));}
        double getDeath2_double(Filtration* filtration){return CGAL::to_double(getDeath2(filtration));}
        double getPersistence_double(Filtration* filtration){if(isInfinite()||getDeath2(filtration)<0||getBirth2(filtration)<0){return -1;}else{return (getDeath_double(filtration))-(getBirth_double(filtration));}}
        double getPersistence2_double(Filtration* filtration){if(isInfinite()){return -1;}else{return getDeath2_double(filtration)-getBirth2_double(filtration);}}
        
        bool operator < (const PersistencePair& other) const {
            //order first by birth, then death
            if(getBirthIndexFiltration()==other.getBirthIndexFiltration()){
                return getDeathIndexFiltration()<other.getDeathIndexFiltration();
            }else{
                return getBirthIndexFiltration()<other.getBirthIndexFiltration();
            }
        }
        
        bool operator==(const PersistencePair& other) const {
           //same if birth and death index are same
           return getBirthIndexFiltration()==other.getBirthIndexFiltration() &&  getDeathIndexFiltration()==other.getDeathIndexFiltration();
        }
        
        void print(){
            std::cout << "dim " << dim << "(" << birth_index_filtration << ", " << death_index_filtration << ")" << std::endl;
        }
};

//class for sparse 0/1-matrices used for persistence computation (R,U)
class Sparse_Matrix
{
    private:
        std::vector<std::vector<int> > elements; //for each column (row): indices of 1s are stored in increasing (decreasing) order
        bool column_matrix; //whether matrix is stored as list of columns (or rows)

    public:
        Sparse_Matrix(){column_matrix=true;}; //empty constructor
        
        void setRowMatrix(bool row_matrix){column_matrix=!row_matrix;} //define as row matrix, no conversion
        int getSize(){return elements.size();} //get number of columns/rows
        bool getElement(int i, int j){ //get matrix(i,j)
            if(column_matrix){
                //need to check whether index i is contained in list for column j
                for(std::vector<int>::iterator it=elements[j].begin(); it < elements[j].end(); ++it){
                    int index = *it;
                    if(index==i){
                        return true;
                    }
                    if(index>i){
                        return false;
                    }
                }
            }else{
                //need to check whether index j is contained in list for row i
                for(std::vector<int>::iterator it=elements[i].begin(); it < elements[i].end(); ++it){
                    int index = *it;
                    if(index==j){
                        return true;
                    }
                    if(index<j){
                        return false;
                    }
                }
            }
            return false;
        }
        std::vector<int> getColumn(int j){ //get column j
            if(column_matrix){
                return elements[j];
            }else{
                std::vector<int> column;
                for(int i=0; i<elements.size(); i++){
                    if(getElement(i,j)){
                        column.push_back(i);
                    }
                }
                return column;
            }
        } 
        std::vector<int> getRow(int i){ //get row i
            if(column_matrix){
                std::vector<int> row;
                for(int j=0; j<elements.size(); j++){
                    if(getElement(i,j)){
                        row.push_back(j);
                    }
                }
                return row;
            }else{
                return elements[i];
            }
        }
        
        bool isZeroList(int j){return elements[j].size() == 0;}  //check whether column j (column-matrix) or row j (row-matrix) is zero
        int getLowest(int j){ //return lowest one  (column-matrix) or leftmost one (row-matrix), if no elements return -1
            if(isZeroList(j)){
                return -1;
            }else{
                return elements[j].back();
            }
        } 
        int getLowestNotFixed(int j, int num_fixed){ //return from below (num_fixed+1)-th element
            if(elements[j].size()<=num_fixed){
                return -1;
            }else{
                return elements[j].rbegin()[num_fixed];
            }
        } 
        int getListLength(int j){return elements[j].size();} //return number of non-zero entries of column (column-matrix) or row (row-matrix) j
        
        void setSize(int size_){elements.clear(); elements.resize(size_);} //set size of matrix
        void setList(int j, std::vector<int> list){ elements[j] = list;} //set column (column-matrix) or row (row-matrix) j
        void setZeroList(int j){ elements[j].clear();} //set column (column-matrix) or row (row-matrix) j to zero
        void addListTo(int s, int t){ //add column (row) s to column (row) t for column-matrix (row-matrix)
            //compute symmetric difference
            std::vector<int> summed_lists;
            std::vector<int> list1 = elements[s];
            std::vector<int> list2 = elements[t];
            if(column_matrix){
                std::set_symmetric_difference(list1.begin(),list1.end(),list2.begin(),list2.end(),std::back_inserter(summed_lists));
            }else{ //rows are reverse ordered, keep that
                std::set_symmetric_difference(list1.rbegin(),list1.rend(),list2.rbegin(),list2.rend(),std::back_inserter(summed_lists));
                std::reverse(summed_lists.begin(),summed_lists.end());
            }
            //store in place of column t
            elements[t] = summed_lists;
        }
        void addList(int j, std::vector<int> list_other){ //add given list to column/row j
            //compute symmetric difference
            std::vector<int> summed_lists;
            std::vector<int> list_this = elements[j];
            if(column_matrix){
                std::merge(list_this.begin(),list_this.end(),list_other.begin(),list_other.end(),std::back_inserter(summed_lists));
            }else{ //rows are reverse ordered, keep that
                std::merge(list_this.rbegin(),list_this.rend(),list_other.rbegin(),list_other.rend(),std::back_inserter(summed_lists));
                std::reverse(summed_lists.begin(),summed_lists.end());
            }
            //store in place of list j
            elements[j] = summed_lists;
        }
        void setIdentity(int size){ //set to identity matrix of given size
            elements.clear();
            setSize(size);
            for(int i=0; i<elements.size(); i++){
                std::vector<int> list;
                list.push_back(i);
                elements[i]=list;
            }
        }
        
        bool isListEqualTo(int j, std::vector<int> other_list){
            if(elements[j].size()==other_list.size()){
                for(int i=0; i<elements[j].size(); i++){
                    if(elements[j][i]!=other_list[i]){
                        return false;
                    }
                }
                return true;
            }else{
                return false;
            }
        }
        
        //check if this matrix and other matrix are the same
        bool isSame(Sparse_Matrix *other_matrix){
            bool is_same = true;
            if(column_matrix){
                for(int i=0; i<getSize(); i++){
                    is_same = isListEqualTo(i,other_matrix->getColumn(i));
                    if(!is_same){
                        break;
                    }
                }
            }else{
                for(int i=0; i<getSize(); i++){
                    is_same = isListEqualTo(i,other_matrix->getRow(i));
                    if(!is_same){
                        break;
                    }
                }
            }
            return is_same;
        }
        
        //convert column-matrix to row-matrix
        void convertToRowMatrix(){
            if(column_matrix){
                column_matrix=false;
                std::vector<std::vector<int> > columns = elements; //store current matrix representation
                int size = columns.size();
                //clear current matrix
                elements.clear();
                elements.resize(size);
                std::vector<int> current_column_position(size,0); //keep track of where we are currently in each column (only go through once)
                for(int i=0; i<size; i++){
                //build rows: check which columns have a 1 in this row
                    std::vector<int> row;
                    for(int j=size-1; j>=i; j--){  //we have upper-diagonal matrix => start with j>=i, we want decreasing order
                        if(current_column_position[j]<columns[j].size()){ //we have not gone through the whole column yet
                            if(columns[j][current_column_position[j]]==i){
                                row.push_back(j);
                                current_column_position[j]=current_column_position[j]+1;
                            }
                        }
                    }
                    elements[i]=row;
                }
            }
        }
        //convert row-matrix to column-matrix
        void convertToColumnMatrix(){
            if(!column_matrix){
                column_matrix=true;
                std::vector<std::vector<int> > rows = elements; //store current matrix representation
                int size = rows.size();
                //clear current matrix
                elements.clear();
                elements.resize(size);
                std::vector<int> current_row_position(size,0); //keep track of where we are currently in each row (only go through once), start from end
                for(int i=0; i<size; i++){
                    current_row_position[i]=rows[i].size();
                }
                for(int i=0; i<size; i++){
                //build columns: check which rows have a 1 in this column
                    std::vector<int> column;
                    for(int j=i; j<=rows.size(); j++){  //we have upper-diagonal matrix 
                        if(current_row_position[j]>=0){ //we have not gone through the whole row yet
                            if(rows[j][current_row_position[j]]==i){
                                column.push_back(j);
                                current_row_position[j]=current_row_position[j]-1;
                            }
                        }
                    }
                    elements[i]=column;
                }
            }
        }
        
        void clear(){elements.clear();}
        
        int getNumEntries(){ //get number of 1s in matrix
            int count_entries = 0;
            for(int i=0;i<elements.size();i++){
                count_entries += elements[i].size();
            }
            return count_entries;
        }
        int getNumZeroLists(int dim, std::vector<int>* dimensions){ //get number of empty columns (rows) of given dimension (any if -1)
            int count_zerolists = 0;
            for(int i=0;i<elements.size();i++){
                if(isZeroList(i)){
                    if(dim==-1 || dimensions->at(i)==dim){
                        count_zerolists++;
                    }
                }
            }
            return count_zerolists;
        }
        int getNumNonZeroLists(int dim, std::vector<int>* dimensions){ //get number of non-empty columns (rows) of given dimension (any if -1)
            int count_nonzerolists = 0;
            for(int i=0;i<elements.size();i++){
                if(!isZeroList(i)){
                    if(dim==-1 || dimensions->at(i)==dim){
                        count_nonzerolists++;
                    }
                }
            }
            return count_nonzerolists;
        }
        
        void print(bool columns, std::ostream& output){ //print as sparse column (row) matrix
            if(columns){
                output << "columns" << std::endl;
            }else{
                output << "rows" << std::endl;
            }
            for(int j=0; j<elements.size(); j++){
                std::vector<int> list;
                if(columns){
                    list = getColumn(j);
                }else{
                    list = getRow(j);
                    if(!column_matrix){ //rows are in decreasing order
                        std::reverse(list.begin(),list.end());
                    }
                }
                output << j << ", ";
                for(int i=0; i<list.size(); i++){
                    output << list[i] << " ";
                }
                output << std::endl;
            }
        }
        void printList(int j){ //print j-th column (row)
            for(int i=0; i<elements[j].size(); i++){
                std::cout << elements[j][i] << " ";
            }
            std::cout << std::endl;
        }
        //print as sparse column (row) matrix, indicate whether list element corresponds to positive or negative simplex
        void printPosNeg(bool columns, std::vector<bool>* positive_simplices, std::ostream& output){
            if(columns){
                output << "columns" << std::endl;
            }else{
                output << "rows" << std::endl;
            }
            for(int j=0; j<elements.size(); j++){
                std::vector<int> list;
                if(columns){
                    list = getColumn(j);
                }else{
                    list = getRow(j);
                    if(!column_matrix){
                        std::reverse(list.begin(),list.end());
                    }
                }
                output << j << ", ";
                for(int i=0; i<list.size(); i++){
                    output << list[i];
                    if(positive_simplices->at(list[i]))
                    {
                        output << "+ ";
                    }else{
                        output << "- ";
                    }
                }
                output << std::endl;
            }
        }
        void printFull(){ //print dense 0/1-matrix 
            std::cout << "[";
            for(int i=0; i<elements.size(); i++){
                std::vector<int> row = getRow(i);
                if(column_matrix){
                    int j=0;
                    std::vector<int>::iterator row_iterator = row.begin();
                    while(j<elements.size() && row_iterator<row.end()){
                        if(j==(*row_iterator)){
                            ++row_iterator;
                            std::cout << "1 ";
                        }else{
                            std::cout << "0 ";
                        }
                        j++;
                    }
                    while(j<elements.size()){
                        std::cout << "0 ";
                        j++;
                    }
                }else{
                    int j=0;
                    std::vector<int>::reverse_iterator row_iterator = row.rbegin();
                    while(j<elements.size() && row_iterator!=row.rend()){
                        if(j==(*row_iterator)){
                            ++row_iterator;
                            std::cout << "1 ";
                        }else{
                            std::cout << "0 ";
                        }
                        j++;
                    }
                    while(j<elements.size()){
                        std::cout << "0 ";
                        j++;
                    }
                }
                if(i<elements.size()-1){
                    std::cout << ";" << std::endl;
                }
            }
            std::cout << "]" << std::endl;
        }
};

//class containing information about persistent homology of a filtered simplicial complex, methods to compute it
class Persistence
{      
    private:
        bool weighted; //weighted point sets, filtration values are squared radii, persistence is difference between squared radii
        int filtration_size; //number of simplices
        bool was_adapted; //whether persistence diagram has changed from original (persistence recomputed for adapted filtration)
        
        //R = boundary_matrix * U
        std::vector<int> dimensions_current; //dimension of simplex corresponding to each column in matrix R/U (corresponding to current (possibly adapted) complex)
        std::vector<int> dimensions_original; //dimension of simplices in the original filtration (corresponding to matrices U_original, R_original)
        std::vector<bool> positive_current; //whether column corresponds to positive or negative simplex for current (possibly adapted) filtration
        std::vector<bool> positive_original; //whether column corresponds to positive or negative simplex (could be critical or non-critical but is always assigned positive or negative during persistence calculation), for original filtration
        Sparse_Matrix boundaryMatrix; //current boundary matrix (for exporting)
        Sparse_Matrix R_current; //R reduced matrix by column (standard or exhaustive) algorithm, boundary of homology classes, for current (possibly adapted) filtration
        Sparse_Matrix U_current; //U upper triangular, storing canonical cycles and chains, for current (possibly adapted) filtration      
        Sparse_Matrix R_original; //R for original filtration
        Sparse_Matrix U_original; //U for original filtration
        
        //cohomology Q = boundary * V
        //matrices corresponding to original (unadapted) filtration
        Sparse_Matrix Q; //Q reduced matrix by row (standard or exhaustive) algorithm
        Sparse_Matrix V; //matrix storing canonical cocycles and cochains

        std::vector<PersistencePair> persistence_pairs_current; //list of persistence pairs of filtered complex, current filtration (after possible manipulations), in order of increasing persistence, last come the infinite pairs sorted by decreasing birth
        std::vector<PersistencePair> persistence_pairs_original; //list of persistence pairs of original filtered complex, in order of increasing persistence, last come the infinite pairs sorted by decreasing birth
        std::vector<int> persistence_pair_indices_for_simplex_current; //store index of persistence pair for simplices in current filtration
        
        //hole dependences of different type, for every simplex
        std::vector<std::vector<int> > dependencesBoundary;
        std::vector<std::vector<int> > dependencesBoundary_T;
        std::vector<std::vector<int> > dependencesDB;
        std::vector<std::vector<int> > dependencesDB_T;
        std::vector<std::vector<int> > dependencesBB;
        std::vector<std::vector<int> > dependencesBB_T;
        std::vector<std::vector<int> > dependencesDD;
        std::vector<std::vector<int> > dependencesDD_T;
        std::vector<std::vector<int> > dependencesBD;
        std::vector<std::vector<int> > dependencesBD_T;
        std::vector<std::vector<int> > codependencesDB;
        std::vector<std::vector<int> > codependencesDB_T;
        std::vector<std::vector<int> > codependencesBB;
        std::vector<std::vector<int> > codependencesBB_T;
        std::vector<std::vector<int> > codependencesDD;
        std::vector<std::vector<int> > codependencesDD_T;
        bool hole_operations_cohomology; //whether hole operations should be based on cohomology (instead of homology)
        
        std::vector<int> status_of_corresponding_persistence_pair_current;  //status of current persistence pairs, 0... past, 1 ... presence, 2 ... future, for current subcomplex
        std::vector<int> status_of_corresponding_persistence_pair_original; //status of original persistence pairs, 0... past, 1 ... presence, 2 ... future, for current subcomplex
       
        //test
        std::vector<std::pair<PersistencePair,std::pair<exact,exact> > > manipulated_persistence_points; //list of persistence points that have (actively) been manipulated by hole operations and the resulting new location in persistence diagram, check if we really catch all side affects by dependences
         
        //statistics
        std::vector<int> count_column_additions; //number of column additions to reduce a column, for column-reduction
        std::vector<int> count_row_additions; //number of row additions to reduce a row, for row-reduction
        
    public:  
        Persistence(){was_adapted=false;hole_operations_cohomology=false;}; //constructor
        void clear(bool full);
    
        //for a given filtered simplicial complex, compute persistence pairs and corresponding basis matrix
        //implementation similar to phat, but also gives matrices R,U for homology and Q,V for relative cohomology (storing (co)cycles and (co)chains)
        void compute(bool exhaustive, bool first_computation, std::vector<Simplex>* simplices, Filtration* filtration, bool weighted, bool printstats); 
        bool setHoleOperationsCohomology(bool hole_operations_cohomology_){if(hole_operations_cohomology!=hole_operations_cohomology_){hole_operations_cohomology=hole_operations_cohomology_; return true;}return false;}
        
        bool wasPersistenceDiagramAdapted(){return was_adapted;}
        std::vector<PersistencePair>* getPersistencePairsOriginal(){return &persistence_pairs_original;}
        std::vector<PersistencePair>* getPersistencePairsCurrent(){return &persistence_pairs_current;}
        bool isPositiveSimplexOriginal(int filtration_index){return positive_original[filtration_index];}
        bool isPositiveSimplexCurrent(int filtration_index){return positive_current[filtration_index];}
        double getMaxPersistenceCurrent(int dim, Filtration* filtration); //get maximum persistence value for given dimension
        
        std::vector<int> getChain(int j){return U_original.getColumn(j);} //get canonical chain or cycle stored in matrix U
        std::vector<int> getCocycle(int i){return V.getRow(i);} //get cocycle/copath stored in matrix V
        
        PersistencePair getPersistencePairForSimplexOriginal(int simplex_index, bool birth); //given a simplex, get its corresponding persistence pair in original filtration
        PersistencePair getPersistencePairForSimplexCurrent(int simplex_index){if(simplex_index<persistence_pair_indices_for_simplex_current.size()){int pair_index = persistence_pair_indices_for_simplex_current[simplex_index]; if(pair_index>=0){return persistence_pairs_current[pair_index];}}return PersistencePair(-1,-1,-1);} //given a simplex, get its corresponding persistence pair for current filtration
        
        PersistencePair getPersistencePairOfDim(int dim, int ranking); //get ranking-th persistence pair of given dim, with order of decreasing persistence
        PersistencePair getPersistencePairOfDimStatus(int dim, int ranking, exact alpha2, bool exclude_notbornyet, bool exclude_active, bool exclude_dead, Filtration* filtration); //get ranking-th persistence pair of given dim with given status for current subcomplex, with order of decreasing persistence
        std::vector<PersistencePair*> getPersistencePairsWithBound(int dim, double persistence_bound, bool lowerbound, Filtration* filtration); //get all non-zero persistence pairs of given (or any if -1) dimension with persistence above or below given threshold
        std::vector<PersistencePair*> getPersistencePairsWithBoundStatus(int dim, double persistence_bound, bool lowerbound, exact alpha2, bool exclude_notbornyet, bool exclude_active, bool exclude_dead, Filtration* filtration); //get all non-zero persistence pairs of given (or any if -1) dimension and status with persistence above or below given threshold
        
        //get dependent simplices for given type, use homology or cohomology dependences
        std::vector<int> getDependencesBoundary(int i){return dependencesBoundary[i];}; 
        std::vector<int> getDependencesBoundary_T(int i){return dependencesBoundary_T[i];}; 
        std::vector<int> getDependencesDB(int i){if(hole_operations_cohomology){return codependencesDB_T[i];}else{return dependencesDB[i];}}; //get death-birth-dependences between simplex i < simplices j
        std::vector<int> getDependencesBB(int i){if(hole_operations_cohomology){return codependencesDD_T[i];}else{return dependencesBB[i];}}; //get birth-birth-dependences between simplex i < simplices j
        std::vector<int> getDependencesDD(int i){if(hole_operations_cohomology){return codependencesBB_T[i];}else{return dependencesDD[i];}}; //get death-death-dependences between simplex i < simplices j
        std::vector<int> getDependencesDB_T(int j){if(hole_operations_cohomology){return codependencesDB[j];}else{return dependencesDB_T[j];}}; //get death-birth-dependences between simplex j > simplices i
        std::vector<int> getDependencesBB_T(int j){if(hole_operations_cohomology){return codependencesDD[j];}else{return dependencesBB_T[j];}}; //get birth-birth-dependences between simplex j > simplices i
        std::vector<int> getDependencesDD_T(int j){if(hole_operations_cohomology){return codependencesBB[j];}else{return dependencesDD_T[j];}}; //get death-death-dependences between simplex j > simplices i
        std::vector<PersistencePair> getDependentPairs(int simplex_index, bool birth_i, bool birth_j, bool transposed); //get dependences of given type for simplex i
        std::vector<PersistencePair> getDependentPairsBoundary(int simplex_index, bool transposed); //get boundary or coboundary dependences for given simplex
        
        //get all dependences recursively
        std::vector<int> getDependencesBoundaryRecursively(int i); //boundary dependences (all faces)
        std::vector<PersistencePair> getDependentPairsOperation(int simplex_index, bool lock, bool un, bool recursively, bool original); //get all dependent persistence pairs relevant for the specified operation (respecting current status), either only first-level dependences or recursively
         
        //hole operations, status of persistence pairs
        void resetPersistencePairsStatus(Filtration* filtration, exact current_alpha2, bool recompute_original); //reset status (0 past, 1 presence, 2 future) of persistence pairs, undo hole operations
        int getStatusOfCorrespondingPersistencePair(int simplex_index){return status_of_corresponding_persistence_pair_current[simplex_index];}
        void setStatusOfCorrespondingPersistencePair(int simplex_index, int status){status_of_corresponding_persistence_pair_current[simplex_index]=status;}
        void computeManipulatedPersistencePointsFromStatus(Filtration* filtration, exact current_alpha2, exact current_exclusion_alpha2); //compute list of persistence points that have (actively) been manipulated by hole operations and the resulting new location in persistence diagram
        std::vector<std::pair<PersistencePair,std::pair<exact,exact> > >* getManipulatedPersistencePoints(){return &manipulated_persistence_points;}
        
        //compute and/or print statistics
        std::vector<std::vector<int> > computeBettiFiltration(); //compute betti numbers for current filtration from persistence matrix R
        void printBoundaryMatrix(std::ostream& output){boundaryMatrix.print(true,output);} //print boundary matrix
        void printRUQV(std::ostream& output); //print matrices after reduction
        void printPersistencePairs(std::ostream& output, Filtration* filtration_current); //print persistence pairs
        void computeResultsPaperHolesDensities(std::string prefix, std::ostream& output); //print densities for all matrices
        void computeResultsPaperHolesCanonicalChainsSize(std::ostream& output); //print size of canonical (co)chain/cycle for average or maximum persistence pair per dimension
        void computeResultsPaperHolesDependencesSize(std::ostream& output); //print sizes of dependence structure
        
        
    private:
        void initialize(std::vector<Simplex>* simplices, Filtration* filtration, bool first_computation);  //initialize with boundary matrix of given filtration
        void reduce(bool exhaustive, bool first_computation, bool printstats); //reduce matrices R, U, for first_computation also reduce Q, V, optionally exhaustive reduction to get canonical (co)chains
        void computePersistencePairs(); //compute persistence pairs from matrices
        void sortByPersistence(Filtration* filtration, bool first_computation); //sort persistence pairs in order of increasing persistence, last come the infinite pairs sorted by decreasing birth
        std::vector<std::vector<int> > getNumsAllDependencesAllOperations(); //for all hole operations, for all simplices get number of relevant dependent simplices (disregarding status)

};

#endif // PERSISTENCE_H