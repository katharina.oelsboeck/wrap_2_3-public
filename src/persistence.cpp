//persistence.cpp
//author: koelsboe

#include "persistence.h"
#include "alpha_complex.h"

//clear all lists
void Persistence::clear(bool full)
{
    dimensions_current.clear();
    positive_current.clear();
    R_current.clear();
    U_current.clear(); 
    persistence_pairs_current.clear();
    persistence_pair_indices_for_simplex_current.clear();
    status_of_corresponding_persistence_pair_current.clear();
    manipulated_persistence_points.clear();
    count_column_additions.clear();
    count_row_additions.clear();
    
    //clear data for original filtration
    if(full){
        dimensions_original.clear();
        positive_original.clear();
        U_original.clear();
        Q.clear(); V.clear();
        persistence_pairs_original.clear();
        dependencesBoundary.clear();
        dependencesBoundary_T.clear();
        dependencesDB.clear();
        dependencesDB_T.clear();
        dependencesBB.clear();
        dependencesBB_T.clear();
        dependencesDD.clear();
        dependencesDD_T.clear();
        dependencesBD.clear();
        dependencesBD_T.clear();
        codependencesDB.clear();
        codependencesDB_T.clear();
        codependencesBB.clear();
        codependencesBB_T.clear();
        codependencesDD.clear();
        codependencesDD_T.clear();
        status_of_corresponding_persistence_pair_original.clear();
    }
}

//for a given filtered simplicial complex, compute persistence pairs and corresponding basis matrix
//implementation similar to phat, but also gives matrices R,U for homology and Q,V for relative cohomology (storing (co)cycles and (co)chains)
void Persistence::compute(bool exhaustive, bool first_computation, std::vector<Simplex>* simplices, Filtration* filtration, bool weighted_, bool printstats)
{
    //clear data from previous computations
    clear(first_computation);
    
    weighted = weighted_;
    filtration_size = filtration->size();
    
    if(first_computation){
        was_adapted=false;
        //initialize lists for hole dependences
        std::vector<int> empty(0);
        dependencesBoundary = std::vector<std::vector<int> >(filtration_size,empty);
        dependencesBoundary_T = std::vector<std::vector<int> >(filtration_size,empty);
        dependencesDB = std::vector<std::vector<int> >(filtration_size,empty);
        dependencesDB_T = std::vector<std::vector<int> >(filtration_size,empty);
        dependencesBB = std::vector<std::vector<int> >(filtration_size,empty);
        dependencesBB_T = std::vector<std::vector<int> >(filtration_size,empty);
        dependencesDD = std::vector<std::vector<int> >(filtration_size,empty);
        dependencesDD_T = std::vector<std::vector<int> >(filtration_size,empty);
        dependencesBD = std::vector<std::vector<int> >(filtration_size,empty);
        dependencesBD_T = std::vector<std::vector<int> >(filtration_size,empty);
        codependencesDB = std::vector<std::vector<int> >(filtration_size,empty);
        codependencesDB_T = std::vector<std::vector<int> >(filtration_size,empty);
        codependencesBB = std::vector<std::vector<int> >(filtration_size,empty);
        codependencesBB_T = std::vector<std::vector<int> >(filtration_size,empty);
        codependencesDD = std::vector<std::vector<int> >(filtration_size,empty);
        codependencesDD_T = std::vector<std::vector<int> >(filtration_size,empty);
    }else{
        //persistence computation on other filtration than original -> persistence diagram adapted
        was_adapted=true;
    }

    //use column-reduction to get matrices R and U for homology, 
    //for first computation also use row-reduction to get matrices Q and V for relative cohomology
    initialize(simplices,filtration,first_computation);
    reduce(exhaustive, first_computation, printstats);

    //extract persistence pairs from reduced matrix, sort by persistence
    persistence_pair_indices_for_simplex_current = std::vector<int>(simplices->size(),-1);
    computePersistencePairs();
    sortByPersistence(filtration,first_computation);

    if(first_computation){
        //store results for original filtration
        persistence_pairs_original = persistence_pairs_current;
        R_original = R_current;
        U_original = U_current;
        dimensions_original = dimensions_current;
        positive_original = positive_current;
    }
}

//initialize with boundary matrix of given filtration
void Persistence::initialize(std::vector<Simplex>* simplices, Filtration* filtration, bool first_computation)
{      
    //initialize size
    R_current.setSize(filtration_size);
    dimensions_current = std::vector<int>(filtration_size,-1);
    //for each column (filtration element): set dimension and entries (boundary indices)
    std::vector<int> filtration_indices(simplices->size(),-1); //store filtration indices of simplices, need to be looked up for later ones
    //set columns of boundary matrix (of dim>=0)
    for(int i=0; i<filtration_size; i++){
        std::vector<int> column; //initialize column
        int dim = filtration->getElementDim(i,simplices);
        dimensions_current[i]=dim;    
        int simplex_index = filtration->getElementIndex(i);
        filtration_indices[simplex_index]=i; //store filtration index for later use
        //set boundary column
        Simplex simplex = simplices->at(simplex_index); 
        std::vector<int> facet_indices;
        if(dim==0){
            facet_indices.push_back(0); //empty simplex is facet of every point
        }else{
            facet_indices = simplex.getFacetIndices();
        }
        for(int j=0; j<facet_indices.size(); j++){
            if(filtration_indices[facet_indices[j]]<0){ //order of simplices wrong
                std::cerr << "ERROR (Persistence::initialize): facet after simplex" << std::endl;
                //filtration->print(simplices); //TEST
                std::cout <<filtration->getElementValue(i) << " "; filtration->getSimplex(i,simplices)->print();
                simplices->at(facet_indices[j]).print();
                return;
            }
            column.push_back(filtration_indices[facet_indices[j]]);
        }
        std::sort(column.begin(), column.end()); //facet indices must be sorted
        if(first_computation){
            //store boundary dependence
            for(int j=0; j<column.size(); j++){
                dependencesBoundary[i].push_back(column[j]);
                dependencesBoundary_T[column[j]].push_back(i);
            }
        }
        //set matrix column
        R_current.setList(i, column);
    }
    //initialize U to identity
    U_current.setIdentity(filtration_size);
    
    if(first_computation){
        boundaryMatrix = R_current; //store boundary matrix
        //initialize cohomology matrices (also with boundary and identity but as row-matrices)
        Q = R_current;
        Q.convertToRowMatrix();
        V.setIdentity(filtration_size);
        V.setRowMatrix(true);
        count_column_additions = std::vector<int>(R_current.getSize(),0);
        count_row_additions = std::vector<int>(R_current.getSize(),0);
    }
}

//reduce matrices R, U, for first_computation also reduce Q, V, optionally exhaustive reduction to get canonical (co)chains
void Persistence::reduce(bool exhaustive, bool first_computation, bool printstats)
{
    std::vector<int> lowest_one_lookup(R_current.getSize(),-1); //look-up table of lowest ones for faster reduction
    double time1 = getCPUTime();
    //basic reduction of R and U
    for(int j=0; j<R_current.getSize(); j++){
        //compute lowest one, standard reduction: make lowest ones unique
        //add column k to j if there is k<j: low(k)=low(j)>=0
        std::vector<int> columns_added;
        int lowest_one = R_current.getLowest(j);
        while(lowest_one != -1 && lowest_one_lookup[lowest_one] != -1){
            int k = lowest_one_lookup[lowest_one];
            R_current.addListTo(k,j);
            U_current.addListTo(k,j);
            lowest_one = R_current.getLowest(j);
            columns_added.push_back(k);
        }
        if(lowest_one != -1){
            if(exhaustive){
                //exhaustive reduction: also add column k to j if there is k<j: R[low(k),j]=1
                //eliminate birth simplices whose cycle already died (appear as lowest one to the left of this column)
                //start from below
                int elements_fixed = 1;
                while(elements_fixed<R_current.getListLength(j)){
                    int lowest_notfixed = R_current.getLowestNotFixed(j,elements_fixed);
                    //check if chain simplex is positive and already appeared as lowest one (its cycle already died)
                    if(R_current.isZeroList(lowest_notfixed) && lowest_one_lookup[lowest_notfixed]!=-1){
                        int k = lowest_one_lookup[lowest_notfixed];

                        R_current.addListTo(k,j); 
                        U_current.addListTo(k,j);
                        columns_added.push_back(k);
                    }else{
                        elements_fixed++;
                    }
                }
            }
            lowest_one_lookup[lowest_one] = j;
        }

        //store dependences
        if(first_computation){    
            count_column_additions[j] += columns_added.size();
            //save dependences between columns
            if(R_current.isZeroList(j)){ //j birth
                for(int k=0; k<columns_added.size(); k++){
                    dependencesDB[columns_added[k]].push_back(j);
                    dependencesDB_T[j].push_back(columns_added[k]);
                }
            }else{ //j death
                for(int k=0; k<columns_added.size(); k++){
                    dependencesDD[columns_added[k]].push_back(j);
                    dependencesDD_T[j].push_back(columns_added[k]);
                }
                //1s in column
                std::vector<int> col_j = R_current.getColumn(j);
                int birth_j = R_current.getLowest(j);
                for(int k=0; k<col_j.size(); k++){
                    if(R_current.isZeroList(col_j[k]) && col_j[k]!=R_current.getLowest(j)){
                        dependencesBB[col_j[k]].push_back(birth_j);
                        dependencesBB_T[birth_j].push_back(col_j[k]);
                    }else if(!R_current.isZeroList(col_j[k])){
                        dependencesDB[col_j[k]].push_back(birth_j);
                        dependencesDB_T[birth_j].push_back(col_j[k]);
                    }
                }
                dependencesBD[R_current.getLowest(j)].push_back(j);
                dependencesBD_T[j].push_back(R_current.getLowest(j));
            }
        }
    } 
    //erase doubles from DB-dependences (could happen since we have two conditions)
    for(int i=0; i<R_current.getSize(); i++){
        std::sort(dependencesDB[i].begin(),dependencesDB[i].end());
        dependencesDB[i].erase(std::unique(dependencesDB[i].begin(),dependencesDB[i].end()),dependencesDB[i].end());
        std::sort(dependencesDB_T[i].begin(),dependencesDB_T[i].end());
        dependencesDB_T[i].erase(std::unique(dependencesDB_T[i].begin(),dependencesDB_T[i].end()),dependencesDB_T[i].end());
    }

    if(first_computation && printstats){
        std::cout << "Reduction of R and U" << std::endl;
        std::cout << " Time, " << getCPUTime()-time1 << std::endl; 
    }

    //store which simplices are positive or negative
    positive_current = std::vector<bool>(R_current.getSize(),false);
    for(int j=0; j<R_current.getSize(); j++){
        if(R_current.isZeroList(j)){
            positive_current[j]=true;
        }
    }
    if(first_computation){
        positive_original = positive_current;
    }
        
        
    if(first_computation){

        //standard row reduction for relative cohomology on Q,V
        //similar algorithm as for absolute homology, just on row matrices with reverse ordered lists, from below
        std::vector<int> leftmost_one_lookup(Q.getSize(),-1);
        time1 = getCPUTime();
        //basic reduction of Q and V, bottom-to-top
        for(int i=Q.getSize()-1; i>=0; i--){
            std::vector<int> rows_added;
            //compute leftmost one, standard reduction: make leftmost ones unique
            //add row if there is k>i: low(k)=low(i)<=Q.size()
            int leftmost_one = Q.getLowest(i);
            while(leftmost_one != -1 && leftmost_one_lookup[leftmost_one] != -1){
                int k = leftmost_one_lookup[leftmost_one];
                Q.addListTo(k,i);
                V.addListTo(k,i);
                leftmost_one = Q.getLowest(i);
                rows_added.push_back(k);
            }
            if(leftmost_one != -1){
                if(exhaustive){
                    //exhaustive reduction: also add row if there is k>i: Q[i, left(k)]=1
                    //eliminate cobirth simplices whose cocycle already died (appear as leftmost one below this row)
                    //start from left
                    int elements_fixed = 1;
                    while(elements_fixed<Q.getListLength(i)){
                        int leftmost_notfixed = Q.getLowestNotFixed(i,elements_fixed);
                        //check if cochain simplex is positive and already appeared as leftmost one (its relative cocycle already died)
                        if(Q.isZeroList(leftmost_notfixed)&&leftmost_one_lookup[leftmost_notfixed]!=-1){
                            int k = leftmost_one_lookup[leftmost_notfixed];
                            Q.addListTo(k,i); 
                            V.addListTo(k,i); 
                            rows_added.push_back(k);
                        }else{
                            elements_fixed++;
                        }
                    }
                }
                leftmost_one_lookup[leftmost_one] = i;
            }
            //store dependences
            if(first_computation){    
                count_row_additions[i] += rows_added.size();
                //save dependences between rows
                if(Q.isZeroList(i)){ //i cobirth
                    for(int k=0; k<rows_added.size(); k++){
                        codependencesDB[rows_added[k]].push_back(i);
                        codependencesDB_T[i].push_back(rows_added[k]);
                    }
                }else{ //i codeath
                    for(int k=0; k<rows_added.size(); k++){
                        codependencesDD[rows_added[k]].push_back(i);
                        codependencesDD_T[i].push_back(rows_added[k]);
                    }
                    //1s in row
                    std::vector<int> row_i = Q.getRow(i);
                    int birth_i = Q.getLowest(i);
                    for(int k=0; k<row_i.size(); k++){
                        if(Q.isZeroList(row_i[k]) && row_i[k]!=Q.getLowest(i)){
                            codependencesBB[row_i[k]].push_back(birth_i);
                            codependencesBB_T[birth_i].push_back(row_i[k]);
                        }else if(!Q.isZeroList(row_i[k])){
                            codependencesDB[row_i[k]].push_back(birth_i);
                            codependencesDB_T[birth_i].push_back(row_i[k]);
                        }
                    }
                }
            }
        }
        //erase doubles from coDB-dependences (could happen since we have two conditions)
        for(int i=0; i<Q.getSize(); i++){
            std::sort(codependencesDB[i].begin(),codependencesDB[i].end());
            codependencesDB[i].erase(std::unique(codependencesDB[i].begin(),codependencesDB[i].end()),codependencesDB[i].end());
            std::sort(codependencesDB_T[i].begin(),codependencesDB_T[i].end());
            codependencesDB_T[i].erase(std::unique(codependencesDB_T[i].begin(),codependencesDB_T[i].end()),codependencesDB_T[i].end());
        }
        if(printstats){
            std::cout << "Reduction of Q and V" << std::endl;
            std::cout << " Time, " << getCPUTime()-time1 << std::endl; 
        }
    }
}

//compute persistence pairs from matrices
void Persistence::computePersistencePairs()
{
    //all points (except first one) are positive simplices
    //2-dim: edges can be positive or negative, triangles can only be negative
    //3-dim: edges and triangles can be positive or negative, tetrahedra can only be negative
    //we also need to capture infinite persistence pairs, simplices that are never paired  
    std::vector<bool> filtration_infinite(filtration_size,true); //indicate whether simplex already appeared in finite pair
    
    //get finite persistence pairs from reduced matrix
    for(int j=0; j<R_current.getSize(); j++){
        if(!R_current.isZeroList(j)){
            int birth_index = R_current.getLowest(j); 
            int death_index = j; 
            int dim = dimensions_current[birth_index];
            
            persistence_pairs_current.push_back(PersistencePair(dim,birth_index,death_index));
            filtration_infinite[death_index]=false; //finite
            filtration_infinite[birth_index]=false; //finite 
        }
    }
    
    //compute infinite pairs
    for(int i=0; i<filtration_size; i++){
        if(filtration_infinite[i]){
            int dim=dimensions_current[i];
            persistence_pairs_current.push_back(PersistencePair(dim,i));
        }      
    } 
}

//sort persistence pairs in order of increasing persistence, last come the infinite pairs sorted by decreasing birth
void Persistence::sortByPersistence(Filtration* filtration, bool first_computation)
{
    std::vector<std::pair<double,int> > persistence_with_index_finite;
    std::vector<std::pair<double,int> > birth_with_index_infinite;
    for(int i=0; i<persistence_pairs_current.size(); i++){
        if(!persistence_pairs_current[i].isInfinite()){
            if(!weighted){
                persistence_with_index_finite.push_back(std::make_pair(persistence_pairs_current[i].getPersistence_double(filtration),i));
            }else{
                persistence_with_index_finite.push_back(std::make_pair(persistence_pairs_current[i].getPersistence2_double(filtration),i));
            }
        }else{
            birth_with_index_infinite.push_back(std::make_pair(persistence_pairs_current[i].getBirthIndexFiltration(),i));
        }
    }
    std::sort(persistence_with_index_finite.begin(),persistence_with_index_finite.end());
    std::sort(birth_with_index_infinite.begin(),birth_with_index_infinite.end());
    std::vector<PersistencePair> persistence_pairs_sorted;
    for(int i=0; i<persistence_with_index_finite.size(); i++){
        persistence_pairs_sorted.push_back(persistence_pairs_current[persistence_with_index_finite[i].second]);
    }
    for(int i=birth_with_index_infinite.size()-1; i>=0; i--){
        persistence_pairs_sorted.push_back(persistence_pairs_current[birth_with_index_infinite[i].second]);
    }
    persistence_pairs_current = persistence_pairs_sorted;
    if(first_computation){
        persistence_pairs_original = persistence_pairs_sorted;
    }
    for(int i=0; i<persistence_pairs_current.size(); i++){
        persistence_pair_indices_for_simplex_current[filtration->getElementIndex(persistence_pairs_current[i].getBirthIndexFiltration())]=i;
        if(!persistence_pairs_current[i].isInfinite()){
            persistence_pair_indices_for_simplex_current[filtration->getElementIndex(persistence_pairs_current[i].getDeathIndexFiltration())]=i;
        }
    }
}

//get maximum persistence value for given dimension
double Persistence::getMaxPersistenceCurrent(int dim, Filtration* filtration)
{
    try{
        PersistencePair max_pair = PersistencePair(-1,-1,-1);
        for (int i=0; i<persistence_pairs_current.size(); i++){
            PersistencePair pair = persistence_pairs_current[i];
            if(!pair.hasZeroPersistence(filtration) && !pair.isInfinite() && (pair.getDim()==dim || dim==-1)){
                max_pair = pair;
            }
        }

        if(max_pair.getDim()==-1){
            return -1;
        }else{
            if(filtration->size()>0){
                if(!weighted){
                    return max_pair.getPersistence_double(filtration);
                }else{
                    return max_pair.getPersistence2_double(filtration);
                }
            }else{
                return -1;
            }
        }
    }catch(...){
        return -1;
    }
}

//given a simplex, get its corresponding persistence pair
PersistencePair Persistence::getPersistencePairForSimplexOriginal(int filtration_index, bool birth)
{
    //we did not store index of pair, but it is easy to reconstruct persistence pair from matrices
    if(birth){ //birth simplex given
        int death_filtration_index = Q.getLowest(filtration_index);
        if(death_filtration_index<0){ //infinite pair
            return PersistencePair(dimensions_original[filtration_index],filtration_index);
        }else{
            return PersistencePair(dimensions_original[filtration_index],filtration_index,death_filtration_index);
        }
    }else{
        int birth_filtration_index = R_original.getLowest(filtration_index);
        return PersistencePair(dimensions_original[birth_filtration_index],birth_filtration_index,filtration_index);
    }
}

//get ranking-th persistence pair of given dim, with order of decreasing persistence, of any dimension if dim == -1
PersistencePair Persistence::getPersistencePairOfDim(int dim, int ranking){
    int count = 0;
    for(int i=persistence_pairs_original.size()-1; i>=0; i--){
        PersistencePair pair = persistence_pairs_original[i];
        if(!pair.isInfinite() && (pair.getDim()==dim || dim==-1)){
            count ++;
            if(count>=ranking){
                return pair;
            }
        }
    }
    return PersistencePair(-1,-1,-1);
} 

//get ranking-th persistence pair of given dim with given status for current subcomplex, with order of decreasing persistence
PersistencePair Persistence::getPersistencePairOfDimStatus(int dim, int ranking, exact alpha2, bool exclude_notbornyet, bool exclude_active, bool exclude_dead, Filtration* filtration){
    int count = 0;
    for(int i=persistence_pairs_original.size()-1; i>=0; i--){
        PersistencePair pair = persistence_pairs_original[i];
        if(pair.getDim()==dim || dim==-1){
            bool ok = true;
            exact birth2 = pair.getBirth2(filtration);
            if(pair.isInfinite()){ //infinite pair
                if(exclude_notbornyet && birth2>alpha2){ 
                    ok = false;
                }
                if(exclude_active && birth2<=alpha2){
                    ok = false;
                }
            }else{ //finite pair
                exact death2 = pair.getDeath2(filtration);
                if(birth2==death2){
                    ok = false; //zero persistence
                }
                if(exclude_notbornyet && birth2>alpha2){
                    ok = false;
                }
                if(exclude_active && birth2<=alpha2 && alpha2<death2){
                    ok = false;
                }
                if(exclude_dead && alpha2>=death2){
                    ok = false;
                }
            }
            if(ok){
                count++;
                if(count>=ranking){
                    return pair;
                }
            }
        }
    }
    return PersistencePair(-1,-1,-1);
}

//get all non-zero persistence pairs of given (or any if -1) dimension with persistence above or below given threshold
std::vector<PersistencePair*> Persistence::getPersistencePairsWithBound(int dim, double persistence_bound, bool lowerbound, Filtration* filtration)
{
    std::vector<PersistencePair*> pairs;
    //get all finite persistence pairs with persistence above given bound
    if(lowerbound){
        for(int i=persistence_pairs_original.size()-1; i>=0; i--){
            PersistencePair pair = persistence_pairs_original[i];
            if(!pair.hasZeroPersistence(filtration) && !pair.isInfinite() && (pair.getDim()==dim || dim==-1)){
                double persistence;
                if(!weighted){
                    persistence = pair.getPersistence_double(filtration);
                }else{
                    persistence = pair.getPersistence2_double(filtration);
                }
                if(persistence>=persistence_bound){
                    pairs.push_back(&persistence_pairs_original[i]);
                }else{
                    break;
                }
            }
        }
    }else{
    //get all finite persistence pairs with persistence below given bound
        for(int i=0; i<persistence_pairs_original.size(); i++){
            PersistencePair pair = persistence_pairs_original[i];
            if(!pair.hasZeroPersistence(filtration) && !pair.isInfinite() && (pair.getDim()==dim || dim==-1)){
                double persistence;
                if(!weighted){
                    persistence = pair.getPersistence_double(filtration);
                }else{
                    persistence = pair.getPersistence2_double(filtration);
                }
                if(persistence<=persistence_bound){
                    pairs.push_back(&persistence_pairs_original[i]);
                }else{
                    break;
                }
            }
        }
    }
    return pairs;
}

//get all non-zero persistence pairs of given (or any if -1) dimension and status with persistence above or below given threshold
std::vector<PersistencePair*> Persistence::getPersistencePairsWithBoundStatus(int dim, double persistence_bound, bool lowerbound, exact alpha2, bool exclude_notbornyet, bool exclude_active, bool exclude_dead, Filtration* filtration)
{
    std::vector<PersistencePair*> pairs;
    //get persistence pairs with persistence above given bound
    if(lowerbound){
        for(int i=persistence_pairs_original.size()-1; i>=0; i--){
            PersistencePair pair = persistence_pairs_original[i];
            //check dimension
            if((pair.getDim()==dim || dim==-1) && !pair.hasZeroPersistence(filtration)){
                double persistence;
                if(!weighted){
                    persistence = pair.getPersistence_double(filtration);
                }else{
                    persistence = pair.getPersistence2_double(filtration);
                }
                //check persistence
                if(persistence>=persistence_bound || pair.isInfinite()){
                    //check status
                    bool ok = true;
                    exact birth2 = pair.getBirth2(filtration);
                    if(pair.isInfinite()){ //infinite pair
                        if(exclude_notbornyet && birth2>alpha2){ 
                            ok = false;
                        }
                        if(exclude_active && birth2<=alpha2){
                            ok = false;
                        }
                    }else{ //finite pair
                        exact death2 = pair.getDeath2(filtration);
                        if(birth2==death2){
                            ok = false; //zero persistence
                        }
                        if(exclude_notbornyet && birth2>alpha2){
                            ok = false;
                        }
                        if(exclude_active && birth2<=alpha2 && alpha2<death2){
                            ok = false;
                        }
                        if(exclude_dead && alpha2>=death2){
                            ok = false;
                        }
                    }
                    if(ok){
                        pairs.push_back(&persistence_pairs_original[i]);
                    }
                }else{
                    break;
                }
            }
        }
    //get persistence pairs with persistence below given bound
    }else{
        for(int i=0; i<persistence_pairs_original.size(); i++){
            PersistencePair pair = persistence_pairs_original[i];
            //check dimension, finite
            if((pair.getDim()==dim || dim==-1) && !pair.hasZeroPersistence(filtration) && !pair.isInfinite()){
                double persistence;
                if(!weighted){
                    persistence = pair.getPersistence_double(filtration);
                }else{
                    persistence = pair.getPersistence2_double(filtration);
                }
                //check persistence
                if(persistence<=persistence_bound){
                    //check status
                    bool ok = true;
                    exact birth2 = pair.getBirth2(filtration);
                    if(pair.isInfinite()){ //infinite pair
                        if(exclude_notbornyet && birth2>alpha2){ 
                            ok = false;
                        }
                        if(exclude_active && birth2<=alpha2){
                            ok = false;
                        }
                    }else{ //finite pair
                        exact death2 = pair.getDeath2(filtration);
                        if(birth2==death2){
                            ok = false; //zero persistence
                        }
                        if(exclude_notbornyet && birth2>alpha2){
                            ok = false;
                        }
                        if(exclude_active && birth2<=alpha2 && alpha2<death2){
                            ok = false;
                        }
                        if(exclude_dead && alpha2>=death2){
                            ok = false;
                        }
                    }
                    if(ok){
                        pairs.push_back(&persistence_pairs_original[i]);
                    }
                }else{
                    break;
                }
            }
        }
    }
    return pairs;
}

//get dependences of given type for simplex i
std::vector<PersistencePair> Persistence::getDependentPairs(int simplex_index, bool birth_i, bool birth_j, bool transposed)
{
    //get dependent simplices from stored relations
    std::vector<int> dependent_simplices;
    if(birth_i && birth_j){ //type BB
        if(!transposed){
            dependent_simplices = getDependencesBB(simplex_index);
        }else{
            dependent_simplices = getDependencesBB_T(simplex_index);
        }
    }else if(!birth_i && birth_j){ //type DB
        if(!transposed){
            dependent_simplices = getDependencesDB(simplex_index);
        }else{
            dependent_simplices = getDependencesDB_T(simplex_index);
        }
    }else if(!birth_i && !birth_j){ //type DD
        if(!transposed){
            dependent_simplices = getDependencesDD(simplex_index);
        }else{
            dependent_simplices = getDependencesDD_T(simplex_index);
        }
    }
    //get persistence pairs of dependent simplices
    std::vector<PersistencePair> dependent_pairs;
    for(int k=0; k<dependent_simplices.size(); k++){
        int dependent_index = dependent_simplices[k];
        
        if((!transposed && birth_j) || (transposed && birth_i) ){ //dependent simplices are birth simplices
            dependent_pairs.push_back(getPersistencePairForSimplexOriginal(dependent_index,true));
        }else{ //dependent simplices are death simplices
            dependent_pairs.push_back(getPersistencePairForSimplexOriginal(dependent_index,false));
        }
    }
    
    return dependent_pairs;
}

//get dependent (co)boundary pairs
std::vector<PersistencePair> Persistence::getDependentPairsBoundary(int simplex_index, bool transposed)
{
    //get dependent simplices from stored relations
    std::vector<int> dependent_simplices;
    if(!transposed){
        dependent_simplices = getDependencesBoundary(simplex_index);
    }else{
        dependent_simplices = getDependencesBoundary_T(simplex_index);
    }
    //get persistence pairs of dependent simplices
    std::vector<PersistencePair> dependent_pairs;
    for(int k=0; k<dependent_simplices.size(); k++){
        dependent_pairs.push_back(getPersistencePairForSimplexOriginal(dependent_simplices[k],isPositiveSimplexOriginal(dependent_simplices[k])));
    }
    
    return dependent_pairs;
}

//get all dependences to boundary simplices (faces) of a given simplex
std::vector<int> Persistence::getDependencesBoundaryRecursively(int simplex_index)
{
    //recursively get dependences
    std::vector<int> dependences = getDependencesBoundary(simplex_index);
    for(int i=0; i<dependences.size(); i++){
        std::vector<int> more_dependences = getDependencesBoundary(dependences[i]);
        dependences.insert(dependences.end(),more_dependences.begin(),more_dependences.end());
    }
    //make vector elements unique and sort
    std::sort(dependences.begin(),dependences.end());
    dependences.erase(std::unique(dependences.begin(),dependences.end()),dependences.end());
    return dependences;
}

//get all dependent persistence pairs relevant for the specified operation (respecting current status), either only first-level dependences or recursively
std::vector<PersistencePair> Persistence::getDependentPairsOperation(int simplex_index, bool lock, bool un, bool recursively, bool original)
{
    std::vector<PersistencePair> dependent_pairs;
    std::vector<PersistencePair> more_pairs;
    std::vector<int> dependent_simplices;
    int status;
    if(original){
        status = status_of_corresponding_persistence_pair_original[simplex_index];
    }else{
        status = status_of_corresponding_persistence_pair_current[simplex_index];
    }
    if((lock && !un && status == 2) || (!lock && !un && status != 0) || (lock && un && status != 2) || (!lock && un && status == 0)){
        //(co)boundary simplices
        if(!un){
            //lock, fill
            dependent_simplices = getDependencesBoundary(simplex_index);
        }else{
            //unlock, unfill
            dependent_simplices = getDependencesBoundary_T(simplex_index);
        }
        for(int i=0; i<dependent_simplices.size(); i++){
            dependent_pairs.push_back(getPersistencePairForSimplexOriginal(dependent_simplices[i],isPositiveSimplexOriginal(dependent_simplices[i])));
            if(recursively){
                more_pairs = getDependentPairsOperation(dependent_simplices[i],isPositiveSimplexOriginal(dependent_simplices[i]),un,recursively,original);
                dependent_pairs.insert(dependent_pairs.end(),more_pairs.begin(),more_pairs.end());
            }
        }
        //other dependences
        if(lock && !un){
            //lock
            //DB_T-dependences
            dependent_simplices = getDependencesDB_T(simplex_index);
            for(int i=0; i<dependent_simplices.size(); i++){
                dependent_pairs.push_back(getPersistencePairForSimplexOriginal(dependent_simplices[i],isPositiveSimplexOriginal(dependent_simplices[i])));
                if(recursively){
                    more_pairs = getDependentPairsOperation(dependent_simplices[i],false,un,recursively,original); //fill
                    dependent_pairs.insert(dependent_pairs.end(),more_pairs.begin(),more_pairs.end());
                }
            }
            //BB_T-dependences
            dependent_simplices = getDependencesBB_T(simplex_index);
            for(int i=0; i<dependent_simplices.size(); i++){
                dependent_pairs.push_back(getPersistencePairForSimplexOriginal(dependent_simplices[i],isPositiveSimplexOriginal(dependent_simplices[i])));
                if(recursively){
                    more_pairs = getDependentPairsOperation(dependent_simplices[i],true,un,recursively,original); //lock
                    dependent_pairs.insert(dependent_pairs.end(),more_pairs.begin(),more_pairs.end());
                }
            }
        }else if(!lock && !un){ 
            //fill
            //DD_T-dependences
            dependent_simplices = getDependencesDD_T(simplex_index);
            for(int i=0; i<dependent_simplices.size(); i++){
                dependent_pairs.push_back(getPersistencePairForSimplexOriginal(dependent_simplices[i],isPositiveSimplexOriginal(dependent_simplices[i])));
                if(recursively){
                    more_pairs = getDependentPairsOperation(dependent_simplices[i],false,un,recursively,original); //fill
                    dependent_pairs.insert(dependent_pairs.end(),more_pairs.begin(),more_pairs.end());
                }
            }
            //same pair
            if(recursively && status == 2){
                PersistencePair persistence_pair = getPersistencePairForSimplexOriginal(simplex_index,isPositiveSimplexOriginal(simplex_index));
                more_pairs = getDependentPairsOperation(persistence_pair.getBirthIndexFiltration(),true,un,recursively,original); //lock
                dependent_pairs.insert(dependent_pairs.end(),more_pairs.begin(),more_pairs.end());
            }
        }else if(lock && un){
            //unlock
            //BB-dependences
            dependent_simplices = getDependencesBB(simplex_index);
            for(int i=0; i<dependent_simplices.size(); i++){
                dependent_pairs.push_back(getPersistencePairForSimplexOriginal(dependent_simplices[i],isPositiveSimplexOriginal(dependent_simplices[i])));
                if(recursively){
                    more_pairs = getDependentPairsOperation(dependent_simplices[i],true,un,recursively,original); //unlock
                    dependent_pairs.insert(dependent_pairs.end(),more_pairs.begin(),more_pairs.end());
                }
            }
            //same pair
            if(recursively && status == 0){
                PersistencePair persistence_pair = getPersistencePairForSimplexOriginal(simplex_index,isPositiveSimplexOriginal(simplex_index));
                more_pairs = getDependentPairsOperation(persistence_pair.getDeathIndexFiltration(),false,un,recursively,original); //unfill
                dependent_pairs.insert(dependent_pairs.end(),more_pairs.begin(),more_pairs.end());
            }
        }else if(!lock && un){
            //unfill
            //DB-dependences
            dependent_simplices = getDependencesDB(simplex_index);
            for(int i=0; i<dependent_simplices.size(); i++){
                dependent_pairs.push_back(getPersistencePairForSimplexOriginal(dependent_simplices[i],isPositiveSimplexOriginal(dependent_simplices[i])));
                if(recursively){
                    more_pairs = getDependentPairsOperation(dependent_simplices[i],true,un,recursively,original); //unlock
                    dependent_pairs.insert(dependent_pairs.end(),more_pairs.begin(),more_pairs.end());
                }
            }
            //DD-dependences
            dependent_simplices = getDependencesDD(simplex_index);
            for(int i=0; i<dependent_simplices.size(); i++){
                dependent_pairs.push_back(getPersistencePairForSimplexOriginal(dependent_simplices[i],isPositiveSimplexOriginal(dependent_simplices[i])));
                if(recursively){
                    more_pairs = getDependentPairsOperation(dependent_simplices[i],false,un,recursively,original); //unfill
                    dependent_pairs.insert(dependent_pairs.end(),more_pairs.begin(),more_pairs.end());
                }
            }
        }
    }
    std::sort(dependent_pairs.begin(),dependent_pairs.end());
    dependent_pairs.erase(std::unique(dependent_pairs.begin(),dependent_pairs.end()),dependent_pairs.end());
    
    return dependent_pairs;
}

//reset status (0 past, 1 presence, 2 future) of persistence pairs, undo hole operations
void Persistence::resetPersistencePairsStatus(Filtration* filtration, exact current_alpha2, bool recompute_original)
{ 
    if(status_of_corresponding_persistence_pair_original.size()==0 || recompute_original){
        //determine status of all persistence pairs, store in vector for each simplex
        status_of_corresponding_persistence_pair_original = std::vector<int>(R_original.getSize(),0);
        for(std::vector<PersistencePair>::iterator it = persistence_pairs_original.begin(); it!=persistence_pairs_original.end(); ++it){
            PersistencePair pair = (*it);
            exact birth2 = pair.getBirth2(filtration);
            exact death2 = pair.getDeath2(filtration);
            if(birth2<=current_alpha2 && current_alpha2<death2){ //presence
                status_of_corresponding_persistence_pair_original[pair.getBirthIndexFiltration()]=1;
                if(pair.getDeathIndexFiltration()>=0){
                    status_of_corresponding_persistence_pair_original[pair.getDeathIndexFiltration()]=1;
                }
            }else if(current_alpha2<birth2){ //future
                status_of_corresponding_persistence_pair_original[pair.getBirthIndexFiltration()]=2;
                if(pair.getDeathIndexFiltration()>=0){
                    status_of_corresponding_persistence_pair_original[pair.getDeathIndexFiltration()]=2;
                }
            }
        }
    }
    status_of_corresponding_persistence_pair_current = status_of_corresponding_persistence_pair_original;
    
    //clear other information about hole operations
    manipulated_persistence_points.clear();
}

void Persistence::computeManipulatedPersistencePointsFromStatus(Filtration* filtration, exact current_alpha2, exact current_exclusion_alpha2)
{
    //check if status of persistence pairs has changed due to hole operation
    //compute status that they should have in this subcomplex, compare to stored status
    
    manipulated_persistence_points.clear();        
    for(std::vector<PersistencePair>::iterator it = persistence_pairs_original.begin(); it!=persistence_pairs_original.end(); ++it){
        PersistencePair pair = (*it);
        exact birth2 = pair.getBirth2(filtration);
        exact death2 = pair.getDeath2(filtration);
        if(current_alpha2>=death2){ //past ... 0
            if(status_of_corresponding_persistence_pair_current[pair.getBirthIndexFiltration()]==1){
                manipulated_persistence_points.push_back(std::make_pair(pair,std::make_pair(birth2,current_exclusion_alpha2)));
            }else if(status_of_corresponding_persistence_pair_current[pair.getBirthIndexFiltration()]==2){
                if(death2-birth2>0){ //check whether had 0 persistence already before
                    manipulated_persistence_points.push_back(std::make_pair(pair,std::make_pair(-1,-1)));
                }
            }
        }else if(birth2<=current_alpha2 && current_alpha2<death2){ //presence ... 1
            if(status_of_corresponding_persistence_pair_current[pair.getBirthIndexFiltration()]==0){
                manipulated_persistence_points.push_back(std::make_pair(pair,std::make_pair(birth2,current_alpha2)));
            }else if(status_of_corresponding_persistence_pair_current[pair.getBirthIndexFiltration()]==2){
                if(death2-birth2>0){ //check whether had 0 persistence already before
                    manipulated_persistence_points.push_back(std::make_pair(pair,std::make_pair(current_exclusion_alpha2,death2)));
                }
            }
        }else if(current_alpha2<birth2){ //future .. 2
            if(status_of_corresponding_persistence_pair_current[pair.getBirthIndexFiltration()]==0){
                if(death2-birth2>0){ //check whether had 0 persistence already before
                    manipulated_persistence_points.push_back(std::make_pair(pair,std::make_pair(current_alpha2,current_alpha2)));
                }
            }else if(status_of_corresponding_persistence_pair_current[pair.getBirthIndexFiltration()]==1){
                manipulated_persistence_points.push_back(std::make_pair(pair,std::make_pair(current_alpha2,death2)));
            }
        }
    }
    std::cout << "# connecting arrows " << manipulated_persistence_points.size() << std::endl;
}

//compute betti numbers for current filtration from persistence matrix R
std::vector<std::vector<int> > Persistence::computeBettiFiltration()
{
    std::vector<int> betti0_filtration(filtration_size,0);
    std::vector<int> betti1_filtration(filtration_size,0);
    std::vector<int> betti2_filtration(filtration_size,0);
    int betti0=0; int betti1=0; int betti2=0; //current values
    for(int i=0; i<filtration_size; i++){
        if(R_current.isZeroList(i)){
            //positive
            if(dimensions_current[i]==0){
                betti0++;
            }else if(dimensions_current[i]==1){
                betti1++;
            }else if(dimensions_current[i]==2){
                betti2++;
            }
        }else{
            //negative
            if(dimensions_current[i]==1){
                betti0--;
            }else if(dimensions_current[i]==2){
                betti1--;
            }else if(dimensions_current[i]==3){
                betti2--;
            }
        }
        betti0_filtration[i]=betti0;
        betti1_filtration[i]=betti1;
        betti2_filtration[i]=betti2;
    }
    std::vector<std::vector<int> > output;
    output.push_back(betti0_filtration);
    output.push_back(betti1_filtration);
    output.push_back(betti2_filtration);
    return output;
}

//print all matrices
void Persistence::printRUQV(std::ostream& output)
{ 
    output << "R" << std::endl;
    R_original.printPosNeg(true,&positive_original,output);
    output << "U" << std::endl;
    U_original.printPosNeg(true,&positive_original,output);
    output << "Q" << std::endl;
    Q.printPosNeg(false,&positive_original,output);
    output << "V" << std::endl;
    V.printPosNeg(false,&positive_original,output);
}

//print persistence pairs
void Persistence::printPersistencePairs(std::ostream& output, Filtration* filtration_current)
{
    if(!weighted){
        output << "PERSISTENCE PAIRS: dim, birth index, death index, birth_value, death_value, persistence" << std::endl;
    }else{
        output << "PERSISTENCE PAIRS: dim, birth index, death index, birth_value2, death_value2, persistence" << std::endl;
    }
    for(int i=0; i<persistence_pairs_current.size();i++){
        PersistencePair pair = persistence_pairs_current[i];
        output << pair.getDim() << ", " << pair.getBirthIndexFiltration() << ", " << pair.getDeathIndexFiltration() << ", "; 
        if(!weighted){
            output << pair.getBirth_double(filtration_current) << ", " << pair.getDeath_double(filtration_current) << ", " << pair.getPersistence_double(filtration_current) << std::endl;
        }else{
            output << pair.getBirth2_double(filtration_current) << ", " << pair.getDeath2_double(filtration_current) << ", " << pair.getPersistence2_double(filtration_current) << std::endl;
        }
                
    }
}

//print densities for all matrices
void Persistence::computeResultsPaperHolesDensities(std::string prefix, std::ostream& output)
{
    int sum_column_additions = 0;
    output << prefix << "num_additions_per_column";
    for(int i=0; i<R_original.getSize(); i++){
        output << ", " << count_column_additions[i];
        sum_column_additions += count_column_additions[i];
    }
    output << std::endl;
    int sum_row_additions = 0;
    output << prefix << "num_additions_per_row";
    for(int i=0; i<R_original.getSize(); i++){
        output << ", " << count_row_additions[i];
        sum_row_additions += count_row_additions[i];
    }
    output << std::endl;
    output << prefix << "density_R, " << R_original.getNumEntries()*1./(R_original.getSize()*R_original.getSize()*1.) << std::endl;
    output << prefix << "density_U, " << U_original.getNumEntries()*1./(U_original.getSize()*U_original.getSize()*1.) << std::endl;
    output << prefix << "density_C, " << sum_column_additions*1./(R_original.getSize()*R_original.getSize()*1.) << std::endl;
    output << prefix << "density_Q, " << Q.getNumEntries()*1./(Q.getSize()*Q.getSize()*1.) << std::endl;
    output << prefix << "density_V, " << V.getNumEntries()*1./(V.getSize()*V.getSize()*1.) << std::endl;
    output << prefix << "density_D, " << sum_row_additions*1./(R_original.getSize()*R_original.getSize()*1.) << std::endl;
}

//print average and maximum size of canonical (co)chain/cycle per dimension
void Persistence::computeResultsPaperHolesCanonicalChainsSize(std::ostream& output)
{
    //dim always means dimension of cycle, thus death simplices are one dimension higher
    for(int dim=-2; dim<3; dim++){
        std::string prefix;
        if(dim>=-1){
            prefix = "dim"+std::to_string(dim)+"_";
        }else{
            prefix = "dimall_";
        }
        int sum_canonical_cycles_size = 0;
        int sum_canonical_chains_size = 0;
        int sum_canonical_cocycles_size = 0; //negative rows V
        int sum_canonical_cochains_size = 0; //positive rows V
        int sum_positive_rows_U_size = 0;
        int sum_negative_rows_U_size = 0;
        int num_positive_simplices = 0;
        int num_negative_simplices = 0;

        for(int i=0; i<R_current.getSize(); i++){
            if(dim<-1 || (dimensions_original[i]==dim && positive_original[i]) || (dimensions_original[i]==dim+1 && !positive_original[i])){
                if(positive_original[i]){
                    //birth simplex
                    num_positive_simplices++;
                    sum_canonical_cycles_size += U_original.getColumn(i).size();
                    sum_canonical_cochains_size += V.getRow(i).size();
                    sum_positive_rows_U_size += U_original.getRow(i).size();
                }else{
                    //death simplex
                    num_negative_simplices++;
                    sum_canonical_chains_size += U_original.getColumn(i).size();
                    sum_canonical_cocycles_size += V.getRow(i).size();
                    sum_negative_rows_U_size += U_original.getRow(i).size();
                }
            }
        }

        output << prefix << "size_canonical_cycle_avg, " << (sum_canonical_cycles_size*1.)/(num_positive_simplices*1.) << std::endl;
        output << prefix << "size_canonical_chain_avg, " << (sum_canonical_chains_size*1.)/(num_negative_simplices*1.) << std::endl;
        output << prefix << "size_canonical_cocycle_avg, " << (sum_canonical_cocycles_size*1.)/(num_negative_simplices*1.) << std::endl;
        output << prefix << "size_canonical_cochain_avg, " << (sum_canonical_cochains_size*1.)/(num_positive_simplices*1.) << std::endl;
        output << prefix << "size_negative_row_U_avg, " << (sum_negative_rows_U_size*1.)/(num_negative_simplices*1.) << std::endl;
        output << prefix << "size_positive_row_U_avg, " << (sum_positive_rows_U_size*1.)/(num_positive_simplices*1.) << std::endl;

        if(dim>=0){
           PersistencePair max_pair = getPersistencePairOfDim(dim,1);
           int max_birth_simplex = max_pair.getBirthIndexFiltration();
           int max_death_simplex = max_pair.getDeathIndexFiltration();
           if(max_death_simplex>=0){
            output << prefix << "size_canonical_cycle_maxpers, " << U_original.getColumn(max_birth_simplex).size() << std::endl;
            output << prefix << "size_canonical_chain_maxpers, " << U_original.getColumn(max_death_simplex).size() << std::endl;
            output << prefix << "size_canonical_cocycle_maxpers, " << V.getRow(max_death_simplex).size() << std::endl;
            output << prefix << "size_canonical_cochain_maxpers, " << V.getRow(max_birth_simplex).size() << std::endl;
           }
        }
    }
}

//print sizes of dependence structure
void Persistence::computeResultsPaperHolesDependencesSize(std::ostream& output)
{
    output << ">>Direct_Dependences<<" << std::endl;
    for(int dim=-2; dim<=3; dim++){
        std::string prefix;
        if(dim>=-1){
            prefix = "dim"+std::to_string(dim)+"_";
        }else{
            prefix = "dimall_";
        }
        if(hole_operations_cohomology){
            prefix = "co_"+prefix;
        }
        int sum_boundary_dependences_birth = 0;
        int sum_boundary_dependences_death = 0;
        int sum_BD_dependences = 0;
        int sum_DB_dependences = 0;
        int sum_BB_dependences = 0;
        int sum_DD_dependences = 0;
        int sum_boundary_T_dependences_birth = 0;
        int sum_boundary_T_dependences_death = 0;
        int sum_BD_T_dependences = 0;
        int sum_DB_T_dependences = 0;
        int sum_BB_T_dependences = 0;
        int sum_DD_T_dependences = 0;
        
        int num_positive_simplices = 0;
        int num_negative_simplices = 0;
        
        for(int i=0; i<R_current.getSize(); i++){
            if(dim<-1 || dimensions_original[i]==dim){
                if(positive_original[i]){
                    num_positive_simplices++;
                    sum_boundary_dependences_birth += getDependencesBoundary(i).size();
                    sum_BD_dependences += dependencesBD[i].size();
                    sum_BB_dependences += getDependencesBB(i).size();
                    sum_boundary_T_dependences_birth += getDependencesBoundary_T(i).size();
                    sum_DB_T_dependences += getDependencesDB_T(i).size();
                    sum_BB_T_dependences += getDependencesBB_T(i).size();
                    
                }else{
                    num_negative_simplices++;
                    sum_boundary_dependences_death += getDependencesBoundary(i).size();
                    sum_DB_dependences += getDependencesDB(i).size();
                    sum_DD_dependences += getDependencesDD(i).size();
                    sum_boundary_T_dependences_death += getDependencesBoundary_T(i).size();
                    sum_BD_T_dependences += dependencesBD_T[i].size();
                    sum_DD_T_dependences += getDependencesDD_T(i).size();
                }
            }
        }
        
        output << prefix << "num_birth_simplices, " << num_positive_simplices << std::endl;
        output << prefix << "num_death_simplices, " << num_negative_simplices << std::endl;
        output << prefix << "num_coboundary_dependences_birth_total, " << sum_boundary_T_dependences_birth << std::endl;
        output << prefix << "num_coboundary_dependences_death_total, " << sum_boundary_T_dependences_death << std::endl;
        output << prefix << "num_BD_dependences_total, " << sum_BD_dependences << std::endl;
        output << prefix << "num_DB_dependences_total, " << sum_DB_dependences << std::endl;
        output << prefix << "num_BB_dependences_total, " << sum_BB_dependences << std::endl;
        output << prefix << "num_DD_dependences_total, " << sum_DD_dependences << std::endl;
        output << prefix << "num_all_dependences_total, " << sum_boundary_T_dependences_birth+sum_boundary_T_dependences_death+sum_BD_dependences+sum_DB_dependences+sum_BB_dependences+sum_DD_dependences << std::endl;
        output << prefix << "num_boundary_dependences_birth_total, " << sum_boundary_dependences_birth << std::endl;
        output << prefix << "num_boundary_dependences_death_total, " << sum_boundary_dependences_death << std::endl;
        output << prefix << "num_BD_T_dependences_total, " << sum_BD_T_dependences << std::endl;
        output << prefix << "num_DB_T_dependences_total, " << sum_DB_T_dependences << std::endl;
        output << prefix << "num_BB_T_dependences_total, " << sum_BB_T_dependences << std::endl;
        output << prefix << "num_DD_T_dependences_total, " << sum_DD_T_dependences << std::endl;
        output << prefix << "num_all_T_dependences_total, " << sum_boundary_dependences_birth+sum_boundary_dependences_death+sum_BD_T_dependences+sum_DB_T_dependences+sum_BB_T_dependences+sum_DD_T_dependences << std::endl;
        output << prefix << "num_coboundary_dependences_birth_avg, " << (sum_boundary_T_dependences_birth*1.)/((num_positive_simplices)*1.)<< std::endl;
        output << prefix << "num_coboundary_dependences_death_avg, " << (sum_boundary_T_dependences_death*1.)/((num_negative_simplices)*1.)<< std::endl;
        output << prefix << "num_BD_dependences_avg, " << (sum_BD_dependences*1.)/(num_positive_simplices*1.) << std::endl;
        output << prefix << "num_DB_dependences_avg, " << (sum_DB_dependences*1.)/(num_negative_simplices*1.) << std::endl;
        output << prefix << "num_BB_dependences_avg, " << (sum_BB_dependences*1.)/(num_positive_simplices*1.) << std::endl;
        output << prefix << "num_DD_dependences_avg, " << (sum_DD_dependences*1.)/(num_negative_simplices*1.) << std::endl;
        output << prefix << "num_all_dependences_avg, " << ((sum_boundary_T_dependences_birth+sum_boundary_T_dependences_death+sum_BD_dependences+sum_DB_dependences+sum_BB_dependences+sum_DD_dependences)*1.)/((num_positive_simplices+num_negative_simplices)*1.) << std::endl;
        output << prefix << "num_boundary_dependences_birth_avg, " << (sum_boundary_dependences_birth*1.)/((num_positive_simplices)*1.) << std::endl;
        output << prefix << "num_boundary_dependences_death_avg, " << (sum_boundary_dependences_death*1.)/((num_negative_simplices)*1.) << std::endl;
        output << prefix << "num_BD_T_dependences_avg, " << (sum_BD_T_dependences*1.)/(num_negative_simplices*1.) << std::endl;
        output << prefix << "num_DB_T_dependences_avg, " << (sum_DB_T_dependences*1.)/(num_positive_simplices*1.) << std::endl;
        output << prefix << "num_BB_T_dependences_avg, " << (sum_BB_T_dependences*1.)/(num_positive_simplices*1.) << std::endl;
        output << prefix << "num_DD_T_dependences_avg, " << (sum_DD_T_dependences*1.)/(num_negative_simplices*1.) << std::endl;
        output << prefix << "num_all_T_dependences_avg, " << ((sum_boundary_dependences_birth+sum_boundary_dependences_death+sum_BD_T_dependences+sum_DB_T_dependences+sum_BB_T_dependences+sum_DD_T_dependences)*1.)/((num_positive_simplices+num_negative_simplices)*1.) << std::endl;
    }
    output << ">>All_Dependences<<" << std::endl;
    std::vector<std::vector<int> > nums_all_dependences_all_operations = getNumsAllDependencesAllOperations();
    for(int dim=-2; dim<3; dim++){
        std::string prefix;
        if(dim>=-1){
            prefix = "dim"+std::to_string(dim)+"_";
        }else{
            prefix = "dimall_";
        }
        if(hole_operations_cohomology){
            prefix = "co_"+prefix;
        }
        
        int num_positive_simplices = 0;
        int num_negative_simplices = 0;
        int sum_lock_dependences = 0;
        int sum_fill_dependences = 0;
        int sum_unlock_dependences = 0;
        int sum_unfill_dependences = 0;
        
        for(int i=0; i<R_current.getSize(); i++){
            if(dim<-1 || (dimensions_original[i]==dim && positive_original[i]) || (dimensions_original[i]==dim+1 && !positive_original[i])){
                if(positive_original[i]){
                    num_positive_simplices++;
                    sum_lock_dependences += nums_all_dependences_all_operations[0][i];
                    sum_unlock_dependences += nums_all_dependences_all_operations[2][i];
                }else{
                    num_negative_simplices++;
                    sum_fill_dependences += nums_all_dependences_all_operations[1][i];
                    sum_unfill_dependences += nums_all_dependences_all_operations[3][i];
                }
            }
        }
        
        output << prefix << "num_lock_dependences_avg, " << (sum_lock_dependences*1.)/(num_positive_simplices) << std::endl;
        output << prefix << "num_fill_dependences_avg, " << (sum_fill_dependences*1.)/(num_negative_simplices) << std::endl;
        output << prefix << "num_unlock_dependences_avg, " << (sum_unlock_dependences*1.)/(num_positive_simplices) << std::endl;
        output << prefix << "num_unfill_dependences_avg, " << (sum_unfill_dependences*1.)/(num_negative_simplices) << std::endl;
        
        if(dim>=0){
            PersistencePair max_pair = getPersistencePairOfDim(dim,1);
            int max_birth_simplex = max_pair.getBirthIndexFiltration();
            int max_death_simplex = max_pair.getDeathIndexFiltration();
            if(max_death_simplex>=0){
                output << prefix << "num_lock_dependences_maxpair, " << nums_all_dependences_all_operations[0][max_birth_simplex] << std::endl;
                output << prefix << "num_fill_dependences_maxpair, " << nums_all_dependences_all_operations[1][max_death_simplex] << std::endl;
                output << prefix << "num_unlock_dependences_maxpair, " << nums_all_dependences_all_operations[2][max_birth_simplex] << std::endl;
                output << prefix << "num_unfill_dependences_maxpair, " << nums_all_dependences_all_operations[3][max_death_simplex] << std::endl;
            }
        }
    }
}

//for all hole operations, for all simplices get number of relevant dependent simplices (disregarding status)
 std::vector<std::vector<int> > Persistence::getNumsAllDependencesAllOperations()
 {
    //temporarily store all dependences for all simplices, will be iteratively computed
    std::vector<std::vector<int> > all_dependences_for_lock(R_current.getSize(),std::vector<int>(0));
    std::vector<std::vector<int> > all_dependences_for_fill(R_current.getSize(),std::vector<int>(0));
    std::vector<std::vector<int> > all_dependences_for_unlock(R_current.getSize(),std::vector<int>(0));
    std::vector<std::vector<int> > all_dependences_for_unfill(R_current.getSize(),std::vector<int>(0));
    
    //iterate through all simplices
    //for lock/fill all dependent simplices have lower index -> go forward
    for(int i=0; i<R_current.getSize(); i++){
        int simplex_index = i;
        std::vector<int> all_dependences;
        std::vector<int> dependent_simplices;
        std::vector<int> recursively_dependent_simplices;
        //boundary dependences
        dependent_simplices = getDependencesBoundary(simplex_index);
        all_dependences.insert(all_dependences.end(),dependent_simplices.begin(),dependent_simplices.end());
        for(int j=0; j<dependent_simplices.size(); j++){
            if(isPositiveSimplexOriginal(dependent_simplices[j])){
                recursively_dependent_simplices = all_dependences_for_lock[dependent_simplices[j]]; //recursive lock
            }else{
                recursively_dependent_simplices = all_dependences_for_fill[dependent_simplices[j]]; //recursive fill
            }
            all_dependences.insert(all_dependences.end(),recursively_dependent_simplices.begin(),recursively_dependent_simplices.end());
        }
        if(isPositiveSimplexOriginal(simplex_index)){
            //lock
            //DB^T-dependences
            dependent_simplices = getDependencesDB_T(simplex_index);
            all_dependences.insert(all_dependences.end(),dependent_simplices.begin(),dependent_simplices.end());
            for(int j=0; j<dependent_simplices.size(); j++){
                recursively_dependent_simplices = all_dependences_for_fill[dependent_simplices[j]]; //recursive fill
                all_dependences.insert(all_dependences.end(),recursively_dependent_simplices.begin(),recursively_dependent_simplices.end());
            }
            //BB^T-dependences
            dependent_simplices = getDependencesBB_T(simplex_index);
            all_dependences.insert(all_dependences.end(),dependent_simplices.begin(),dependent_simplices.end());
            for(int j=0; j<dependent_simplices.size(); j++){
                recursively_dependent_simplices = all_dependences_for_lock[dependent_simplices[j]]; //recursive lock
                all_dependences.insert(all_dependences.end(),recursively_dependent_simplices.begin(),recursively_dependent_simplices.end());
            }
            //make elements unique
            std::sort(all_dependences.begin(),all_dependences.end());
            all_dependences.erase(std::unique(all_dependences.begin(),all_dependences.end()),all_dependences.end());
            
            all_dependences_for_lock[simplex_index]=all_dependences;
        }else{
            //fill
            //DD^T-dependences
            dependent_simplices = getDependencesDD_T(simplex_index);
            all_dependences.insert(all_dependences.end(),dependent_simplices.begin(),dependent_simplices.end());
            for(int j=0; j<dependent_simplices.size(); j++){
                recursively_dependent_simplices = all_dependences_for_fill[dependent_simplices[j]]; //recursive fill
                all_dependences.insert(all_dependences.end(),recursively_dependent_simplices.begin(),recursively_dependent_simplices.end());
            }
            //birth simplex
            PersistencePair pair = getPersistencePairForSimplexOriginal(simplex_index,false);
            recursively_dependent_simplices = all_dependences_for_lock[pair.getBirthIndexFiltration()]; //recursive lock
            all_dependences.insert(all_dependences.end(),recursively_dependent_simplices.begin(),recursively_dependent_simplices.end());  
            //make elements unique
            std::sort(all_dependences.begin(),all_dependences.end());
            all_dependences.erase(std::unique(all_dependences.begin(),all_dependences.end()),all_dependences.end());
            
            all_dependences_for_fill[simplex_index]=all_dependences;
        }
        
    }
    
    //for unlock/unfill all dependent simplices have higher index -> go backward
    for(int i=R_current.getSize()-1; i>=0; i--){
        int simplex_index = i;
        std::vector<int> all_dependences;
        std::vector<int> dependent_simplices;
        std::vector<int> recursively_dependent_simplices;
        //coboundary dependences
        dependent_simplices = getDependencesBoundary_T(simplex_index);
        all_dependences.insert(all_dependences.end(),dependent_simplices.begin(),dependent_simplices.end());
        for(int j=0; j<dependent_simplices.size(); j++){
            if(isPositiveSimplexOriginal(dependent_simplices[j])){
                recursively_dependent_simplices = all_dependences_for_unlock[dependent_simplices[j]]; //recursive unlock
            }else{
                recursively_dependent_simplices = all_dependences_for_unfill[dependent_simplices[j]]; //recursive unfill
            }
            all_dependences.insert(all_dependences.end(),recursively_dependent_simplices.begin(),recursively_dependent_simplices.end());
        }
        
        if(isPositiveSimplexOriginal(simplex_index)){
            //unlock
            //BB-dependences
            dependent_simplices = getDependencesBB(simplex_index);
            all_dependences.insert(all_dependences.end(),dependent_simplices.begin(),dependent_simplices.end());
            for(int j=0; j<dependent_simplices.size(); j++){
                recursively_dependent_simplices = all_dependences_for_unlock[dependent_simplices[j]]; //recursive unlock
                all_dependences.insert(all_dependences.end(),recursively_dependent_simplices.begin(),recursively_dependent_simplices.end());
            }
            //death simplex
            PersistencePair pair = getPersistencePairForSimplexOriginal(simplex_index,true);
            recursively_dependent_simplices = all_dependences_for_unfill[pair.getDeathIndexFiltration()]; //recursive unfill
            all_dependences.insert(all_dependences.end(),recursively_dependent_simplices.begin(),recursively_dependent_simplices.end()); 
            //make elements unique
            std::sort(all_dependences.begin(),all_dependences.end());
            all_dependences.erase(std::unique(all_dependences.begin(),all_dependences.end()),all_dependences.end());
            
            all_dependences_for_unlock[simplex_index]=all_dependences;
            
        }else{
            //unfill
            //DB-dependences
            dependent_simplices = getDependencesDB(simplex_index);
            all_dependences.insert(all_dependences.end(),dependent_simplices.begin(),dependent_simplices.end());
            for(int j=0; j<dependent_simplices.size(); j++){
                recursively_dependent_simplices = all_dependences_for_unlock[dependent_simplices[j]]; //recursive unlock
                all_dependences.insert(all_dependences.end(),recursively_dependent_simplices.begin(),recursively_dependent_simplices.end());
            }
            //DD-dependences
            dependent_simplices = getDependencesDD(simplex_index);
            all_dependences.insert(all_dependences.end(),dependent_simplices.begin(),dependent_simplices.end());
            for(int j=0; j<dependent_simplices.size(); j++){
                recursively_dependent_simplices = all_dependences_for_unfill[dependent_simplices[j]]; //recursive unfill
                all_dependences.insert(all_dependences.end(),recursively_dependent_simplices.begin(),recursively_dependent_simplices.end());
            }        
            //make elements unique
            std::sort(all_dependences.begin(),all_dependences.end());
            all_dependences.erase(std::unique(all_dependences.begin(),all_dependences.end()),all_dependences.end());
            
            all_dependences_for_unfill[simplex_index]=all_dependences;
        }
    }
    
    //just return vectors of sizes
    std::vector<int> num_all_dependences_for_lock(R_current.getSize(),0);
    std::vector<int> num_all_dependences_for_fill(R_current.getSize(),0);
    std::vector<int> num_all_dependences_for_unlock(R_current.getSize(),0);
    std::vector<int> num_all_dependences_for_unfill(R_current.getSize(),0);
    for(int i=0; i<R_current.getSize();i++){
        num_all_dependences_for_lock[i]=all_dependences_for_lock[i].size();
        num_all_dependences_for_fill[i]=all_dependences_for_fill[i].size();
        num_all_dependences_for_unlock[i]=all_dependences_for_unlock[i].size();
        num_all_dependences_for_unfill[i]=all_dependences_for_unfill[i].size();
    }
    std::vector<std::vector<int> > num_all_dependences_all_operations;
    num_all_dependences_all_operations.push_back(num_all_dependences_for_lock);
    num_all_dependences_all_operations.push_back(num_all_dependences_for_fill);
    num_all_dependences_all_operations.push_back(num_all_dependences_for_unlock);
    num_all_dependences_all_operations.push_back(num_all_dependences_for_unfill);
    
    return num_all_dependences_all_operations;
 }



