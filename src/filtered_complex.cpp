//filtered_complex.cpp
//author: koelsboe

#include "basics.h"
#include "filtered_complex.h"
#include "alpha_complex.h"

//clear complex
void Filtered_Complex::clear0()
{
    num_data_points=0;
    in_complex.clear();
    filtration_values.clear();
    
    excluded.clear();
    included.clear();
    inclusion_values_and_counter.clear();
    inclusion_counter = 0;
    
    filtration_current.clear();
    filtration_original.clear();
    filtration_index_greater_than_alpha2=0;
      
    persistence.clear(true);
    persistence_computed=false;
    
    holes_modified=false;
    current_exclusion_alpha2=-1;
    last_hole_operation_simplex=-1;
    
    filtration_statistics_computed=false;
    num_simplices_filtration.clear();
    betti_filtration.clear();
}

//check if simplex of given index is in complex
bool Filtered_Complex::isInComplex(int index)
{
    Simplex* simplex = &(simplices_ptr->at(index));
    //check if deleted (by point manipulation)
    if(simplex->isDeleted()){
        return false;
    }
    //check if excluded (by hole manipulation)
    if(excluded[index]){
        return false;
    }
    //check if included (by hole manipulation)
    if(included[index]){
        return true;
    }
    //otherwise stored in array for original filtration
    return in_complex[index];
}

//return filtration value of simplex of given index (respecting inclusions, exclusions)
exact Filtered_Complex::getAlpha2Simplex(int index)
{
    if((simplices_ptr->at(index).isDeleted()) || excluded[index]){
        return -1;
    }
    exact alpha2;
    if(included[index]){
        //values for inclusion stored
        alpha2 = getIncludedWithAlpha2(index);
    }else{
        //original filtration values stored
        alpha2 = filtration_values[index];
    }
    return alpha2;
}

//set new alpha value, determines current subcomplex
void Filtered_Complex::setAlpha2(exact alpha2)
{
    current_alpha2 = alpha2;
    
    //determine alpha2 just outside current subcomplex of original filtration, will be used for hole manipulations on subcomplex
    int next_filtration_index = filtration_original.getIndexGreaterThanAlpha2(alpha2,simplices_ptr,critical_ptr,&included,&inclusion_values_and_counter);
    if(next_filtration_index<filtration_original.size()){
        exact next_alpha2 = filtration_original.getElement(next_filtration_index).first;
        current_exclusion_alpha2 = (alpha2+next_alpha2)/2.;
    }else{
        current_exclusion_alpha2 = alpha2*1.05;
    }
  
    //need to determine which part of filtration is smaller equal to alpha
    if(filtration_current.getIndexGreaterThanAlpha2(alpha2,simplices_ptr,critical_ptr,&included,&inclusion_values_and_counter) != filtration_index_greater_than_alpha2){
        //subcomplex has changed
        filtration_index_greater_than_alpha2 = filtration_current.getIndexGreaterThanAlpha2(alpha2,simplices_ptr,critical_ptr,&included,&inclusion_values_and_counter);
    } 
    
    //compute status of persistence pairs in current subcomplex
    resetPersistencePairsStatus(true); 
}

//scale transformation values by given factor
void Filtered_Complex::scaleFiltrationValues(exact factor)
{
    for(int i=0; i<filtration_values.size(); i++){
        filtration_values[i]=filtration_values[i]*factor;
    }
    for(int i=1; i<filtration_current.size(); i++){
        filtration_current.scaleValue(i,2);
    }
    for(int i=1; i<filtration_original.size(); i++){
        filtration_original.scaleValue(i,2);
    }
}

void Filtered_Complex::computePersistence(bool first_computation, bool printstats)
{
    if(printstats){
        std::cout << "PERSISTENCE COMPUTATION FOR " << name_uppercase << " COMPLEX" << std::endl;
    }
    persistence.compute(exhaustive_persistence, first_computation, simplices_ptr, &filtration_current, weighted, printstats);
    
    persistence_computed = true;
}

//set inclusion alpha2 (earlier or later than original alpha2) for simplex, dependent simplices already handled
bool Filtered_Complex::setSimplexIncludedWithAlpha2(int simplex_index, exact inclusion_alpha2, bool earlier)
{
   if((earlier && filtration_values[simplex_index]>inclusion_alpha2 && (!included[simplex_index] || inclusion_values_and_counter[simplex_index].first>inclusion_alpha2))
            || (!earlier && filtration_values[simplex_index]<inclusion_alpha2 && (!included[simplex_index] || inclusion_values_and_counter[simplex_index].first<inclusion_alpha2))){
        included[simplex_index]=true;
        if(earlier){
            inclusion_values_and_counter[simplex_index]=std::make_pair(inclusion_alpha2,inclusion_counter);
        }else{ //when including simplices later we want filtration order to be reverse of inclusion order
            inclusion_values_and_counter[simplex_index]=std::make_pair(inclusion_alpha2,-inclusion_counter);
        }
        inclusion_counter++;
        return true;
    }
    return false;
}

//perform hole operation on given persistence pair or above/below given bound
//  - lock cycle (advance birth) of num-highest persistence or above/below given bound
//  - unlock cycle (delay birth) of num-highest persistence or above/below given bound
//  - fill hole (advance death) of num-highest persistence or above/below given bound
//  - unfill hole (delay death) of num-highest persistence or above/below given bound
void Filtered_Complex::holeOperation(bool lock, bool un, int dim, int pers_pair_num, bool subcomplex, double persistence_bound, bool lowerbound, bool printinfo, bool printstats, bool recompute_persistence, bool compute_statistics)
{
    //assumption: persistence pairs of original complex were computed
    if(!wasPersistenceComputed()){
        computePersistence(true,false);
    }
    
    std::string operation_name;
    std::string operation_name_big;
    if(lock && !un){
        operation_name = "lock"; operation_name_big = "LOCK";
    }else if(lock && un){
        operation_name = "unlock"; operation_name_big = "UNLOCK";
    }else if(!lock && !un){
        operation_name = "fill"; operation_name_big = "FILL";
    }else if(!lock && un){
        operation_name = "unfill"; operation_name_big = "UNFILL";
    }
    
    if(dim>=0 && dim<=2 && (dim3 || dim<2)){ //only for dimensions 0,1,(2)
        if(persistence_bound<0){
        //apply for persistence pair with num-highest persistence
            PersistencePair pers_pair;
            if(!subcomplex){
                pers_pair = persistence.getPersistencePairOfDim(dim,pers_pair_num); 
            }else{
                if(lock && !un){ //lock
                    //only take persistence pairs with future birth
                    pers_pair = persistence.getPersistencePairOfDimStatus(dim,pers_pair_num,current_alpha2,false,true,true,&filtration_original);
                }else if(!lock && !un){ //fill
                    //only take persistence pairs which are not dead yet
                    pers_pair = persistence.getPersistencePairOfDimStatus(dim,pers_pair_num,current_alpha2,false,false,true,&filtration_original);
                }else if(lock && un){ //unlock
                    //only take persistence pairs which are born already
                    pers_pair = persistence.getPersistencePairOfDimStatus(dim,pers_pair_num,current_alpha2,true,false,false,&filtration_original);
                }else if(!lock && un){ //unfill
                    //only take persistence pairs which are already dead
                    pers_pair = persistence.getPersistencePairOfDimStatus(dim,pers_pair_num,current_alpha2,true,true,false,&filtration_original);
                }
            }
            if(pers_pair.getDeathIndexFiltration()>=0){ //check if pair was found
                //get birth and death index (in filtration)
                int death_filtration_index = pers_pair.getDeathIndexFiltration();
                int birth_filtration_index = pers_pair.getBirthIndexFiltration();
                last_hole_operation_simplex = birth_filtration_index;
                if(!lock){
                    last_hole_operation_simplex = death_filtration_index;
                }
                if(printinfo){
                    std::cout << operation_name << " cycle/hole in " << name << "complex, with " << pers_pair_num << "-highest persistence" << std::endl;
                    if(!weighted){
                        std::cout << "pers pair (" << birth_filtration_index << " " << death_filtration_index << ") " << pers_pair.getPersistence_double(&filtration_original) << std::endl;
                    }else{
                        std::cout << "pers pair (" << birth_filtration_index << " " << death_filtration_index << ") " << pers_pair.getPersistence2_double(&filtration_original) << std::endl;
                    }
                }
                if(printstats){
                    std::cout << "HOLE OPERATION, " << operation_name_big << " CYCLE/HOLE IN " << name_uppercase << " COMPLEX";
                    if(subcomplex){
                        std::cout << " SUBCOMPLEX";
                    }
                    std::cout << std::endl;
                    std::cout << " dim, " << dim << std::endl;
                    if(!weighted){
                        std::cout << " birth, " << pers_pair.getBirth_double(&filtration_original) << std::endl;
                        std::cout << " death, " << pers_pair.getDeath_double(&filtration_original) << std::endl;
                    }else{
                        std::cout << " birth2, " << pers_pair.getBirth2_double(&filtration_original) << std::endl;
                        std::cout << " death2, " << pers_pair.getDeath2_double(&filtration_original) << std::endl;
                    }
                }
                double time1 = getCPUTime();
                
                //apply operation, either on subcomplex with recursive implementation or on full complex by adapting canonical (co)cycles and (co)chains
                if(lock && !un){
                    //lock cycle
                    if(subcomplex){
                        lockCycleRecursively(pers_pair,current_alpha2,printinfo,printstats);
                    }else{
                        lockOrFillCycleFullComplex(true,pers_pair,filtration_original.getMinValue(),printinfo,printstats);
                    }
                }else if(lock && un){
                    //unlock cycle
                    if(subcomplex){
                        unlockCycleRecursively(pers_pair,current_exclusion_alpha2,printinfo,printstats);
                    }else{
                        unlockOrUnfillCycleFullComplex(true,pers_pair,printinfo,printstats);
                    }
                }else if(!lock && !un){
                    //fill hole
                    if(subcomplex){
                        fillHoleRecursively(pers_pair,current_alpha2,printinfo,printstats);
                    }else{
                        exact birth_alpha2;
                        if(birth_filtration_index<1){
                            birth_alpha2 = filtration_original.getMinValue();
                        }else{
                            birth_alpha2 = filtration_original.getElement(birth_filtration_index).first;
                        }
                        lockOrFillCycleFullComplex(false,pers_pair,birth_alpha2,printinfo,printstats);
                    }
                }else if(!lock && un){
                    //unfill hole
                    if(subcomplex){
                        unfillHoleRecursively(pers_pair,current_exclusion_alpha2,printinfo,printstats);
                    }else{
                        unlockOrUnfillCycleFullComplex(false, pers_pair,printinfo,printstats);
                    }
                }
                
                if(printstats){
                    std::cout << " Time, " << getCPUTime()-time1 << std::endl;
                }
                
                //recompute filtration after inclusions and exclusions
                filtration_current.recomputeWithIncluded(&filtration_original,&filtration_values,simplices_ptr,critical_ptr,&excluded,&included,&inclusion_values_and_counter);
                
                //recompute persistence 
                if(recompute_persistence){
                    computePersistence(false,false);
                }
                //update statistics 
                if(compute_statistics){
                    computeFiltrationStatistics(false,true); 
                }
                
                //for full complex operations, change alpha to value that operation result is nicely visible
                if(!subcomplex){
                    if(lock && !un){ //lock
                        setAlpha2(CGAL::to_double(getMinAlpha2()));
                    }
                    if(!lock && !un){ //fill
                        //set alpha to birth value of hole (simplices inserted with this filtration value)
                        exact birth_alpha2=(filtration_original.getElement(birth_filtration_index)).first;
                        setAlpha2(CGAL::to_double(birth_alpha2));
                    }   
                }
                
                //subcomplex has changed
                filtration_index_greater_than_alpha2 = filtration_current.getIndexGreaterThanAlpha2(current_alpha2,simplices_ptr,critical_ptr,&included,&inclusion_values_and_counter);
                
                if(printinfo){
                    int count_simplices_included = 0;
                    int count_tetrahedra_included = 0;
                    int count_triangles_included = 0;
                    int count_edges_included = 0;
                    int count_points_included = 0;
                    int count_simplices_excluded = 0;
                    int count_tetrahedra_excluded = 0;
                    int count_triangles_excluded = 0;
                    int count_edges_excluded = 0;
                    int count_points_excluded = 0;
                    for(int i=0; i<filtration_original.size(); i++){
                        if(excluded[filtration_original.getElementIndex(i)]){
                            count_simplices_excluded++;
                            int dim_simplex = filtration_original.getElementDim(i,simplices_ptr);
                            if(dim_simplex==0){
                                count_points_excluded++;
                            }else if(dim_simplex==1){
                                count_edges_excluded++;
                            }else if(dim_simplex==2){
                                count_triangles_excluded++;
                            }else if(dim_simplex==3){
                                count_tetrahedra_excluded++;
                            }
                        }
                        if(included[filtration_original.getElementIndex(i)]){
                            count_simplices_included++;
                            int dim_simplex = filtration_original.getElementDim(i,simplices_ptr);
                            if(dim_simplex==0){
                                count_points_included++;
                            }else if(dim_simplex==1){
                                count_edges_included++;
                            }else if(dim_simplex==2){
                                count_triangles_included++;
                            }else if(dim_simplex==3){
                                count_tetrahedra_included++;
                            }
                        }
                    }
                    if(un){
                        std::cout << " # all simplices excluded, " << count_simplices_excluded << std::endl;
                        std::cout << " # all tetrahedra excluded, " << count_tetrahedra_excluded << std::endl;
                        std::cout << " # all triangles excluded, " << count_triangles_excluded << std::endl;
                        std::cout << " # all edges excluded, " << count_edges_excluded << std::endl;
                        std::cout << " # all points excluded, " << count_points_excluded << std::endl;
                    }else{
                        std::cout << " # all simplices included, " << count_simplices_included << std::endl;
                        std::cout << " # all tetrahedra included, " << count_tetrahedra_included << std::endl;
                        std::cout << " # all triangles included, " << count_triangles_included << std::endl;
                        std::cout << " # all edges included, " << count_edges_included << std::endl;
                        std::cout << " # all points included, " << count_points_included << std::endl;
                    }
                    if(lock && !un){
                        std::cout << " Betti0(min_alpha), " << getBetti0() << std::endl;
                        std::cout << " Betti1(min_alpha), " << getBetti1() << std::endl;
                        std::cout << " Betti2(min_alpha), " << getBetti2() << std::endl;
                        std::cout << " min_alpha: " << CGAL::to_double(getMinAlpha2()) << std::endl;
                    }else{
                        std::cout << " Betti0, " << getBetti0Full() << std::endl;
                        std::cout << " Betti1, " << getBetti1Full() << std::endl;
                        std::cout << " Betti2, " << getBetti2Full() << std::endl;
                    }
                }
            }else{
                std::cout << "No cycle/hole of given dimension can be " << operation_name << "ed!" << std::endl;
            }
        }else{
        //apply for persistence pairs with persistence above/below given threshold
            std::vector<PersistencePair*> pers_pairs;
            if(!subcomplex){
                pers_pairs = persistence.getPersistencePairsWithBound(dim, persistence_bound, lowerbound, &filtration_original);
            }else{
                if(lock && !un){ //lock
                    //only take persistence pairs with future birth
                    pers_pairs = persistence.getPersistencePairsWithBoundStatus(dim,persistence_bound,lowerbound,current_alpha2,false,true,true,&filtration_original);
                }else if(!lock && !un){ //fill
                    //only take persistence pairs which are not dead
                    pers_pairs = persistence.getPersistencePairsWithBoundStatus(dim,persistence_bound,lowerbound,current_alpha2,false,false,true,&filtration_original);
                }else if(lock && un){ //unlock
                    //only take persistence pairs which are born already
                    pers_pairs = persistence.getPersistencePairsWithBoundStatus(dim,persistence_bound,lowerbound,current_alpha2,true,false,false,&filtration_original);
                }else if(!lock && un){ //unfill
                    //only take persistence pairs which are already dead
                    pers_pairs = persistence.getPersistencePairsWithBoundStatus(dim,persistence_bound,lowerbound,current_alpha2,true,true,false,&filtration_original);
                }
            }
            
            if(printinfo){
                std::cout << operation_name << " cycle/hole in " << name << " complex, with persistence ";
                if(lowerbound){
                    std::cout << "above ";
                }else{
                    std::cout << "below ";
                }
                std::cout << persistence_bound << std::endl;
            }
            for(int i=0; i<pers_pairs.size(); i++){
                PersistencePair* pair = pers_pairs[i]; 
                int death_filtration_index = pair->getDeathIndexFiltration();
                int birth_filtration_index = pair->getBirthIndexFiltration();
                if(printinfo){
                    if(!weighted){
                        std::cout << "pers pair (" << birth_filtration_index << " " << death_filtration_index << ") " << pair->getPersistence_double(&filtration_original) << std::endl;
                    }else{
                        std::cout << "pers pair (" << birth_filtration_index << " " << death_filtration_index << ") " << pair->getPersistence2_double(&filtration_original) << std::endl;
                    }
                }
                if(lock && !un){
                    //lock cycle
                    if(subcomplex){
                        lockCycleRecursively(*pair,current_alpha2,false,false);
                    }else{
                        lockOrFillCycleFullComplex(true,*pair,filtration_original.getMinValue(),false,false);
                    }
                }else if(lock && un){
                    //unlock cycle
                    if(subcomplex){
                        unlockCycleRecursively(*pair,current_exclusion_alpha2,false,false);
                    }else{
                        unlockOrUnfillCycleFullComplex(true,*pair,false,false);
                    }
                }else if(!lock && !un){
                    //fill hole
                    if(subcomplex){
                        fillHoleRecursively(*pair,current_alpha2,false,false);
                    }else{
                        exact birth_alpha2;
                        if(birth_filtration_index<1){
                            birth_alpha2 = filtration_original.getMinValue();
                        }else{
                            birth_alpha2 = filtration_original.getElement(birth_filtration_index).first;
                        }
                        lockOrFillCycleFullComplex(false,*pair,birth_alpha2,false,false);
                    }
                }else if(!lock && un){
                    //unfill hole
                    if(subcomplex){
                        unfillHoleRecursively(*pair,current_exclusion_alpha2,false,false);
                    }else{
                        unlockOrUnfillCycleFullComplex(false, *pair,false,false);
                    }
                }    
            }
            if(pers_pairs.size()>0){
                //recompute filtration
                filtration_current.recomputeWithIncluded(&filtration_original,&filtration_values,simplices_ptr,critical_ptr,&excluded,&included,&inclusion_values_and_counter);
                
                if(!subcomplex){
                    setAlpha2(current_alpha2);
                }
                //recompute persistence (for adapted complex, but original matrices and pairs are still stored)
                if(recompute_persistence){
                    computePersistence(false,false);
                }
                //update statistics 
                if(compute_statistics){
                    computeFiltrationStatistics(false,true); 
                }
            }
        }
    }
    
    holes_modified = true;
    if(subcomplex && printinfo){
        //check if operations exactly manipulated the holes for which the corresponding point in the persistence diagram has moved
        computeManipulatedPersistencePointsFromStatus();
        comparePersistenceDiagramsOldNew(); 
    }
}

//undo all hole operations
void Filtered_Complex::undoHoleOperations(bool recompute_persistence, bool compute_statistics)
{
    if(holes_modified){
        last_hole_operation_simplex = -1;

        //reset status of persistence pairs in current subcomplex
        resetPersistencePairsStatus(false);

        //clear all exclusions and inclusions
        for(int i=0; i<simplices_ptr->size(); i++){
            excluded[i]=false;
            included[i]=false;
            inclusion_values_and_counter.clear();
        }
        filtration_current=filtration_original; //reset original filtration
        setAlpha2(current_alpha2); //recompute current filtration index
        //optionally recompute persistence 
        if(recompute_persistence){
            computePersistence(false,false);
        }
        //optionally update statistics 
        if(compute_statistics){
            computeFiltrationStatistics(false,false); 
        }
        
        holes_modified=false;
    }
} 

//recursive lock-operation on current subcomplex, include simplices in current subcomplex
void Filtered_Complex::lockCycleRecursively(PersistencePair persistence_pair, exact current_alpha2, bool printinfo, bool printstats)
{
    //check if pair is in the future
    if(persistence.getStatusOfCorrespondingPersistencePair(persistence_pair.getBirthIndexFiltration())==2){
        
        if(printinfo){
            std::cout << "  lock_" << persistence_pair.getDim() <<  " (" << persistence_pair.getBirthIndexFiltration() << "," << persistence_pair.getDeathIndexFiltration() << ")" << std::endl;
            if(persistence_pair.getBirthIndexFiltration()>0){
                std::cout << "    include simplex "; filtration_original.getSimplex(persistence_pair.getBirthIndexFiltration(),simplices_ptr)->print();
            }
        }
        
        //lock and fill holes defined by facet simplices
        std::vector<int> dependencesBoundary = persistence.getDependencesBoundary(persistence_pair.getBirthIndexFiltration());
        
        for(int i=0; i<dependencesBoundary.size(); i++){
            if(persistence.isPositiveSimplexOriginal(dependencesBoundary[i])){
                lockCycleRecursively(getPersistencePairForSimplex(dependencesBoundary[i],true),current_alpha2,printinfo,printstats);
            }else{
                fillHoleRecursively(getPersistencePairForSimplex(dependencesBoundary[i],false),current_alpha2,printinfo,printstats);
            }
        }
        //fill all pairs which are DB_T-dependent
        std::vector<PersistencePair> dependent_pairs_DB_T = persistence.getDependentPairs(persistence_pair.getBirthIndexFiltration(),false,true,true);
        
        for(int i=0; i<dependent_pairs_DB_T.size(); i++){
            fillHoleRecursively(dependent_pairs_DB_T[i],current_alpha2,printinfo,printstats);
        }
        //lock all pairs which are BB_T-dependent
        std::vector<PersistencePair> dependent_pairs_BB_T = persistence.getDependentPairs(persistence_pair.getBirthIndexFiltration(),true,true,true);
        
        for(int i=0; i<dependent_pairs_BB_T.size(); i++){
            lockCycleRecursively(dependent_pairs_BB_T[i],current_alpha2,printinfo,printstats);
        }
        //include birth simplex with current alpha value
        setSimplexIncludedWithAlpha2(filtration_original.getElementIndex(persistence_pair.getBirthIndexFiltration()),current_alpha2,true);
        //change status to presence
        persistence.setStatusOfCorrespondingPersistencePair(persistence_pair.getBirthIndexFiltration(),1);
        if(persistence_pair.getDeathIndexFiltration()>=0){
            persistence.setStatusOfCorrespondingPersistencePair(persistence_pair.getDeathIndexFiltration(),1);
        }
    }
}

//recursive fill-operation on current subcomplex, include simplices in current subcomplex
void Filtered_Complex::fillHoleRecursively(PersistencePair persistence_pair, exact current_alpha2, bool printinfo, bool printstats)
{
    //check if pair not in past
    if(persistence.getStatusOfCorrespondingPersistencePair(persistence_pair.getDeathIndexFiltration())!=0){
        
        if(printinfo){
            std::cout << "  fill_" << persistence_pair.getDim() <<  " (" << persistence_pair.getBirthIndexFiltration() << "," << persistence_pair.getDeathIndexFiltration() << ")" << std::endl;
            if(persistence_pair.getDeathIndexFiltration()>0){
                std::cout << "    include simplex "; filtration_original.getSimplex(persistence_pair.getDeathIndexFiltration(),simplices_ptr)->print();
            }
        }
        
        //lock and fill holes defined by facet simplices
        std::vector<int> dependencesBoundary = persistence.getDependencesBoundary(persistence_pair.getDeathIndexFiltration());
        
        for(int i=0; i<dependencesBoundary.size(); i++){
            if(persistence.isPositiveSimplexOriginal(dependencesBoundary[i])){
                lockCycleRecursively(getPersistencePairForSimplex(dependencesBoundary[i],true),current_alpha2,printinfo,printstats);
            }else{
                fillHoleRecursively(getPersistencePairForSimplex(dependencesBoundary[i],false),current_alpha2,printinfo,printstats);
            }
        }
        //fill all pairs which are DD_T-dependent
        std::vector<PersistencePair> dependent_pairs_DD_T = persistence.getDependentPairs(persistence_pair.getDeathIndexFiltration(),false,false,true);
        
        for(int i=0; i<dependent_pairs_DD_T.size(); i++){
            fillHoleRecursively(dependent_pairs_DD_T[i],current_alpha2,printinfo,printstats);
        }
        //if pair in future, lock first
        if(persistence.getStatusOfCorrespondingPersistencePair(persistence_pair.getDeathIndexFiltration())==2){
            lockCycleRecursively(persistence_pair,current_alpha2,printinfo,printstats);
        }
        //include death simplex
        setSimplexIncludedWithAlpha2(filtration_original.getElementIndex(persistence_pair.getDeathIndexFiltration()),current_alpha2,true);
        //change status to past
        persistence.setStatusOfCorrespondingPersistencePair(persistence_pair.getBirthIndexFiltration(),0);
        if(persistence_pair.getDeathIndexFiltration()>=0){
            persistence.setStatusOfCorrespondingPersistencePair(persistence_pair.getDeathIndexFiltration(),0);
        }
    }
}

//recursive unfill-operation on current subcomplex, move simplices just outside of current subcomplex
void Filtered_Complex::unlockCycleRecursively(PersistencePair persistence_pair, exact current_exclusion_alpha2, bool printinfo, bool printstats)
{
    //check if pair not in future
    if(persistence.getStatusOfCorrespondingPersistencePair(persistence_pair.getBirthIndexFiltration())!=2){
        
        if(printinfo){
            std::cout << "  unlock_" << persistence_pair.getDim() <<  " (" << persistence_pair.getBirthIndexFiltration() << "," << persistence_pair.getDeathIndexFiltration() << ")" << std::endl;
        }
        
        //unlock and unfill holes defined by cofacet simplices
        std::vector<int> dependencesBoundary_T = persistence.getDependencesBoundary_T(persistence_pair.getBirthIndexFiltration());
        for(int i=0; i<dependencesBoundary_T.size(); i++){
            if(persistence.isPositiveSimplexOriginal(dependencesBoundary_T[i])){
                unlockCycleRecursively(getPersistencePairForSimplex(dependencesBoundary_T[i],true),current_exclusion_alpha2,printinfo,printstats);
            }else{
                unfillHoleRecursively(getPersistencePairForSimplex(dependencesBoundary_T[i],false),current_exclusion_alpha2,printinfo,printstats);
            }
        }
        //unlock all pairs which are BB-dependent
        std::vector<PersistencePair> dependent_pairs_BB = persistence.getDependentPairs(persistence_pair.getBirthIndexFiltration(),true,true,false);
        for(int i=0; i<dependent_pairs_BB.size(); i++){
            unlockCycleRecursively(dependent_pairs_BB[i],current_exclusion_alpha2,printinfo,printstats);
        }
        //if pair in past, unfill first
        if(persistence.getStatusOfCorrespondingPersistencePair(persistence_pair.getBirthIndexFiltration())==0){
            unfillHoleRecursively(persistence_pair,current_exclusion_alpha2,printinfo,printstats);
        }
        //include birth simplex after current subcomplex (but before next simplex)
        setSimplexIncludedWithAlpha2(filtration_original.getElementIndex(persistence_pair.getBirthIndexFiltration()),current_exclusion_alpha2,false);
        //change status to future
        persistence.setStatusOfCorrespondingPersistencePair(persistence_pair.getBirthIndexFiltration(),2);
        if(persistence_pair.getDeathIndexFiltration()>=0){
            persistence.setStatusOfCorrespondingPersistencePair(persistence_pair.getDeathIndexFiltration(),2);
        }
    }
}

//recursive unfill-operation on current subcomplex, move simplices just outside of current subcomplex
void Filtered_Complex::unfillHoleRecursively(PersistencePair persistence_pair, exact current_exclusion_alpha2, bool printinfo, bool printstats)
{
    //check if pair in past
    if(persistence.getStatusOfCorrespondingPersistencePair(persistence_pair.getDeathIndexFiltration())==0){
        
        if(printinfo){
            std::cout << "  unfill_" << persistence_pair.getDim() <<  " (" << persistence_pair.getBirthIndexFiltration() << "," << persistence_pair.getDeathIndexFiltration() << ")" << std::endl;
        }
        
        //unlock and unfill holes defined by cofacet simplices
        std::vector<int> dependencesBoundary_T = persistence.getDependencesBoundary_T(persistence_pair.getDeathIndexFiltration());
        for(int i=0; i<dependencesBoundary_T.size(); i++){
            if(persistence.isPositiveSimplexOriginal(dependencesBoundary_T[i])){
                unlockCycleRecursively(getPersistencePairForSimplex(dependencesBoundary_T[i],true),current_exclusion_alpha2,printinfo,printstats);
            }else{
                unfillHoleRecursively(getPersistencePairForSimplex(dependencesBoundary_T[i],false),current_exclusion_alpha2,printinfo,printstats);
            }
        }
        //unlock all pairs which are DB-dependent
        std::vector<PersistencePair> dependent_pairs_DB = persistence.getDependentPairs(persistence_pair.getDeathIndexFiltration(),false,true,false);
        for(int i=0; i<dependent_pairs_DB.size(); i++){
            unlockCycleRecursively(dependent_pairs_DB[i],current_exclusion_alpha2,printinfo,printstats);
        }
        //unfill all pairs which are DD-dependent
        std::vector<PersistencePair> dependent_pairs_DD = persistence.getDependentPairs(persistence_pair.getDeathIndexFiltration(),false,false,false);
        for(int i=0; i<dependent_pairs_DD.size(); i++){
            unfillHoleRecursively(dependent_pairs_DD[i],current_exclusion_alpha2,printinfo,printstats);
        }
        //include death simplex after current subcomplex (but before next simplex)
        setSimplexIncludedWithAlpha2(filtration_original.getElementIndex(persistence_pair.getDeathIndexFiltration()),current_exclusion_alpha2,false);

        //change status to presence
        persistence.setStatusOfCorrespondingPersistencePair(persistence_pair.getBirthIndexFiltration(),1);
        if(persistence_pair.getDeathIndexFiltration()>=0){
            persistence.setStatusOfCorrespondingPersistencePair(persistence_pair.getDeathIndexFiltration(),1);
        }
    }
}

//compare persistence diagram of original filtration and after hole operations, check if changes correspond exactly to recursive hole operations
void Filtered_Complex::comparePersistenceDiagramsOldNew()
{
    std::cout << "COMPARE PERSISTENCE DIAGRAMS OLD NEW" << std::endl;
    std::vector<PersistencePair>* pairs_old = persistence.getPersistencePairsOriginal();
    std::vector<PersistencePair>* pairs_new = persistence.getPersistencePairsCurrent();
    std::vector<std::pair<std::pair<int,exact>,std::pair<int,exact> > > persistence_points_old;
    std::vector<std::pair<std::pair<int,exact>,std::pair<int,exact> > > persistence_points_new;
    std::vector<std::pair<std::pair<std::pair<int,exact>,std::pair<int,exact> >,std::pair<exact,exact> > > point_manipulations;
    int count_matched_points = 0;
    int count_matched_witharrow = 0;
    //gather all information about old persistence points
    for(int i=0; i<pairs_old->size(); i++){
        PersistencePair pair = pairs_old->at(i);
        if(pair.getBirthIndexFiltration()>0){
            int birth_simplex_index = filtration_original.getElementIndex(pair.getBirthIndexFiltration());
            exact birth_alpha2 = filtration_original.getElementValue(pair.getBirthIndexFiltration());
            int death_simplex_index = -1;
            exact death_alpha2 = -1;
            if(!pair.isInfinite()){
                death_simplex_index = filtration_original.getElementIndex(pair.getDeathIndexFiltration());
                death_alpha2 = filtration_original.getElementValue(pair.getDeathIndexFiltration());
            }
            persistence_points_old.push_back(std::make_pair(std::make_pair(birth_simplex_index,birth_alpha2),std::make_pair(death_simplex_index,death_alpha2)));
        }
    }
    std::sort(persistence_points_old.begin(),persistence_points_old.end());
    //gather all information about new persistence points
    for(int i=0; i<pairs_new->size(); i++){
        PersistencePair pair = pairs_new->at(i);
        if(pair.getBirthIndexFiltration()>0){
            int birth_simplex_index = filtration_current.getElementIndex(pair.getBirthIndexFiltration());
            exact birth_alpha2 = filtration_current.getElementValue(pair.getBirthIndexFiltration());
            int death_simplex_index = -1;
            exact death_alpha2 = -1;
            if(!pair.isInfinite()){
                death_simplex_index = filtration_current.getElementIndex(pair.getDeathIndexFiltration());
                death_alpha2 = filtration_current.getElementValue(pair.getDeathIndexFiltration());
            }
            persistence_points_new.push_back(std::make_pair(std::make_pair(birth_simplex_index,birth_alpha2),std::make_pair(death_simplex_index,death_alpha2)));
        }
    }
    std::sort(persistence_points_new.begin(),persistence_points_new.end());
    //gather all information about point manipulations recorded by hole operations
    std::vector<std::pair<PersistencePair,std::pair<exact,exact> > >* manipulated_persistence_points = getManipulatedPersistencePoints();
    for(int i=0; i<manipulated_persistence_points->size(); i++){
        std::pair<PersistencePair,std::pair<exact,exact> > manipulated_point = manipulated_persistence_points->at(i);
        PersistencePair pair = manipulated_point.first;
        int birth_simplex_index = filtration_original.getElementIndex(pair.getBirthIndexFiltration());
        exact birth_alpha2 = filtration_original.getElementValue(pair.getBirthIndexFiltration());
        int death_simplex_index = -1;
        exact death_alpha2 = -1;
        if(!pair.isInfinite()){
            death_simplex_index = filtration_original.getElementIndex(pair.getDeathIndexFiltration());
            death_alpha2 = filtration_original.getElementValue(pair.getDeathIndexFiltration());
        }
        point_manipulations.push_back(std::make_pair(std::make_pair(std::make_pair(birth_simplex_index,birth_alpha2),std::make_pair(death_simplex_index,death_alpha2)),std::make_pair(manipulated_point.second.first,manipulated_point.second.second)));
        
    }
    std::sort(point_manipulations.begin(),point_manipulations.end());

    //compare
    int current_index_new = 0;
    int current_index_manipulations = 0;
    for(int i=0; i<persistence_points_old.size();){
        bool match = false;
        std::pair<std::pair<int,exact>,std::pair<int,exact> > old_point = persistence_points_old[i];
        std::pair<std::pair<int,exact>,std::pair<int,exact> > new_point;
        std::pair<std::pair<std::pair<int,exact>,std::pair<int,exact> >,std::pair<exact,exact> > point_manipulation;
        if(current_index_manipulations < point_manipulations.size()){
            point_manipulation = point_manipulations[current_index_manipulations];
        }
        if(current_index_new < persistence_points_new.size()){
            new_point = persistence_points_new[current_index_new];
        }
        if(current_index_new < persistence_points_new.size() && old_point.first.first == new_point.first.first){
            //same birth simplex
            if((old_point.second.first == new_point.second.first) || new_point.second.first==-1 ){
                //same death simplex
                //same pairing, check coordinates
                if((new_point.second.first>=0 && (old_point.first.second == new_point.first.second && old_point.second.second == new_point.second.second)
                       || (old_point.first.second == old_point.second.second && new_point.first.second == new_point.second.second)) ){
                    //same coordinates or both zero persistence -> no change
                    match = true;
                    count_matched_points++;
                    current_index_new ++;
                    i++;
                }else{
                    //coordinates have changed or death simplex was excluded, check if this was recorded by hole operation
                    if(current_index_manipulations < point_manipulations.size() && old_point.first.first == point_manipulation.first.first.first){
                        //same birth simplex
                        if(old_point.second.first == point_manipulation.first.second.first){
                            //same death simplex
                            if(old_point.first.second == point_manipulation.first.first.second && old_point.second.second == point_manipulation.first.second.second){
                                //old coordinates match start of connecting arrow
                                if((new_point.first.second == point_manipulation.second.first && new_point.second.second == point_manipulation.second.second)||(new_point.first.second==new_point.second.second && point_manipulation.second.first==point_manipulation.second.second)){
                                    //arrow connects old and new point (which could be diagonal point)
                                    match = true;
                                    count_matched_witharrow++;
                                    current_index_manipulations++;
                                    current_index_new++;
                                    i++;
                                }else{
                                    std::cout << "NO MATCHING: end coordinates of connecting arrow wrong!" << std::endl;
                                    current_index_manipulations++;
                                    current_index_new++;
                                    i++;
                                }
                            }else{
                                std::cout << "NO MATCHING: start coordinates of connecting arrow wrong!" << std::endl;
                                current_index_manipulations++;
                                current_index_new++;
                                i++;
                            }
                        }else{
                            std::cout << "NO MATCHING: death simplex of connecting arrow wrong!" << std::endl;
                            current_index_manipulations++;
                            current_index_new++;
                            i++;
                        }
                    }else{
                        if(current_index_manipulations < point_manipulations.size() && old_point.first.first>point_manipulation.first.first.first){
                            //list of point manipulations less advanced
                            std::cout << "NO MATCHING: birth index of connecting arrow could not be matched!" << std::endl;
                            current_index_manipulations++;
                        }else{
                            //list of point manipulations less advanced
                            std::cout << "NO MATCHING: adapted persistence pair could not be matched to connecting arrow!" << std::endl;
                            current_index_new++;
                            i++;
                        }
                    }
                    
                }
            }else{
                std::cout << "NO MATCHING: birth simplices are paired differently!" << std::endl;
                current_index_new++;
                i++;
            }
        }else{
            //different birth simplices
            //check if there is deleting arrow
            if(current_index_manipulations < point_manipulations.size() && old_point.first.first == point_manipulation.first.first.first){
                //same birth simplex
                if(old_point.second.first == point_manipulation.first.second.first){
                    //same death simplex
                    if(point_manipulation.second.first == -1 && point_manipulation.second.second == -1){
                        //old point was deleted, recorded by connection
                        match = true;
                        count_matched_witharrow++;
                        current_index_manipulations++;
                        i++;
                    }else{
                        std::cout << "NO MATCHING: coordinates of connecting arrow wrong!" << std::endl;
                        current_index_manipulations++;
                        i++;
                    }
                }else{
                    std::cout << "NO MATCHING: death simplex of connecting arrow wrong!" << std::endl;
                    current_index_manipulations++;
                    i++;
                }
            }else{
                if(current_index_manipulations < point_manipulations.size() && old_point.first.first>point_manipulation.first.first.first){
                    //list of point manipulations less advanced
                    std::cout << "NO MATCHING: birth index of connecting arrow could not be matched!" << std::endl;
                    current_index_manipulations++;
                }else if(current_index_new < persistence_points_new.size() && old_point.first.first>new_point.first.first){
                    //list of new points advanced less
                    std::cout << "NO MATCHING: birth simplex of new point could not be matched!" << std::endl;
                    current_index_new ++;
                }else{
                    std::cout << "NO MATCHING: birth simplex of old point could not be matched!" << std::endl;
                    i++;
                }
            }
        }   
        if(!match){
            std::cout << "  old point: birth " << old_point.first.first << " " << old_point.first.second << ", death " << old_point.second.first <<  ", " << old_point.second.second << std::endl;
            if(current_index_new < persistence_points_new.size()){
                std::cout << "  new point: birth " << new_point.first.first << " " << new_point.first.second << " death " << new_point.second.first << " " << new_point.second.second << std::endl;
            }
            if(current_index_manipulations < point_manipulations.size()){
                std::cout << "  connecting arrow: start birth " << point_manipulation.first.first.first  << ", " << point_manipulation.first.first.second << " death " << point_manipulation.first.second.first << ", " << point_manipulation.first.second.second << ", end " << point_manipulation.second.first << "," << point_manipulation.second.second << std::endl;
            }
        }
    }
    std::cout << "# not matched points: " << persistence_points_old.size() - count_matched_points - count_matched_witharrow <<  " of " << persistence_points_old.size() << std::endl;
    std::cout << "# matched with arrow: " << count_matched_witharrow << std::endl;
    
}

//filtration statistics for any filtered complex, called by computeFiltrationStatistics
void Filtered_Complex::computeFiltrationStatistics0(bool printstats)
{
    //compute # simplices
    num_simplices_filtration.clear();
    std::vector<int> num_points_filtration = std::vector<int>(filtration_current.size(),0);
    std::vector<int> num_edges_filtration = std::vector<int>(filtration_current.size(),0);
    std::vector<int> num_triangles_filtration = std::vector<int>(filtration_current.size(),0);
    std::vector<int> num_tetrahedra_filtration;
    if(dim3){
        num_tetrahedra_filtration = std::vector<int>(filtration_current.size(),0);
    }
    int num_points=0; int num_edges=0; int num_triangles=0; int num_tetrahedra=0; //current values
    for(int i=0; i<filtration_current.size(); i++){
        int element_dim = filtration_current.getElementDim(i,simplices_ptr);
        if(element_dim==0){ //point
            num_points++;
        }else if(element_dim==1){ //edge
            num_edges++;
        }else if(element_dim==2){ //triangle
            num_triangles++;
        }else if(element_dim==3){ //tetrahedron
            num_tetrahedra++;
        }
        num_points_filtration[i]=num_points;
        num_edges_filtration[i]=num_edges;
        num_triangles_filtration[i]=num_triangles;
        if(dim3){
            num_tetrahedra_filtration[i]=num_tetrahedra;
        }
    } 
    num_simplices_filtration.push_back(num_points_filtration);
    num_simplices_filtration.push_back(num_edges_filtration);
    num_simplices_filtration.push_back(num_triangles_filtration);
    if(dim3){
        num_simplices_filtration.push_back(num_tetrahedra_filtration);
    }
    
    //compute betti numbers from persistence filtration
    betti_filtration = persistence.computeBettiFiltration();
    
    if(printstats){
        std::cout << name_uppercase << " COMPLEX" << std::endl;
        std::cout << " # simplices, " << filtration_current.size() << std::endl;
        std::cout << " # tetrahedra, " << getNumTetrahedraFull() << std::endl;
        std::cout << " # triangles, " << getNumTrianglesFull() << std::endl;
        std::cout << " # edges, " << getNumEdgesFull() << std::endl;
        std::cout << " # points, " << getNumPointsFull() << std::endl;
    }
}

//print simplices of filtered complex together with filtration values, for exporting and importing
void Filtered_Complex::printFilteredComplex(std::ostream& output)
{
    //general parameters
    output << "dim, weighted, periodic_size, on_standard_simplex" << std::endl;
    int dim=2; 
    if(dim3){
        dim=3;
    }
    output << dim << ", " << weighted << ", " << periodic_size << ", " << on_standard_simplex << std::endl;
    output << std::endl;
    
    //points
    output << "POINTS: index, x, y";
    if(dim3){
        output << ", z";
    }
    if(weighted){
        output << ", weight";
    }
    output << std::endl;
    for(int i=0; i<data_points->size(); i++){
        DataPoint point = data_points->at(i);
        if(!(point.isDeleted())){
            output << i << ", " << point.getX_double() << ", " << point.getY_double();
            if(dim3){
                output << ", " << point.getZ_double();
            }
            if(weighted){
                output << ", " << point.getWeight_double();
            }
            output << std::endl;
        }
    }
    output << std::endl;
    
    output << "SIMPLICES: index, dim, radius^2, point1 point2 ..." << std::endl;
    std::cout << "0, -1, -1," << std::endl;
    for(int i=1; i<filtration_current.size();i++){ 
        exact value = filtration_current.getElement(i).first;
        Simplex* simplex = filtration_current.getSimplex(i,simplices_ptr);
        output << i << ", " << simplex->getDim() << ", ";
        output << (CGAL::to_double(value)) << ", ";
        std::vector<int> vertices = simplex->getVertices();
        for(int j=0; j<vertices.size()-1; j++){
            output << vertices[j] << " ";
        }
        if(vertices.size()>0){
            output << vertices[vertices.size()-1];
        }
        output << std::endl;
    }
}

//print persistence barcode ((dim,birth,death)-triples sorted by dimension and persistence)
void Filtered_Complex::printPersistenceBarcode(){
    if(!persistence_computed){
        computePersistence(true,false);
    }
    std::cout << "dim, birth_value, death_value" << std::endl;
    std::vector<PersistencePair>* persistence_pairs = persistence.getPersistencePairsCurrent();
    int dim=2;
    if(dim3){
        dim=3;
    }
    //print persistence pairs ordered by dimension and persistence
    for(int d=0; d<dim; d++){
        for(int i=persistence_pairs->size()-1; i>=0; i--){
            PersistencePair pair = persistence_pairs->at(i);
            if(pair.getDim()==d && !pair.hasZeroPersistence(&filtration_current)){
                std::cout << pair.getDim() << ", ";
                if(!weighted){
                    std::cout << pair.getBirth_double(&filtration_current) << ", " << pair.getDeath_double(&filtration_current) << std::endl; 
                }else{
                    std::cout << pair.getBirth2_double(&filtration_current) << ", " << pair.getDeath2_double(&filtration_current) << std::endl; 
                }
            }
        }
    }
}