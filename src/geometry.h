//geometry.h
//author: koelsboe

#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "basics.h" //basic geometric classes, CGAL headers, Qt libraries
#include "alpha_complex.h"
#include "opengl_widget.h" //opengl 3d rendering
#include "bregman.h"

#include <QOpenGLWidget>
#include <QOpenGLFunctions>


//group of graphics items
class MyQGraphicsItemGroup: public QGraphicsItemGroup{
    private:
        QList<QGraphicsItem*> members; //list of member graphics items 
        
    public:
        int size(){ return members.size();} //number of members
        
        QGraphicsItem* getItem(int index){return members[index];}
        
        void add(QGraphicsItem* item){ //add new graphics item to group
            members.append(item);
            this->addToGroup(item);
        }
        
        void clear(){ //delete all group members from scene, clear group
            qDeleteAll(members.begin(),members.end());
            members.clear();
        }   
        
        //set all items visible/invisible
        void setVisible(bool visible){
            for(int i=0; i<members.size(); i++){
                members[i]->setVisible(visible);
            }
        }
};

//group of graphics items with associated keys, items will only be visible if their key is below the current alpha value
class FilteredQGraphicsItemGroup: public QGraphicsItemGroup{
    private:
        QList<QGraphicsItem*> members; //list of member graphics items 
        std::vector<exact> keys; //keys associated to graphics items (alpha values)
        
    public:
        int size(){ return keys.size();} //number of members
        
        void add(exact key, QGraphicsItem* item){ //add new graphics item to group
            members.append(item);
            keys.push_back(key);
            this->addToGroup(item);
        }
        
        void clear(){ //delete all group members from scene, clear group
            qDeleteAll(members.begin(),members.end());
            members.clear();
            keys.clear();
        }   
        
        //set all items with key lower equal than alpha visible, others invisible
        void filterAlpha(double alpha){
            for(int i=0; i<keys.size(); i++){
                members[i]->setVisible(keys[i]<=alpha);
            }
        }
        
        //set all items visible/invisible
        void setVisible(bool visible){
            for(int i=0; i<keys.size(); i++){
                members[i]->setVisible(visible);
            }
        }
};

//group of graphics items with associated lower and upper bounds, will only be visible if current alpha between those bounds
class IntervalFilteredQGraphicsItemGroup: public QGraphicsItemGroup{
    private:
        QList<QGraphicsItem*> members; //list of member graphics items 
        std::vector<exact> lower_bounds; //lower bounds associated to graphics items (alpha values)
        std::vector<exact> upper_bounds; //upper bounds associated to graphics items
        std::vector<bool> lower_bounded; //if associated item has lower bound
        std::vector<bool> upper_bounded; //if associated item has upper bound
        
    public:
        int size(){ return members.size();} //number of members
        
        void add(exact lower_bound, exact upper_bound, QGraphicsItem* item){ //add new graphics item to group
            members.append(item);
            lower_bounds.push_back(lower_bound);
            upper_bounds.push_back(upper_bound);
            lower_bounded.push_back(true); upper_bounded.push_back(true);
            this->addToGroup(item);
        }
        
        void add(exact lower_bound, QGraphicsItem* item){ //add item without upper bound
            members.append(item);
            lower_bounds.push_back(lower_bound);
            upper_bounds.push_back(-1);
            lower_bounded.push_back(true); upper_bounded.push_back(false);
            this->addToGroup(item);
        }
        
        void add(QGraphicsItem* item){ //add item without bounds
            members.append(item);
            lower_bounds.push_back(-1);
            upper_bounds.push_back(-1);
            lower_bounded.push_back(false); upper_bounded.push_back(false);
            this->addToGroup(item);
        }
        
        void clear(){ //delete all group members from scene, clear group
            qDeleteAll(members.begin(),members.end());
            members.clear();
            lower_bounds.clear();
            upper_bounds.clear();
            lower_bounded.clear();
            upper_bounded.clear();
        }   
        
        //set all items with alpha between their lower and upper bound visible, others invisible
        void filterAlpha(double alpha){
            for(int i=0; i<members.size(); i++){
                if(upper_bounded[i]){ //no upper bound
                    if(lower_bounded[i]){
                        members[i]->setVisible(lower_bounds[i]<=alpha && upper_bounds[i]>=alpha);
                    }else{
                        members[i]->setVisible(upper_bounds[i]>=alpha);
                    }
                }else{
                    if(lower_bounded[i]){
                       members[i]->setVisible(lower_bounds[i]<=alpha);
                    }else{
                        members[i]->setVisible(true);
                    }
                }
            }
        }
        
        //draw items corresponding to "infinite" alpha, i.e., those without an upper bound
        void filterFull(){
            for(int i=0; i<members.size(); i++){
                if(!upper_bounded[i]){ //no upper bound
                    members[i]->setVisible(true);
                }else{
                    members[i]->setVisible(false);
                }
            }
        }
        
        //set all items visible/invisible
        void setVisible(bool visible){
            for(int i=0; i<members.size(); i++){
                members[i]->setVisible(visible);
            }
        }
};

//zoomable graphicsview
class DrawingPlane : public QGraphicsView
{
    Q_OBJECT
    
    private:
        int scheduled_scaling; //used for smooth zooming
        
    public:
        DrawingPlane(QWidget *parent = 0):QGraphicsView(parent)
        {
            scheduled_scaling=0;
        } 
            
    protected:
        //smooth zooming with mouse wheel 
        void wheelEvent(QWheelEvent * event) //mouse wheel for zooming   
        {
            int degrees = event->delta() /8; //how much the mouse wheel was moved
            int nsteps = degrees / 15; //smooth zooming every 15°
            scheduled_scaling += nsteps; //add to scheduled scaling
            if(scheduled_scaling * nsteps < 0){
                //reset scheduled scaling if wheel was turned in other direction
                scheduled_scaling = nsteps;
            }
            QTimeLine *animate_zoom = new QTimeLine(350, this); //new animation lasting 350ms
            animate_zoom->setUpdateInterval(20); //update animation every 20ms

            connect(animate_zoom, SIGNAL (valueChanged(qreal)), SLOT (scaleScene(qreal)));
            connect(animate_zoom, SIGNAL (finished()), SLOT (scaleFinished()));
            animate_zoom->start(); //start animation
        }
    private Q_SLOTS:
        void scaleScene(qreal x) //zoom
        {
            qreal factor = 1. + qreal(scheduled_scaling) / 300.;
            scale(factor,factor); 
            //scale the scene, factor depends on size of scheduled scaling
            //(faster when more scheduled)
        }
        void scaleFinished() //zoom animation terminated  
        {
            //take care of dynamically created QTimeLines
            if (scheduled_scaling > 0){
                scheduled_scaling--;
            }else{
                scheduled_scaling++;
                sender()->~QObject();
            }
        }
};

//class to compute and draw all geometric objects
class Geometry : public DrawingPlane
{
    Q_OBJECT
    
    private:
        bool labels_intervals_bool; //whether to show interval labels (2-dim => triangle labels)
        bool labels_points_bool; //whether to show point labels
        bool input_points_bool; //whether to draw input points
        bool delaunay_bool; //whether to draw Delaunay triangulation
        bool wrap_bool; //whether to draw Wrap complex 
        bool alpha_bool; //whether to draw Alpha complex
        bool full_bool; //whether to draw full Wrap and Alpha complex (alpha infinity)
        bool plots_logscale; //whether statistics plots in normal or log scale
        bool weighted_point_spheres_bool; //whether to show weighted points as spheres
        bool convex_hull_bool; //whether to draw the convex hull of the points (2d)
        bool union_of_balls_bool; //whether to draw the union of balls
        
        bool persDiag_wrap_fulldrawn; //whether full persistence diagram is drawn or just pairs with persistence above threshold
        bool persDiag_alpha_fulldrawn;
        bool persDiag_wrap_bool; //whether to show persistence diagram for the Wrap or Alpha complex
        bool persDiag_adapted_bool; //whether persistence diagram of current (possibly adapted) complex is shown, else: original
        bool persDiag_subcomplex_bool; //whether current subcomplex is highlighted in persistence diagram
        bool persDiag_connections_bool; //whether to display connections between old and new persistence diagram after hole manipulations
        bool persDiag_status_bool; //whether to display old persistence diagram with points filled depending on current status after hole operations
               
        std::vector<DataPoint> input_data_points; //list of input data points 
        std::vector<DataPoint>* transformed_data_points; //list of data points for computation of Alpha and Wrap complex, same as input except for Bregman
        Alpha_Complex* alpha_complex; //Alpha complex object
        Wrap* wrap_complex; //Wrap complex object
        Bregman* bregman_complexes; //Bregman-Alpha, Bregman-Wrap, alpha_complex = (Alpha_complex*) bregman_complexes, derived class
        bool dim3; //3-dim instead of 2-dim space
        bool on_standard_simplex; //on standard 2-simplex (overrides dim3)
        bool bregman; //whether current complexes are Bregman
        bool draw_on_input_points; //whether to draw on input points or transformed points (only different in Bregman geometry)
        std::vector<int> free_indices_data_points; //manipulating points and thereby deleting simplices might result in unused indices in the lists of data points
        int last_point_inserted; //index of last data point that was inserted
        
        QGraphicsScene scene; //Qt graphics scene containing all geometric items
        MyQGraphicsItemGroup labels_intervals_items; //group of items corresponding to interval (= triangle) labels
        MyQGraphicsItemGroup labels_points_items; //group of items corresponding to point labels
        MyQGraphicsItemGroup input_points_items; //group of input points 
        MyQGraphicsItemGroup delaunay_items; //group of items corresponding to Delaunay triangles
        FilteredQGraphicsItemGroup alpha_items; //group of items corresponding to Alpha complex (triangles and edges), can be filtered by alpha value
        FilteredQGraphicsItemGroup wrap_items; //group of items corresponding to Wrap complex, can be filtered by alpha value
        MyQGraphicsItemGroup transformed_points_items; //group of transformed points (for Bregman-Delaunay)
        MyQGraphicsItemGroup labels_intervals_items_bregman;
        MyQGraphicsItemGroup labels_points_items_bregman; 
        MyQGraphicsItemGroup delaunay_items_bregman; 
        FilteredQGraphicsItemGroup alpha_items_bregman; 
        FilteredQGraphicsItemGroup wrap_items_bregman; 
        MyQGraphicsItemGroup persistence_diag_wrap_items_adapted; //group of items corresponding to points in the persistence diagram of Wrap complex
        MyQGraphicsItemGroup persistence_diag_alpha_items_adapted; //group of items corresponding to points in the persistence diagram of Alpha complex
        MyQGraphicsItemGroup persistence_diag_wrap_items_original; //group of items corresponding to points in the persistence diagram of Wrap complex
        MyQGraphicsItemGroup persistence_diag_alpha_items_original; //group of items corresponding to points in the persistence diagram of Alpha complex
        MyQGraphicsItemGroup persistence_diag_wrap_items_status; //original persistence diagram, points are filled or not depending on status after hole operation
        MyQGraphicsItemGroup persistence_diag_alpha_items_status; //original persistence diagram, points are filled or not depending on status after hole operation
        IntervalFilteredQGraphicsItemGroup persistence_diag_wrap_items_subcomplex; //currently active points in the persistence diagram of Wrap complex, can be filtered by alpha value
        IntervalFilteredQGraphicsItemGroup persistence_diag_alpha_items_subcomplex; //currently active points in the persistence diagram of Alpha complex, can be filtered by alpha value
        QGraphicsRectItem* persistence_diag_subcomplex_box; //box to highlight current subcomplex
        QGraphicsEllipseItem *old_point;
        QGraphicsEllipseItem *new_point;
        MyQGraphicsItemGroup weighted_point_spheres_items; //group of items corresponding to spheres that visualize weighted points
        FilteredQGraphicsItemGroup highlighted_items_wrap; //group of highlighted simplices
        FilteredQGraphicsItemGroup highlighted_items_filtered_wrap; //group of highlighted simplices, filtered by alpha
        FilteredQGraphicsItemGroup highlighted_items_alpha; 
        FilteredQGraphicsItemGroup highlighted_items_filtered_alpha; 
        MyQGraphicsItemGroup highlighted_persistence_points_wrap; //highlight point in persistence diagram
        MyQGraphicsItemGroup highlighted_persistence_points_alpha; 
        MyQGraphicsItemGroup persistence_diag_connections_wrap; //connections between old and new persistence diagram resulting from hole operation
        MyQGraphicsItemGroup persistence_diag_connections_alpha;
        MyQGraphicsItemGroup convex_hull_items; //group of items corresponding to convex hull (2d)
        MyQGraphicsItemGroup union_of_balls_items; //group of items corresponding to union of balls, can be filtered by alpha value
        
        std::vector<QString> statistics_plots_titles; //titles for statistics plots
        std::vector<QGraphicsItem*> statistics_plots; //statistics plots
        std::vector<QGraphicsItem*> statistics_plots_log; //statistics plots in log-scale
        std::vector<QGraphicsItem*> statistics_plots_axislabel; //y-axis labels (max value) statistics
        QGraphicsLineItem* statistics_plots_currentvalue; //vertical line to indicate the current filtration value
        
        DrawingPlane  second_drawing_plane; //second drawing plane in additional window
        QGraphicsScene second_scene;
        DrawingPlane  third_drawing_plane; //third drawing plane in yet another window
        QGraphicsScene third_scene;
        DrawingPlane  fourth_drawing_plane; //fourth drawing plane in yet another window
        QGraphicsScene fourth_scene;
        
        OpenGL_Widget* opengl_widget; //3d rendering window
        
        //drawing parameters (for 2-dim) 
        bool use_radius2; //use radius^2 value also in non-weighted case
        double pointsize;
        double pointsize_input; //alternative point size for input points which are not in current complex
        double linewidth;   
        int labelsize;
        double pointsize_persdiag; //point size in persistence diagram
        double maxvalue_plots; //max coordinate value in persistence diagram and statistic plots (allows to zoom in), and for color gradient
        int color_delaunay_by_radius; //draw Delaunay triangulation with colors depending on radius, 0 .. no, 1 .. Delaunay radius, 2 .. Wrap radius
        
        //colors
        QColor color_points;
        QColor color_delaunay_points;
        QColor color_delaunay_dark;
        QColor color_delaunay_light;
        QColor color_alpha_dark;
        QColor color_alpha_light;
        QColor color_wrap_dark;
        QColor color_wrap_light;
        QColor color_old_point; 
        QColor color_new_point;
        QColor color_weighted_points_pos;
        QColor color_weighted_points_neg;
        
        //thresholds to ensure fast computation
        int intervalgraph_maxsize; //maximal number of intervals (containing a maximal simplex) s.t. interval graph is drawn        
        bool intervalgraph_drawn; //whether interval graph is drawn
        int persdiagfull_maxpairs; //maximal number of persistence pairs s.t. full diagram is drawn
        double persdiag_minpers_rel; //if not full persistence diagram is drawn, only pairs with persistence above persdiag_minpers_rel*maxWrapAlpha2 are drawn
    
    public:
        Geometry(double alpha2, QWidget *parent = 0); //constructor for default input
        Geometry(std::vector<DataPoint> data_points_, Alpha_Complex* alpha_complex_, int PPP_window_size, double alpha2, QWidget *parent = 0); //constructor for given input
        
        exact getAlpha2MinAlpha(){return alpha_complex->getMinAlpha2();}
        exact getAlpha2MaxAlpha(){return alpha_complex->getMaxAlpha2();}
        exact getAlpha2MinWrap(){return wrap_complex->getMinAlpha2();}
        exact getAlpha2MaxWrap(){return wrap_complex->getMaxAlpha2();}
        
        double getAlpha2Current(){return alpha_complex->getAlpha2_double();}
        void setAlpha2Current(exact alpha2_){alpha_complex->setAlpha2(alpha2_);}
        
        QGraphicsView* getSecondDrawingPlane(){return &second_drawing_plane;}
        QGraphicsView* getThirdDrawingPlane(){return &third_drawing_plane;}
        QGraphicsView* getFourthDrawingPlane(){return &fourth_drawing_plane;}
        OpenGL_Widget* getOpenGLWidget(){return opengl_widget;} 
        void emitInitialSignals(); //ensure that interactive output shown at start
        
    Q_SIGNALS:
        void dimChanged(bool dim3); //signal: 2-dim  (false) or 3-dim (true) points
        void numPointsChanged(QString num); //signal: number of points has changed
        void numEdgesDelaunayChanged(QString num); //signal: number of Delaunay edges has changed
        void numTrianglesDelaunayChanged(QString num); //signal: number of Delaunay triangles has changed
        void numTetrahedraDelaunayChanged(QString num); //signal: number of Delaunay tetrahedra has changed
        void numEdgesAlphaChanged(QString num); //signal: number of alpha edges has changed
        void numTrianglesAlphaChanged(QString num); //signal: number of alpha triangles has changed
        void numTetrahedraAlphaChanged(QString num); //signal: number of alpha tetrahedra has changed
        void numEdgesWrapChanged(QString num); //signal: number of wrap edges has changed
        void numTrianglesWrapChanged(QString num); //signal: number of wrap triangles has changed
        void numTetrahedraWrapChanged(QString num); //signal: number of wrap tetrahedra has changed
        void numIntervalsChanged(QString num); //signal: number of intervals has changed
        void numCritEdgesPosChanged(QString num); //signal: number of positive critical edges has changed
        void numCritEdgesNegChanged(QString num); //signal: number of negative critical edges has changed
        void numCritTrianglesPosChanged(QString num); //signal: number of positive critical triangles  has changed
        void numCritTrianglesNegChanged(QString num); //signal: number of negative critical triangles has changed
        void numCritTetrahedraChanged(QString num); //signal: number of negative critical triangles has changed
        void betti0Changed(QString num); //signal: Betti number 0 has changed
        void betti1Changed(QString num); //signal: Betti number 1 has changed
        void betti2Changed(QString num); //signal: Betti number 2 has changed
        void maxPersistence0Changed(QString num); //signal: maximum persistence of dim 0 has changed
        void maxPersistence1Changed(QString num);
        void maxPersistence2Changed(QString num);
        void mouseCoordsChanged(QString coords); //signal: mouse coordinates changed (x,y)
        void showMessagePersDiag(bool persDiag_fulldrawn); //signal: full persistence diagram is drawn or only pairs with persistence above threshold
        void intervalGraphDrawn(bool intervalgraph_drawn); //signal: interval graph is drawn or not -> enable or disable button
        void plotsChanged(); //signal: new set of points, use for statistics plots update
        void sceneDrawn(); //finished drawScene
        void alphaChanged(); //alpha changed, new subcomplex
        void bregmanChanged(bool bregman); //changed from normal to Bregman geometry, or other way round
    
    public Q_SLOTS:
        void drawDelaunay(bool checked); //draw/not Delaunay triangulation
        void drawConvexHull(bool checked); //draw/not convex hull
        void drawUnionOfBalls(bool checked); //draw/not union of balls
        void showLabelsIntervals(bool checked); //show labels of intervals = labels of triangles
        void showLabelsPoints(bool checked); //show labels of points
        void showWeightedPointSpheres(bool checked); //show weighted points as spheres
        void updateAlpha(double new_alpha); //update alpha complex for new alpha value from slider or spin boxes
        void drawAlpha(bool checked); //draw/not Alpha complex
        void drawWrap(bool checked); //draw/not Wrap complex       
        void drawFull(bool checked); //draw full Wrap and Alpha complex 
        void changePersDiagSubcomplex(bool highlight_subcomplex); //show/hide box and red points to highlight current subcomplex in persistence diagram
        void showPersDiagConnections(bool checked); //whether to display connections between old and new persistence diagram after hole manipulations
        void drawBregmanOnInputPoints(bool checked); //change drawing of Bregman-Delaunay complexes between input points and transformed points 
        
    public: 
        void setAlphaComplex(std::vector<DataPoint> input_data_points_, Alpha_Complex* alpha_complex_, int drawing_window_size, double alpha2, bool compute_statistics); //alpha complex as input

        void clearPoints(); //clear list of points
        void clearScene(); //clear Alpha complex, scene items
        std::vector<DataPoint> getDataPoints(){return input_data_points;}
        std::vector<DataPoint>* getDataPointsPtr(){return &input_data_points;}
        bool setPoints(std::vector<DataPoint> data_points, bool draw, int periodic, bool weighted, std::string bregman_type, bool compute_persistence_and_statistics); //set new list of points, compute complexes
        bool recomputeWithUpdatedPoints(bool draw, bool compute_wrapstatistics, bool compute_persistence_and_statistics); //recompute complexes (from scratch) after point set has been changed
        void importFilteredComplexFromFile(std::string filename, bool draw); //import filtered complex from file
        DataPoint* getInputDataPoint(int point_index){if(point_index<input_data_points.size()){return &input_data_points[point_index];}}
        int getNumDataPoints(){return input_data_points.size()-free_indices_data_points.size();}
        int getMaxPointIndex(){return input_data_points.size()-1;}
        bool isBregman(){return bregman;}
        void setBregman(bool bregman_){if(bregman_!=bregman){bregman=bregman_;Q_EMIT(bregmanChanged(bregman));}}
        void setStandardSimplex(bool on_standard_simplex_){on_standard_simplex = on_standard_simplex_;}
        
        void drawScene(); //draw complexes and interval graph
        void displayDrawing(); //display current scene
        void setDrawingParameters(double pointsize, double pointsize_input, double linewidth, double labelsize, double pointsize_persdiag, double maxvalue_persdiag, int color_delaunay_by_radius, bool draw); //redraw 2-dim scene with new parameters 
        
        //generate point set from Poisson point process in window [0,size]^2 with given parameter, repeat given number of times, optionally draw last trial, optionally periodic point set (on flat torus), optionally in Bregman geometry, perturbed point set and more
        void generatePointsPPP(double window_size, double parameter, bool fixed_number, int ntrials, bool weighted, double min_weight, double max_weight, bool draw, bool periodic, bool dim3, bool on_standard_simplex, bool uniform_for_fisher, bool compute_persistence_and_statistics, std::string test, double test_value); 
        
        //point manipulation
        bool addPoint(double x, double y, double z, bool recompute, bool test, bool printstats); //add point to point set
        bool deletePoint(int point_index, bool recompute, bool test, bool printstats); //delete point from point set
        bool movePoint(int point_index, double x, double y, double z, bool recompute); //move existing point
        
        void testIncrementalPointAddition(std::vector<DataPoint> input_data_points, int periodic); //test running time of local vs global point operation for incrementally adding all points
        //void testPersistenceMatrices(); 
        void computeResultsPaperHoles(){alpha_complex->computeResultsPaperHoles(std::cout);}
        void testDynamicRuntime(double window_size, double lambda, int num_trials, bool periodic, bool dim3, bool draw); //compare dynamic point insertion and deletion to static computation
        void computeStatisticsPPP(double window_size, double lambda, int num_trials, bool periodic, bool dim3, bool draw); //run trials to get data for statistics on ppp
        
        //hole operations
        void holeOperation(bool lock, bool un, int dim, int pers_pair_num, bool subcomplex, double persistence_bound, bool lowerbound, bool printinfo); //perform hole operation on given persistence pair or above/below given bound
        void undoHoleOperations();
        int getLastHoleOperationSimplex(){return alpha_complex->getLastHoleOperationSimplex();}
        void setHoleOperationsCohomology(bool hole_operation_cohomology){alpha_complex->setHoleOperationsCohomology(hole_operation_cohomology);wrap_complex->setHoleOperationsCohomology(hole_operation_cohomology);}
        
        void recomputeWrapWithThreshold(double threshold, bool printinfo); //recompute intervals (-> Wrap complex) with prescribed imprecision threshold, as with noisy filtration values
        
        void computeCircumcentersMaxSimplices(std::string type, bool restrict_to_domain, bool print); //compute circumcenters of maximum simplices in Bregman-Delaunay triangulation, either restrict to domain (delete all simplices with circumcenter outside of domain (positive quadrant/orthant or standard simplex)) or print as output
        
        void changePersDiag(bool persDiag_wrap_bool, bool adapted, bool status); //show persistence diagram for Alpha or Wrap complex depending on boolean, original, current or original with current status
        void changeStatisticPlot(QString title); //show statistic plot of given title
        void setStatisticPlotScale(bool plots_logscale_){plots_logscale=plots_logscale_;} //set scale of statistics plots
        
        void exportPointsToFile(std::string filename);  //export points to given file
        
        //update drawings of special points
        void updateOldPoint(bool show, int index);
        void updateNewPoint(bool show, double x, double y, double z);
        int getLastPointInserted(){return last_point_inserted;}
        
        //highlight
        PersistencePair getPersistencePairOfDim(int dim, int ranking, bool subcomplex, bool lock, bool un, bool wrap); //get ranking-th persistence pair of given dim, ordered by decreasing persistence
        double getPersistenceValueOfDim(int dim, int ranking, bool subcomplex, bool lock, bool un);
        void highlightChain(int dim, int pers_pair_ranking, bool lock, bool un, bool highlight_mode, bool subcomplex, QColor color1, QColor color2); //highlight canonical cycle (type 0), chain (type 1), cocycle (type 2) or cochain (type 3) of ranking-th persistence pair of given dimension, mode: true = highlight, false = only draw these simplices
        void highlightPersistencePairsWithBoundStatus(int dim, double persistence_bound, bool lowerbound, bool lock, bool un, bool subcomplex, QColor color); //highlight persistence pairs that will be manipulated for hole operation with persistence bound
        void highlightSimplices(std::vector<int>* simplices_to_highlight1,std::vector<int>* simplices_to_highlight2, bool highlight_mode, bool wrap, QColor color1, QColor color2); //highlight two sets of simplices in different colors, mode: true = highlight, false = only draw these simplices, subcomplex true: only for current subcomplex
        void highlightPersistencePoint(PersistencePair persistence_pair, QColor color, bool wrap); //highlight point in persistence diagram of wrap or alpha complex
        void clearHighlightedItems();
        
        void updatePersistenceDiagramStatusSelection(std::vector<bool> draw_dim, std::vector<bool> draw_status, int highlight_dependences_of_simplex, bool lock, bool un, bool recursive, bool original_status);
        
        void highlightDifferentSimplices(QString filename); //highlight simplices in the current (full) complex that do not belong to the complex stored in the given file
        void highlightPTree(int tree_dim, QColor color); //highlight p-tree (death simplices) of given dimension
        
        //view      
        double fitView(); //fit view to scene, return max(width,height)
        double fitView(int drawing_window_size); //fit view to scene of given size
        void fitViewThirdScene(); //fit view for third scene (persistence diagram)
        void fitViewFourthScene(); //fit view for fourth scene (statistic plots)
        //get drawing parameters
        double getPointSize(){if(!dim3){return pointsize;}else{return opengl_widget->getPointSize();}}
        double getPointSizeInput(){if(!dim3){return pointsize_input;}else{return opengl_widget->getPointSizeInput();}}
        double getLineWidth(){if(!dim3){return linewidth;}else{return opengl_widget->getLineWidth();}}
        int getLabelSize(){if(!dim3){return labelsize;}else{return opengl_widget->getLabelSize();}}
        double getPointSizePersDiag(){return pointsize_persdiag;}
        double getMaxValuePlots(){return maxvalue_plots;}
        int getRotationAngle(int dimension){if(!dim3){return -1;}else{if(dimension==0){return opengl_widget->getXRotation();}else if(dimension==1){return opengl_widget->getYRotation();}else{return opengl_widget->getZRotation();}}}
        void setRotationAngles(int x, int y, int z){if(dim3){opengl_widget->setXRotation(x);opengl_widget->setYRotation(y);opengl_widget->setZRotation(z);}}
        int getRotationAngleLight(int dimension){if(!dim3){return -1;}else{if(dimension==0){return opengl_widget->getXRotationLight();}else if(dimension==1){return opengl_widget->getYRotationLight();}else{return opengl_widget->getZRotationLight();}}}
        void setRotationAnglesLight(int x, int y, int z){if(dim3){opengl_widget->setXRotationLight(x);opengl_widget->setYRotationLight(y);opengl_widget->setZRotationLight(z);}}
        double getScale(){return opengl_widget->getScale();}
        void setScale(float scale_){opengl_widget->setScale(scale_);}
        int getTranslateX(){return opengl_widget->getTranslateX();}
        int getTranslateY(){return opengl_widget->getTranslateY();}
        void setTranslate(int translate_x, int translate_y){opengl_widget->setTranslateX(translate_x);opengl_widget->setTranslateY(translate_y);}
        bool getPaleTrianglesBool(){return opengl_widget->getPaleTrianglesBool();}
        void setPaleTrianglesBool(bool pale){opengl_widget->setPaleTrianglesBool(pale);}
        bool getFilterHighlightedBool(){return opengl_widget->getFilterHighlightedBool();}
        void setFilterHighlightedBool(bool filter){opengl_widget->setFilterHighlightedBool(filter);}
        int getColorDelaunayByRadius(){return color_delaunay_by_radius;}
        bool isUseRadius2(){return use_radius2;}
        void setUseRadius2(bool use_radius2_){use_radius2=use_radius2_;}
        
        //colors
        void setDefaultColors(); //set default colors for drawing
        QColor getColorPoints(){if(dim3){return opengl_widget->getColorPoints();} return color_points;}
        void setColorPoints(QColor color){if(dim3){opengl_widget->setColorPoints(color);}else{color_points = color;}}
        QColor getColorDelaunayPoints(){return color_delaunay_points;}
        void setColorDelaunayPoints(QColor color){color_delaunay_points = color;}
        QColor getColorDelaunayDark(){return color_delaunay_dark;}
        void setColorDelaunayDark(QColor color){color_delaunay_dark = color;}
        QColor getColorDelaunayLight(){return color_delaunay_light;}
        void setColorDelaunayLight(QColor color){color_delaunay_light = color;}
        QColor getColorAlphaDark(){if(dim3){return opengl_widget->getColorAlphaDark();} return color_alpha_dark;}
        void setColorAlphaDark(QColor color){if(dim3){opengl_widget->setColorAlphaDark(color);}else{color_alpha_dark = color;}}
        QColor getColorAlphaLight(){if(dim3){return opengl_widget->getColorAlphaLight();} return color_alpha_light;}
        void setColorAlphaLight(QColor color){if(dim3){opengl_widget->setColorAlphaLight(color);}else{color_alpha_light = color;}}
        QColor getColorAlphaPale(){if(dim3){return opengl_widget->getColorAlphaPale();} return Qt::black;}
        void setColorAlphaPale(QColor color){if(dim3){opengl_widget->setColorAlphaPale(color);}}
        QColor getColorWrapDark(){if(dim3){return opengl_widget->getColorWrapDark();} return color_wrap_dark;}
        void setColorWrapDark(QColor color){if(dim3){opengl_widget->setColorWrapDark(color);}else{color_wrap_dark = color;}}
        QColor getColorWrapLight(){if(dim3){return opengl_widget->getColorWrapLight();} return color_wrap_light;}
        void setColorWrapLight(QColor color){if(dim3){opengl_widget->setColorWrapLight(color);}else{color_wrap_light = color;}}
        QColor getColorWrapPale(){if(dim3){return opengl_widget->getColorWrapPale();} return Qt::black;}
        void setColorWrapPale(QColor color){if(dim3){opengl_widget->setColorWrapPale(color);}}
        QColor getColorOldPoint(){return color_old_point;}
        void setColorOldPoint(QColor color){opengl_widget->setColorOldPoint(color);color_old_point = color;}
        QColor getColorNewPoint(){return color_new_point;}
        void setColorNewPoint(QColor color){opengl_widget->setColorNewPoint(color);color_new_point = color;}
        QColor getColorWeightedPointsPos(){if(dim3){return opengl_widget->getColorWeightedPoints();} return color_weighted_points_pos;}
        void setColorWeightedPointsPos(QColor color){if(dim3){opengl_widget->setColorWeightedPoints(color);}else{color_weighted_points_pos = color;}}
        QColor getColorWeightedPointsNeg(){if(dim3){return opengl_widget->getColorWeightedPoints();} return color_weighted_points_neg;}
        void setColorWeightedPointsNeg(QColor color){if(dim3){opengl_widget->setColorWeightedPoints(color);}else{color_weighted_points_neg = color;}}
        
        void printOutput(){wrap_complex->printOutput();}
        void printFullInformation(std::ostream& output){alpha_complex->printFullInformation(output);}
        void printAlphaComplexes(std::ostream& output){alpha_complex->printAlphaComplexes(output);}
        void printWrapComplexes(std::ostream& output){alpha_complex->printWrapComplexes(output);}
        void printSimplices(std::ostream& output);
        void printIntervals(){wrap_complex->printIntervals();}
        void printIntervalStatistics(){wrap_complex->computeIntervalStatisticsPaper();}  //print statistics for all intervals in filtration order, also print statistics about full complex and overlapping lower sets
        void printPersistenceBarcode(){wrap_complex->printPersistenceBarcode();} //print persistence barcode (same for wrap and alpha filtration)
        void printPersistencePairs(bool wrap, std::ostream& output){if(wrap){wrap_complex->printPersistencePairs(output);}else{alpha_complex->printPersistencePairs(output);}} //print persistence pairs for wrap or alpha filtration
        void printBoundaryMatrix(bool wrap, std::ostream& output){if(wrap){wrap_complex->printBoundaryMatrix(output);}else{alpha_complex->printBoundaryMatrix(output);}} //print boundary matric
        void printPersistenceMatrices(bool wrap, std::ostream& output){if(wrap){wrap_complex->printPersistenceMatrices(output);}else{alpha_complex->printPersistenceMatrices(output);}} //print persistence matrices for wrap or alpha filtration
        void printAlphaSimplicesPartition(){alpha_complex->printAlphaSimplicesPartition();} //print tri-partition of alpha simplices
        
        bool isDim3(){return dim3;}
        void setDim3(bool dim3_){if(input_data_points.size()==0){dim3=dim3_;}}
        bool isEmpty(){return input_data_points.size()==0;}
        bool isWeighted(){return alpha_complex->isWeighted();}
        bool isPeriodic(){return alpha_complex->isPeriodic();}
    
    protected:
        //mouse interactions
        void mousePressEvent(QMouseEvent *event); //click -> draw point
        void mouseMoveEvent(QMouseEvent *event); //show mouse coordinates 
        
    private:
        void init(); //initialize scenes and drawing
        void drawPersistenceDiag(bool wrap, bool adapted, bool fill_by_status, std::vector<bool> draw_pair_of_birth_simplex, int highlight_point_birth_index); //draw persistence diagram to third scene, for wrap or alpha complex, adapted or original
        void drawPersistenceDiagConnection(std::pair<double,double> start, std::pair<double,double> end, bool wrap); //draw connecting arrow between points in old and new persistence diagram
        void updateVisibleGeometry(); //update visibility of groups of simplices depending on bools (Wrap, Alpha, Delaunay, Bregman, labels, ...)
};

#endif // GEOMETRY_H