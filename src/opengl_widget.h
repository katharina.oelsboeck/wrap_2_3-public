//opengl_widget.h
//author: koelsboe

#ifndef OPENGL_WIDGET_H
#define OPENGL_WIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

#include <QMatrix4x4>
#include <QVector3D>
#include <QVector4D>
#include <qmath.h>

#include <QMouseEvent>

#include "basics.h"

class OpenGL_Widget: public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
            
    public:
        OpenGL_Widget(QWidget *parent = 0); //constructor
        
        //recommended minimum size for the widget
        QSize minimumSizeHint() const override;
        //recommended size for the widget
        QSize sizeHint() const override;
        
        void clear();
        
        //new filtered simplicial complex to be drawn
        //store vertex coordinates, and vertex indices for all simplices of a filtration
        void setPoints(int periodic_size, bool weighted_, bool bregman_, std::vector<DataPoint>* data_points, int num_data_points, std::vector<DataPoint>* data_points_transformed, std::vector<Simplex>* simplices, std::vector<int>* interval_indices,
            Filtration* filtration_wrap, int filtration_index_wrap,           
            std::vector<int>* num_points_wrap_filtration_, std::vector<int>* num_edges_wrap_filtration_, std::vector<int>* num_triangles_wrap_filtration_, std::vector<int>* num_tetrahedra_wrap_filtration_,
            Filtration* filtration_alpha, int filtration_index_alpha,
            std::vector<int>* num_points_alpha_filtration_, std::vector<int>* num_edges_alpha_filtration_, std::vector<int>* num_triangles_alpha_filtration_, std::vector<int>* num_tetrahedra_alpha_filtration_,
                std::vector<exact>* alpha_filtration_values_, std::vector<exact>* wrap_filtration_values_);
        
        //change filtration value alpha, filtration indices given
        void updateAlpha(int filtration_index_wrap_, int filtration_index_alpha_);
        
        void drawAlpha(bool checked); //draw/not Alpha complex
        void drawWrap(bool checked); //draw/not Wrap complex       
        void drawFull(bool checked); //draw full Wrap and Alpha complex        
        void showLabelsIntervals(bool checked); //show/hide tetrahedron labels
        void showLabelsPoints(bool checked); //show/hide point labels
        void showWeightedPointsSpheres(bool checked); //show/hide sphere representation of weighted points
        void drawBregmanOnTransformedPoints(bool checked); //whether to draw Bregman-Delaunay on transformed points or input points
        
        void updateOldPoint(bool show, double x, double y, double z); //update old point for point manipulation
        void updateNewPoint(bool show, double x, double y, double z); //update new point for point manipulation
        
        void highlightSimplices(std::vector<int>* simplices_to_highlight1, std::vector<int>* simplices_to_highlight2, bool highlight_mode, bool wrap, QColor color1, QColor color2, std::vector<DataPoint>* data_points, std::vector<Simplex>* simplices); //highlight given chain of simplices
        void clearHighlighted();
        
        //get drawing parameters
        double getPointSize(){return pointsize;}
        double getPointSizeInput(){return pointsize_input;}
        double getLineWidth(){return linewidth;}
        int getLabelSize(){return labelsize;}
        
        void setDrawingParameters(double pointsize, double pointsize_input, double linewidth, double labelsize, bool draw); //redraw with new parameters 
        
        //fit scale of the scene to bounding sphere of points, reset translation and rotation
        void fitView();
        
    public Q_SLOTS:
        //clean up before destroying openGL context
        void cleanup(); 
        
        
    Q_SIGNALS:
        
    protected:
        //set up and draw with OpenGL
        void initializeGL() override; //initialize required OpenGL resources
        void resizeGL(int width, int height) override; //update after resizing the widget
        void paintGL() override; //paint objects
        
        //interactive navigation with mouse and keyboard
        void mousePressEvent(QMouseEvent *event) override; //when pressing a mouse button update its last position (necessary for mouseMoveEvent)
        void mouseMoveEvent(QMouseEvent *event) override; //when moving the pressed mouse, rotate the scene, left button: z-axis, right button: y-axis
        void wheelEvent(QWheelEvent *event) override; //zoom in and out with mouse wheel
        void keyPressEvent(QKeyEvent *keyEvent) override; //move camera with arrow keys
        
    private:
        
        bool wrap_bool; //whether to draw Wrap complex 
        bool alpha_bool; //whether to draw Alpha complex
        bool full_bool; //whether to draw full Wrap and Alpha complex (alpha infinity)
        bool labels_intervals_bool; //whether to draw labels for tetrahedra
        bool labels_points_bool; //whether to draw labels for points
        bool pale_triangles_bool; //whether triangles with no incident tetrahedra should be drawn pale
        bool weighted_points_spheres_bool; //whether to draw weighted input points as spheres
        bool draw_bregman_on_transformed_points_bool; //whether to draw on transformed points instead of input points (differ for Bregman-Delaunay)
        bool color_by_radius_bool; //draw Alpha and Wrap complex with colors depending on radius
        double max_value_color_gradient; //color gradient from min_value of filtration to this value if given, range to max_value of filtration if -1
        
        bool initializedGL; //opengl variables are set up already
        bool pointsWereSet; //setPoints was called (necessary before initializeGL)
        bool periodic;
        bool weighted;
        bool bregman; //Bregman-Delaunay complexes
        int highlight_mode; //0.. no highlight, 1.. additionally draw highlighted, 2.. only draw highlighted
        bool filter_highlighted; //whether to filter highlighted simplices
        
        bool show_oldpoint; //whether to show old point for point manipulation
        bool show_newpoint; //whether to show new point for point manipulation
        
        std::vector<GLfloat> vertex_data; //array of vertex coordinates
        std::vector<GLfloat> vertex_data_weighted_points_spheres;
        std::vector<GLfloat> vertex_data_bregman_transformed; //transformed points for Bregman-Delaunay
        //indices of points in filtration order
        std::vector<unsigned int> indicesPointsAlpha; 
        std::vector<unsigned int> indicesPointsWrap;
        std::vector<unsigned int> indicesWeightedPointsSpheres;
        //indices of vertices for all simplices of Wrap complex
        std::vector<unsigned int> indicesEdgesWrap; //indices of edge vertices
        std::vector<unsigned int> indicesTrianglesWrap; //indices of triangle vertices
        std::vector<unsigned int> indicesTetrahedraWrap; //indices of tetrahedron vertices
        //indices of vertices for all simplices of Alpha complex
        std::vector<unsigned int> indicesEdgesAlpha; //indices of edge vertices
        std::vector<unsigned int> indicesTrianglesAlpha; //indices of triangle vertices
        std::vector<unsigned int> indicesTetrahedraAlpha; //indices of tetrahedron vertices
        //indices of vertices for all highlighted simplices
        std::vector<unsigned int> indicesPointsHighlighted1;
        std::vector<unsigned int> indicesEdgesHighlighted1; //indices of edge vertices
        std::vector<unsigned int> indicesTrianglesHighlighted1; //indices of triangle vertices
        std::vector<unsigned int> indicesTetrahedraHighlighted1; //indices of tetrahedron vertices
        std::vector<unsigned int> indicesPointsHighlighted2;
        std::vector<unsigned int> indicesEdgesHighlighted2; //indices of edge vertices
        std::vector<unsigned int> indicesTrianglesHighlighted2; //indices of triangle vertices
        std::vector<unsigned int> indicesTetrahedraHighlighted2; //indices of tetrahedron vertices
        
        int num_data_points; //number of points (without deleted)
        std::vector<DataPoint>* data_points; //list of points
        Filtration* filtration_alpha; //filtration of alpha complex (need for coloring with radius)
        Filtration* filtration_wrap; //filtration of wrap complex (need for coloring with radius)
        std::vector<Simplex>* simplices; //simplices
        std::vector<exact>* alpha_filtration_values;
        std::vector<exact>* wrap_filtration_values;
        
        int filtration_index_wrap; //current filtration index for Wrap complex
        int filtration_index_alpha; //current filtration index for Alpha complex       
        //for every filtration index: number of points, edges, triangles and tetrahedra, only these (prefixes of arrays) will be drawn
        std::vector<int>* num_points_alpha_filtration;
        std::vector<int>* num_points_wrap_filtration;
        std::vector<int>* num_edges_wrap_filtration;
        std::vector<int>* num_triangles_wrap_filtration;
        std::vector<int>* num_tetrahedra_wrap_filtration; 
        std::vector<int>* num_edges_alpha_filtration;
        std::vector<int>* num_triangles_alpha_filtration;
        std::vector<int>* num_tetrahedra_alpha_filtration; 
        std::vector<int> num_points_highlighted1_filtration;
        std::vector<int> num_edges_highlighted1_filtration;
        std::vector<int> num_triangles_highlighted1_filtration;
        std::vector<int> num_tetrahedra_highlighted1_filtration;
         std::vector<int> num_points_highlighted2_filtration;
        std::vector<int> num_edges_highlighted2_filtration;
        std::vector<int> num_triangles_highlighted2_filtration;
        std::vector<int> num_tetrahedra_highlighted2_filtration;
        
        std::vector<std::pair<int,QVector3D> > labels_intervals_coords; //list of tetrahedra interval labels with coordinates (centroid)
        std::vector<QVector3D> labels_points_coords; //list of point label coordinates
        std::vector<std::pair<int,QVector3D> > labels_intervals_coords_bregman_transformed; //... for Bregman-Delaunay on transformed points
        std::vector<QVector3D> labels_points_coords_bregman_transformed; 
        
        QOpenGLShaderProgram *shader_program; //shader program (vertex and fragment shader combined)
        //VAO = openGL object that stores all of the state needed to supply data for drawing (vertex data, buffers)
        //as we want to draw subsets (according to filtration), we store simplices of different dim separately, only draw preset for each 
        QOpenGLVertexArrayObject VAOPointsWrap;
        QOpenGLVertexArrayObject VAOEdgesWrap;
        QOpenGLVertexArrayObject VAOTrianglesWrap;
        QOpenGLVertexArrayObject VAOTetrahedraWrap;
        QOpenGLVertexArrayObject VAOPointsAlpha;
        QOpenGLVertexArrayObject VAOEdgesAlpha;
        QOpenGLVertexArrayObject VAOTrianglesAlpha;
        QOpenGLVertexArrayObject VAOTetrahedraAlpha;
        QOpenGLVertexArrayObject VAOOldPoint;
        QOpenGLVertexArrayObject VAONewPoint;
        QOpenGLVertexArrayObject VAOWeightedPointsSpheres;
        QOpenGLVertexArrayObject VAOPointsHighlighted1;
        QOpenGLVertexArrayObject VAOEdgesHighlighted1;
        QOpenGLVertexArrayObject VAOTrianglesHighlighted1;
        QOpenGLVertexArrayObject VAOTetrahedraHighlighted1;
        QOpenGLVertexArrayObject VAOPointsHighlighted2;
        QOpenGLVertexArrayObject VAOEdgesHighlighted2;
        QOpenGLVertexArrayObject VAOTrianglesHighlighted2;
        QOpenGLVertexArrayObject VAOTetrahedraHighlighted2;
        //VBO = buffer holding vertex data (coordinates)
        QOpenGLBuffer vertexBuffer;
        QOpenGLBuffer VBOOldPoint;
        QOpenGLBuffer VBONewPoint;
        QOpenGLBuffer VBOWeightedPointsSpheres;
        //index buffers (when rendering objects just specify indices of vertices (in vertex buffer) not coordinates)
        QOpenGLBuffer indexBufferPointsWrap; 
        QOpenGLBuffer indexBufferEdgesWrap; 
        QOpenGLBuffer indexBufferTrianglesWrap;
        QOpenGLBuffer indexBufferTetrahedraWrap;
        QOpenGLBuffer indexBufferPointsAlpha;
        QOpenGLBuffer indexBufferEdgesAlpha; 
        QOpenGLBuffer indexBufferTrianglesAlpha;
        QOpenGLBuffer indexBufferTetrahedraAlpha;
        QOpenGLBuffer indexBufferWeightedPointsSpheres;
        QOpenGLBuffer indexBufferPointsHighlighted1;
        QOpenGLBuffer indexBufferEdgesHighlighted1;
        QOpenGLBuffer indexBufferTrianglesHighlighted1;
        QOpenGLBuffer indexBufferTetrahedraHighlighted1;
        QOpenGLBuffer indexBufferPointsHighlighted2;
        QOpenGLBuffer indexBufferEdgesHighlighted2;
        QOpenGLBuffer indexBufferTrianglesHighlighted2;
        QOpenGLBuffer indexBufferTetrahedraHighlighted2;
        
        //rotation angles, translation values and scale factor for the scene (can be changed interactively)
        int rotate_x; 
        int rotate_y;
        int rotate_z;
        int translate_x;
        int translate_y;
        float scale;
        int rotate_x_light;
        int rotate_y_light;
        int rotate_z_light;
        //last position of mouse (necessary for navigating in scene)
        QPoint last_mouse_pos;
        
        //locations of uniform variables for shaders
        int projMatrix_loc;
        int modelViewMatrix_loc;
        int color_loc;
        int shading_loc;
        int pointsize_loc;
        
        QMatrix4x4 modelMatrix; //model matrix, transforms from object to world space
        QMatrix4x4 viewMatrix; //view matrix, transforms from world to view space (depending on camera location)
        QMatrix4x4 projMatrix; //projection matrix, transforms from view space to screen  
        QMatrix4x4 lightMatrix; //matrix to transform light position, used for rotation
        
        QVector3D center; //centroid of points, used to center the scene
        QVector3D center_bregman_transformed; //centroid of transformed points for Bregman-Delaunay, used to center the scene
        double max_coord; //maximum absolute coordinate value over all points (radius of bounding sphere), used to fit the input to the screen by zooming
        double max_coord_bregman_transformed;
        
        //drawing parameters
        double pointsize;
        double pointsize_input;
        double linewidth;   
        int labelsize;
        
        //colors
        QColor color_points;
        QColor color_alpha_dark;
        QColor color_alpha_light;
        QColor color_alpha_pale;
        QColor color_wrap_dark;
        QColor color_wrap_light;
        QColor color_wrap_pale;
        QColor color_old_point; 
        QColor color_new_point;
        QColor color_weighted_points;
        
        //highlighting
        QColor highlight_color1;
        QColor highlight_color2;
        
    private:
        //get number of edges, triangles, tetrahedra for given filtration index
        int getNumPointsWrap(){if(num_data_points==0 || filtration_index_wrap==0||num_points_wrap_filtration->size()==0){return 0;}else{return num_points_wrap_filtration->at(filtration_index_wrap-1);}}
        int getNumEdgesWrap(){if(num_data_points==0 || filtration_index_wrap==0||num_edges_wrap_filtration->size()==0){return 0;}else{return num_edges_wrap_filtration->at(filtration_index_wrap-1);}}
        int getNumTrianglesWrap(){if(num_data_points==0 || filtration_index_wrap==0||num_triangles_wrap_filtration->size()==0){return 0;}else{return num_triangles_wrap_filtration->at(filtration_index_wrap-1);}}
        int getNumTetrahedraWrap(){if(num_data_points==0 || filtration_index_wrap==0||num_tetrahedra_wrap_filtration->size()==0){return 0;}else{return num_tetrahedra_wrap_filtration->at(filtration_index_wrap-1);}}
        int getNumPointsAlpha(){if(num_data_points==0 || filtration_index_alpha==0||num_points_alpha_filtration->size()==0){return 0;}else{return num_points_alpha_filtration->at(filtration_index_alpha-1);}}
        int getNumEdgesAlpha(){if(num_data_points==0 || filtration_index_alpha==0||num_edges_alpha_filtration->size()==0){return 0;}else{return num_edges_alpha_filtration->at(filtration_index_alpha-1);}}
        int getNumTrianglesAlpha(){if(num_data_points==0 || filtration_index_alpha==0||num_triangles_alpha_filtration->size()==0){return 0;}else{return num_triangles_alpha_filtration->at(filtration_index_alpha-1);}}
        int getNumTetrahedraAlpha(){if(num_data_points==0 || filtration_index_alpha==0||num_tetrahedra_alpha_filtration->size()==0){return 0;}else{return num_tetrahedra_alpha_filtration->at(filtration_index_alpha-1);}}
        int getNumPointsHighlighted1(){if(full_bool||!filter_highlighted){return num_points_highlighted1_filtration.back();}if(num_data_points==0 || filtration_index_alpha==0||num_points_highlighted1_filtration.size()==0){return 0;}else{return num_points_highlighted1_filtration.at(filtration_index_alpha-1);}}
        int getNumEdgesHighlighted1(){if(full_bool||!filter_highlighted){return num_edges_highlighted1_filtration.back();}if(num_data_points==0 || filtration_index_alpha==0||num_edges_highlighted1_filtration.size()==0){return 0;}else{return num_edges_highlighted1_filtration.at(filtration_index_alpha-1);}}
        int getNumTrianglesHighlighted1(){if(full_bool||!filter_highlighted){return num_triangles_highlighted1_filtration.back();}if(num_data_points==0 || filtration_index_alpha==0||num_triangles_highlighted1_filtration.size()==0){return 0;}else{return num_triangles_highlighted1_filtration.at(filtration_index_alpha-1);}}
        int getNumTetrahedraHighlighted1(){if(full_bool||!filter_highlighted){return num_tetrahedra_highlighted1_filtration.back();}if(num_data_points==0 || filtration_index_alpha==0||num_tetrahedra_highlighted1_filtration.size()==0){return 0;}else{return num_tetrahedra_highlighted1_filtration.at(filtration_index_alpha-1);}}
        int getNumPointsHighlighted2(){if(full_bool||!filter_highlighted){return num_points_highlighted1_filtration.back();}if(num_data_points==0 || filtration_index_alpha==0||num_points_highlighted2_filtration.size()==0){return 0;}else{return num_points_highlighted2_filtration.at(filtration_index_alpha-1);}}
        int getNumEdgesHighlighted2(){if(full_bool||!filter_highlighted){return num_edges_highlighted2_filtration.back();}if(num_data_points==0 || filtration_index_alpha==0||num_edges_highlighted2_filtration.size()==0){return 0;}else{return num_edges_highlighted2_filtration.at(filtration_index_alpha-1);}}
        int getNumTrianglesHighlighted2(){if(full_bool||!filter_highlighted){return num_triangles_highlighted2_filtration.back();}if(num_data_points==0 || filtration_index_alpha==0||num_triangles_highlighted2_filtration.size()==0){return 0;}else{return num_triangles_highlighted2_filtration.at(filtration_index_alpha-1);}}
        int getNumTetrahedraHighlighted2(){if(full_bool||!filter_highlighted){return num_tetrahedra_highlighted2_filtration.back();}if(num_data_points==0 || filtration_index_alpha==0||num_tetrahedra_highlighted2_filtration.size()==0){return 0;}else{return num_tetrahedra_highlighted2_filtration.at(filtration_index_alpha-1);}}
        //get number of edges, triangles, tetrahedra for full complexes
        int getNumPointsWrapFull(){if(num_data_points==0 || num_points_wrap_filtration->size()==0){return 0;}else{return num_points_wrap_filtration->back();}}
        int getNumEdgesWrapFull(){if(num_data_points==0 || num_edges_wrap_filtration->size()==0){return 0;}else{return num_edges_wrap_filtration->back();}}
        int getNumTrianglesWrapFull(){if(num_data_points==0 || num_triangles_wrap_filtration->size()==0){return 0;}else{return num_triangles_wrap_filtration->back();}}
        int getNumTetrahedraWrapFull(){if(num_data_points==0 || num_tetrahedra_wrap_filtration->size()==0){return 0;}else{return num_tetrahedra_wrap_filtration->back();}}
        int getNumPointsAlphaFull(){if(num_data_points==0 || num_points_alpha_filtration->size()==0){return 0;}else{return num_points_alpha_filtration->back();}}
        int getNumEdgesAlphaFull(){if(num_data_points==0 || num_edges_alpha_filtration->size()==0){return 0;}else{return num_edges_alpha_filtration->back();}}
        int getNumTrianglesAlphaFull(){if(num_data_points==0 || num_triangles_alpha_filtration->size()==0){return 0;}else{return num_triangles_alpha_filtration->back();}}
        int getNumTetrahedraAlphaFull(){if(num_data_points==0 || num_tetrahedra_alpha_filtration->size()==0){return 0;}else{return num_tetrahedra_alpha_filtration->back();}}
  
        void renderText(QVector3D word_coords, QString text, QPainter* painter); //draw 2d text at given projected 3d location
        
    public:
        //get/set rotation angle of scene in x/y/z-direction
        int getXRotation(){return rotate_x;};
        int getYRotation(){return rotate_y;};
        int getZRotation(){return rotate_z;};
        void setXRotation(int angle);
        void setYRotation(int angle);
        void setZRotation(int angle);
        //get/set rotation angle of scene in x/y/z-direction, for light source
        int getXRotationLight(){return rotate_x_light;};
        int getYRotationLight(){return rotate_y_light;};
        int getZRotationLight(){return rotate_z_light;};
        void setXRotationLight(int angle);
        void setYRotationLight(int angle);
        void setZRotationLight(int angle);
        //get/set scale and position of scene
        double getScale(){return scale;}
        void setScale(float scale_){scale=scale_;}
        int getTranslateX(){return translate_x;}
        int getTranslateY(){return translate_y;}
        void setTranslateX(int translate_x_){translate_x=translate_x_;}
        void setTranslateY(int translate_y_){translate_y=translate_y_;}
        void setPaleTrianglesBool(bool pale_triangles_bool_){pale_triangles_bool = pale_triangles_bool_;}
        bool getPaleTrianglesBool(){return pale_triangles_bool;}
        void setFilterHighlightedBool(bool filter_highlighted_){filter_highlighted=filter_highlighted_;}
        bool getFilterHighlightedBool(){return filter_highlighted;}
        
        //colors
        void setDefaultColors(); //set default colors for drawing
        QColor getColorPoints(){return color_points;}
        void setColorPoints(QColor color){color_points = color;}
        QColor getColorAlphaDark(){return color_alpha_dark;}
        void setColorAlphaDark(QColor color){color_alpha_dark = color;}
        QColor getColorAlphaLight(){return color_alpha_light;}
        void setColorAlphaLight(QColor color){color_alpha_light = color;}
        QColor getColorAlphaPale(){return color_alpha_pale;}
        void setColorAlphaPale(QColor color){color_alpha_pale = color;}
        QColor getColorWrapDark(){return color_wrap_dark;}
        void setColorWrapDark(QColor color){color_wrap_dark = color;}
        QColor getColorWrapLight(){return color_wrap_light;}
        void setColorWrapLight(QColor color){color_wrap_light = color;}
        QColor getColorWrapPale(){return color_wrap_pale;}
        void setColorWrapPale(QColor color){color_wrap_pale = color;}
        QColor getColorOldPoint(){return color_old_point;}
        void setColorOldPoint(QColor color){color_old_point = color;}
        QColor getColorNewPoint(){return color_new_point;}
        void setColorNewPoint(QColor color){color_new_point = color;}
        QColor getColorWeightedPoints(){return color_weighted_points;}
        void setColorWeightedPoints(QColor color){color_weighted_points = color;}
        
        void setColorByRadius(bool color_by_radius_bool_){color_by_radius_bool = color_by_radius_bool_;}
        void setMaxValueColorGradient(double max_value){max_value_color_gradient = max_value;}
};

#endif /* OPENGL_WIDGET_H */

