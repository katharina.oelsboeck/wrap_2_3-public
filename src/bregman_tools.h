#pragma once //CHANGED

//author: hwagner

#include <Eigen/Core>
#include <Eigen/Dense>
#include <iostream>
#include <vector>
#include <functional>
#include <iomanip>

#include "bregman_convex_optimize.h"

using real = double;
using vec = Eigen::VectorXd;

using F_fun_t = std::function<real(const vec&)>;
using F_prime_fun_t = std::function<vec(const vec&)>; 

using hyperplane_fun_t = std::function<vec(const vec&)>;
using divergence_fun_t = std::function<real(const vec&, const vec&)>;

namespace misc {
  inline real sum_log_exp(vec x) {
    auto xs = x.maxCoeff();
    double s = 0;
    for (int i = 0; i < x.size(); i++)
      s += exp(x[i] - xs);
    return xs + log(s);
  }

  inline vec soft_max(vec x) {
    real xs = x.maxCoeff();
    real s = 0;
    for (int i = 0; i < x.size(); i++)
      s += exp(x[i]);
    for (int i = 0; i < x.size(); i++)
      x[i] = exp(x[i]) / s;
    return x;
  }
}

// Definitions needed for various Bregman divergences.
namespace BregmanFunctions {

// Info for Squared Euclidean distance.
struct EUCLSQ {
  static real F(const vec& x) {
    real s = 0;
    for (int i = 0; i < x.size(); i++)
      s += x[i] * x[i] / 2;
    return s;
  }
  static vec F_prime(const vec &x) {
    return x;
  }
  static vec F_conj_prime(const vec &x) {
    return x;
  }
};

// Info for Kullback-Leibler divergence.
struct KL {
  static real F(const vec& x) {
    real s = 0;
    
    for (int i = 0; i < x.size(); i++)
    {
      s += (x[i] == 0.) ? 0 : x[i]*log(x[i]) - x[i];
    }
    
    return s;
  }
  static vec F_prime(vec x) {
    for (int i = 0; i < x.size(); i++)
      x[i] = (x[i] == 0) ? -1e12 : log(x[i]);
    return x;
  }

  static vec F_conj_prime(vec x) {
    for (int i = 0; i < x.size(); i++)
      x[i] = std::min(exp(x[i]),1e13); //CHANGE
    return x;
  }
};

/* INSERTED START */
// Info for conjugate of Kullback-Leibler-divergence.
struct KL_conj {
  static real F(const vec& x) {
    real s = 0;
    for (int i = 0; i < x.size(); i++)
      s += std::min(exp(x[i]),1e13); //CHANGE
    return s;
  };
  static vec F_prime(vec x) {
    for (int i = 0; i < x.size(); i++)
      x[i] = std::min(exp(x[i]),1e13); //CHANGE
    return x;
  };

  static vec F_conj_prime(vec x) {
    for (int i = 0; i < x.size(); i++)
      x[i] = (x[i] == 0) ? -1e12 : log(x[i]);
    return x;
  };
};
/* INSERTED END */


// Info for conjugate of Kullback-Leibler-divergence-restricted-to-the-simplex.
struct KL_simp_conj {

  // 1 - x/d + log(exp(x)).sum()/d = 1 + sumlogexp(x)
  static real F(const vec &x) {

    real s = 1.; // not really necessary
    for (int i = 0; i < x.size(); i++)
      s += x[i] / x.size();

    real s2 = 0;
    for (int i = 0; i < x.size(); i++)
      s2 += exp(x[i]);
    
    return s + misc::sum_log_exp(x);
  }

  // exp(x)/(exp(x).sum()) = softmax(x)
  static vec F_prime(const vec &x) {
    return misc::soft_max(x);
  }

  // np.log(x) - np.log(x).sum()/d
  static vec F_conj_prime(const vec &x) {
    vec y = x;
    real s = 0;
    for (int i = 0; i < x.size(); i++)
      s += log(x[i]) / x.size();

    for (int i = 0; i < x.size(); i++)
      y[i] = log(x[i]) - s;

    return y;
  }
};

}


// Returns a function computing the Bregman divergence for a F and F'.
// For simplicity it assumes F is decomposable, and all components are the same.
inline divergence_fun_t make_bregman_divergence(F_fun_t F, F_prime_fun_t F_prime) {
  return [=](const vec& p, const vec& q) -> real {
    assert(p.size() == q.size());
    real s = F(p) - F(q);
    for (size_t i = 0; i < p.size(); i++)
      s +=  -(p[i] - q[i])*F_prime(q)[i];
    return s;
  };
}

// Returns a function computing the Bregman divergence based 
// on a structure containing information about the divergence.
template<typename BregmanF_t>
inline divergence_fun_t make_bregman_divergence(BregmanF_t bregman) {
  return make_bregman_divergence(bregman.F, bregman.F_prime);
}

// Computes a point along a convex hull between x, y.
template<typename T>
T ch(const T &x, const T&y, real t) {
  return x + t*(y - x);
}

// Computes the smallest circumsphere for (k+1) points in k-space 
// for a specified Bregman divergence.
// It returns a pair: radius and vector representing the center.
template<typename BregmanF_t>
std::pair<real, vec> smallest_circumsphere(const std::vector<vec> &Q, BregmanF_t bregman)
{
  auto new_col = vec::Ones(Q.size());
  Eigen::MatrixXd M(Q.size(), Q.size());

  vec b = vec::Zero(Q.size());

  for (size_t i = 0; i < b.size(); i++)    
      b[i] = bregman.F(Q[i]);

  for (size_t i = 0; i < Q.size(); i++)
  {
    for (size_t j = 0; j < Q[0].size(); j++)
    {
      M(i, j) = Q[i][j];
    }

    M(i, Q[0].size()) = -1;
  }

  vec xs = M.fullPivLu().solve(b).transpose();  
  vec x(xs.size() - 1);   

  // skipping the last coord!
  for (size_t i = 0; i < xs.size() - 1; i++)
    x[i] = xs[i];

  x = bregman.F_conj_prime(x);  

  auto div = make_bregman_divergence(bregman);
  auto r = div(Q[0], x); // for example
  
  return{ r, x };
}

// Computes the smallest circumsphere for two points p,q in arbitrary dimension,
// for a specified Bregman divergence.
// It returns a pair: radius and vector representing the center.
template<typename BregmanF_t>
std::pair<real, vec> smallest_circumsphere(vec p, vec q, BregmanF_t bregman, real eps = 1e-9)
{
  real t1 = 0., t2 = 1.;

  auto P = bregman.F(p);
  auto Q = bregman.F(q);  

  while (t2 - t1 > eps)
  {
    auto m1 = ch(t1, t2, 1/3.);
    auto m2 = ch(t1, t2, 2/3.);
    auto x1 = ch(p, q, m1);
    auto x2 = ch(p, q, m2);
    auto F1 = ch(P, Q, m1);
    auto F2 = ch(P, Q, m2);

    auto r1 = F1 - bregman.F(x1);
    auto r2 = F2 - bregman.F(x2);    

    if (r1 < r2)
      t1 = m1;
    else
      t2 = m2;
  }

  auto x = ch(p, q, t1);
  auto F = ch(P, Q, t1);
  auto r = F - bregman.F(x);

  return{ r, x };
}


/*int main() {
  Eigen::Vector3d a{ 0.6, 0.3, 0.1 };
  Eigen::Vector3d b{ 0.7, 0.1, 0.2};
  Eigen::Vector3d c{ 0.2, 0.7, 0.1 };

  // Eigen::Vector3d d{ 8.3, 0.6, 0.4 };

  double e = 0.1;  


  auto BR = BregmanFunctions::KL_simp_conj();
  //auto BR = BregmanFunctions::EUCLSQ();
  // auto BR = BregmanFunctions::KL();


  // move to conjugate space (plane passing through 0)
  a = BR.F_conj_prime(a);
  b = BR.F_conj_prime(b);
  c = BR.F_conj_prime(c);
  // d = BR.F_conj_prime(d);

  auto div = make_bregman_divergence(BR);  
  
  std::vector<vec> Q = {a,b,c};

  for (auto q : Q)
    std::cout << q.transpose() << " " << std::endl;

  // we can't really use the algebraic method since we don't have full rank! :(
  // auto r_c = smallest_circumsphere(Q, BR);

  auto r_c = smallest_circumball_by_convex_minimization(Q, BR);

  auto r_c_conv = smallest_circumball_by_convex_minimization(Q, BR);

  std::cout << "radius: " << r_c.first << std::endl;
  std::cout << "center:  " << r_c.second.transpose() << std::endl;
  std::cout << "center CONV:  " << r_c_conv.second.transpose() << std::endl;
  std::cout << "radius CONV:  " << r_c_conv.first << std::endl;

  std::cout << "divergences from points to center: " << std::endl;
  for (auto p : Q) {
    const auto center = r_c.second;    
    std::cout << std::setprecision(10) << div(p, center) << std::endl;
    // std::cout << std::setprecision(10) << div(center, p) << std::endl;
    std::cout << "CONV:" << std::setprecision(10) << div(p, r_c_conv.second) << std::endl;
  }    

  std::cout << "\n\n for 2 points";  
  
  auto r_c2 = smallest_circumsphere(a, b, BR);

  auto r_c_conv2 = smallest_circumball_by_convex_minimization({a,b}, BR);

  std::cout << "radius: " << r_c2.first << std::endl;
  std::cout << "center:  " << r_c2.second.transpose() << std::endl;   

  std::cout << "center CONV:  " << r_c_conv2.second.transpose() << std::endl;

  for (auto p : {a,b}) {
    const auto center = r_c2.second;
    std::cout << std::setprecision(10) << div(p, center) << std::endl;
    std::cout << "CONV:" << std::setprecision(10) << div(p, r_c_conv2.second) << std::endl;
  }

  std::cout << std::endl;  
}*/

/* OUTPUT
0.828302  0.135155 -0.963457 
1.06622 -0.879686 -0.186539 
-0.186539   1.06622 -0.879686 
G: 0.569329  0.107231 -0.676561
G:-0.178844  0.384162 -0.205318
G:-0.398497 -0.191583   0.59008
G:-0.277259 -0.024536  0.301795
G:-0.286929 -0.029944  0.316873
G: -0.288219 -0.0290654   0.317284
G: -0.288511 -0.0287634   0.317274
G:  -0.28851 -0.0287595    0.31727
G: 0.569329  0.107231 -0.676561
G:-0.178844  0.384162 -0.205318
G:-0.398497 -0.191583   0.59008
G:-0.277259 -0.024536  0.301795
G:-0.286929 -0.029944  0.316873
G: -0.288219 -0.0290654   0.317284
G: -0.288511 -0.0287634   0.317274
G:  -0.28851 -0.0287595    0.31727
radius: 0.456021
center:    -0.28851 -0.0287595    0.31727
center CONV:    -0.28851 -0.0287595    0.31727
radius CONV:  0.456021
divergences from points to center: 
0.4560209517
CONV:0.4560209517
0.456020877
CONV:0.456020877
0.4560209399
CONV:0.4560209399


 for 2 points
G: 0.9472632946 -0.3722653703 -0.5749979243
G:0.7093411386 0.6425754423 -1.351916581
G: 0.8283022166   0.135155036 -0.9634572526
G: 0.8877827556 -0.1185551671 -0.7692275885
G: 0.9175230251 -0.2454102687 -0.6721127564
G: 0.9323931598 -0.3088378195 -0.6235553403
G: 0.9398282272 -0.3405515949 -0.5992766323
G: 0.9411056816   -0.34600049 -0.5951051916
G:  0.941095266 -0.3459560631 -0.5951392029
radius: 0.03811608964
center:     0.94109524 -0.3459559522 -0.5951392878
center CONV:    0.941095266 -0.3459560631 -0.5951392029
0.03811608442
CONV:0.03811610042
0.03811609543
CONV:0.03811607769
*/