//window.h
//author: koelsboe

#ifndef WINDOW_H
#define WINDOW_H

#include "basics.h"
#include "geometry.h"

class MySpinBox : public QSpinBox {
    Q_OBJECT
    private:
        int decimals;
        double spacing;
        int min_int; //the integer minimum is always shifted to 0 since negative values are not accepted

        int valueFromText(const QString& text) const {
            double double_value = text.toDouble();
            return ((int) double_value / spacing)-min_int;
        }

        QString textFromValue(int value) const {
            double double_value = (value+min_int) * spacing;
            return QString::number(double_value, 'f', decimals);
        }

    public:

        MySpinBox(QWidget *parent = 0) : QSpinBox(parent) {
            spacing = 1.;
            decimals = 0;
            min_int = 0;
            connect(this, SIGNAL(valueChanged(int)), this, SLOT(notifyValueChanged(int)));
        }

        void setDecimals(int decimals_) {
            if (decimals_ >= 0 && decimals_ != decimals) {
                decimals = decimals_;
                spacing = 1.;
                for (int i = 0; i < decimals; i++) {
                    spacing = spacing / 10.;
                }
            }
        }

        double getMinDouble() {
            return spacing * (double) (minimum()+min_int);
        }

        double getMaxDouble() {
            return spacing * (double) (maximum()+min_int);
        }

        void setRangeDouble(double minimum_double, double maximum_double) {
            int mini = ((int) std::floor(minimum_double / spacing) - 2);
            int maxi = ((int) std::ceil(maximum_double / spacing) + 2);
            min_int = mini;
            setRange(mini-min_int, maxi-min_int);
        }

    Q_SIGNALS:
        void valueChangedDouble(double value);

        public


    Q_SLOTS:
        void notifyValueChanged(int value) {
            double doubleValue = spacing * (double) (value+min_int);
            Q_EMIT valueChangedDouble(doubleValue);
        }
        
        void setValueDouble(double value) {
            int int_value = ((int) (std::ceil(value / spacing)))-min_int;
            if(int_value<0){
                int min_int_old = min_int;
                min_int = ((int) std::floor(value / spacing) - 2);
                setRange(0,maximum()+min_int_old-min_int);
                setValue(0);
            }else{
                setValue(int_value);
            }
        }
};

class QDoubleSlider : public QSlider {
    Q_OBJECT
    private:
        int decimals;
        double spacing;
        int min_int; //the integer minimum is always shifted to 0 since negative values are not accepted for spin box

    public:

        QDoubleSlider(QWidget *parent = 0) : QSlider(parent) {
            spacing = 1.;
            decimals = 0;
            min_int = 0;
            connect(this, SIGNAL(valueChanged(int)), this, SLOT(notifyValueChanged(int)));
        }

        void setDecimals(int decimals_) {
            if (decimals_ >= 0 && decimals_ != decimals) {
                decimals = decimals_;
                spacing = 1.;
                for (int i = 0; i < decimals; i++) {
                    spacing = spacing / 10.;
                }
            }
        }

        void setRangeDouble(double minimum_double, double maximum_double) {
            int mini = ((int) std::floor(minimum_double / spacing) - 2);
            int maxi = ((int) std::ceil(maximum_double / spacing) + 2);
            min_int = mini;
            setRange(mini-min_int, maxi-min_int);
        }

        double getMinimumDouble() {
            return (minimum()+min_int) * spacing;
        }

        double getMaximumDouble() {
            return (maximum()+min_int) * spacing;
        }

    Q_SIGNALS:
        void valueChangedDouble(double value);

        public


    Q_SLOTS:
        void notifyValueChanged(int value) {
            double doubleValue = spacing * (double) (value+min_int);
            Q_EMIT valueChangedDouble(doubleValue);
        }
        
        void setValueDouble(double value) {
            int int_value = ((int) (std::ceil(value / spacing)))-min_int;
            if(int_value<0){
                int min_int_old = min_int;
                min_int = ((int) std::floor(value / spacing) - 2);
                setRange(0,maximum()+min_int_old-min_int);
                setValue(0);
            }else{
                setValue(int_value);
            }
        }
};

class SelectColorButton : public QPushButton
{
    
    Q_OBJECT
    public:
        SelectColorButton( QWidget* parent ){
            connect( this, SIGNAL(clicked()), this, SLOT(changeColor()) );
        }

        void setColor( const QColor& color ){
            this->color = color;
            updateColor();
        }
        const QColor& getColor(){
            return color;
        }
        
        
    Q_SIGNALS:
        void colorChanged();

    public Q_SLOTS:
        void updateColor(){
            setStyleSheet( "background-color: " + color.name() );
        }
        void changeColor(){
            QColor newColor = QColorDialog::getColor(color,parentWidget());
            if ( newColor != color )
            {
                setColor( newColor );
                Q_EMIT colorChanged();
            }
        }

    private:
        QColor color;
};

//main window of the application, including menus, graphics view and interactive tools

class Window : public QMainWindow {
    Q_OBJECT
    public:
        explicit Window(QMainWindow *parent = 0); //for default input points
        explicit Window(std::vector<DataPoint> data_points, Alpha_Complex* alpha_complex, int drawing_window_size, QMainWindow *parent = 0); //for given input points
        void setAlphaComplex(std::vector<DataPoint> data_points, Alpha_Complex* alpha_complex, int drawing_window_size); //new alpha complex
        
    private:
        //central widget (Qt Object) 
        QWidget *centralWidget;
        //menu bar categories
        QMenu *menuInput;
        QMenu *menuExport;
        QMenu *menuOperation;
        QMenu *menuView;
        //menu bar elements
        QAction *actionClear2d;
        QAction *actionClear3d;
        QAction *actionLoadPoints;
        QAction *actionLoadPointsNoWeights;
        QAction *actionLoadPointsTest;
        QAction *actionRecomputeWith;
        QAction *actionGeneratePointsPPP;
        QAction *actionImportFilteredComplex;
        QAction *actionExportPoints;
        QAction *actionExportFullInformation;
        QAction *actionExportSimplices;
        QAction *actionExportFilteredComplex;
        QAction *actionExportIntervalInformation;
        QAction *actionExportPersistenceInformation;
        QAction *actionExportTripartition;
        QAction *actionPointManipulation;
        QAction *actionHoleOperation;
        QAction *actionUndoHoleOperations;
        QAction *actionRecomputeWrapWithThreshold;
        QAction *actionHighlightDifferences; 
        QAction *actionHighlightPTree; 
        QAction *actionSetAlphaRange;
        QAction *actionFitAlphaRangeAlpha;
        QAction *actionFitAlphaRangeWrap;
        QAction *actionFitView;
        QAction *actionSetView;
        QAction *actionRedraw;
        QAction *actionChangeColors;
        QAction *actionExportImagePNG;
        QAction *actionExportImageSVG;
        QAction *actionExportImagePersDiag;
        //additional windows
        QWidget *secondWindow;
        QWidget *thirdWindow;
        QWidget *fourthWindow;
        QWidget *pointManipulationWindow;
        QWidget *holeOperationWindow;


    Q_SIGNALS:

        private Q_SLOTS :
        void clear2d(); //remove all points and change to 2d-view   
        void clear3d(); //remove all points and change to 3d-view   
        void loadPointsFromFileStandard(); //load points from file containing list of 2D or 3D coordinates 
        void loadPointsFromFileNoWeights(); //load points from file containing list of 2D or 3D coordinates, ignore any weights
        void recomputeWith(); //recompute complexes for current point set using Euclidean metric, Bregman divergence or Fisher metric
        void loadPointsFromFileTest(); //load points from file containing list of 2D or 3D coordinates, perform some test
        void generatePointsPPP(); //generate points from Poisson point process with specified parameters
        void importFilteredComplexFromFile(); //load filtered complex that was stored to file
        
        void exportPoints(); //write current point set to file
        void exportFullInformation(); //write full information about alpha and wrap complexes to file
        void exportSimplices(); //write set of simplices to file
        void exportFilteredComplex(); //write filtered complex (alpha or wrap) to file, can later be imported
        void exportIntervalInformation(); //write info about intervals to file 
        void exportPersistenceInformation(); //write info about persistence to file
        void exportTripartition(); //write tri-partition of alpha simplices to file
        
        void startHoleOperations(); //open window to highlight canonical (co)cycles/chains and perform hole operations
        void applyHoleOperation(); //perform hole operation
        void exitHoleOperations(); //close window to perform hole operations and highlight canonical (co)cycles/chains
        void updateHoleOperationWindow(); //update hole operation window after any changes
        void highlightChain(); //highlight chain chosen with current window settings
        void updateHoleOperationSubcomplexMode(); //whether to perform operations on full complex or subcomplex
        void updateHoleOperationCohomology(); //hole operations with dependence structure of homology or cohomology
        void updateHighlightPersistenceValue(); //update value of currently chosen persistence pair, for subcomplex
        void undoHoleOperations(); //undo all hole operations
        void updatePersistenceDiagramStatusSelection(); //update how to draw persistence diagram with status information
        
        void recomputeWrapWithThreshold(); //recompute intervals (-> Wrap complex) with prescribed imprecision threshold, as with noisy filtration values
        void highlightDifferences(); //highlight differences to complex from input file
        void highlightPTree(); //highlight p-tree (p-dimensional death simplices)

        void loadPointsFromFile(int test, bool noweights); //load points from file containing list of 2D or 3D coordinates  
        void chooseAlphaRange(); //set range of alpha slider
        void fitAlphaRangeAlpha(); //fit range of alpha slider to critical values of current Alpha complex
        double fitView(); //fit graphics view to current points
        void setView(); //set viewing rectangle
        void redraw(); //redraw scene with different parameters (linewidth, point size)
        void changeColors(); //redraw scene with different colors
        void exportImagePNG(); //export PNG-image
        void exportImageSVG(); //export SVG-image (only 2D)
        void exportImagePersDiag(); //export SVG-image of persistence diagram
        
        void showIntervalGraph(); //open second window to display interval graph of complex
        void showPersDiag(); //open third window to display persistence diagram of current Wrap complex
        void showPlots(); //open fourth window to display plots of statistics over whole alpha range
        void changePersDiag(); //radio buttons decide if persistence diagram for Alpha or Wrap complex is shown
        void changeStatisticPlot(); //radio buttons decide which statistic plot is shown
        void drawBregmanOnInputPoints(bool on_input_points); //change between drawing on input points or transformed points for bregman geometry
        void updatePointManipulationWindowNewPoints(); //update point manipulation window after new set of points was drawn
        void updatePointManipulationLayout(); //update layout of point manipulation window depending on chosen operation
        void updateOldPoint(); //update displayed coordinates and drawing of old point
        void updateNewPoint(); //update drawing of new point
        void startPointManipulation(); //open point manipulation window
        void applyPointManipulation(); //apply point manipulation operation as specified in interactive window
        void exitPointManipulation(); //exit point manipulation window
        void deleteLastPoint(); //delete last inserted point
        void setOldPointNumMaximum(int num){spinBoxOldPointNum->setMaximum(num);}; //change maximum value of spin box for old point in point manipulation
          
    public Q_SLOTS:
        void fitAlphaRangeWrap(); //fit range of alpha slider to critical values of Wrap complex
        void updateLayoutDim(bool dim3); //update layout of window to 2-dim or 3-dim view depending on current points
        void updateLayoutWeighted(bool weighted); //update alpha label and window layout depending on whether points are weighted
        void updateLayoutBregman(bool bregman); //update layout of window to Bregman or normal (Euclidean) geometry depending on current complex
        void assignLayoutShowHideStatistics(); //assign layout to main window, with or without statistics data
        

    private:
        Geometry *geometry; //class in which geometric elements are computed and drawn

        bool show_statistics; //whether to show or hide statistics in main window
        bool holeoperations_highlight; //whether to highlight for hole operations
        bool holeoperations_subcomplex; //whether to only highlight chains in subcomplex
        bool holeoperations_cohomology; //perform hole operations on subcomplex with cohomology dependence structure

        //interactive elements

        //Delaunay triangulation
        QCheckBox *checkBoxDelaunay; //checkbox: display/not Delaunay triangulation of points
        QCheckBox *checkBoxConvexHull; //checkbox: display/not convex hull of points (2d)
        QCheckBox *checkBoxUnionOfBalls; //checkbox: display/not union of balls of points
        QCheckBox *checkBoxLabelsIntervals; //checkbox: display/not interval indices
        QCheckBox *checkBoxLabelsPoints; //checkbox: display/not point indices
        QCheckBox *checkBoxWeighted; //checkbox: display weighted points as spheres

        //Alpha and Wrap complex a       
        double current_alpha;
        QDoubleSlider *sliderAlpha; //slider to choose alpha value
        MySpinBox *spinBoxAlpha; //spin box to choose alpha value
        QCheckBox *checkBoxAlpha; //checkbox: display/not Alpha complex
        QCheckBox *checkBoxWrap; //checkbox: display/not Wrap complex
        QCheckBox *checkBoxFull; //checkbox: show full Alpha and Wrap complex
        QPushButton *buttonShowIntervalGraph; //button to open window with interval graph

        //Persistence
        QRadioButton *radioButtonAlphaPers; //radio buttons to choose whether persistence is computed for Alpha or Wrap complex
        QRadioButton *radioButtonWrapPers;
        QRadioButton *radioButtonOldPersDiag; //radio buttons to choose whether original, current persistence diagram or original with current status is shown
        QRadioButton *radioButtonNewPersDiag;
        QRadioButton *radioButtonStatusPersDiag;
        QCheckBox *checkBoxPersDiagConnections; //show connections for manipulated holes in persistence diagram
        QCheckBox *checkBoxPersDiagSubcomplex; //draw box for subcomplex in persistence diagram, highlight active homology
        QPushButton *buttonShowPersDiag; //button to open window with persistence diagram
        std::vector<QCheckBox*> checkBoxPersDiagStatusDrawDim; //whether to draw persistence points of specific dimension
        std::vector<QCheckBox*> checkBoxPersDiagStatusDrawStatus; //whether to draw persistence points of specific status
        QCheckBox* checkBoxPersDiagStatusDrawDependentPairs; //whether to only draw dependent (first level) pairs (specified by current hole operation)
        QCheckBox* checkBoxPersDiagStatusDrawRecursivelyDependentPairs; //whether to only draw dependent (recursively) pairs (specified by current hole operation)
        QCheckBox* checkBoxPersDiagStatusDrawDependentPairsLastOperation; //check if we want to highlight dependent points of last operation

        //Statistics plots
        QPushButton *buttonShowPlots; //button to open window with statistics plots
        std::vector<QRadioButton*> radioButtonsPlotTitles23; //radio buttons to choose which statistics should be plotted, for 2d and 3d
        std::vector<QRadioButton*> radioButtonsPlotTitles3; //radio buttons to choose which statistics should be plotted, only for 3d
        std::vector<QRadioButton*> radioButtonsPlotScale; //radio buttons to choose between normal and log scale

        //show statistics
        QPushButton *buttonShowHideStatistics; //button to either show or hide statistics in main window

        //manipulate points
        QCheckBox *checkBoxRecompute; //checkbox: point manipulation by recomputing everything or local updates
        std::vector<QRadioButton*> radioButtonsManipulatePoints; //radio buttons to choose between operations to manipulate points
        QSpinBox *spinBoxOldPointNum; //choose index of old point
        QLineEdit *lineEditNewPointX; //choose coordinates of new point
        QLineEdit *lineEditNewPointY;
        QLineEdit *lineEditNewPointZ;
        QPushButton *buttonApplyPointManipulation; //button to apply point manipulation
        QPushButton *buttonExitPointManipulation; //button to close window, not display old and new point
        QPushButton *buttonDeleteLastPoint; //button to delete last inserted point
        
        //hole operations
        QSpinBox *spinBoxHoleOperationPersPairDim; //spinbox to select dimension of persistence pair for hole operation
        QSpinBox *spinBoxHoleOperationPersPairNum; //spinbox to select rank of persistence pair for hole operation, in order of decreasing persistence
        std::vector<QRadioButton*> radioButtonsHoleOperationType; //radio button to select chain/operation type
        std::vector<QRadioButton*> radioButtonsHoleOperationSubcomplex; //radio button to select whether to perform operation on full complex or current subcomplex
        QCheckBox* checkBoxHoleOperationCohomology; //whether to use cohomology dependence structure for hole operations
        QCheckBox* checkBoxHoleOperationHighlight; //checkbox whether to highlight chains for current persistence pair
        std::vector<QRadioButton*> radioButtonsHighlightMode; //radio button to select whether to highlight chains with color or not draw anything else
        SelectColorButton *buttonSelectHighlightColor1; //button to select highlight color of chain
        SelectColorButton *buttonSelectHighlightColor2; //button to select highlight color of main simplex
        QCheckBox *checkBoxHoleOperationPersistenceBound; //whether hole operation with persistence bound performed
        QLineEdit *lineEditHoleOperationPersistenceBound; //persistence bound
        std::vector<QRadioButton*> radioButtonsHoleOperationPersistenceBound; //whether persistence bound is upper or lower
        QCheckBox *checkBoxHoleOperationPrintInfo; //whether to print info about operation
        QPushButton *buttonApplyHoleOperation; //button to apply hole operation
        QPushButton *buttonUndoHoleOperations; //button to undo all hole operations
        QPushButton *buttonExitHoleOperation; //button to exit window, stop highlighting
        
        QRadioButton* radioButtonBregmanInputPoints; //display Bregman-Delaunay complex on original input points
        QRadioButton* radioButtonBregmanTransformedPoints; //display Bregman-Delaunay complex on transformed (weighted) points
        
        //statistics
        QLabel *labelNumPoints;
        QLabel *labelNumEdgesDelaunay;
        QLabel *labelNumTrianglesDelaunay;
        QLabel *labelNumTetrahedraDelaunay;
        QLabel *labelNumEdgesAlpha;
        QLabel *labelNumTrianglesAlpha;
        QLabel *labelNumTetrahedraAlpha;
        QLabel *labelNumEdgesWrap;
        QLabel *labelNumTrianglesWrap;
        QLabel *labelNumTetrahedraWrap;
        QLabel *labelNumIntervals;
        QLabel *labelNumCritEdgesPos;
        QLabel *labelNumCritEdgesNeg;
        QLabel *labelNumCritTrianglesPos;
        QLabel *labelNumCritTrianglesNeg;
        QLabel *labelNumCritTetrahedra;
        QLabel *labelBetti0;
        QLabel *labelBetti1;
        QLabel *labelBetti2;
        QLabel *labelMaxPersistence0;
        QLabel *labelMaxPersistence1;
        QLabel *labelMaxPersistence2;
        QLabel *labelMaxPersistenceDim2;

        //mouse coordinates
        QLabel *labelMouseCoords;

        //text labels and lines for layout in main window
        QLabel *labelAlpha;
        QLabel *labelAlpha2;
        QLabel *labelPoints;
        QLabel *labelEdges;
        QLabel *labelTriangles;
        QLabel *labelTetrahedra;
        QFrame* line1;
        QFrame* line2;
        QLabel *labelCritical;
        QLabel *labelPos;
        QLabel *labelNeg;
        QFrame* line3;
        QLabel *labelIntervals;
        QLabel *labelBet0;
        QLabel *labelBet1;
        QLabel *labelBet2;
        QLabel *labelCoordsText;

        //text labels for point manipulation window
        QLabel *labelOldPoint;
        QLabel *labelOldX;
        QLabel *labelOldY;
        QLabel *labelOldZ;
        QLabel *labelOldPointX;
        QLabel *labelOldPointY;
        QLabel *labelOldPointZ;
        QLabel *labelNewPoint;
        QLabel *labelNewX;
        QLabel *labelNewY;
        QLabel *labelNewZ;
        
        //text labels for hole operation window
        QLabel *labelHoleOperationType;
        QLabel *labelHoleOperationPersPair;
        QLabel *labelHoleOperationPersPairDim;
        QLabel *labelHoleOperationPersPairNum;
        QLabel *labelHoleOperationPersPairPersistence;
        QLabel *labelHoleOperationPersPairPersistenceValue;
        QLabel *labelHighlight;
        
        //labels for persistence diagram
        QLabel *labelPersDiagNotFull; //not full persistence diagram drawn
        QLabel* labelPersDiagDrawSelection;


        //layout of main window
        QGridLayout* layout;

        void init1(); //setup windows

    public:
        void init2(int drawing_window_size); //some initial commands that only work after window.show()
        void initAlphaSliderValue(){if(geometry->isUseRadius2()||geometry->isWeighted()){sliderAlpha->setValueDouble(geometry->getAlpha2Current());}else{sliderAlpha->setValueDouble(std::sqrt(geometry->getAlpha2Current()));}}
        Geometry* getGeometry(){return geometry;}
        void updateGeometry(Geometry* geometry_){geometry=geometry_;}
        

    private:

};

#endif // WINDOW_H