//window.cpp
//author: koelsboe

#include "window.h"

#include <iostream>
#include <fstream>
 
//for default input points
Window::Window(QMainWindow *parent) :
 QMainWindow(parent)
{
    int alpha_init = 50;
    //initial alpha value, for input/output: Delaunay radius, internally computed with squared radius
    centralWidget = new QWidget(); //stacked widget: can have more than one layout  
    
    //construct geometry class, everything is computed and drawn within
    geometry = new Geometry(alpha_init*alpha_init);
    
    init1();
    
}

//for given input points
Window::Window(std::vector<DataPoint> data_points, Alpha_Complex* alpha_complex, int drawing_window_size, QMainWindow *parent) :
 QMainWindow(parent)
{
    int alpha_init = 50;
    //initial alpha value, for input/output: Delaunay radius, internally computed with squared radius
    centralWidget = new QWidget(); //stacked widget: can have more than one layout  
    
    //construct geometry class, everything is computed and drawn within
    geometry = new Geometry(data_points, alpha_complex, drawing_window_size, alpha_init*alpha_init,centralWidget);
    
    //adjust drawing parameters
    double maxwidthheight = fitView();
    if(maxwidthheight<2){
        geometry->setDrawingParameters(0.01,0.005,0.01,15,geometry->getPointSizePersDiag(),geometry->getMaxValuePlots(),geometry->getColorDelaunayByRadius(),true);
    }else{
        if(maxwidthheight<20){
            geometry->setDrawingParameters(0.1,0.005,0.1,15,geometry->getPointSizePersDiag(),geometry->getMaxValuePlots(),geometry->getColorDelaunayByRadius(),true);
        }
    }
    
    init1();
    
    fitAlphaRangeWrap();
}

//set new alpha complex (after initialization)
void Window::setAlphaComplex(std::vector<DataPoint> data_points, Alpha_Complex* alpha_complex, int drawing_window_size)
{
    int alpha_init = 50;
    //initial alpha value, for input/output: Delaunay radius, internally computed with squared radius
    
    //construct geometry class, everything is computed and drawn within
    geometry = new Geometry(data_points, alpha_complex, drawing_window_size, alpha_init*alpha_init);
    
    //adjust drawing parameters
    double maxwidthheight = fitView();
    if(maxwidthheight<2){
        geometry->setDrawingParameters(0.01,0.005,0.01,15,geometry->getPointSizePersDiag(),geometry->getMaxValuePlots(),geometry->getColorDelaunayByRadius(),true);
    }else{
        if(maxwidthheight<20){
            geometry->setDrawingParameters(0.1,0.005,0.1,15,geometry->getPointSizePersDiag(),geometry->getMaxValuePlots(),geometry->getColorDelaunayByRadius(),true);
        }
    }
    
    fitAlphaRangeWrap();
}

//setup windows
void Window::init1()
{ 
    //initialize second window
    secondWindow = geometry->getSecondDrawingPlane();
    secondWindow->resize(600,600);
    secondWindow->setWindowFlags(Qt::Window);
    secondWindow->setAttribute( Qt::WA_QuitOnClose, false ); //close when main window closed
    secondWindow->setWindowTitle(tr("Interval graph"));
    //initialize third window
    thirdWindow = new QWidget();
    thirdWindow->resize(600,600);
    thirdWindow->setWindowFlags(Qt::Window);
    thirdWindow->setAttribute( Qt::WA_QuitOnClose, false ); //close when main window closed
    thirdWindow->setWindowTitle(tr("Persistence diagram (birth-death)"));
    //initialize fourth window
    fourthWindow = new QWidget();
    fourthWindow->resize(800,600);
    fourthWindow->setWindowFlags(Qt::Window);
    fourthWindow->setAttribute( Qt::WA_QuitOnClose, false ); //close when main window closed
    fourthWindow->setWindowTitle(tr("Statistics for whole filtration"));
    //initialize point manipulation window
    pointManipulationWindow = new QWidget();
    pointManipulationWindow->resize(400,200);
    pointManipulationWindow->setWindowFlags(Qt::Window);
    pointManipulationWindow->setAttribute( Qt::WA_QuitOnClose, false ); //close when main window closed
    pointManipulationWindow->setWindowTitle(tr("Manipulate Input Points"));
    //initialize chain highlighting window
    holeOperationWindow = new QWidget();
    holeOperationWindow->resize(400,200);
    holeOperationWindow->setWindowFlags(Qt::Window);
    holeOperationWindow->setAttribute( Qt::WA_QuitOnClose, false ); //close when main window closed
    holeOperationWindow->setWindowTitle(tr("Hole Operations"));
    
    //change layout of window if dimension of points has changed
    connect(geometry, SIGNAL(dimChanged(bool)), this, SLOT(updateLayoutDim(bool)));
    //add radio buttons if bregman has changed
    connect(geometry, SIGNAL(bregmanChanged(bool)), this, SLOT(updateLayoutBregman(bool)));
     
    //INTERACTIVE ELEMENTS
    //checkbox Delaunay triangulation
    checkBoxDelaunay = new QCheckBox(tr("Delaunay"));
    connect(checkBoxDelaunay, SIGNAL(toggled(bool)), geometry, SLOT(drawDelaunay(bool)));
    //checkbox convex hull
    checkBoxConvexHull = new QCheckBox(tr("Convex Hull"));
    connect(checkBoxConvexHull, SIGNAL(toggled(bool)), geometry, SLOT(drawConvexHull(bool)));
    //checkbox union of balls
    checkBoxUnionOfBalls = new QCheckBox(tr("Union of Balls"));
    connect(checkBoxUnionOfBalls, SIGNAL(toggled(bool)), geometry, SLOT(drawUnionOfBalls(bool)));
    
    //checkbox triangle labels
    checkBoxLabelsIntervals = new QCheckBox(tr("Interval labels"));
    connect(checkBoxLabelsIntervals, SIGNAL(toggled(bool)), geometry, SLOT(showLabelsIntervals(bool)));
    //checkbox point labels
    checkBoxLabelsPoints = new QCheckBox(tr("Point labels"));
    connect(checkBoxLabelsPoints, SIGNAL(toggled(bool)), geometry, SLOT(showLabelsPoints(bool)));
    //checkbox weighted points
    checkBoxWeighted = new QCheckBox(tr("Weighted point spheres"));
    connect(checkBoxWeighted, SIGNAL(toggled(bool)), geometry, SLOT(showWeightedPointSpheres(bool)));
    
    //slider and spin box for alpha value (Delaunay radius, squared for weighted points)
    labelAlpha = new QLabel(tr("r:"));
    labelAlpha2 = new QLabel(tr("r^2:"));
    sliderAlpha = new QDoubleSlider(centralWidget);
    sliderAlpha->setOrientation(Qt::Horizontal);  
    sliderAlpha->setRange(0,102);
    spinBoxAlpha = new MySpinBox(centralWidget);
    spinBoxAlpha->setRange(0,102);
    //spinBoxAlpha->setValue(alpha_init);  
    //sliderAlpha->setSliderPosition(alpha_init); 
    connect(spinBoxAlpha, SIGNAL(valueChangedDouble(double)), sliderAlpha, SLOT(setValueDouble(double)));  
    connect(sliderAlpha, SIGNAL(valueChangedDouble(double)), spinBoxAlpha, SLOT(setValueDouble(double)));  
    connect(spinBoxAlpha, SIGNAL(valueChangedDouble(double)), geometry, SLOT(updateAlpha(double)));
    connect(sliderAlpha, SIGNAL(valueChangedDouble(double)), geometry, SLOT(updateAlpha(double)));
    //connect(geometry, SIGNAL(alphaChanged(double)), spinBoxAlpha, SLOT(setValue(double)));         
     
    //checkbox for alpha complex
    checkBoxAlpha = new QCheckBox(tr("Alpha"));
    connect(checkBoxAlpha, SIGNAL(toggled(bool)), geometry, SLOT(drawAlpha(bool)));   
    //checkbox for wrap complex
    checkBoxWrap = new QCheckBox(tr("Wrap"));
    connect(checkBoxWrap, SIGNAL(toggled(bool)), geometry, SLOT(drawWrap(bool)));
    //checkbox for full Wrap (infinite alpha)
    checkBoxFull = new QCheckBox(trUtf8("\u221E")); //infinity symbol
    connect(checkBoxFull, SIGNAL(toggled(bool)), geometry, SLOT(drawFull(bool)));
    //button to show interval graph in second window
    buttonShowIntervalGraph = new QPushButton("Interval graph",this);
    buttonShowIntervalGraph->setCheckable(true);
    connect(buttonShowIntervalGraph, SIGNAL(clicked()), this, SLOT(showIntervalGraph()));
    connect(geometry, SIGNAL(intervalGraphDrawn(bool)), buttonShowIntervalGraph, SLOT(setEnabled(bool)));
    //button to show persistence diagram of Wrap complex in third window
    buttonShowPersDiag = new QPushButton("Persistence diag",this);
    buttonShowPersDiag->setCheckable(true);
    connect(buttonShowPersDiag, SIGNAL(clicked()), this, SLOT(showPersDiag()));
    //button to show statistic plots in fourth window
    buttonShowPlots = new QPushButton("Statistics plots",this);
    buttonShowPlots->setCheckable(true);
    connect(buttonShowPlots, SIGNAL(clicked()), this, SLOT(showPlots()));
    //button to show or hide statistics in main window
    buttonShowHideStatistics = new QPushButton("Hide statistics",this);
    buttonShowHideStatistics->setCheckable(true);
    connect(buttonShowHideStatistics, SIGNAL(clicked()), this, SLOT(assignLayoutShowHideStatistics()));
    QGroupBox *groupBregman = new QGroupBox(); 
    radioButtonBregmanInputPoints = new QRadioButton(tr("Bregman on input points"));
    radioButtonBregmanTransformedPoints = new QRadioButton(tr("... on transformed points"));
    radioButtonBregmanInputPoints->setChecked(true); //default value      
    QHBoxLayout *layoutBregman = new QHBoxLayout; //horizontal layout
    layoutBregman->addWidget(radioButtonBregmanInputPoints);
    layoutBregman->addWidget(radioButtonBregmanTransformedPoints);
    groupBregman->setLayout(layoutBregman);
    connect(radioButtonBregmanInputPoints,SIGNAL(toggled(bool)),this, SLOT(drawBregmanOnInputPoints(bool))); //toggled both when activated or deactivated
    
    //display statistics  
    labelNumPoints = new QLabel();
    labelNumEdgesDelaunay = new QLabel();
    labelNumTrianglesDelaunay = new QLabel();
    labelNumTetrahedraDelaunay = new QLabel();
    labelNumEdgesAlpha = new QLabel();
    labelNumTrianglesAlpha = new QLabel();
    labelNumTetrahedraAlpha = new QLabel();
    labelNumEdgesWrap = new QLabel();
    labelNumTrianglesWrap = new QLabel();
    labelNumTetrahedraWrap = new QLabel();
    labelNumIntervals = new QLabel();
    labelNumCritEdgesPos = new QLabel();
    labelNumCritEdgesNeg = new QLabel();
    labelNumCritTrianglesPos = new QLabel();
    labelNumCritTrianglesNeg = new QLabel();
    labelNumCritTetrahedra = new QLabel();
    labelBetti0 = new QLabel();
    labelBetti1 = new QLabel();
    labelBetti2 = new QLabel();
    connect(geometry, SIGNAL(numPointsChanged(QString)), labelNumPoints, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numEdgesDelaunayChanged(QString)), labelNumEdgesDelaunay, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numTrianglesDelaunayChanged(QString)), labelNumTrianglesDelaunay, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numTetrahedraDelaunayChanged(QString)), labelNumTetrahedraDelaunay, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numEdgesAlphaChanged(QString)), labelNumEdgesAlpha, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numTrianglesAlphaChanged(QString)), labelNumTrianglesAlpha, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numTetrahedraAlphaChanged(QString)), labelNumTetrahedraAlpha, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numEdgesWrapChanged(QString)), labelNumEdgesWrap, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numTrianglesWrapChanged(QString)), labelNumTrianglesWrap, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numTetrahedraWrapChanged(QString)), labelNumTetrahedraWrap, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numIntervalsChanged(QString)), labelNumIntervals, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numCritEdgesPosChanged(QString)), labelNumCritEdgesPos, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numCritEdgesNegChanged(QString)), labelNumCritEdgesNeg, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numCritTrianglesNegChanged(QString)), labelNumCritTrianglesNeg, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numCritTrianglesPosChanged(QString)), labelNumCritTrianglesPos, SLOT(setText(QString)));
    connect(geometry, SIGNAL(numCritTetrahedraChanged(QString)), labelNumCritTetrahedra, SLOT(setText(QString)));
    connect(geometry, SIGNAL(betti0Changed(QString)), labelBetti0, SLOT(setText(QString)));
    connect(geometry, SIGNAL(betti1Changed(QString)), labelBetti1, SLOT(setText(QString)));
    connect(geometry, SIGNAL(betti2Changed(QString)), labelBetti2, SLOT(setText(QString)));
    
    //radio buttons to choose whether persistence is computed for Wrap or Alpha complex 
    QGroupBox *groupPers = new QGroupBox(); 
    radioButtonAlphaPers = new QRadioButton(tr("Alpha complex"));
    radioButtonWrapPers = new QRadioButton(tr("Wrap complex"));
    radioButtonAlphaPers->setChecked(true); //default value      
    QHBoxLayout *layoutPers = new QHBoxLayout; //horizontal layout
    layoutPers->addWidget(radioButtonAlphaPers);
    layoutPers->addWidget(radioButtonWrapPers);
    groupPers->setLayout(layoutPers);
    connect(radioButtonAlphaPers,SIGNAL(toggled(bool)),this, SLOT(changePersDiag())); //toggled both when activated or deactivated
    
    //radio buttons to choose whether original or current (after hole operations) persistence diagram is shown
    QGroupBox *groupPersOldNew = new QGroupBox(); 
    radioButtonOldPersDiag = new QRadioButton(tr("original"));
    radioButtonNewPersDiag = new QRadioButton(tr("current"));
    radioButtonStatusPersDiag = new QRadioButton(tr("status"));
    radioButtonNewPersDiag->setChecked(true); //default value      
    QHBoxLayout *layoutPersOldNew = new QHBoxLayout; //horizontal layout
    layoutPersOldNew->addWidget(radioButtonOldPersDiag);
    layoutPersOldNew->addWidget(radioButtonNewPersDiag);
    layoutPersOldNew->addWidget(radioButtonStatusPersDiag);
    groupPersOldNew->setLayout(layoutPersOldNew);
    connect(radioButtonOldPersDiag,SIGNAL(toggled(bool)),this, SLOT(changePersDiag())); //toggled both when activated or deactivated
    connect(radioButtonNewPersDiag,SIGNAL(toggled(bool)),this, SLOT(changePersDiag())); //toggled both when activated or deactivated
    connect(radioButtonStatusPersDiag,SIGNAL(toggled(bool)),this, SLOT(changePersDiag())); //toggled both when activated or deactivated
    //checkbox whether connections for manipulated holes in persistence diagram shown
    //checkBoxPersDiagConnections = new QCheckBox("show arrows");
    //checkBoxPersDiagConnections->setChecked(false);
    //connect(checkBoxPersDiagConnections,SIGNAL(toggled(bool)),this, SLOT(changePersDiag())); //toggled both when activated or deactivated
    
    //message if not full diagrams are drawn
    labelPersDiagNotFull = new QLabel("Only pairs with persistence above some threshold are drawn!");
    connect(geometry, SIGNAL(showMessagePersDiag(bool)), labelPersDiagNotFull, SLOT(setVisible(bool)));
    
    //checkbox whether subcomplex is highlighted in persistence diagram
    checkBoxPersDiagSubcomplex = new QCheckBox("highlight subcomplex");
    checkBoxPersDiagSubcomplex->setChecked(false);
    connect(checkBoxPersDiagSubcomplex,SIGNAL(toggled(bool)),geometry, SLOT(changePersDiagSubcomplex(bool))); //toggled both when activated or deactivated
    
    //maximum persistence values
    labelMaxPersistence0 = new QLabel();
    labelMaxPersistence1 = new QLabel();
    labelMaxPersistence2 = new QLabel();
    connect(geometry, SIGNAL(maxPersistence0Changed(QString)), labelMaxPersistence0, SLOT(setText(QString)));
    connect(geometry, SIGNAL(maxPersistence1Changed(QString)), labelMaxPersistence1, SLOT(setText(QString)));
    connect(geometry, SIGNAL(maxPersistence2Changed(QString)), labelMaxPersistence2, SLOT(setText(QString)));
    
    labelPersDiagDrawSelection = new QLabel("draw selection of points:");
    QCheckBox* checkbox = new QCheckBox("dim 0"); //whether to draw persistence points of specific dimension
    checkbox->setChecked(true);
    connect(checkbox,SIGNAL(toggled(bool)),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    checkBoxPersDiagStatusDrawDim.push_back(checkbox);
    checkbox = new QCheckBox("dim 1"); 
    checkbox->setChecked(true);
    connect(checkbox,SIGNAL(toggled(bool)),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    checkBoxPersDiagStatusDrawDim.push_back(checkbox);
    checkbox = new QCheckBox("dim 2"); 
    checkbox->setChecked(true);
    connect(checkbox,SIGNAL(toggled(bool)),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    checkBoxPersDiagStatusDrawDim.push_back(checkbox);
    checkbox = new QCheckBox("past"); ////whether to draw persistence points of specific status
    checkbox->setChecked(true);
    connect(checkbox,SIGNAL(toggled(bool)),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    checkBoxPersDiagStatusDrawStatus.push_back(checkbox);
    checkbox = new QCheckBox("presence"); ////whether to draw persistence points of specific status
    checkbox->setChecked(true);
    connect(checkbox,SIGNAL(toggled(bool)),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    checkBoxPersDiagStatusDrawStatus.push_back(checkbox);
    checkbox = new QCheckBox("future"); ////whether to draw persistence points of specific status
    checkbox->setChecked(true);
    connect(checkbox,SIGNAL(toggled(bool)),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    checkBoxPersDiagStatusDrawStatus.push_back(checkbox);
    checkBoxPersDiagStatusDrawDependentPairs = new QCheckBox("dependent pairs only");
    checkBoxPersDiagStatusDrawDependentPairs->setChecked(false);
    connect(checkBoxPersDiagStatusDrawDependentPairs,SIGNAL(toggled(bool)),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    checkBoxPersDiagStatusDrawRecursivelyDependentPairs = new QCheckBox("recursively");
    checkBoxPersDiagStatusDrawRecursivelyDependentPairs->setChecked(false);
    connect(checkBoxPersDiagStatusDrawRecursivelyDependentPairs,SIGNAL(toggled(bool)),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    checkBoxPersDiagStatusDrawDependentPairsLastOperation = new QCheckBox("last operation");
    checkBoxPersDiagStatusDrawDependentPairsLastOperation->setChecked(false);
    connect(checkBoxPersDiagStatusDrawDependentPairsLastOperation,SIGNAL(toggled(bool)),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    
    //radio buttons to choose which statistics should be plotted over filtration values
    QGroupBox *groupPlotButtons = new QGroupBox();
    QVBoxLayout *layoutPlotButtons = new QVBoxLayout; //vertical layout
    //exclusive buttons grouped in a box
    QRadioButton* plot_button = new QRadioButton(QString("# Alpha edges"));
    radioButtonsPlotTitles23.push_back(plot_button); layoutPlotButtons->addWidget(plot_button); 
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("# Alpha triangles"));
    radioButtonsPlotTitles23.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("# Alpha tetrahedra"));
    radioButtonsPlotTitles3.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("# Wrap edges"));
    radioButtonsPlotTitles23.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("# Wrap triangles"));
    radioButtonsPlotTitles23.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("# Wrap tetrahedra"));
    radioButtonsPlotTitles3.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("# neg critical edges"));
    radioButtonsPlotTitles23.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("# pos critical edges"));
    radioButtonsPlotTitles23.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("# neg critical triangles"));
    radioButtonsPlotTitles23.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("# pos critical triangles"));
    radioButtonsPlotTitles3.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("# critical tetrahedra"));
    radioButtonsPlotTitles3.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("Betti 0"));
    radioButtonsPlotTitles23.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("Betti 1"));
    radioButtonsPlotTitles23.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("Betti 2"));
    radioButtonsPlotTitles3.push_back(plot_button); layoutPlotButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    groupPlotButtons->setLayout(layoutPlotButtons);
    radioButtonsPlotTitles23[0]->setChecked(true);
    connect(geometry, SIGNAL(plotsChanged()), this, SLOT(changeStatisticPlot()));  
    //radio buttons to choose between log scale and normal scale
    QGroupBox *groupPlotScaleButtons = new QGroupBox();
    QHBoxLayout *layoutPlotScaleButtons = new QHBoxLayout; //horizontal layout
    plot_button = new QRadioButton(QString("normal scale"));
    radioButtonsPlotScale.push_back(plot_button); layoutPlotScaleButtons->addWidget(plot_button); 
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    plot_button = new QRadioButton(QString("log scale"));
    radioButtonsPlotScale.push_back(plot_button); layoutPlotScaleButtons->addWidget(plot_button);
    connect(plot_button,SIGNAL(clicked()),this, SLOT(changeStatisticPlot())); 
    groupPlotScaleButtons->setLayout(layoutPlotScaleButtons);
    radioButtonsPlotScale[0]->setChecked(true); 
    
    //display mouse coordinates
    labelMouseCoords = new QLabel("0, 0");
    labelMouseCoords->setFixedWidth(150);
    connect(geometry, SIGNAL(mouseCoordsChanged(QString)), labelMouseCoords, SLOT(setText(QString)));
    
    //checkbox recompute
    checkBoxRecompute = new QCheckBox(tr("recompute"));
    checkBoxRecompute->setChecked(false);
    
    //radio buttons to choose point operation
    QGroupBox *groupManipulatePointsRadioButtons = new QGroupBox();
    QHBoxLayout *layoutManipulatePointsRadioButtons = new QHBoxLayout; 
    QRadioButton* manipulation_radiobutton = new QRadioButton(QString("add"));
    radioButtonsManipulatePoints.push_back(manipulation_radiobutton); layoutManipulatePointsRadioButtons->addWidget(manipulation_radiobutton); 
    connect(manipulation_radiobutton,SIGNAL(clicked()),this, SLOT(updatePointManipulationLayout()));
    manipulation_radiobutton = new QRadioButton(QString("delete"));
    radioButtonsManipulatePoints.push_back(manipulation_radiobutton); layoutManipulatePointsRadioButtons->addWidget(manipulation_radiobutton); 
    connect(manipulation_radiobutton,SIGNAL(clicked()),this, SLOT(updatePointManipulationLayout()));
    manipulation_radiobutton = new QRadioButton(QString("move point"));
    radioButtonsManipulatePoints.push_back(manipulation_radiobutton); layoutManipulatePointsRadioButtons->addWidget(manipulation_radiobutton); 
    connect(manipulation_radiobutton,SIGNAL(clicked()),this, SLOT(updatePointManipulationLayout()));
    groupManipulatePointsRadioButtons->setLayout(layoutManipulatePointsRadioButtons);
    
    //choose point for manipulation
    //old point
    spinBoxOldPointNum = new QSpinBox();
    spinBoxOldPointNum->setValue(0);
    labelOldPointX = new QLabel(tr("0"));
    labelOldPointY = new QLabel(tr("0"));
    labelOldPointZ = new QLabel(tr("0"));
    connect(spinBoxOldPointNum, SIGNAL(valueChanged(int)), this, SLOT(updateOldPoint()));
    //new point
    lineEditNewPointX = new QLineEdit();
    lineEditNewPointY = new QLineEdit();
    lineEditNewPointZ = new QLineEdit();
    lineEditNewPointX->setText(QString::number(0));
    lineEditNewPointY->setText(QString::number(0));
    lineEditNewPointZ->setText(QString::number(0));
    connect(lineEditNewPointX, SIGNAL(textChanged(const QString &)), this, SLOT(updateNewPoint()));
    connect(lineEditNewPointY, SIGNAL(textChanged(const QString &)), this, SLOT(updateNewPoint()));
    connect(lineEditNewPointZ, SIGNAL(textChanged(const QString &)), this, SLOT(updateNewPoint()));
    
    //apply/exit point manipulation
    buttonApplyPointManipulation = new QPushButton(tr("Apply"),this);
    buttonApplyPointManipulation->setCheckable(true);
    connect(buttonApplyPointManipulation,SIGNAL(clicked()),this,SLOT(applyPointManipulation()));
    buttonExitPointManipulation = new QPushButton(tr("Exit"),this);
    buttonExitPointManipulation->setCheckable(true);
    connect(buttonExitPointManipulation,SIGNAL(clicked()),this,SLOT(exitPointManipulation()));
    
    //delete last inserted point
    buttonDeleteLastPoint = new QPushButton(tr("Delete last point"),this);
    buttonDeleteLastPoint->setCheckable(true);
    connect(buttonDeleteLastPoint,SIGNAL(clicked()),this,SLOT(deleteLastPoint()));
    
    //radio buttons to choose hole operation type / type of chains to highlight
    QGroupBox *groupHoleOperationTypeRadioButtons = new QGroupBox();
    QHBoxLayout *layoutHoleOperationTypeRadioButtons = new QHBoxLayout;
    QRadioButton* holeoperation_radiobutton = new QRadioButton(QString("lock (cycle)"));
    holeoperation_radiobutton->setChecked(true);
    radioButtonsHoleOperationType.push_back(holeoperation_radiobutton); layoutHoleOperationTypeRadioButtons->addWidget(holeoperation_radiobutton); 
    connect(holeoperation_radiobutton,SIGNAL(clicked()),this, SLOT(highlightChain()));
    connect(holeoperation_radiobutton,SIGNAL(clicked()),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    holeoperation_radiobutton = new QRadioButton(QString("unlock (cochain)"));
    radioButtonsHoleOperationType.push_back(holeoperation_radiobutton); layoutHoleOperationTypeRadioButtons->addWidget(holeoperation_radiobutton); 
    connect(holeoperation_radiobutton,SIGNAL(clicked()),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    connect(holeoperation_radiobutton,SIGNAL(clicked()),this, SLOT(highlightChain()));
    holeoperation_radiobutton = new QRadioButton(QString("fill (chain)"));
    radioButtonsHoleOperationType.push_back(holeoperation_radiobutton); layoutHoleOperationTypeRadioButtons->addWidget(holeoperation_radiobutton); 
    connect(holeoperation_radiobutton,SIGNAL(clicked()),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    connect(holeoperation_radiobutton,SIGNAL(clicked()),this, SLOT(highlightChain()));
    holeoperation_radiobutton = new QRadioButton(QString("unfill (cocycle)"));
    radioButtonsHoleOperationType.push_back(holeoperation_radiobutton); layoutHoleOperationTypeRadioButtons->addWidget(holeoperation_radiobutton); 
    connect(holeoperation_radiobutton,SIGNAL(clicked()),this, SLOT(updatePersistenceDiagramStatusSelection())); 
    connect(holeoperation_radiobutton,SIGNAL(clicked()),this, SLOT(highlightChain()));
    groupHoleOperationTypeRadioButtons->setLayout(layoutHoleOperationTypeRadioButtons);
    //choose persistence pair for hole operation and highlighting chains
    //dim
    spinBoxHoleOperationPersPairDim = new QSpinBox();
    spinBoxHoleOperationPersPairDim->setValue(0);
    spinBoxHoleOperationPersPairDim->setMaximum(2);
    connect(spinBoxHoleOperationPersPairDim, SIGNAL(valueChanged(int)), this, SLOT(highlightChain()));
    connect(spinBoxHoleOperationPersPairDim, SIGNAL(valueChanged(int)), this, SLOT(updatePersistenceDiagramStatusSelection()));
    //num
    spinBoxHoleOperationPersPairNum = new QSpinBox();
    spinBoxHoleOperationPersPairNum->setValue(1);
    spinBoxHoleOperationPersPairNum->setMinimum(1);
    connect(spinBoxHoleOperationPersPairNum, SIGNAL(valueChanged(int)), this, SLOT(highlightChain()));
    connect(spinBoxHoleOperationPersPairNum, SIGNAL(valueChanged(int)), this, SLOT(updatePersistenceDiagramStatusSelection()));
    //persistence value
    labelHoleOperationPersPairPersistenceValue = new QLabel("-");
    //radio buttons to choose whether to perform hole operation on full complex or subcomplex
    QGroupBox *groupHoleOperationSubcomplexRadioButtons = new QGroupBox();
    QHBoxLayout *layoutHoleOperationSubcomplexRadioButtons = new QHBoxLayout; 
    holeoperation_radiobutton = new QRadioButton(QString("full"));
    radioButtonsHoleOperationSubcomplex.push_back(holeoperation_radiobutton); layoutHoleOperationSubcomplexRadioButtons->addWidget(holeoperation_radiobutton); 
    connect(holeoperation_radiobutton,SIGNAL(clicked()),this, SLOT(updateHoleOperationSubcomplexMode()));
    holeoperation_radiobutton = new QRadioButton(QString("subcomplex"));
    holeoperation_radiobutton->setChecked(true);
    radioButtonsHoleOperationSubcomplex.push_back(holeoperation_radiobutton); layoutHoleOperationSubcomplexRadioButtons->addWidget(holeoperation_radiobutton); 
    connect(holeoperation_radiobutton,SIGNAL(clicked()),this, SLOT(updateHoleOperationSubcomplexMode()));
    groupHoleOperationSubcomplexRadioButtons->setLayout(layoutHoleOperationSubcomplexRadioButtons);
    //checkbox whether to use dependence structure from homology or cohomology
    checkBoxHoleOperationCohomology = new QCheckBox(tr("cohomology"));
    checkBoxHoleOperationCohomology->setChecked(false);
    connect(checkBoxHoleOperationCohomology,SIGNAL(toggled(bool)),this, SLOT(updateHoleOperationCohomology())); //toggled both when activated or deactivated
    //checkbox whether to highlight chains for current persistence pair
    checkBoxHoleOperationHighlight = new QCheckBox(tr("Highlight:"));
    checkBoxHoleOperationHighlight->setChecked(true);
    connect(checkBoxHoleOperationHighlight,SIGNAL(toggled(bool)),this, SLOT(highlightChain())); //toggled both when activated or deactivated
    //radio buttons to choose whether to highlight chain with color or do not draw anything else
    QGroupBox *groupHighlightRadioButtons = new QGroupBox();
    QHBoxLayout *layoutHighlightRadioButtons = new QHBoxLayout; 
    holeoperation_radiobutton = new QRadioButton(QString("highlight"));
    holeoperation_radiobutton->setChecked(true);
    radioButtonsHighlightMode.push_back(holeoperation_radiobutton); layoutHighlightRadioButtons->addWidget(holeoperation_radiobutton); 
    connect(holeoperation_radiobutton,SIGNAL(clicked()),this, SLOT(highlightChain()));
    holeoperation_radiobutton = new QRadioButton(QString("draw only"));
    radioButtonsHighlightMode.push_back(holeoperation_radiobutton); layoutHighlightRadioButtons->addWidget(holeoperation_radiobutton); 
    connect(holeoperation_radiobutton,SIGNAL(clicked()),this, SLOT(highlightChain()));
    groupHighlightRadioButtons->setLayout(layoutHighlightRadioButtons);
    //choose highlight color
    buttonSelectHighlightColor1 = new SelectColorButton(this);
    buttonSelectHighlightColor1->setText(tr("set color 1"));
    buttonSelectHighlightColor1->setColor(Qt::magenta);
    buttonSelectHighlightColor2 = new SelectColorButton(this);
    buttonSelectHighlightColor2->setText(tr("set color 2"));
    buttonSelectHighlightColor2->setColor(Qt::black);
    connect(buttonSelectHighlightColor1,SIGNAL(colorChanged()),this,SLOT(highlightChain()));
    connect(buttonSelectHighlightColor2,SIGNAL(colorChanged()),this,SLOT(highlightChain()));
    //checkbox whether operation performed on persistence pairs with persistence above/below bound
    checkBoxHoleOperationPersistenceBound = new QCheckBox(tr("with persistence bound:"));
    checkBoxHoleOperationPersistenceBound->setChecked(false);
    connect(checkBoxHoleOperationPersistenceBound,SIGNAL(toggled(bool)),this, SLOT(highlightChain())); //toggled both when activated or deactivated
    //choose value for persistence bound
    lineEditHoleOperationPersistenceBound = new QLineEdit();
    lineEditHoleOperationPersistenceBound->setText(QString::number(1));
    connect(lineEditHoleOperationPersistenceBound,SIGNAL(textChanged(const QString &)),this, SLOT(highlightChain())); 
    //radio buttons to choose if (possible) persistence bound is upper or lower
    QGroupBox *groupHoleOperationPersistenceBoundRadioButtons = new QGroupBox();
    QHBoxLayout *layoutHoleOperationPersistenceBoundRadioButtons = new QHBoxLayout; 
    holeoperation_radiobutton = new QRadioButton(tr("holes above"),this);
    holeoperation_radiobutton->setChecked(true);
    connect(holeoperation_radiobutton,SIGNAL(toggled(bool)),this, SLOT(highlightChain())); //toggled both when activated or deactivated
    radioButtonsHoleOperationPersistenceBound.push_back(holeoperation_radiobutton); layoutHoleOperationPersistenceBoundRadioButtons->addWidget(holeoperation_radiobutton);
    holeoperation_radiobutton = new QRadioButton(tr("holes below"),this);
    radioButtonsHoleOperationPersistenceBound.push_back(holeoperation_radiobutton); layoutHoleOperationPersistenceBoundRadioButtons->addWidget(holeoperation_radiobutton);
    groupHoleOperationPersistenceBoundRadioButtons->setLayout(layoutHoleOperationPersistenceBoundRadioButtons);
    //checkbox for printing info
    checkBoxHoleOperationPrintInfo = new QCheckBox(tr("print info"));
    checkBoxHoleOperationPrintInfo->setChecked(false);
    //button to apply hole operation
    buttonApplyHoleOperation = new QPushButton(tr("Apply"),this);
    buttonApplyHoleOperation->setCheckable(true);
    connect(buttonApplyHoleOperation,SIGNAL(clicked()),this,SLOT(applyHoleOperation()));
    //button to undo all hole operations
    buttonUndoHoleOperations = new QPushButton(tr("Undo all"),this);
    buttonUndoHoleOperations->setCheckable(true);
    connect(buttonUndoHoleOperations,SIGNAL(clicked()),this,SLOT(undoHoleOperations()));
    //close window and stop highlighting
    buttonExitHoleOperation = new QPushButton(tr("Close"),this);
    buttonExitHoleOperation->setCheckable(true);
    connect(buttonExitHoleOperation,SIGNAL(clicked()),this,SLOT(exitHoleOperations()));
    connect(geometry,SIGNAL(sceneDrawn()),this,SLOT(updateHoleOperationWindow()));
    connect(geometry,SIGNAL(alphaChanged()),this,SLOT(updateHoleOperationWindow()));
    connect(geometry,SIGNAL(sceneDrawn()),this,SLOT(updatePersistenceDiagramStatusSelection()));
    connect(geometry,SIGNAL(sceneDrawn()),this,SLOT(highlightChain()));
    connect(geometry,SIGNAL(sceneDrawn()),this,SLOT(updatePointManipulationWindowNewPoints()));
    
      
    //LAYOUT for main window
    //window layout (grid of rows and columns)
    layout = new QGridLayout; 
      
    //initialize labels (non-interactive elements not initialized yet)
    //first row
    labelAlpha->setFixedWidth(100);
    labelAlpha2->setFixedWidth(100);
    spinBoxAlpha->setFixedWidth(100); 
    //second row   
    labelPoints = new QLabel(tr("# points"));
    labelPoints->setFixedWidth(100);
    labelPoints->setAlignment(Qt::AlignRight);
    labelEdges = new QLabel(tr("# edges"));
    labelEdges->setFixedWidth(100);
    labelEdges->setAlignment(Qt::AlignRight);
    labelTriangles = new QLabel(tr("# triangles")); 
    labelTriangles->setFixedWidth(100);
    labelTriangles->setAlignment(Qt::AlignRight);
    labelTetrahedra = new QLabel(tr("# tetrahedra"));
    labelTetrahedra->setFixedWidth(100);
    labelTetrahedra->setAlignment(Qt::AlignRight);
    line1 = new QFrame();
    line1->setFrameShape(QFrame::HLine);
    line2 = new QFrame();
    line2->setFrameShape(QFrame::HLine);
    //sixth and seventh row
    labelCritical = new QLabel(tr("# critical:"));
    labelPos = new QLabel(tr("+"));
    labelNeg = new QLabel(tr("-"));
    line3 = new QFrame();
    line3->setFrameShape(QFrame::HLine);
    //eight and ninth row
    labelIntervals = new QLabel(tr("# intervals"));
    labelBet0 = new QLabel(tr("Betti_0"));
    labelBet1 = new QLabel(tr("Betti_1"));  
    labelBet2 = new QLabel(tr("Betti_2"));  
    
    
    show_statistics = true;
    holeoperations_subcomplex = false;
    
    //set central widget ("window"), choose current layout  
    centralWidget->setLayout(layout);
    setCentralWidget(centralWidget);  
    
    
    //LAYOUT third window
    int row = 0;
    QGridLayout *layout3 = new QGridLayout;    
    layout3->addWidget(groupPers,row,0,1,4);
    row++;
    layout3->addWidget(geometry->getThirdDrawingPlane(),row,0,1,4);
    row++;
    layout3->addWidget(groupPersOldNew,row,0,1,2);
    //layout3->addWidget(checkBoxPersDiagConnections,row,2,1,2);
    layout3->addWidget(labelPersDiagNotFull,row,0,1,4);
    row++;
    layout3->addWidget(checkBoxPersDiagSubcomplex,row,0,1,2);
    row++;
    QLabel *labelMaxPersistence = new QLabel(tr("max persistence"));
    QLabel *labelMaxPersistenceDim0 = new QLabel(tr("dim 0"));
    QLabel *labelMaxPersistenceDim1 = new QLabel(tr("dim 1"));
    labelMaxPersistenceDim2 = new QLabel(tr("dim 2"));
    layout3->addWidget(labelMaxPersistence,row,0,1,1);
    layout3->addWidget(labelMaxPersistenceDim0,row,1,1,1,Qt::AlignRight);
    layout3->addWidget(labelMaxPersistenceDim1,row,2,1,1,Qt::AlignRight);
    layout3->addWidget(labelMaxPersistenceDim2,row,3,1,1,Qt::AlignRight);
    row++;
    layout3->addWidget(labelMaxPersistence0,row,1,1,1,Qt::AlignRight);
    layout3->addWidget(labelMaxPersistence1,row,2,1,1,Qt::AlignRight);
    layout3->addWidget(labelMaxPersistence2,row,3,1,1,Qt::AlignRight);
    row++;
    layout3->addWidget(labelPersDiagDrawSelection,row,0,1,2);
    row++;
    layout3->addWidget(checkBoxPersDiagStatusDrawDim[0],row,1,1,1);
    layout3->addWidget(checkBoxPersDiagStatusDrawDim[1],row,2,1,1);
    layout3->addWidget(checkBoxPersDiagStatusDrawDim[2],row,3,1,1);
    row++;
    layout3->addWidget(checkBoxPersDiagStatusDrawStatus[0],row,1,1,1);
    layout3->addWidget(checkBoxPersDiagStatusDrawStatus[1],row,2,1,1);
    layout3->addWidget(checkBoxPersDiagStatusDrawStatus[2],row,3,1,1);
    row++;
    layout3->addWidget(checkBoxPersDiagStatusDrawDependentPairs,row,0,1,2,Qt::AlignRight);
    layout3->addWidget(checkBoxPersDiagStatusDrawRecursivelyDependentPairs,row,2,1,1);
    layout3->addWidget(checkBoxPersDiagStatusDrawDependentPairsLastOperation,row,3,1,1);
    
    thirdWindow->setLayout(layout3);   
    
    //LAYOUT fourth window
    QGridLayout *layout4 = new QGridLayout;  
    layout4->addWidget(groupPlotScaleButtons,0,1,1,1);
    layout4->addWidget(geometry->getFourthDrawingPlane(),1,1,1,1); 
    layout4->addWidget(groupPlotButtons,1,0,1,1);
    fourthWindow->setLayout(layout4);
    
    //LAYOUT point manipulation window
    QGridLayout *layout5 = new QGridLayout;
    
    row = 0;
    
    layout5->addWidget(buttonDeleteLastPoint,row,0,1,2);
    layout5->addWidget(checkBoxRecompute,row,6,1,2);
    row++;
    
    layout5->addWidget(groupManipulatePointsRadioButtons,row,0,1,8);
    
    labelOldPoint = new QLabel(tr("point #"));
    labelOldX = new QLabel(tr("x"));
    labelOldY = new QLabel(tr("y"));
    labelOldZ = new QLabel(tr("z"));
    labelNewPoint = new QLabel(tr("new point"));
    labelNewX = new QLabel(tr("x"));
    labelNewY = new QLabel(tr("y"));
    labelNewZ = new QLabel(tr("z"));
    row++;
    labelOldPoint->setFixedWidth(70);
    layout5->addWidget(labelOldPoint,row,0,1,1,Qt::AlignRight);
    spinBoxOldPointNum->setFixedWidth(60);
    layout5->addWidget(spinBoxOldPointNum,row,1,1,1,Qt::AlignLeft);
    labelOldPointX->setFixedWidth(75);
    labelOldPointY->setFixedWidth(75);
    labelOldPointZ->setFixedWidth(75);
    layout5->addWidget(labelOldX,row,2,1,1,Qt::AlignRight); layout5->addWidget(labelOldPointX,row,3,1,1);
    layout5->addWidget(labelOldY,row,4,1,1,Qt::AlignRight); layout5->addWidget(labelOldPointY,row,5,1,1);
    layout5->addWidget(labelOldZ,row,6,1,1,Qt::AlignRight); layout5->addWidget(labelOldPointZ,row,7,1,1);
    row++;
    labelNewPoint->setFixedWidth(130);
    layout5->addWidget(labelNewPoint,row,0,1,2,Qt::AlignRight);
    lineEditNewPointX->setFixedWidth(75);
    lineEditNewPointY->setFixedWidth(75);
    lineEditNewPointZ->setFixedWidth(75);
    layout5->addWidget(labelNewX,row,2,1,1,Qt::AlignRight); layout5->addWidget(lineEditNewPointX,row,3,1,1);
    layout5->addWidget(labelNewY,row,4,1,1,Qt::AlignRight); layout5->addWidget(lineEditNewPointY,row,5,1,1);
    layout5->addWidget(labelNewZ,row,6,1,1,Qt::AlignRight); layout5->addWidget(lineEditNewPointZ,row,7,1,1);
    
    row++;
    labelCoordsText = new QLabel(tr("mouse coordinates:"));
    layout5->addWidget(labelCoordsText,row,0,1,4,Qt::AlignRight);   
    layout5->addWidget(labelMouseCoords,row,4,1,4,Qt::AlignRight);
    row++;
    buttonApplyPointManipulation->setFixedWidth(90);
    layout5->addWidget(buttonApplyPointManipulation,row,4,1,2,Qt::AlignRight);
    buttonExitPointManipulation->setFixedWidth(90);
    layout5->addWidget(buttonExitPointManipulation,row,6,1,2);
    
    radioButtonsManipulatePoints[0]->setChecked(true); 
    
    pointManipulationWindow->setLayout(layout5);
    
    exitPointManipulation();
    
    //LAYOUT for hole operation window
    QGridLayout *layout6 = new QGridLayout;
    
    row = 0;
    
    layout6->addWidget(groupHoleOperationSubcomplexRadioButtons,row,0,1,3);
    layout6->addWidget(checkBoxHoleOperationCohomology,row,3,1,1);
    row++;
    labelHoleOperationType = new QLabel(tr("Hole operation type:"));
    layout6->addWidget(labelHoleOperationType,row,0,1,2);
    labelHoleOperationPersPairDim = new QLabel(tr("dim"));
    layout6->addWidget(labelHoleOperationPersPairDim,row,2,1,1,Qt::AlignRight);
    spinBoxHoleOperationPersPairDim->setFixedWidth(75);
    layout6->addWidget(spinBoxHoleOperationPersPairDim,row,3,1,1);
    row++;
    layout6->addWidget(groupHoleOperationTypeRadioButtons,row,1,1,5);
    row++;
    labelHoleOperationPersPair = new QLabel(tr("Persistence Pair:"));
    layout6->addWidget(labelHoleOperationPersPair,row,0,1,2);
    labelHoleOperationPersPairNum = new QLabel(tr("#"));
    layout6->addWidget(labelHoleOperationPersPairNum,row,2,1,1,Qt::AlignRight);
    spinBoxHoleOperationPersPairNum->setFixedWidth(75);
    layout6->addWidget(spinBoxHoleOperationPersPairNum,row,3,1,1);
    labelHoleOperationPersPairPersistence = new QLabel(tr("persistence"));
    layout6->addWidget(labelHoleOperationPersPairPersistence,row,4,1,1,Qt::AlignRight);
    layout6->addWidget(labelHoleOperationPersPairPersistenceValue,row,5,1,1);
    row++;
    layout6->addWidget(checkBoxHoleOperationHighlight,row,0,1,2);
    layout6->addWidget(groupHighlightRadioButtons,row,2,1,2);
    layout6->addWidget(buttonSelectHighlightColor1,row,4,1,1);
    layout6->addWidget(buttonSelectHighlightColor2,row,5,1,1);
    row++;
    layout6->addWidget(checkBoxHoleOperationPersistenceBound,row,0,1,2);
    lineEditHoleOperationPersistenceBound->setFixedWidth(75);
    layout6->addWidget(lineEditHoleOperationPersistenceBound,row,2,1,2);
    layout6->addWidget(groupHoleOperationPersistenceBoundRadioButtons,row,4,1,2);
    row++;
    layout6->addWidget(checkBoxHoleOperationPrintInfo,row,0,1,2);
    layout6->addWidget(buttonApplyHoleOperation,row,3,1,1);
    layout6->addWidget(buttonUndoHoleOperations,row,4,1,1);
    layout6->addWidget(buttonExitHoleOperation,row,5,1,1);
    
    holeOperationWindow->setLayout(layout6);
    
    exitHoleOperations();
    
     
    //ACTIONS
    //clear points
    actionClear2d = new QAction(tr("Clear -> 2D"), this); 
    actionClear2d->setStatusTip(tr("Clear points and go to 2D view")); //hover over text
    connect(actionClear2d, &QAction::triggered, this, &Window::clear2d); //connect signal and response
    actionClear3d = new QAction(tr("Clear -> 3D"), this); 
    actionClear3d->setStatusTip(tr("Clear points and go to 3D view")); //hover over text
    connect(actionClear3d, &QAction::triggered, this, &Window::clear3d); //connect signal and response
    //load points from file
    actionLoadPoints = new QAction(tr("Load points"), this);
    actionLoadPoints->setStatusTip(tr("Load points from file"));
    connect(actionLoadPoints, &QAction::triggered, this, &Window::loadPointsFromFileStandard);
    //load points from file, ignoring any weights
    actionLoadPointsNoWeights = new QAction(tr("Load points (no weights)"), this);
    actionLoadPointsNoWeights->setStatusTip(tr("Load points from file (ignoring weights)"));
    connect(actionLoadPointsNoWeights, &QAction::triggered, this, &Window::loadPointsFromFileNoWeights);
    //recompute complexes for current point set using Euclidean metric, Bregman divergence or Fisher metric
    actionRecomputeWith = new QAction(tr("Recompute with ..."), this);
    actionRecomputeWith->setStatusTip(tr("Recompute with other metric or divergence"));
    connect(actionRecomputeWith, &QAction::triggered, this, &Window::recomputeWith);
    //load points from file, with test on some operation
    actionLoadPointsTest = new QAction(tr("Load points (test ...)"), this);
    actionLoadPointsTest->setStatusTip(tr("Load points from file (with test)"));
    connect(actionLoadPointsTest, &QAction::triggered, this, &Window::loadPointsFromFileTest);
    //generate points from Poisson point process
    actionGeneratePointsPPP = new QAction(tr("Generate points"), this);
    actionGeneratePointsPPP->setStatusTip(tr("Generate points from a Poisson point process"));
    connect(actionGeneratePointsPPP, &QAction::triggered, this, &Window::generatePointsPPP);
    //import filtered complex that was stored to file
    actionImportFilteredComplex = new QAction(tr("Import filtered complex"), this);
    connect(actionImportFilteredComplex, &QAction::triggered, this, &Window::importFilteredComplexFromFile);
    //write current point set to file
    actionExportPoints = new QAction(tr("Export points"),this);
    actionExportPoints->setStatusTip(tr("Write current point set to file"));
    connect(actionExportPoints, &QAction::triggered, this, &Window::exportPoints);
    //write full information about alpha and wrap complexes to file
    actionExportFullInformation = new QAction(tr("Export full information"),this);
    actionExportFullInformation->setStatusTip(tr("Write full information about Alpha and Wrap complexes to file"));
    connect(actionExportFullInformation, &QAction::triggered, this, &Window::exportFullInformation);
    //write set of simplices to file
    actionExportSimplices = new QAction(tr("Export simplices"),this);
    actionExportSimplices->setStatusTip(tr("Write set of simplices to file"));
    connect(actionExportSimplices, &QAction::triggered, this, &Window::exportSimplices);
    //write filtered complex (alpha or wrap) to file, can later be imported
    actionExportFilteredComplex = new QAction(tr("Export filtered complex ..."),this);
    actionExportFilteredComplex->setStatusTip(tr("Write alpha or wrap complexes to file"));
    connect(actionExportFilteredComplex, &QAction::triggered, this, &Window::exportFilteredComplex);
    //write info about intervals to file
    actionExportIntervalInformation = new QAction(tr("Export interval info ..."),this);
    actionExportIntervalInformation->setStatusTip(tr("Write information about intervals to file"));
    connect(actionExportIntervalInformation, &QAction::triggered, this, &Window::exportIntervalInformation);
    //write info about persistence
    actionExportPersistenceInformation = new QAction(tr("Export persistence info ..."),this);
    actionExportPersistenceInformation->setStatusTip(tr("Write persistence information to file"));
    connect(actionExportPersistenceInformation, &QAction::triggered, this, &Window::exportPersistenceInformation);
    //write tripartition of alpha complexes
    actionExportTripartition = new QAction(tr("Export tri-partition (Alpha)"),this);
    actionExportTripartition->setStatusTip(tr("Export tri-partition of Alpha filtration to file"));
    connect(actionExportTripartition, &QAction::triggered, this, &Window::exportTripartition);
    
    //point manipulation
    actionPointManipulation = new QAction(tr("Point manipulation"),this);
    actionPointManipulation->setStatusTip(tr("Open point manipulation window"));
    connect(actionPointManipulation, &QAction::triggered, this, &Window::startPointManipulation);
    
    //hole operation
    actionHoleOperation = new QAction(tr("Hole operation"),this);
    actionHoleOperation->setStatusTip(tr("Open hole operations window"));
    connect(actionHoleOperation, &QAction::triggered, this, &Window::startHoleOperations);
    
    //undo hole operations
    actionUndoHoleOperations = new QAction(tr("Undo hole operations"),this);
    actionUndoHoleOperations->setStatusTip(tr("Undo all hole operations"));
    connect(actionUndoHoleOperations, &QAction::triggered, this, &Window::undoHoleOperations);
    
    //recompute wrap with threshold
    actionRecomputeWrapWithThreshold = new QAction(tr("Recompute with threshold"),this);
    actionRecomputeWrapWithThreshold->setStatusTip(tr("Recompute Wrap complex with imprecision threshold"));
    connect(actionRecomputeWrapWithThreshold, &QAction::triggered, this, &Window::recomputeWrapWithThreshold);
    
    //highlight differences to given complex
    actionHighlightDifferences = new QAction(tr("Highlight differences"),this);
    actionHighlightDifferences->setStatusTip(tr("Highlight differences with given simplicial complex"));
    connect(actionHighlightDifferences, &QAction::triggered, this, &Window::highlightDifferences);
    
    //highlight p-tree (death simplices)
    actionHighlightPTree = new QAction(tr("Highlight p-tree"),this);
    actionHighlightPTree->setStatusTip(tr("Highlight p-tree (death simplices)"));
    connect(actionHighlightPTree, &QAction::triggered, this, &Window::highlightPTree);
    
    //set alpha range
    actionSetAlphaRange = new QAction(tr("Set radius range"),this);
    actionSetAlphaRange->setStatusTip(tr("Set range of radius slider"));
    connect(actionSetAlphaRange, &QAction::triggered, this, &Window::chooseAlphaRange);
    //fit alpha range to critical values
    actionFitAlphaRangeAlpha = new QAction(tr("Fit radius range (Alpha)"),this);
    actionFitAlphaRangeAlpha->setStatusTip(tr("Fit range of radius slider to critical values of the current Alpha complex"));
    connect(actionFitAlphaRangeAlpha, &QAction::triggered, this, &Window::fitAlphaRangeAlpha);
    actionFitAlphaRangeWrap = new QAction(tr("Fit radius range (Wrap)"),this);
    actionFitAlphaRangeWrap->setStatusTip(tr("Fit range of radius slider to critical values of the current Wrap complex"));
    connect(actionFitAlphaRangeWrap, &QAction::triggered, this, &Window::fitAlphaRangeWrap);
   
    //fit view to points
    actionFitView = new QAction(tr("Fit view"),this);
    actionFitView->setStatusTip(tr("Fit view to current data"));
    connect(actionFitView, &QAction::triggered, this, &Window::fitView);
    //set viewing rectangle
    actionSetView = new QAction(tr("Set view"),this);
    connect(actionSetView, &QAction::triggered, this, &Window::setView);
    //redraw scene with new parameters (linewidth, point size)
    actionRedraw = new QAction(tr("Redraw"),this);
    actionRedraw->setStatusTip(tr("Redraw scene with new parameters"));
    connect(actionRedraw, &QAction::triggered, this, &Window::redraw);
    //redraw scene with different colors
    actionChangeColors = new QAction(tr("Change colors"),this);
    actionChangeColors->setStatusTip(tr("Redraw scene with different colors"));
    connect(actionChangeColors, &QAction::triggered, this, &Window::changeColors);
    //export image
    actionExportImagePNG = new QAction(tr("Export PNG-image"),this);
    actionExportImagePNG->setStatusTip(tr("Export drawing as PNG file"));
    connect(actionExportImagePNG, &QAction::triggered, this, &Window::exportImagePNG);
    actionExportImageSVG = new QAction(tr("Export SVG-image"),this);
    actionExportImageSVG->setStatusTip(tr("Export drawing as SVG file"));
    connect(actionExportImageSVG, &QAction::triggered, this, &Window::exportImageSVG);
    //export persistence diagram
    actionExportImagePersDiag = new QAction(tr("Export persistence diagram"),this);
    actionExportImagePersDiag->setStatusTip(tr("Export persistence diagram as SVG file"));
    connect(actionExportImagePersDiag, &QAction::triggered, this, &Window::exportImagePersDiag);
    
    //MENU BAR
    //input menu
    menuInput = menuBar()->addMenu(tr("Input"));
    menuInput->addAction(actionClear2d);
    menuInput->addAction(actionClear3d);
    menuInput->addAction(actionLoadPoints);
    menuInput->addAction(actionLoadPointsNoWeights);
    menuInput->addAction(actionLoadPointsTest);
    menuInput->addAction(actionRecomputeWith);
    menuInput->addAction(actionGeneratePointsPPP);
    menuInput->addAction(actionImportFilteredComplex);
    //export menu
    menuExport = menuBar()->addMenu(tr("Export"));
    menuExport->addAction(actionExportPoints);
    menuExport->addAction(actionExportFullInformation);
    menuExport->addAction(actionExportSimplices);
    menuExport->addAction(actionExportFilteredComplex);
    menuExport->addAction(actionExportIntervalInformation);
    menuExport->addAction(actionExportPersistenceInformation);
    menuExport->addAction(actionExportTripartition);
    //operations menu
    menuOperation = menuBar()->addMenu(tr("Operation"));
    menuOperation->addAction(actionPointManipulation);
    menuOperation->addAction(actionHoleOperation);
    menuOperation->addAction(actionUndoHoleOperations);
    menuOperation->addAction(actionRecomputeWrapWithThreshold);
    menuOperation->addAction(actionHighlightDifferences);
    menuOperation->addAction(actionHighlightPTree);
    //view menu
    menuView = this->menuBar()->addMenu(tr("View"));
    menuView->addAction(actionSetAlphaRange);
    menuView->addAction(actionFitAlphaRangeAlpha);
    menuView->addAction(actionFitAlphaRangeWrap);
    menuView->addAction(actionFitView);
    menuView->addAction(actionSetView);
    menuView->addAction(actionRedraw);
    menuView->addAction(actionChangeColors);
    menuView->addAction(actionExportImagePNG);
    menuView->addAction(actionExportImageSVG);
    menuView->addAction(actionExportImagePersDiag);
    
    //initialize checkbox
    checkBoxAlpha->setChecked(false);
    checkBoxWrap->setChecked(true);
     
    //window properties
    setWindowTitle(tr("Wrap 2 3")); //title
    this->resize(600,800); //size
    
    assignLayoutShowHideStatistics(); //assign layout to main window, update to 2-dim or 3-dim, with or without statistics, adapt menu
}

//assign layout to main window, with or without statistics data
void Window::assignLayoutShowHideStatistics()
{
    show_statistics = !show_statistics; //change current status
    
    //clear layout: remove all widgets
    QLayoutItem *child;
    while ((child = layout->takeAt(0)) != 0) {
        QWidget *widget = child->widget();
        if(widget!=NULL){
            widget->setVisible(false); //hide 
        }
        layout->removeItem(child); //remove from layout
    }
    
    //relabel button
    if(show_statistics){
        buttonShowHideStatistics->setText("Hide statistics");
    }else{
        buttonShowHideStatistics->setText("Show statistics");
    }
    
    //assign widgets newly
    int row = 0;
    //first row
    layout->addWidget(labelAlpha,row,0,1,1);  
    layout->addWidget(labelAlpha2,row,0,1,1);  
    layout->addWidget(checkBoxFull,row,1,1,1,Qt::AlignRight);
    layout->addWidget(sliderAlpha,row,2,1,4);
    layout->addWidget(spinBoxAlpha,row,6,1,1);     
    row++;
    
    //second row   
    if(show_statistics){
        layout->addWidget(buttonShowHideStatistics,row,0,1,2);
        layout->addWidget(labelPoints,row,2,1,1,Qt::AlignRight);
        layout->addWidget(labelEdges,row,3,1,1,Qt::AlignRight);
        layout->addWidget(labelTriangles,row,4,1,1,Qt::AlignRight);
        layout->addWidget(labelTetrahedra,row,5,1,1,Qt::AlignRight); 
        row++;
        layout->addWidget(line1,row,2,1,4,Qt::AlignBottom);
        row++;
        //third row  
        layout->addWidget(checkBoxDelaunay,row,0,1,1);
        layout->addWidget(labelNumPoints,row,2,1,1,Qt::AlignRight);
        layout->addWidget(labelNumEdgesDelaunay,row,3,1,1,Qt::AlignRight);
        layout->addWidget(labelNumTrianglesDelaunay,row,4,1,1,Qt::AlignRight);
        layout->addWidget(labelNumTetrahedraDelaunay,row,5,1,1,Qt::AlignRight);
        row++;
        
        //fourth row   
        layout->addWidget(checkBoxAlpha,row,0,1,1);
        layout->addWidget(labelNumEdgesAlpha,row,3,1,1,Qt::AlignRight);
        layout->addWidget(labelNumTrianglesAlpha,row,4,1,1,Qt::AlignRight);
        layout->addWidget(labelNumTetrahedraAlpha,row,5,1,1,Qt::AlignRight);
        row++;
        
        //fifth row   
        layout->addWidget(checkBoxWrap,row,0,1,1);  
        layout->addWidget(labelNumEdgesWrap,row,3,1,1,Qt::AlignRight);
        layout->addWidget(labelNumTrianglesWrap,row,4,1,1,Qt::AlignRight);
        layout->addWidget(labelNumTetrahedraWrap,row,5,1,1,Qt::AlignRight);
        row++;
        layout->addWidget(line2,row,2,1,4,Qt::AlignBottom);
        row++;
        
        //sixth and seventh row
        layout->addWidget(labelCritical,row,0,1,1);
        layout->addWidget(labelNeg,row,1,1,1,Qt::AlignCenter);
        layout->addWidget(labelPos,row+1,1,1,1,Qt::AlignCenter);
        layout->addWidget(labelNumCritEdgesNeg,row,3,1,1,Qt::AlignRight);
        layout->addWidget(labelNumCritEdgesPos,row+1,3,1,1,Qt::AlignRight);
        layout->addWidget(labelNumCritTrianglesNeg,row,4,1,1,Qt::AlignRight);
        layout->addWidget(labelNumCritTrianglesPos,row+1,4,1,1,Qt::AlignRight);
        layout->addWidget(labelNumCritTetrahedra,row,5,1,1,Qt::AlignRight); 
        row++; row++;
        layout->addWidget(line3,row,2,1,4,Qt::AlignBottom);
        row++;
        
        //eight and ninth row
        layout->addWidget(labelIntervals,row,2,1,1,Qt::AlignRight);
        layout->addWidget(labelNumIntervals,row+1,2,1,1,Qt::AlignRight);
        layout->addWidget(labelBet0,row,3,1,1,Qt::AlignRight);
        layout->addWidget(labelBetti0,row+1,3,1,1,Qt::AlignRight);
        layout->addWidget(labelBet1,row,4,1,1,Qt::AlignRight);
        layout->addWidget(labelBetti1,row+1,4,1,1,Qt::AlignRight);
        layout->addWidget(labelBet2,row,5,1,1,Qt::AlignRight);
        layout->addWidget(labelBetti2,row+1,5,1,1,Qt::AlignRight);
        layout->addWidget(buttonShowPlots,row+1,0,1,2);
        row++;
        row++;
    }else{
        layout->addWidget(buttonShowHideStatistics,row,0,1,2);
        layout->addWidget(checkBoxDelaunay,row,2,1,1);
        layout->addWidget(checkBoxAlpha,row,3,1,1);
        layout->addWidget(checkBoxWrap,row,4,1,1);  
        layout->addWidget(buttonShowPlots,row,5,1,2);
        row++;
    }
    
    //tenth row
    layout->addWidget(geometry,row,0,1,7); //position (row,0), over 1 row and 7 columns
    layout->addWidget(geometry->getOpenGLWidget(),row,0,1,7);
    row++;
    
    //eleventh row  
    layout->addWidget(checkBoxConvexHull,row,0,1,1);
    layout->addWidget(checkBoxUnionOfBalls,row,1,1,2);
    layout->addWidget(checkBoxLabelsIntervals,row,3,1,1);
    layout->addWidget(checkBoxLabelsPoints,row,4,1,1);
    layout->addWidget(buttonShowIntervalGraph,row,5,1,1);
    layout->addWidget(buttonShowPersDiag,row,6,1,1);
    row++;
    
    //twelfth row
    layout->addWidget(checkBoxWeighted,row,0,1,3);
    layout->addWidget(radioButtonBregmanInputPoints,row,3,1,2);
    layout->addWidget(radioButtonBregmanTransformedPoints,row,5,1,2);
    
    //set all of layout's widgets visible
    for (int i = 0; i <layout->count(); ++i)
    {
      QWidget *widget = layout->itemAt(i)->widget();
      if (widget != NULL && widget != geometry->getOpenGLWidget()) 
      {
        widget->setVisible(true);
      }
    }
    
    updateLayoutDim(geometry->isDim3());
    updateLayoutWeighted(geometry->isWeighted()||geometry->isUseRadius2());
    updateLayoutBregman(geometry->isBregman());
}

//some initial commands that only work after window.show()
void Window::init2(int drawing_window_size)
{
    geometry->emitInitialSignals(); //ensure that interactive output shown at start
    geometry->fitView(drawing_window_size); //initial fit to scene
    updateLayoutDim(geometry->isDim3());
    updateLayoutWeighted(geometry->isWeighted()||geometry->isUseRadius2());
    updateLayoutBregman(geometry->isBregman());
    changePersDiag();
    exitHoleOperations();
}

//remove all points and change to 2d view
void Window::clear2d()
{
    geometry->clearPoints();
    geometry->clearScene();
    geometry->setDim3(false);
    geometry->drawScene(); //need to draw special points!
    updateLayoutDim(false); //change to 2-dim view
    updateLayoutWeighted(false);
    updateLayoutBregman(false);
    updatePointManipulationLayout();
}

//remove all points and change to 3d view
void Window::clear3d()
{
    geometry->clearPoints();
    geometry->clearScene();
    geometry->setDim3(true);
    geometry->drawScene(); //need to draw special points!
    updateLayoutDim(true); //change to 2-dim view
    updateLayoutWeighted(false);
    updateLayoutBregman(false);
    updatePointManipulationLayout();
}

void Window::loadPointsFromFileStandard(){
    loadPointsFromFile(0,false);
}

void Window::loadPointsFromFileNoWeights(){
    loadPointsFromFile(false,true);
}

void Window::loadPointsFromFileTest(){
    
    //open dialog to choose test
    QDialog dialog(this);
    QFormLayout form(&dialog); //form layout allows to have labels next to each field
    QString labelTest = "Test ...";
    QGroupBox *groupTest = new QGroupBox();
    QVBoxLayout *layoutTest = new QVBoxLayout;
    QRadioButton *radioButtonDynamicRuntime = new QRadioButton(tr("dynamic runtime"));
    QRadioButton *radioButtonHoleOperations = new QRadioButton(tr("hole operations"));
    radioButtonDynamicRuntime->setChecked(true);
    layoutTest->addWidget(radioButtonDynamicRuntime);
    layoutTest->addWidget(radioButtonHoleOperations);
    groupTest->setLayout(layoutTest);
    form.addRow(labelTest,groupTest);
    //buttons (Cancel/Ok)
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                           Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
    
    //show dialog
    if(dialog.exec() == QDialog::Accepted){
        //if Ok pressed, set values
        int test = 0;
        if(radioButtonDynamicRuntime->isChecked()){
            test=1;
        }else if(radioButtonHoleOperations->isChecked()){
            test=2;
        }
        loadPointsFromFile(test,false);
    }
}

//load points from file, automatically detect if 2-dim or 3-dim
void Window::loadPointsFromFile(int test, bool noweights)
{
    //open dialog to choose file
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open Points file"),QCoreApplication::applicationDirPath());
    if(! fileName.isEmpty()){
        bool correct_input = true;
        try{
            std::vector<DataPoint> data_points;
            std::ifstream ifs(qPrintable(fileName));

            std::string firstline;
            std::getline(ifs, firstline);
            std::istringstream streamLine(firstline);
            int dim=0;
            std::string s;
            for(; streamLine >> s;){
                dim++;
            }
            
            bool weighted=false;
            bool dim3=false;
            if(dim==1 && s=="OFF"){
            //OFF-file
                dim3=true;
                CPoint3 p;
                //save points from input stream 
                std::getline(ifs, firstline); //skip second line
                int i = 1;
                while(std::getline(ifs, firstline)){
                    //check length of line to know if it still contains point information (3 entries, information about triangles 4 entries)
                    std::istringstream streamLine2(firstline);
                    int linelength=0;
                    std::string s2;
                    for(; streamLine2 >> s2;){
                        linelength++;
                    }
                    if(linelength>3){
                        break;
                    } 
                    std::istringstream(firstline) >> p;
                    
                    DataPoint point = DataPoint(false,p.x(),p.y(),p.z());
                    data_points.push_back(point);
                    i++;
                }
            
            //coordinates file
            }else{
                //first line optionally contains dimension, important for weighted input
                if(dim==1){
                    dim=atoi(s.c_str());
                    //check length of second line to know if weighted points
                    std::getline(ifs, firstline);
                    std::istringstream streamLine2(firstline);
                    int linelength=0;
                    std::string s2;
                    for(; streamLine2 >> s2;){
                        linelength++;
                    }
                    if(linelength>dim){
                        weighted=true;
                    }
                }
                if(dim==2){
                    CPoint2 p;
                    exact weight;
                    //save first point
                    if(!weighted){
                        std::istringstream(firstline) >> p;
                    }else{
                        std::istringstream(firstline) >> p >> weight;
                    }
                    DataPoint point = DataPoint(p.x(),p.y());
                    if(weighted && !noweights){
                        point.setWeight(weight);
                    }
                    data_points.push_back(point);
                    //save points from input stream 
                    //(important: directly stream to CGAL points to ensure exact computation)          
                    int i = 1;
                    if(!weighted){
                        while(ifs >> p){
                            point = DataPoint(p.x(),p.y());
                            data_points.push_back(point);
                            i++;
                        }
                    }else{
                        while(ifs >> p >> weight){
                            point = DataPoint(p.x(),p.y());
                            point.setWeight(weight);
                            data_points.push_back(point);
                            i++;
                        }
                    }
                }else if(dim==3){
                    dim3=true;
                    CPoint3 p;
                    exact weight;
                    //save first point
                    if(!weighted){
                        std::istringstream(firstline) >> p;
                    }else{
                        std::istringstream(firstline) >> p >> weight;
                    }
                    DataPoint point = DataPoint(false,p.x(),p.y(),p.z());
                    if(weighted && !noweights){
                        point.setWeight(weight);
                    }
                    data_points.push_back(point);
                    //save points from input stream 
                    //(important: directly stream to CGAL points to ensure exact computation)
                    int i = 1;
                    if(!weighted){
                        while(ifs >> p){
                           point = DataPoint(false,p.x(),p.y(),p.z());
                           data_points.push_back(point);
                           i++;
                        }
                    }else{
                        while(ifs >> p >> weight){
                           point = DataPoint(false,p.x(),p.y(),p.z());
                           if(!noweights){
                            point.setWeight(weight);
                           }
                           data_points.push_back(point);
                           i++;
                        }
                    }
                }else{
                    correct_input = false;
                }
                if(noweights){
                    weighted = false;
                }
            }
            std::string filename = fileName.toStdString();
            std::cout << "Input file: " << filename << std::endl;
            
            //write output file
            s = "../output/output";
            if(!dim3){
                s = s + "2";
            }else{
                s = s + "3";
            }
            std::string base_filename = filename.substr(filename.find_last_of("/\\") + 1);
            s = s + "_" + getCurrentDate() + "_" + base_filename;
            char const* output_filename = s.c_str();
            std::ofstream myfile(output_filename);
            std::cout << s << std::endl;

            //compute and draw
            geometry->clearPoints(); //clear old points (properly deleted)
            geometry->setDrawingParameters(1,0.5,1,15,geometry->getPointSizePersDiag(),geometry->getMaxValuePlots(),geometry->getColorDelaunayByRadius(),false);
            
            //redirect std::cout output to file
            std::streambuf *coutbuf = std::cout.rdbuf(); //save old buffer
            std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile   

            if(test==1){
                //TEST incremental point addition
                std::cout << "# points, " << data_points.size() << std::endl;
                geometry->testIncrementalPointAddition(data_points,-1);
            }
            
            geometry->setPoints(data_points,true,-1,weighted,"",true);

            if(test==2){
                //TEST statistics hole operations
                //geometry->testPersistenceMatrices();
                geometry->computeResultsPaperHoles();
            }

            if(test==0){
                geometry->printOutput();
            }
            
            //geometry->printOutput();

            myfile.close(); //close output file
            std::cout.rdbuf(coutbuf); //reset to standard output again

            double maxwidthheight = fitView();
            if(maxwidthheight<2){
            }else{
                if(maxwidthheight<20){
                    geometry->setDrawingParameters(0.1,0.005,0.1,15,geometry->getPointSizePersDiag(),geometry->getMaxValuePlots(),geometry->getColorDelaunayByRadius(),true);
                }
            }
            fitAlphaRangeWrap(); 
            updateLayoutDim(geometry->isDim3());
            updateLayoutWeighted(geometry->isWeighted()||geometry->isUseRadius2());
            updateLayoutBregman(geometry->isBregman());
        }catch(...){
            std::cerr << "ERROR: Wrong input format!" << std::endl;
            std::cerr << "  expected input: " << std::endl;
            std::cerr << "  < dimension (2 or 3) > (optional, important for weighted case)" << std::endl;
            std::cerr << "  for each point:" << std::endl;
            std::cerr << "  < x-coordinate > < y-coordinate > ( < z-coordinate >) (< weight >)" << std::endl;
        }
        if(!correct_input){
            std::cerr << "ERROR: Wrong input format!" << std::endl;
            std::cerr << "  expected input: " << std::endl;
            std::cerr << "  < dimension (2 or 3) > (optional, important for weighted case)" << std::endl;
            std::cerr << "  for each point:" << std::endl;
            std::cerr << "  < x-coordinate > < y-coordinate > ( < z-coordinate >) (< weight >)" << std::endl;
        }
    }
}

void Window::recomputeWith(){
    //open dialog
    QDialog dialog(this);
    dialog.setWindowTitle("Recompute with ...");
    QFormLayout form(&dialog); //form layout allows to have labels next to each field  
    
    QGroupBox *groupOptions = new QGroupBox();
    QVBoxLayout *layoutOptions = new QVBoxLayout();
    QRadioButton *radioButtonEuclidean = new QRadioButton(tr("Euclidean metric"));
    QRadioButton *radioButtonFisher = new QRadioButton(tr("Fisher metric"));
    QRadioButton *radioButtonBregman = new QRadioButton(tr("Bregman divergence"));
    radioButtonEuclidean->setChecked(true);
    layoutOptions->addWidget(radioButtonEuclidean);
    layoutOptions->addWidget(radioButtonFisher);
    layoutOptions->addWidget(radioButtonBregman);
    groupOptions->setLayout(layoutOptions);
    QGroupBox *groupBregman = new QGroupBox(); 
    QHBoxLayout *layoutBregman = new QHBoxLayout; 
    QRadioButton *radioButtonHalfEuclidean = new QRadioButton(tr("halfEuclidean"));
    QRadioButton *radioButtonShannon = new QRadioButton(tr("Shannon"));
    QRadioButton *radioButtonConjugateShannon = new QRadioButton(tr("conjugateShannon"));
    radioButtonShannon->setChecked(true); //default
    layoutBregman->addWidget(radioButtonHalfEuclidean);
    layoutBregman->addWidget(radioButtonShannon);
    layoutBregman->addWidget(radioButtonConjugateShannon);
    groupBregman->setLayout(layoutBregman);
    QString labelBregman = QString("Bregman");
    form.addRow(groupOptions);
    form.addRow(labelBregman,groupBregman);
    QCheckBox *checkBoxPrimalBalls = new QCheckBox(tr("primal balls"));
    checkBoxPrimalBalls->setChecked(true);
    QCheckBox *checkBoxStandardSimplex = new QCheckBox(tr("standard 2-simplex"));
    checkBoxStandardSimplex->setChecked(false);
    QCheckBox *checkBoxWeightedEuclideanRadii = new QCheckBox(tr("weighted Euclidean radii"));
    checkBoxWeightedEuclideanRadii->setChecked(false);
    form.addRow(checkBoxPrimalBalls,checkBoxWeightedEuclideanRadii);
    QCheckBox *checkBoxRestrictDomain = new QCheckBox(tr("restrict to domain"));
    checkBoxRestrictDomain->setChecked(false);
    form.addRow(checkBoxStandardSimplex);
    QCheckBox *checkBoxPrintCircumcenters = new QCheckBox(tr("print circumcenters"));
    checkBoxPrintCircumcenters->setChecked(false);
    form.addRow(checkBoxRestrictDomain,checkBoxPrintCircumcenters);
    
    //buttons (Cancel/Ok)
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                           Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
    
    //show dialog
    if(dialog.exec() == QDialog::Accepted){
        //if Ok pressed
        bool periodic = (geometry->isPeriodic());
        //declare Euclidean points as weighted in order to have same window size as Bregman complexes
        bool weighted = (geometry->isWeighted());
        std::string bregman_type = "";
        
        QRectF old_scene_rect = geometry->sceneRect();
        
        if(radioButtonBregman->isChecked()){
            periodic = false;
            weighted = false;
            if(radioButtonHalfEuclidean->isChecked()){
                bregman_type = "bregman_halfEuclidean";
            }else if(radioButtonShannon->isChecked()){
                bregman_type = "bregman_Shannon";
                if(checkBoxWeightedEuclideanRadii->isChecked()){
                    bregman_type = "bregman_Shannon_weightedEuclidean";
                }
            }else if(radioButtonConjugateShannon->isChecked()){
                bregman_type = "bregman_conjugateShannon";
                if(checkBoxWeightedEuclideanRadii->isChecked()){
                    bregman_type = "bregman_conjugateShannon_weightedEuclidean";
                }
            }
            if(checkBoxPrimalBalls->isChecked()){
                bregman_type = bregman_type + "_primal";
            }else{
                bregman_type = bregman_type + "_dual";
            }
        }else if(radioButtonFisher->isChecked()){
            periodic = false;
            weighted = false;
            bregman_type = "fisher";
        }
        geometry->setStandardSimplex(checkBoxStandardSimplex->isChecked());
        if(checkBoxStandardSimplex->isChecked()){
            std::vector<DataPoint>* data_points = geometry->getDataPointsPtr();
            for(int i=0; i<data_points->size(); i++){
                (data_points->at(i)).setZ(1-data_points->at(i).getX()-data_points->at(i).getY());
            }
        }
        geometry->setPoints(geometry->getDataPoints(),true,periodic,weighted,bregman_type,true);
        if(checkBoxRestrictDomain->isChecked()||checkBoxPrintCircumcenters->isChecked()){
            geometry->computeCircumcentersMaxSimplices(bregman_type,checkBoxRestrictDomain->isChecked(),checkBoxPrintCircumcenters->isChecked());
        }

        //geometry->printOutput();

        fitAlphaRangeWrap(); 
        updateLayoutWeighted(weighted||geometry->isUseRadius2());
        updateLayoutBregman(geometry->isBregman());
        
        drawBregmanOnInputPoints(radioButtonBregmanInputPoints->isChecked());
        
        if(radioButtonEuclidean->isChecked()){
            //all examples should have same number of rows
            checkBoxWeighted->show(); 
        }
        
        //keep same view as before (for better comparability)
        geometry->setSceneRect(old_scene_rect);
        geometry->fitInView(geometry->sceneRect(), Qt::KeepAspectRatio);
    }
}

//generate points from Poisson point process with specified parameters
void Window::generatePointsPPP()
{
    //open dialog
    QDialog dialog(this);
    dialog.setWindowTitle("Generate points with PPP");
    QFormLayout form(&dialog); //form layout allows to have labels next to each field  
    //switch between 2d and 3d
    QGroupBox *groupDim = new QGroupBox(); //group for radio buttons
    QHBoxLayout *layoutDim = new QHBoxLayout; //horizontal layout
    QRadioButton *radioButton2d = new QRadioButton(tr("2D"));
    QRadioButton *radioButton3d = new QRadioButton(tr("3D"));
    QRadioButton *radioButtonStandardSimplex = new QRadioButton(tr("standard 2-simplex"));
    radioButton2d->setChecked(true); //default
    layoutDim->addWidget(radioButton2d);
    layoutDim->addWidget(radioButton3d);
    layoutDim->addWidget(radioButtonStandardSimplex);
    groupDim->setLayout(layoutDim);
    form.addRow(groupDim);
    QCheckBox *checkBoxUniformForFisher = new QCheckBox(tr("uniform for Fisher metric"));
    checkBoxUniformForFisher->setChecked(false);
    form.addRow(checkBoxUniformForFisher);
    //input fields
    form.addRow(new QLabel("Set the parameters for the Poisson Point process:")); //text
    QLineEdit *lineEdit1 = new QLineEdit(&dialog);
    QString label1 = QString("Window size");
    QLineEdit *lineEdit2 = new QLineEdit(&dialog);
    QString label2 = QString("Lambda");
    QCheckBox *checkBoxFixedNumPoints = new QCheckBox(tr("fixed # points = lambda"));
    QLineEdit *lineEdit3 = new QLineEdit(&dialog);
    QString label3 = QString("# Trials");
    form.addRow(label1,lineEdit1);
    form.addRow(label2,lineEdit2);
    form.addRow(checkBoxFixedNumPoints);
    form.addRow(label3,lineEdit3);
    QCheckBox *checkBoxWeights = new QCheckBox(tr("Weights"));
    checkBoxWeights->setChecked(false);
    QString labelMin = QString("min weight");
    QString labelMax = QString("max weight");
    QLineEdit *lineEditMinWeight = new QLineEdit(&dialog);
    QLineEdit *lineEditMaxWeight = new QLineEdit(&dialog);
    form.addRow(checkBoxWeights);
    form.addRow(labelMin,lineEditMinWeight);
    form.addRow(labelMax,lineEditMaxWeight);
    QCheckBox *checkBoxDraw = new QCheckBox(tr("Draw"));
    checkBoxDraw->setChecked(true);
    QCheckBox *checkBoxPeriodic = new QCheckBox(tr("Periodic"));
    checkBoxPeriodic->setChecked(false);
    form.addRow(checkBoxDraw,checkBoxPeriodic);
    QCheckBox *checkBoxTestHoles = new QCheckBox(tr("hole manipulation statistics"));
    checkBoxTestHoles->setChecked(false);
    form.addRow(checkBoxTestHoles);
    QCheckBox *checkBoxTestPerturbation = new QCheckBox(tr("perturb filtration"));
    QLineEdit *lineEditPerturbation = new QLineEdit(&dialog);
    checkBoxTestPerturbation->setChecked(false);
    form.addRow(checkBoxTestPerturbation,lineEditPerturbation);
    QCheckBox *checkBoxTestBregman = new QCheckBox(tr("Bregman"));
    checkBoxTestBregman->setChecked(false);
    QGroupBox *groupBregman = new QGroupBox(); //group for radio buttons
    QHBoxLayout *layoutBregman = new QHBoxLayout; //horizontal layout
    QRadioButton *radioButtonEuclidean = new QRadioButton(tr("halfEuclidean"));
    QRadioButton *radioButtonShannon = new QRadioButton(tr("Shannon"));
    QRadioButton *radioButtonConjugateShannon = new QRadioButton(tr("conjugateShannon"));
    radioButtonEuclidean->setChecked(true); //default
    layoutBregman->addWidget(radioButtonEuclidean);
    layoutBregman->addWidget(radioButtonShannon);
    layoutBregman->addWidget(radioButtonConjugateShannon);
    groupBregman->setLayout(layoutBregman);
    form.addRow(checkBoxTestBregman);
    form.addRow(groupBregman);
    //buttons (Cancel/Ok)
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                           Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
    
    lineEdit1->setText(QString::number(5));
    lineEdit2->setText(QString::number(1));
    lineEdit3->setText(QString::number(1));
    lineEditMinWeight->setText(QString::number(0));
    lineEditMaxWeight->setText(QString::number(1));
    lineEditPerturbation->setText(QString::number(0.01));
    
    //show dialog
    if(dialog.exec() == QDialog::Accepted){
        //if Ok pressed, set values
        double window_size = lineEdit1->text().toDouble();
        double lambda = lineEdit2->text().toDouble();
        int ntrials = lineEdit3->text().toInt();
        bool draw = checkBoxDraw->isChecked();
        bool periodic = checkBoxPeriodic->isChecked();
        bool dim3 = radioButton3d->isChecked();
        bool on_standard_simplex = radioButtonStandardSimplex->isChecked();
        bool uniform_for_fisher = checkBoxUniformForFisher->isChecked();
        double min_weight = -1;
        double max_weight = -1;
        bool weighted = checkBoxWeights->isChecked();
        if(weighted){
            min_weight = lineEditMinWeight->text().toDouble();
            max_weight = lineEditMaxWeight->text().toDouble();
        }
        double test_value = -1;
        std::string test = "";
        if(checkBoxTestHoles->isChecked()){
            test = "holes_paper";
        }
        if(checkBoxTestPerturbation->isChecked()){
            test = "perturb_filtration";
            test_value = lineEditPerturbation->text().toDouble();
        }
        if(checkBoxTestBregman->isChecked()){
            if(radioButtonEuclidean->isChecked()){
                test = "bregman_halfEuclidean";
            }else if(radioButtonShannon->isChecked()){
                test = "bregman_Shannon";
            }else if(radioButtonConjugateShannon->isChecked()){
                test = "bregman_conjugateShannon";
            }
        }
        bool fixed_num_points = checkBoxFixedNumPoints->isChecked();
        geometry->generatePointsPPP(window_size,lambda,fixed_num_points,ntrials,weighted,min_weight,max_weight,draw,periodic,dim3,on_standard_simplex,uniform_for_fisher,true,test,test_value);
        fitAlphaRangeWrap();
        updateLayoutDim(geometry->isDim3());
        updateLayoutWeighted(geometry->isWeighted()||geometry->isUseRadius2());
        updateLayoutBregman(geometry->isBregman());
        if(checkBoxTestBregman->isChecked()){
            drawBregmanOnInputPoints(radioButtonBregmanInputPoints->isChecked());
        }
    }
}

//import filtered complex that was stord to file
void Window::importFilteredComplexFromFile()
{
    //open dialog to choose file
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open filtered complex file"),QCoreApplication::applicationDirPath());
    if(! fileName.isEmpty()){
        try{ 
          
            geometry->importFilteredComplexFromFile(fileName.toStdString(),true);
      
            fitAlphaRangeWrap(); 
            updateLayoutDim(geometry->isDim3());
            updateLayoutWeighted(geometry->isWeighted()||geometry->isUseRadius2());
            updateLayoutBregman(geometry->isBregman());
        }catch(...){ 
           std::cerr << "ERROR: Wrong input format!" << std::endl;
        }
    }
}

//write current point set to file
void Window::exportPoints()
{
    //open dialog to choose file location
    QString filename = QFileDialog::getSaveFileName(this,tr("Export points to file"),QCoreApplication::applicationDirPath()+"/data_export", tr("TXT files (*.txt)"));
    if(! filename.isEmpty()){
        if(!filename.endsWith(".txt")){
            filename += ".txt";
        }
        std::string filename_str = filename.toStdString();
        geometry->exportPointsToFile(filename_str);

        std::cout << "Points exported to: " << filename_str << std::endl;
    }
}

//write full information about alpha and wrap complexes to file
void Window::exportFullInformation()
{
    //open dialog to choose file location
    QString filename = QFileDialog::getSaveFileName(this,tr("Export full information to file"),QCoreApplication::applicationDirPath()+"/output", tr("TXT files (*.txt)"));
    if(! filename.isEmpty()){
         if(!filename.endsWith(".txt")){
            filename += ".txt";
        }
        //open output file
        std::string filename_str = filename.toStdString();
        std::ofstream myfile(filename_str.c_str());
        //redirect output to file
        std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
        std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile 
        
        geometry->printFullInformation(std::cout); //print
        
        myfile.close(); //close output file
        std::cout.rdbuf(coutbuf); //reset to standard output again

        std::cout << "Full information printed to: " << filename_str << std::endl;
    }
}

//write set of simplices to file
void Window::exportSimplices()
{
    //open dialog to choose file location
    QString filename = QFileDialog::getSaveFileName(this,tr("Export set of simplices to file"),QCoreApplication::applicationDirPath()+"/output", tr("TXT files (*.txt)"));
    if(! filename.isEmpty()){
        if(!filename.endsWith(".txt")){
            filename += ".txt";
        }
        std::string filename_str = filename.toStdString();
        std::ofstream myfile(filename_str.c_str());
        //redirect output to file
        std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
        std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile 
        
        geometry->printSimplices(std::cout); //print
        
        myfile.close(); //close output file
        std::cout.rdbuf(coutbuf); //reset to standard output again

        std::cout << "Simplices printed to: " << filename_str << std::endl;
    }
}

//write filtered complex (alpha or wrap) to file, can later be imported
void Window::exportFilteredComplex()
{
    //open dialog to choose complex
    QDialog dialog(this);
    QFormLayout form(&dialog); //form layout allows to have labels next to each field
    QString label0 = "Export ...";
    QGroupBox *group0 = new QGroupBox();
    QVBoxLayout *layout0 = new QVBoxLayout;
    QRadioButton *radioButton1 = new QRadioButton(tr("Alpha complexes"));
    QRadioButton *radioButton2 = new QRadioButton(tr("Wrap complexes"));
    radioButton1->setChecked(true);
    layout0->addWidget(radioButton1);
    layout0->addWidget(radioButton2);
    group0->setLayout(layout0);
    form.addRow(label0,group0);
    //buttons (Cancel/Ok)
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                           Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
    
    //show dialog
    if(dialog.exec() == QDialog::Accepted){
        //if Ok pressed, set values
        bool export_alpha = false;
        if(radioButton1->isChecked()){
            export_alpha = true;
        }
        QString dialog_title = "Export Alpha complexes to file";
        if(!export_alpha){
            dialog_title = "Export Wrap complexes to file";
        }
        
        //open dialog to choose file location
        QString filename = QFileDialog::getSaveFileName(this,dialog_title,QCoreApplication::applicationDirPath()+"/output", tr("TXT files (*.txt)"));
        if(! filename.isEmpty()){
            if(!filename.endsWith(".txt")){
                filename += ".txt";
            }
            std::string filename_str = filename.toStdString();
            std::ofstream myfile(filename_str.c_str());
            //redirect output to file
            std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
            std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile 

            if(export_alpha){
                geometry->printAlphaComplexes(std::cout); //print alpha complexes
            }else{
                geometry->printWrapComplexes(std::cout); //print wrap complexes
            }

            myfile.close(); //close output file
            std::cout.rdbuf(coutbuf); //reset to standard output again

            if(export_alpha){
                std::cout << "Alpha complexes printed to: " << filename_str << std::endl;
            }else{
                std::cout << "Wrap complexes printed to: " << filename_str << std::endl;
            }
        } 
    }  
}

//write info about intervals to file
void Window::exportIntervalInformation()
{
    //open dialog to choose complex
    QDialog dialog(this);
    QFormLayout form(&dialog); //form layout allows to have labels next to each field
    QString label0 = "Export ...";
    QGroupBox *group0 = new QGroupBox();
    QVBoxLayout *layout0 = new QVBoxLayout;
    QRadioButton *radioButton1 = new QRadioButton(tr("interval statistics"));
    QRadioButton *radioButton2 = new QRadioButton(tr("detailed interval information"));
    radioButton1->setChecked(true);
    layout0->addWidget(radioButton1);
    layout0->addWidget(radioButton2);
    group0->setLayout(layout0);
    form.addRow(label0,group0);
    //buttons (Cancel/Ok)
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                           Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
    
    //show dialog
    if(dialog.exec() == QDialog::Accepted){
        //if Ok pressed, set values
        bool export_statistics = false;
        if(radioButton1->isChecked()){
            export_statistics = true;
        }
        QString dialog_title = "Export interval statistics to file";
        if(!export_statistics){
            dialog_title = "Export detailed interval information to file";
        }
        
        //open dialog to choose file location
        QString filename = QFileDialog::getSaveFileName(this,dialog_title,QCoreApplication::applicationDirPath()+"/output", tr("TXT files (*.txt)"));
        if(! filename.isEmpty()){
             if(!filename.endsWith(".txt")){
                filename += ".txt";
            }
            //open output file
            std::string filename_str = filename.toStdString();
            std::ofstream myfile(filename_str.c_str());
            //redirect output to file
            std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
            std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile 

            if(export_statistics){
                geometry->printIntervalStatistics(); //print interval statistics
            }else{
                geometry->printIntervals(); //print detailed interval information
            }

            myfile.close(); //close output file
            std::cout.rdbuf(coutbuf); //reset to standard output again

            if(export_statistics){
                std::cout << "Interval statistics printed to: " << filename_str << std::endl;
            }else{
                std::cout << "Interval information printed to: " << filename_str << std::endl;
            }
        }
    }
}

//write info about persistence to file
void Window::exportPersistenceInformation()
{
    //open dialog to choose complex
    QDialog dialog(this);
    QFormLayout form(&dialog); //form layout allows to have labels next to each field
    QString label0 = "Export ...";
    QGroupBox *group0 = new QGroupBox();
    QVBoxLayout *layout0 = new QVBoxLayout;
    QRadioButton *radioButton1 = new QRadioButton(tr("persistence barcode"));
    QRadioButton *radioButton2 = new QRadioButton(tr("persistence pairs (Wrap)"));
    QRadioButton *radioButton3 = new QRadioButton(tr("persistence pairs (Alpha)"));
    QRadioButton *radioButton4 = new QRadioButton(tr("boundary matrix (Wrap)"));
    QRadioButton *radioButton5 = new QRadioButton(tr("boundary matrix (Alpha)"));
    QRadioButton *radioButton6 = new QRadioButton(tr("reduction matrices (Wrap)"));    
    QRadioButton *radioButton7 = new QRadioButton(tr("reduction matrices (Alpha)"));    
    layout0->addWidget(radioButton1);
    layout0->addWidget(radioButton2);
    layout0->addWidget(radioButton3);
    layout0->addWidget(radioButton4);
    layout0->addWidget(radioButton5);
    layout0->addWidget(radioButton6);
    layout0->addWidget(radioButton7);
    radioButton1->setChecked(true);
    group0->setLayout(layout0);
    form.addRow(label0,group0);
    //buttons (Cancel/Ok)
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                           Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
    
    //show dialog
    if(dialog.exec() == QDialog::Accepted){
        //if Ok pressed, set values
        int option = 1;
        if(radioButton2->isChecked()){
            option = 2;
        }else if(radioButton3->isChecked()){
            option = 3;
        }else if(radioButton4->isChecked()){
            option = 4;
        }else if(radioButton5->isChecked()){
            option = 5;
        }else if(radioButton6->isChecked()){
            option = 6;
        }else if(radioButton7->isChecked()){
            option = 7;
        }
        QString dialog_title = "Export persistence barcode to file";
        if(option==2){
            dialog_title = "Export persistence pairs for Wrap filtration to file";
        }else if(option==3){
            dialog_title = "Export persistence pairs for Alpha filtration to file";
        }else if(option==4){ 
            dialog_title = "Export boundary matrix for Wrap filtration to file";
        }else if(option==5){ 
            dialog_title = "Export boundary matrix for Alpha filtration to file";
        }else if(option==6){ 
            dialog_title = "Export reduction matrices for Wrap filtration to file";
        }else if(option==7){ 
            dialog_title = "Export reduction matrices for Alpha filtration to file";
        }
        
        //open dialog to choose file location
        QString filename = QFileDialog::getSaveFileName(this,dialog_title,QCoreApplication::applicationDirPath()+"/output", tr("TXT files (*.txt)"));
        if(! filename.isEmpty()){
             if(!filename.endsWith(".txt")){
                filename += ".txt";
            }
            //open output file
            std::string filename_str = filename.toStdString();
            std::ofstream myfile(filename_str.c_str());
            //redirect output to file
            std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
            std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile 

            if(option==1){
                geometry->printPersistenceBarcode();
            }else if(option==2){
                geometry->printPersistencePairs(true,std::cout);
            }else if(option==3){
                geometry->printPersistencePairs(false,std::cout);
            }else if(option==4){
                geometry->printBoundaryMatrix(true,std::cout); 
            }else if(option==5){
                geometry->printBoundaryMatrix(false,std::cout); 
            }else if(option==6){
                geometry->printPersistenceMatrices(true,std::cout);
            }else{
                geometry->printPersistenceMatrices(false,std::cout);
            }

            myfile.close(); //close output file
            std::cout.rdbuf(coutbuf); //reset to standard output again

            if(option==1){
                std::cout << "Persistence barcode printed to: " << filename_str << std::endl;
            }else if(option==2){
                std::cout << "Persistence pairs for Wrap filtration printed to: " << filename_str << std::endl;
            }else if(option==3){
                std::cout << "Persistence pairs for Alpha filtration printed to: " << filename_str << std::endl;
            }else if(option==4){
                std::cout << "Boundary matrix for Wrap filtration printed to: " << filename_str << std::endl;
            }else if(option==5){
                std::cout << "Boundary matrix for Alpha filtration printed to: " << filename_str << std::endl;
            }else if(option==6){ 
                std::cout << "Persistence matrices for Wrap filtration printed to: " << filename_str << std::endl;
            }else if(option==7){ 
                std::cout << "Persistence matrices for Alpha filtration printed to: " << filename_str << std::endl;
            }
        }
    }
}

//write tri-partition of alpha simplices to file
void Window::exportTripartition()
{
    //open dialog to choose file location
    QString filename = QFileDialog::getSaveFileName(this,tr("Export tri-partition for Alpha filtration to file"),QCoreApplication::applicationDirPath()+"/output", tr("TXT files (*.txt)"));
    if(! filename.isEmpty()){
         if(!filename.endsWith(".txt")){
            filename += ".txt";
        }
        //open output file
        std::string filename_str = filename.toStdString();
        std::ofstream myfile(filename_str.c_str());
        //redirect output to file
        std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
        std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile 
        
        geometry->printAlphaSimplicesPartition(); //print
        
        myfile.close(); //close output file
        std::cout.rdbuf(coutbuf); //reset to standard output again

        std::cout << "Tri-partition for Alpha filtration printed to: " << filename_str << std::endl;
    }
}

//open hole operations window
void Window::startHoleOperations()
{
    holeoperations_subcomplex = radioButtonsHoleOperationSubcomplex[1]->isChecked();
    
    holeOperationWindow->show();
    holeOperationWindow->raise();
    holeOperationWindow->activateWindow();
    
    updateHighlightPersistenceValue();
    highlightChain();
}

//update hole operation window after any changes
void Window::updateHoleOperationWindow()
{
    holeoperations_subcomplex = radioButtonsHoleOperationSubcomplex[1]->isChecked();
    updateHighlightPersistenceValue();
    if(holeOperationWindow->isVisible()){
        highlightChain();
    }
}

//apply hole operation with current window settings
void Window::applyHoleOperation()
{
    geometry->clearHighlightedItems();
    
    int dim = spinBoxHoleOperationPersPairDim->value();
    int num_pers_pair = spinBoxHoleOperationPersPairNum->value();
    bool lock = true;
    bool un = false;
    if(radioButtonsHoleOperationType[1]->isChecked()){
        un = true;
    }else if(radioButtonsHoleOperationType[2]->isChecked()){
        lock = false;
    }else if(radioButtonsHoleOperationType[3]->isChecked()){
        un = true;
        lock = false;;
    }
    bool subcomplex = holeoperations_subcomplex;
    bool with_bound = checkBoxHoleOperationPersistenceBound->isChecked();
    double persistence_bound = -1;
    if(with_bound){
        persistence_bound = lineEditHoleOperationPersistenceBound->text().toDouble();
    }
    bool lowerbound = radioButtonsHoleOperationPersistenceBound[0]->isChecked();
    bool printinfo = checkBoxHoleOperationPrintInfo->isChecked();
    geometry->holeOperation(lock,un,dim,num_pers_pair,subcomplex,persistence_bound,lowerbound,printinfo);
    if(!(geometry->isWeighted()||geometry->isUseRadius2())){
        sliderAlpha->setValueDouble(std::sqrt(geometry->getAlpha2Current()));
    }else{
        sliderAlpha->setValueDouble(geometry->getAlpha2Current());
    }
    update();
    
}

//whether to perform hole operations and highlight chains and full complex or current subcomplex
void Window::updateHoleOperationSubcomplexMode()
{
    holeoperations_subcomplex = radioButtonsHoleOperationSubcomplex[1]->isChecked();
    highlightChain();
}

//hole operations with dependence structure of homology or cohomology
void Window::updateHoleOperationCohomology()
{
    geometry->setHoleOperationsCohomology(checkBoxHoleOperationCohomology->isChecked());
    highlightChain();
}

//highlight chain chosen with current window settings
void Window::highlightChain()
{
    updateHighlightPersistenceValue();
    
    holeoperations_highlight = checkBoxHoleOperationHighlight->isChecked() && holeOperationWindow->isVisible();
    
    if(holeoperations_highlight){
        int dim = spinBoxHoleOperationPersPairDim->value();
        int num = spinBoxHoleOperationPersPairNum->value();
        bool highlight_mode = radioButtonsHighlightMode[0]->isChecked();
        bool lock = true;
        bool un = false;
        if(radioButtonsHoleOperationType[1]->isChecked()){
            un = true;
        }else if(radioButtonsHoleOperationType[2]->isChecked()){
            lock = false;
        }else if(radioButtonsHoleOperationType[3]->isChecked()){
            lock = false; un = true;
        }
        if(!checkBoxHoleOperationPersistenceBound->isChecked()){
            geometry->highlightChain(dim,num,lock,un,highlight_mode,holeoperations_subcomplex,buttonSelectHighlightColor1->getColor(),buttonSelectHighlightColor2->getColor());
        }else{
            geometry->highlightPersistencePairsWithBoundStatus(dim, lineEditHoleOperationPersistenceBound->text().toDouble(), radioButtonsHoleOperationPersistenceBound[0]->isChecked(), lock, un, holeoperations_subcomplex, buttonSelectHighlightColor2->getColor());
        }
    }else{
       geometry->clearHighlightedItems(); 
    }
}

//update value of currently chosen persistence pair
void Window::updateHighlightPersistenceValue()
{
    int dim = spinBoxHoleOperationPersPairDim->value();
    int num = spinBoxHoleOperationPersPairNum->value();
    bool lock = true;
    bool un = false;
    if(radioButtonsHoleOperationType[1]->isChecked()){
        un = true;
    }else if(radioButtonsHoleOperationType[2]->isChecked()){
        lock = false;
    }else if(radioButtonsHoleOperationType[3]->isChecked()){
        lock = false; un = true;
    }
    double value = geometry->getPersistenceValueOfDim(dim,num,holeoperations_subcomplex,lock,un);

    labelHoleOperationPersPairPersistenceValue->setText(QString::number(value));
 }

//close highlight chains window
void Window::exitHoleOperations(){ 
    holeOperationWindow->hide();
    geometry->clearHighlightedItems();
    geometry->displayDrawing();
    holeoperations_subcomplex = false;
}

//undo all hole operations
void Window::undoHoleOperations()
{
    geometry->clearHighlightedItems();
    geometry->undoHoleOperations();
    if(holeOperationWindow->isVisible()){
        highlightChain();
    }
}

//update how to draw persistence diagram with status information
void Window::updatePersistenceDiagramStatusSelection()
{
    std::vector<bool> draw_dim;
    draw_dim.push_back(checkBoxPersDiagStatusDrawDim[0]->isChecked());
    draw_dim.push_back(checkBoxPersDiagStatusDrawDim[1]->isChecked());
    draw_dim.push_back(checkBoxPersDiagStatusDrawDim[2]->isChecked());
    std::vector<bool> draw_status;
    draw_status.push_back(checkBoxPersDiagStatusDrawStatus[0]->isChecked());
    draw_status.push_back(checkBoxPersDiagStatusDrawStatus[1]->isChecked());
    draw_status.push_back(checkBoxPersDiagStatusDrawStatus[2]->isChecked());
    int highlight_dependences_of_simplex = -1;
    bool lock = true;
    bool un = false;
    int simplex_index_last_operation = -1;
    if(checkBoxPersDiagStatusDrawDependentPairs->isChecked()){
        if(radioButtonsHoleOperationType[1]->isChecked()){
            un = true;
        }else if(radioButtonsHoleOperationType[2]->isChecked()){
            lock = false;
        }else if(radioButtonsHoleOperationType[3]->isChecked()){
            un = true;
            lock = false;
        } 
        int dim = spinBoxHoleOperationPersPairDim->value();
        int num_pers_pair = spinBoxHoleOperationPersPairNum->value();
        if(checkBoxPersDiagStatusDrawDependentPairsLastOperation->isChecked()){
            simplex_index_last_operation = geometry->getLastHoleOperationSimplex();
        }
        if(simplex_index_last_operation < 0){
            PersistencePair pair = geometry->getPersistencePairOfDim(dim,num_pers_pair,true,lock,un,false);
            if(lock){
                highlight_dependences_of_simplex = pair.getBirthIndexFiltration();
            }else{
                highlight_dependences_of_simplex = pair.getDeathIndexFiltration();
            }
        }else{
            highlight_dependences_of_simplex = simplex_index_last_operation;
        }
    }
    geometry->updatePersistenceDiagramStatusSelection(draw_dim,draw_status, highlight_dependences_of_simplex, lock, un, checkBoxPersDiagStatusDrawRecursivelyDependentPairs->isChecked(),simplex_index_last_operation>=0);
}

//set range of alpha slider
void Window::chooseAlphaRange()
{
    //open dialog
    QDialog dialog(this);
    QFormLayout form(&dialog); //form layout allows to have labels next to each field
    form.addRow(new QLabel("Set the slider's range of radius values:")); //text
    //input fields
    QLineEdit *lineEditMin = new QLineEdit(&dialog);
    QString labelMin = QString("min");
    QLineEdit *lineEditMax = new QLineEdit(&dialog);
    QString labelMax = QString("max");
    form.addRow(labelMin,lineEditMin);
    form.addRow(labelMax,lineEditMax);
    //buttons (Cancel/Ok)
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                           Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
    
    lineEditMin->setText(QString::number(spinBoxAlpha->getMinDouble()));
    lineEditMax->setText(QString::number(spinBoxAlpha->getMaxDouble()));
    
    //show dialog
    if(dialog.exec() == QDialog::Accepted){
        //if Ok pressed, set values
        double new_min = lineEditMin->text().toDouble();
        double new_max = lineEditMax->text().toDouble();
        spinBoxAlpha->setDecimals(0);
        sliderAlpha->setDecimals(0);
        if(new_max-new_min<=0.01){
            spinBoxAlpha->setDecimals(5);
            sliderAlpha->setDecimals(5);
        }else if(new_max-new_min<=0.1){
            spinBoxAlpha->setDecimals(4);
            sliderAlpha->setDecimals(4);
        }else if(new_max-new_min<=1){
            spinBoxAlpha->setDecimals(3);
            sliderAlpha->setDecimals(3);
        }else if(new_max-new_min<=10){
            spinBoxAlpha->setDecimals(2);
            sliderAlpha->setDecimals(2);
        }else if(new_max-new_min<=100){
            spinBoxAlpha->setDecimals(1);
            sliderAlpha->setDecimals(1);
        }  
        spinBoxAlpha->setRangeDouble(new_min, new_max);
        sliderAlpha->setRangeDouble(new_min, new_max);
        spinBoxAlpha->setValue(spinBoxAlpha->maximum());
    }
}

//fit range of alpha slider to critical values of Alpha complex
void Window::fitAlphaRangeAlpha()
{
    double new_max, new_min;
    if(!(geometry->isWeighted()||geometry->isUseRadius2())){
        new_min = std::sqrt(CGAL::to_double(geometry->getAlpha2MinAlpha()));
        new_max = std::sqrt(CGAL::to_double(geometry->getAlpha2MaxAlpha())); 
    }else{
        new_min = (CGAL::to_double(geometry->getAlpha2MinAlpha()));
        new_max = (CGAL::to_double(geometry->getAlpha2MaxAlpha())); 
    }
    spinBoxAlpha->setDecimals(0);
    sliderAlpha->setDecimals(0);
    if(new_max-new_min<=0.01){
        spinBoxAlpha->setDecimals(5);
        sliderAlpha->setDecimals(5);
    }else if(new_max-new_min<=0.1){
        spinBoxAlpha->setDecimals(4);
        sliderAlpha->setDecimals(4);
    }else if(new_max-new_min<=1){
        spinBoxAlpha->setDecimals(3);
        sliderAlpha->setDecimals(3);
    }else if(new_max-new_min<=10){
        spinBoxAlpha->setDecimals(2);
        sliderAlpha->setDecimals(2);
    }else if(new_max-new_min<=100){
        spinBoxAlpha->setDecimals(1);
        sliderAlpha->setDecimals(1);
    }  
    spinBoxAlpha->setRangeDouble(new_min, new_max);
    sliderAlpha->setRangeDouble(new_min, new_max);
    spinBoxAlpha->setValue(spinBoxAlpha->maximum());
    
    geometry->update();
}

//fit range of alpha slider to critical values of Wrap complex
void Window::fitAlphaRangeWrap()
{
    double new_min, new_max;
    if(!geometry->isEmpty()){
        if(!(geometry->isWeighted()||geometry->isUseRadius2())){
            new_min = std::sqrt(CGAL::to_double(geometry->getAlpha2MinWrap()));
            new_max = std::sqrt(CGAL::to_double(geometry->getAlpha2MaxWrap()));
        }else{
            new_min = (CGAL::to_double(geometry->getAlpha2MinWrap()));
            new_max = (CGAL::to_double(geometry->getAlpha2MaxWrap()));
        }
        new_max = std::min(new_max,10000.);
        
        spinBoxAlpha->setDecimals(0);
        sliderAlpha->setDecimals(0);
        if(new_max-new_min<=0.01){
            spinBoxAlpha->setDecimals(5);
            sliderAlpha->setDecimals(5);
        }else if(new_max-new_min<=0.1){
            spinBoxAlpha->setDecimals(4);
            sliderAlpha->setDecimals(4);
        }else if(new_max-new_min<=1){
            spinBoxAlpha->setDecimals(3);
            sliderAlpha->setDecimals(3);
        }else if(new_max-new_min<=10){
            spinBoxAlpha->setDecimals(2);
            sliderAlpha->setDecimals(2);
        }else if(new_max-new_min<=100){
            spinBoxAlpha->setDecimals(1);
            sliderAlpha->setDecimals(1);
        }
        spinBoxAlpha->setRangeDouble(new_min, new_max);
        sliderAlpha->setRangeDouble(new_min, new_max);
        spinBoxAlpha->setValue(spinBoxAlpha->maximum());
        
        //geometry->update();
    }
}

//fit window view to current points
double Window::fitView()
{
    return geometry->fitView();
}

//set viewing rectangle
void Window::setView()
{
    //open dialog
    QDialog dialog(this);
    QFormLayout form(&dialog); //form layout allows to have labels next to each field
    form.addRow(new QLabel("Set the values for the graphics view:")); //text
    //input fields
    QLineEdit *lineEditMinX = new QLineEdit(&dialog);
    QString labelMinX = QString("min_x");
    QLineEdit *lineEditMaxX = new QLineEdit(&dialog);
    QString labelMaxX = QString("max_x");
    QLineEdit *lineEditMinY = new QLineEdit(&dialog);
    QString labelMinY = QString("min_y");
    QLineEdit *lineEditMaxY = new QLineEdit(&dialog);
    QString labelMaxY = QString("max_y");
    form.addRow(labelMinX,lineEditMinX);
    form.addRow(labelMaxX,lineEditMaxX);
    form.addRow(labelMinY,lineEditMinY);
    form.addRow(labelMaxY,lineEditMaxY);
    //buttons (Cancel/Ok)
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                           Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
    
    lineEditMinX->setText(QString::number(geometry->sceneRect().left()));
    lineEditMaxX->setText(QString::number(geometry->sceneRect().right()));
    lineEditMinY->setText(QString::number(geometry->sceneRect().top()));
    lineEditMaxY->setText(QString::number(geometry->sceneRect().bottom()));
    
    //show dialog
    if(dialog.exec() == QDialog::Accepted){
        //if Ok pressed, set values
        double new_min_x = lineEditMinX->text().toDouble();
        double new_max_x = lineEditMaxX->text().toDouble();
        double new_min_y = lineEditMinY->text().toDouble();
        double new_max_y = lineEditMaxY->text().toDouble();
        geometry->setSceneRect(new_min_x,new_min_y,new_max_x-new_min_x,new_max_y-new_min_y);
        geometry->fitInView(geometry->sceneRect(), Qt::KeepAspectRatio);
    }
}

//redraw scene with different parameters (linewidth, point size)
void Window::redraw(){
    geometry->clearHighlightedItems();
    //open dialog
    QDialog dialog(this);
    QFormLayout form(&dialog); //form layout allows to have labels next to each field
    form.addRow(new QLabel("Set the parameters for the scene:")); //text
    //input fields
    QLineEdit *lineEdit1 = new QLineEdit(&dialog);
    QString label1 = QString("Point size (complex)");
    QLineEdit *lineEdit1b = new QLineEdit(&dialog);
    QString label1b = QString("Point size (input)");
    QLineEdit *lineEdit2 = new QLineEdit(&dialog);
    QString label2 = QString("Line width");
    QLineEdit *lineEdit3 = new QLineEdit(&dialog);
    QString label3 = QString("Font size (labels)");
    QLineEdit *lineEdit4 = new QLineEdit(&dialog);
    QString label4 = QString("Point size (pers diag)");
    QLineEdit *lineEditMaxValue = new QLineEdit(&dialog);
    QString labelMaxValue = QString("Max value (plots)");
    form.addRow(label1,lineEdit1);
    form.addRow(label1b,lineEdit1b);
    form.addRow(label2,lineEdit2);
    form.addRow(label3,lineEdit3);
    form.addRow(label4,lineEdit4);
    form.addRow(labelMaxValue,lineEditMaxValue);
    
    QLineEdit *lineEdit5;
    QLineEdit *lineEdit6;
    QLineEdit *lineEdit7;
    QLineEdit *lineEdit8;
    QLineEdit *lineEdit9;
    QLineEdit *lineEdit10;
    QLineEdit *lineEdit11;
    QLineEdit *lineEdit12;
    QLineEdit *lineEdit13;
    QCheckBox *checkBoxPaleTriangles;
    QCheckBox *checkBoxFilterHighlighted;
    QCheckBox *checkBoxColorByRadius = new QCheckBox(tr("color by radius"));
    QRadioButton *radioButtonColorByAlpha;
    QRadioButton *radioButtonColorByWrap;
    //in 3d also enable change of rotation angles
    if(geometry->isDim3()){
        lineEdit5 = new QLineEdit(&dialog);
        QString label5 = QString("x-direction");
        lineEdit6 = new QLineEdit(&dialog);
        QString label6 = QString("y-direction");
        lineEdit7 = new QLineEdit(&dialog);
        QString label7 = QString("z-direction");
        form.addRow(new QLabel("Rotate scene (0°-359°):")); //text
        form.addRow(label5,lineEdit5);
        form.addRow(label6,lineEdit6);
        form.addRow(label7,lineEdit7);
        lineEdit8 = new QLineEdit(&dialog);
        QString label8 = QString("x-direction");
        lineEdit9 = new QLineEdit(&dialog);
        QString label9 = QString("y-direction");
        lineEdit10 = new QLineEdit(&dialog);
        QString label10 = QString("z-direction");
        form.addRow(new QLabel("Rotate light source (0°-359°):")); //text
        form.addRow(label8,lineEdit8);
        form.addRow(label9,lineEdit9);
        form.addRow(label10,lineEdit10);
        lineEdit11 = new QLineEdit(&dialog);
        QString label11 = QString("Scale factor:");
        form.addRow(label11,lineEdit11);
        lineEdit12 = new QLineEdit(&dialog);
        lineEdit13 = new QLineEdit(&dialog);
        form.addRow(new QLabel("Translate view:"));
        form.addRow(lineEdit12,lineEdit13);
        checkBoxPaleTriangles = new QCheckBox(tr("draw lone triangles pale"));
        checkBoxFilterHighlighted = new QCheckBox(tr("filter highlighted items"));
        form.addRow(checkBoxPaleTriangles);
        form.addRow(checkBoxFilterHighlighted);
        form.addRow(checkBoxColorByRadius);
    }else{
        QGroupBox *groupColorByRadius = new QGroupBox(); 
        QHBoxLayout *layoutColorByRadius = new QHBoxLayout; 
        radioButtonColorByAlpha = new QRadioButton(tr("Alpha"));
        radioButtonColorByWrap = new QRadioButton(tr("Wrap"));
        radioButtonColorByWrap->setChecked((geometry->getColorDelaunayByRadius())==2); 
        radioButtonColorByAlpha->setChecked((geometry->getColorDelaunayByRadius())==1); 
        layoutColorByRadius->addWidget(radioButtonColorByAlpha);
        layoutColorByRadius->addWidget(radioButtonColorByWrap);
        groupColorByRadius->setLayout(layoutColorByRadius);
        form.addRow(checkBoxColorByRadius,groupColorByRadius);
    }
    QCheckBox *checkBoxUseRadius2 = new QCheckBox(tr("always use radius^2"));
    form.addRow(checkBoxUseRadius2);
    
    //buttons (Cancel/Ok)
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                           Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
    
    lineEdit1->setText(QString::number(geometry->getPointSize()));
    lineEdit1b->setText(QString::number(geometry->getPointSizeInput()));
    lineEdit2->setText(QString::number(geometry->getLineWidth()));
    lineEdit3->setText(QString::number(geometry->getLabelSize()));
    lineEdit4->setText(QString::number(geometry->getPointSizePersDiag()));
    lineEditMaxValue->setText(QString::number(-1));
    checkBoxUseRadius2->setChecked(geometry->isUseRadius2());
    checkBoxColorByRadius->setChecked((geometry->getColorDelaunayByRadius())>0);
    if(geometry->isDim3()){
        lineEdit5->setText(QString::number(geometry->getRotationAngle(0)));
        lineEdit6->setText(QString::number(geometry->getRotationAngle(1)));
        lineEdit7->setText(QString::number(geometry->getRotationAngle(2)));
        lineEdit8->setText(QString::number(geometry->getRotationAngleLight(0)));
        lineEdit9->setText(QString::number(geometry->getRotationAngleLight(1)));
        lineEdit10->setText(QString::number(geometry->getRotationAngleLight(2)));
        lineEdit11->setText(QString::number(geometry->getScale()));
        lineEdit12->setText(QString::number(geometry->getTranslateX()));
        lineEdit13->setText(QString::number(geometry->getTranslateY()));
        checkBoxPaleTriangles->setChecked(geometry->getPaleTrianglesBool());
        checkBoxFilterHighlighted->setChecked(geometry->getFilterHighlightedBool());
    }
    
    //show dialog
    if(dialog.exec() == QDialog::Accepted){
        //if Ok pressed, set values
        double pointsize = lineEdit1->text().toDouble();
        double pointsize_input = lineEdit1b->text().toDouble();
        double linewidth = lineEdit2->text().toDouble();
        int labelsize  = lineEdit3->text().toInt();
        double pointsize_persdiag = lineEdit4->text().toDouble();
        double maxvalue_plots = lineEditMaxValue->text().toDouble();
        int color_delaunay_by_radius = 0;
        if(checkBoxColorByRadius->isChecked()){
            color_delaunay_by_radius=1;
            if(!(geometry->isDim3())&&radioButtonColorByWrap->isChecked()){
                color_delaunay_by_radius=2;
            }
        }
        geometry->setUseRadius2(checkBoxUseRadius2->isChecked());
        updateLayoutWeighted(geometry->isWeighted()||geometry->isUseRadius2());
        geometry->setDrawingParameters(pointsize,pointsize_input,linewidth,labelsize,pointsize_persdiag,maxvalue_plots,color_delaunay_by_radius,true);
        if(geometry->isDim3()){
            geometry->setRotationAngles(lineEdit5->text().toInt(),lineEdit6->text().toInt(),lineEdit7->text().toInt());
            geometry->setRotationAnglesLight(lineEdit8->text().toInt(),lineEdit9->text().toInt(),lineEdit10->text().toInt());
            geometry->setScale(lineEdit11->text().toFloat());
            geometry->setTranslate(lineEdit12->text().toInt(),lineEdit13->text().toInt());
            geometry->setPaleTrianglesBool(checkBoxPaleTriangles->isChecked());
            geometry->setFilterHighlightedBool(checkBoxFilterHighlighted->isChecked());
        }
        fitAlphaRangeWrap();
    }
}

//redraw scene with different colors
void Window::changeColors(){
    geometry->clearHighlightedItems();
    //open dialog
    QDialog dialog(this);
    QFormLayout form(&dialog); //form layout allows to have labels next to each field
    form.addRow(new QLabel("Change colors:")); 
    
    SelectColorButton* buttonSelectColorPoints = new SelectColorButton(&dialog);
    buttonSelectColorPoints->setText(tr("points"));
    buttonSelectColorPoints->setColor(geometry->getColorPoints());
    SelectColorButton* buttonSelectColorDelaunayPoints = new SelectColorButton(&dialog);
    buttonSelectColorDelaunayPoints->setText(tr("Delaunay points"));
    buttonSelectColorDelaunayPoints->setColor(geometry->getColorDelaunayPoints());
    SelectColorButton* buttonSelectColorDelaunayDark = new SelectColorButton(&dialog);
    buttonSelectColorDelaunayDark->setText(tr("Delaunay dark"));
    buttonSelectColorDelaunayDark->setColor(geometry->getColorDelaunayDark());
    SelectColorButton* buttonSelectColorDelaunayLight = new SelectColorButton(&dialog);
    buttonSelectColorDelaunayLight->setText(tr("Delaunay light"));
    buttonSelectColorDelaunayLight->setColor(geometry->getColorDelaunayLight());   
    SelectColorButton* buttonSelectColorAlphaDark = new SelectColorButton(&dialog);
    buttonSelectColorAlphaDark->setText(tr("Alpha dark"));
    buttonSelectColorAlphaDark->setColor(geometry->getColorAlphaDark());
    SelectColorButton* buttonSelectColorAlphaLight = new SelectColorButton(&dialog);
    buttonSelectColorAlphaLight->setText(tr("Alpha light"));
    buttonSelectColorAlphaLight->setColor(geometry->getColorAlphaLight());   
    SelectColorButton* buttonSelectColorWrapDark = new SelectColorButton(&dialog);
    buttonSelectColorWrapDark->setText(tr("Wrap dark"));
    buttonSelectColorWrapDark->setColor(geometry->getColorWrapDark());
    SelectColorButton* buttonSelectColorWrapLight = new SelectColorButton(&dialog);
    buttonSelectColorWrapLight->setText(tr("Wrap light"));
    buttonSelectColorWrapLight->setColor(geometry->getColorWrapLight());
    SelectColorButton* buttonSelectColorOldPoint = new SelectColorButton(&dialog);
    buttonSelectColorOldPoint->setText(tr("old point"));
    buttonSelectColorOldPoint->setColor(geometry->getColorOldPoint());
    SelectColorButton* buttonSelectColorNewPoint = new SelectColorButton(&dialog);
    buttonSelectColorNewPoint->setText(tr("new point"));
    buttonSelectColorNewPoint->setColor(geometry->getColorNewPoint());
    SelectColorButton* buttonSelectColorWeightedPointsPos = new SelectColorButton(&dialog);
    buttonSelectColorWeightedPointsPos->setText(tr("weighted points"));
    buttonSelectColorWeightedPointsPos->setColor(geometry->getColorWeightedPointsPos());
    SelectColorButton* buttonSelectColorWeightedPointsNeg;
    SelectColorButton* buttonSelectColorAlphaPale;
    SelectColorButton* buttonSelectColorWrapPale;
    buttonSelectColorPoints->setFixedWidth(150);
    buttonSelectColorDelaunayPoints->setFixedWidth(150);
    buttonSelectColorDelaunayDark->setFixedWidth(150);
    buttonSelectColorDelaunayLight->setFixedWidth(150);
    buttonSelectColorAlphaDark->setFixedWidth(150);
    buttonSelectColorAlphaLight->setFixedWidth(150);
    buttonSelectColorWrapDark->setFixedWidth(150);
    buttonSelectColorWrapLight->setFixedWidth(150);
    buttonSelectColorOldPoint->setFixedWidth(150);
    buttonSelectColorNewPoint->setFixedWidth(150);
    buttonSelectColorWeightedPointsPos->setFixedWidth(150);
    
    if(!(geometry->isDim3())){
        buttonSelectColorWeightedPointsPos->setText(tr("balls, w points +"));
        buttonSelectColorWeightedPointsNeg = new SelectColorButton(&dialog);
        buttonSelectColorWeightedPointsNeg->setText(tr("weighted points -"));
        buttonSelectColorWeightedPointsNeg->setColor(geometry->getColorWeightedPointsNeg());
        buttonSelectColorWeightedPointsNeg->setFixedWidth(150);
    }else{
        buttonSelectColorAlphaPale = new SelectColorButton(&dialog);
        buttonSelectColorAlphaPale->setText(tr("Alpha pale"));
        buttonSelectColorAlphaPale->setColor(geometry->getColorAlphaPale());
        buttonSelectColorWrapPale = new SelectColorButton(&dialog);
        buttonSelectColorWrapPale->setText(tr("Wrap pale"));
        buttonSelectColorWrapPale->setColor(geometry->getColorWrapPale());
        buttonSelectColorAlphaPale->setFixedWidth(150);
        buttonSelectColorWrapPale->setFixedWidth(150);
    }
    
    form.addRow(buttonSelectColorPoints,buttonSelectColorDelaunayPoints);
    form.addRow(buttonSelectColorDelaunayDark,buttonSelectColorDelaunayLight);
    form.addRow(buttonSelectColorAlphaDark,buttonSelectColorWrapDark);
    form.addRow(buttonSelectColorAlphaLight,buttonSelectColorWrapLight);
    if(geometry->isDim3()){
        form.addRow(buttonSelectColorAlphaPale,buttonSelectColorWrapPale);
    }
    form.addRow(buttonSelectColorOldPoint,buttonSelectColorNewPoint);
    if(!(geometry->isDim3())){
        form.addRow(buttonSelectColorWeightedPointsPos,buttonSelectColorWeightedPointsNeg);
    }else{
        form.addRow(buttonSelectColorWeightedPointsPos);
    }
 
    //buttons (Cancel/Ok)
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                           Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
    
    //show dialog
    if(dialog.exec() == QDialog::Accepted){
        //if Ok pressed, set values
        //set colors
        geometry->setColorPoints(buttonSelectColorPoints->getColor());
        geometry->setColorDelaunayPoints(buttonSelectColorDelaunayPoints->getColor());
        geometry->setColorDelaunayDark(buttonSelectColorDelaunayDark->getColor());
        geometry->setColorDelaunayLight(buttonSelectColorDelaunayLight->getColor());
        geometry->setColorAlphaDark(buttonSelectColorAlphaDark->getColor());
        geometry->setColorAlphaLight(buttonSelectColorAlphaLight->getColor());
        geometry->setColorWrapDark(buttonSelectColorWrapDark->getColor());
        geometry->setColorWrapLight(buttonSelectColorWrapLight->getColor());
        geometry->setColorOldPoint(buttonSelectColorOldPoint->getColor());
        geometry->setColorNewPoint(buttonSelectColorNewPoint->getColor());
        geometry->setColorWeightedPointsPos(buttonSelectColorWeightedPointsPos->getColor());
        if(geometry->isDim3()){
            geometry->setColorAlphaPale(buttonSelectColorAlphaPale->getColor());
            geometry->setColorWrapPale(buttonSelectColorWrapPale->getColor());
        }else{
            geometry->setColorWeightedPointsNeg(buttonSelectColorWeightedPointsNeg->getColor());
        }
        //redraw
        geometry->setDrawingParameters(geometry->getPointSize(),geometry->getPointSizeInput(),geometry->getLineWidth(),geometry->getLabelSize(),geometry->getPointSizePersDiag(),geometry->getMaxValuePlots(),geometry->getColorDelaunayByRadius(),true);
    }
}

//export PNG-image
void Window::exportImagePNG()
{   
    QString path = QFileDialog::getSaveFileName(this, tr("Export as PNG"),
        QCoreApplication::applicationDirPath()+"/screenshots", tr("PNG files (*.png)"));
    if (path.isEmpty())
        return;
    if(!(path.endsWith(".png")||path.endsWith(".PNG"))){
        path += ".png";
    }
    QFile outputfile(path);
    outputfile.open(QIODevice::WriteOnly);
    QPixmap pixmap;
    if(geometry->isDim3()){
        pixmap = QPixmap(geometry->getOpenGLWidget()->size());
        geometry->getOpenGLWidget()->render(&pixmap);
    }else{
        pixmap = geometry->grab();
    }
    pixmap.save(&outputfile,"PNG");
}

//export SVG-image (only 2-dim)
void Window::exportImageSVG()
{   
    if(!geometry->isDim3()){ //2-dim: vector graphics
        QString path = QFileDialog::getSaveFileName(this, tr("Export as SVG"),
            QCoreApplication::applicationDirPath()+"/screenshots", tr("SVG files (*.svg)"));
        if (path.isEmpty())
            return;
        if(!(path.endsWith(".svg")||path.endsWith(".SVG"))){
            path += ".svg";
        }
        int size = 500;
        QSvgGenerator generator;
        generator.setFileName(path);       
        generator.setSize(QSize(size, size));
        generator.setViewBox(QRect(0, 0, size, size));
        generator.setTitle(tr("wrap_2_3 example"));
        QPainter painter;
        painter.begin(&generator);
        geometry->render(&painter);
        painter.end();
    }else{ //3-dim: png
        exportImagePNG();
    }
}

//export image of persistence diagram (as SVG (vector graphics)
void Window::exportImagePersDiag()
{   
    QString path = QFileDialog::getSaveFileName(this, tr("Export Persistence Diagram as SVG"),
        QCoreApplication::applicationDirPath()+"/screenshots", tr("SVG files (*.svg)"));
    if (path.isEmpty())
        return;
    if(!(path.endsWith(".svg")||path.endsWith(".SVG"))){
        path += ".svg";
    }
    int size = 300;
    QSvgGenerator generator;
    generator.setFileName(path);       
    generator.setSize(QSize(size, size));
    generator.setViewBox(QRect(0, 0, size, size));
    generator.setTitle(tr("Persistence Diagram"));
    QPainter painter;
    painter.begin(&generator);
    (geometry->getThirdDrawingPlane())->render(&painter);
    painter.end();
}


//open second window to show interval graph of complex
void Window::showIntervalGraph(){    
    secondWindow->show();
    secondWindow->raise();
    secondWindow->activateWindow();
}

//open third window display persistence diagram of current Wrap complex
void Window::showPersDiag(){    
    thirdWindow->show();
    geometry->fitViewThirdScene();
    thirdWindow->raise();
    thirdWindow->activateWindow();
}

//radio buttons decide if persistence diagram for Alpha or Wrap complex is shown
void Window::changePersDiag(){
    bool status = radioButtonStatusPersDiag->isChecked();
    labelPersDiagDrawSelection->setVisible(status);
    checkBoxPersDiagStatusDrawDim[0]->setVisible(status);
    checkBoxPersDiagStatusDrawDim[1]->setVisible(status);
    checkBoxPersDiagStatusDrawDim[2]->setVisible(status);
    checkBoxPersDiagStatusDrawStatus[0]->setVisible(status);
    checkBoxPersDiagStatusDrawStatus[1]->setVisible(status);
    checkBoxPersDiagStatusDrawStatus[2]->setVisible(status);
    checkBoxPersDiagStatusDrawDependentPairs->setVisible(status);
    checkBoxPersDiagStatusDrawRecursivelyDependentPairs->setVisible(status);
    checkBoxPersDiagStatusDrawDependentPairsLastOperation->setVisible(status);
    
    //geometry->showPersDiagConnections(checkBoxPersDiagConnections->isChecked());
    
    if(radioButtonAlphaPers->isChecked()){
        if(radioButtonNewPersDiag->isChecked()){
            geometry->changePersDiag(false,true,false);
        }else if(radioButtonStatusPersDiag->isChecked()){
            geometry->changePersDiag(false,false,true);
        }else{
            geometry->changePersDiag(false,false,false);
        }
    }else{
        if(radioButtonNewPersDiag->isChecked()){
            geometry->changePersDiag(true,true,false);
        }else if(radioButtonStatusPersDiag->isChecked()){
            geometry->changePersDiag(true,false,true);
        }else{
            geometry->changePersDiag(true,false,false);
        }
    }
}

//open fourth window to display statistics for whole filtration
void Window::showPlots(){    
    fourthWindow->show();
    geometry->fitViewFourthScene();
    fourthWindow->raise();
    fourthWindow->activateWindow();
}

//radio buttons decide which statistic plot is shown
void Window::changeStatisticPlot(){   
    if(radioButtonsPlotScale[0]->isChecked()){
        geometry->setStatisticPlotScale(false);
    }else{
        geometry->setStatisticPlotScale(true);
    }
    for(int i=0; i<radioButtonsPlotTitles23.size(); i++){
        if(radioButtonsPlotTitles23[i]->isChecked()){
            geometry->changeStatisticPlot(radioButtonsPlotTitles23[i]->text());
        }
    }
    for(int i=0; i<radioButtonsPlotTitles3.size(); i++){
        if(radioButtonsPlotTitles3[i]->isChecked()){
            geometry->changeStatisticPlot(radioButtonsPlotTitles3[i]->text());
        }
    }
} 

//update layout of window to 2-dim or 3-dim view depending on current points 
void Window::updateLayoutDim(bool dim3)
{
    //some widgets are only shown in either 2-dim or 3-dim view
    
    if(dim3){ //3-dim
        setWindowTitle(tr("Wrap 3")); //title
        actionClear2d->setText(tr("Clear -> 2D"));
        actionClear3d->setText(tr("Clear"));
        actionExportImageSVG->setVisible(false); 
        
        checkBoxDelaunay->setCheckable(false);
        checkBoxConvexHull->hide();
        checkBoxUnionOfBalls->hide();
        
        geometry->hide();
        labelCoordsText->hide();
        labelMouseCoords->hide();
        
        if(show_statistics){
            labelTetrahedra->show();
            labelNumTetrahedraDelaunay->show();
            labelNumTetrahedraAlpha->show();
            labelNumTetrahedraWrap->show();
            labelNumCritTrianglesPos->show();
            labelNumCritTetrahedra->show();
            labelBet2->show();
            labelBetti2->show();
        }
        geometry->getOpenGLWidget()->show();
        for(int i=0; i<radioButtonsPlotTitles3.size(); i++){
            radioButtonsPlotTitles3[i]->setVisible(true);
        }
        labelMaxPersistenceDim2->show();
        labelMaxPersistence2->show();
    }else{ //2-dim
        setWindowTitle(tr("Wrap 2")); //title
        actionClear2d->setText(tr("Clear")); 
        actionClear3d->setText(tr("Clear -> 3D")); 
        actionExportImageSVG->setVisible(true); 
        
        checkBoxDelaunay->setCheckable(true);
        checkBoxConvexHull->show();
        checkBoxUnionOfBalls->show();
        
        geometry->show();
        labelCoordsText->show();
        labelMouseCoords->show();
        
        labelTetrahedra->hide();
        labelNumTetrahedraDelaunay->hide();
        labelNumTetrahedraAlpha->hide();
        labelNumTetrahedraWrap->hide();
        labelNumCritTrianglesPos->hide();
        labelNumCritTetrahedra->hide();
        labelBet2->hide();
        labelBetti2->hide();
        geometry->getOpenGLWidget()->hide();
        for(int i=0; i<radioButtonsPlotTitles3.size(); i++){
            radioButtonsPlotTitles3[i]->setVisible(false);
        }
        radioButtonsPlotTitles23[0]->setChecked(true);
        labelMaxPersistenceDim2->hide();
        labelMaxPersistence2->hide();
    }
}

//update alpha label and window layout depending on whether points are weighted
void Window::updateLayoutWeighted(bool weighted)
{
    if(weighted||geometry->isUseRadius2()){
        labelAlpha2->show();
        labelAlpha->hide();
        checkBoxWeighted->show();
    }else{
        labelAlpha2->hide();
        labelAlpha->show();
        if(geometry->isBregman() && radioButtonBregmanTransformedPoints->isChecked()){
            checkBoxWeighted->show();
        }else{
            checkBoxWeighted->hide();
        }
    }
}

//update layout of window to Bregman or normal (Euclidean) geometry depending on current complex
void Window::updateLayoutBregman(bool bregman){
    if(bregman){
        radioButtonBregmanInputPoints->show();
        radioButtonBregmanTransformedPoints->show();
    }else{
        radioButtonBregmanInputPoints->hide();
        radioButtonBregmanTransformedPoints->hide();
    }
}

//change between drawing on input points or transformed points for bregman geometry
void Window::drawBregmanOnInputPoints(bool on_input_points)
{
    if(on_input_points){
        checkBoxWeighted->setChecked(false);
        checkBoxWeighted->hide();
    }else{
        checkBoxWeighted->show();
    }
    geometry->drawBregmanOnInputPoints(on_input_points);
}

//update layout of point manipulation window depending on chosen operation
void Window::updatePointManipulationLayout(){   
    bool dim3 = geometry->isDim3();
    if(radioButtonsManipulatePoints[0]->isChecked()){
        labelOldPoint->hide();
        labelOldX->hide();
        labelOldY->hide();
        labelOldZ->hide();
        spinBoxOldPointNum->hide();
        labelOldPointX->hide();
        labelOldPointY->hide();
        labelOldPointZ->hide();
        labelNewPoint->show();
        labelNewX->show();
        labelNewY->show();
        lineEditNewPointX->show();
        lineEditNewPointY->show();
        if(dim3){
            labelNewZ->show();
            lineEditNewPointZ->show();
        }else{
            labelNewZ->hide();
            lineEditNewPointZ->hide();
        }
        geometry->updateOldPoint(false,-1);
        updateNewPoint();
    }else if(radioButtonsManipulatePoints[1]->isChecked()){
        labelOldPoint->show();
        labelOldX->show();
        labelOldY->show();
        spinBoxOldPointNum->show();
        labelOldPointX->show();
        labelOldPointY->show();
        if(dim3){
            labelOldZ->show();
            labelOldPointZ->show();
        }else{
            labelOldZ->hide();
            labelOldPointZ->hide();
        }
        labelNewPoint->hide();
        labelNewX->hide();
        labelNewY->hide();
        labelNewZ->hide();
        lineEditNewPointX->hide();
        lineEditNewPointY->hide();
        lineEditNewPointZ->hide();
        geometry->updateNewPoint(false,-1,-1,-1);
        updateOldPoint();
    }else{
        labelOldPoint->show();
        labelOldX->show();
        labelOldY->show();
        spinBoxOldPointNum->show();
        labelOldPointX->show();
        labelOldPointY->show();
        labelNewPoint->show();
        labelNewX->show();
        labelNewY->show();
        lineEditNewPointX->show();
        lineEditNewPointY->show();
        if(dim3){
            labelOldZ->show();
            labelOldPointZ->show();
            labelNewZ->show();
            lineEditNewPointZ->show();
        }else{
            labelOldZ->hide();
            labelOldPointZ->hide();
            labelNewZ->hide();
            lineEditNewPointZ->hide();
        }
        updateOldPoint();
        updateNewPoint();
    }
} 

void Window::updatePointManipulationWindowNewPoints()
{
    if(geometry->getNumDataPoints()>0){
        spinBoxOldPointNum->setMaximum(geometry->getMaxPointIndex());
        spinBoxOldPointNum->setValue(0);
        spinBoxOldPointNum->setEnabled(true);
        updatePointManipulationLayout();
    }else{
        spinBoxOldPointNum->setValue(-1);
        spinBoxOldPointNum->setEnabled(false);
        geometry->updateOldPoint(false,-1);
        labelOldPointX->setText(QString::number(0));
        labelOldPointY->setText(QString::number(0));
        labelOldPointZ->setText(QString::number(0));
        if(radioButtonsManipulatePoints[1]->isChecked()){
            geometry->updateNewPoint(false,-1,-1,-1);
        }else{
            updateNewPoint();
        }
        if(!geometry->isDim3()){
            labelOldZ->hide();
            labelOldPointZ->hide();
            labelNewZ->hide();
            lineEditNewPointZ->hide();
        }
    }
    //do not draw old and new point if point manipulation window is closed
    if(!pointManipulationWindow->isVisible()){
        geometry->updateOldPoint(false,-1);
        geometry->updateNewPoint(false,-1,-1,-1);
    }
}

//update displayed coordinates of old point
void Window::updateOldPoint()
{
    int point_index = spinBoxOldPointNum->value();

    if(point_index>=0 && point_index<=geometry->getMaxPointIndex() && !geometry->getInputDataPoint(point_index)->isDeleted()){
        DataPoint* point = geometry->getInputDataPoint(point_index);
        labelOldPointX->setText(QString::number(point->getX_double()));
        labelOldPointY->setText(QString::number(point->getY_double()));
        labelOldPointZ->setText(QString::number(point->getZ_double()));

        geometry->updateOldPoint(true,point_index);
    }else{
        geometry->updateOldPoint(false,-1);
        labelOldPointX->setText("-");
        labelOldPointY->setText("-");
        labelOldPointZ->setText("-");
    }
}

//update displayed coordinates of old point
void Window::updateNewPoint()
{
    double newx= lineEditNewPointX->text().toDouble();
    double newy= lineEditNewPointY->text().toDouble();
    double newz= lineEditNewPointZ->text().toDouble();

    geometry->updateNewPoint(true,newx,newy,newz);
}

//open point manipulation window
void Window::startPointManipulation()
{
    updatePointManipulationLayout();
    
    pointManipulationWindow->show();
    pointManipulationWindow->raise();
    pointManipulationWindow->activateWindow();
}

//apply point manipulation operation as specified in interactive window
void Window::applyPointManipulation()
{       
    bool recompute = checkBoxRecompute->isChecked();
    if(radioButtonsManipulatePoints[0]->isChecked()){
        double newx = lineEditNewPointX->text().toDouble();
        double newy = lineEditNewPointY->text().toDouble();
        double newz = lineEditNewPointZ->text().toDouble();
        
        bool added = geometry->addPoint(newx,newy,newz,recompute,false,true);
        
        if(added){
            spinBoxOldPointNum->setMaximum(geometry->getMaxPointIndex());
            
            std::cout << "Added point with coordinates (" << newx << ", " << newy;
            if(geometry->isDim3()){
                std::cout << ", " << newz;
            }
            std::cout << ")." << std::endl;
        }
        
    }else if(radioButtonsManipulatePoints[1]->isChecked()){
        int point_index = spinBoxOldPointNum->value();
        if(point_index>=0 && point_index<=geometry->getMaxPointIndex()){
            DataPoint* old_point = geometry->getInputDataPoint(point_index);
            
            double oldx = old_point->getX_double();
            double oldy = old_point->getY_double();
            double oldz = old_point->getZ_double();
            
            bool deleted = geometry->deletePoint(point_index,recompute,false,true);
            
            if(deleted){
                std::cout << "Deleted point #" << point_index << " with coordinates (" << oldx << ", " << oldy;
                if(geometry->isDim3()){
                    std::cout << ", " << oldz;
                }
                std::cout << ")." << std::endl;
            }
        }else{
            std::cout << "Please choose valid point index (0-" << geometry->getMaxPointIndex() << ") to delete point! (Bad input: " << point_index << ")" << std::endl;
        }
    }else{
        int point_index = spinBoxOldPointNum->value();
        if(point_index>=0 && point_index<=geometry->getMaxPointIndex()){
            DataPoint* old_point = geometry->getInputDataPoint(point_index);
            double oldx = old_point->getX_double();
            double oldy = old_point->getY_double();
            double oldz = old_point->getZ_double();
            double newx = lineEditNewPointX->text().toDouble();
            double newy = lineEditNewPointY->text().toDouble();
            double newz = lineEditNewPointZ->text().toDouble();
            
            bool moved = geometry->movePoint(point_index,newx,newy,newz,recompute);
            
            if(moved){
                std::cout << "Moved point #" << point_index << " from (" << oldx << ", ";
                std::cout << oldy << ", " << oldz << ") to (";
                std::cout << newx << ", " << newy << ", " << newz << ")." << std::endl;
            }
            
        }else{
            std::cout << "Please choose valid point index (0-" << geometry->getMaxPointIndex() << ") to move point! (Bad input: " << point_index << ")" << std::endl;
        }
    }
}

//exit point manipulation window
void Window::exitPointManipulation()
{
    geometry->updateOldPoint(false,-1);
    geometry->updateNewPoint(false,-1,-1,-1);
    
    pointManipulationWindow->hide();
}

//delete last inserted point
void Window::deleteLastPoint()
{
    geometry->deletePoint(geometry->getLastPointInserted(),false,false,false);
}

//highlight differences to complex from input file
void Window::highlightDifferences()
{
    geometry->clearHighlightedItems();
    //open dialog to choose file
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open simplices file"),QCoreApplication::applicationDirPath());
    if(! fileName.isEmpty()){
        geometry->highlightDifferentSimplices(fileName);
    }
}

//highlight differences to complex from input file
void Window::highlightPTree()
{
    geometry->clearHighlightedItems();
    
    //open dialog to choose dimension
    QDialog dialog(this);
    QFormLayout form(&dialog); //form layout allows to have labels next to each field
    //input fields
    QLineEdit *lineEditDim = new QLineEdit(&dialog);
    lineEditDim->setFixedWidth(100);
    QLabel* labelDim = new QLabel(tr("Highlight tree of dimension:"));
    lineEditDim->setText(QString::number(1));
    form.addRow(labelDim,lineEditDim);
    SelectColorButton* buttonSelectColorTree = new SelectColorButton(&dialog);
    buttonSelectColorTree->setText(tr("color"));
    buttonSelectColorTree->setColor(Qt::magenta);
    form.addRow(buttonSelectColorTree);
    //buttons (Cancel/Ok)
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                           Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
    
    //show dialog
    if(dialog.exec() == QDialog::Accepted){
        //if Ok pressed, set values
        double tree_dim = lineEditDim->text().toInt();
        geometry->highlightPTree(tree_dim,buttonSelectColorTree->getColor());
    }
}

//recompute intervals (-> Wrap complex) with prescribed imprecision threshold, as with noisy filtration values
void Window::recomputeWrapWithThreshold()
{
    //open dialog
    QDialog dialog(this);
    QFormLayout form(&dialog); //form layout allows to have labels next to each field
    //input fields
    QLineEdit *lineEditThreshold = new QLineEdit(&dialog);
    lineEditThreshold->setFixedWidth(100);
    QLabel* labelText = new QLabel(tr("Choose imprecision threshold:"));
    QCheckBox *checkBoxInfinity = new QCheckBox(trUtf8("\u221E"));
    QGroupBox *groupThreshold = new QGroupBox(&dialog);
    QHBoxLayout *layoutThreshold = new QHBoxLayout;
    layoutThreshold->addWidget(labelText);
    layoutThreshold->addWidget(lineEditThreshold);
    layoutThreshold->addWidget(checkBoxInfinity);
    groupThreshold->setLayout(layoutThreshold);
    form.addRow(groupThreshold);
    lineEditThreshold->setText(QString::number(0.01));
    QCheckBox *checkBoxInfo = new QCheckBox(tr("print info"));
    form.addRow(checkBoxInfo);
    //buttons (Cancel/Ok)
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                           Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
    
    //show dialog
    if(dialog.exec() == QDialog::Accepted){
        //if Ok pressed, set values
        double threshold = lineEditThreshold->text().toDouble();
        bool printinfo = checkBoxInfo->isChecked();
        if(checkBoxInfinity->isChecked()){
            //threshold should as be as big as possible (max difference between any filtration values)
            threshold = CGAL::to_double(2*(geometry->getAlpha2MaxAlpha()-geometry->getAlpha2MinAlpha()));
        }
        geometry->recomputeWrapWithThreshold(threshold,printinfo);
    }
}