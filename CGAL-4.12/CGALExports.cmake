# Generated by CMake 3.5.1

if("${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}" LESS 2.5)
   message(FATAL_ERROR "CMake >= 2.6.0 required")
endif()
cmake_policy(PUSH)
cmake_policy(VERSION 2.6)
#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Protect against multiple inclusion, which would fail when already imported targets are added once more.
set(_targetsDefined)
set(_targetsNotDefined)
set(_expectedTargets)
foreach(_expectedTarget CGAL::CGAL CGAL::CGAL_Core CGAL::CGAL_ImageIO CGAL::CGAL_Qt5)
  list(APPEND _expectedTargets ${_expectedTarget})
  if(NOT TARGET ${_expectedTarget})
    list(APPEND _targetsNotDefined ${_expectedTarget})
  endif()
  if(TARGET ${_expectedTarget})
    list(APPEND _targetsDefined ${_expectedTarget})
  endif()
endforeach()
if("${_targetsDefined}" STREQUAL "${_expectedTargets}")
  set(CMAKE_IMPORT_FILE_VERSION)
  cmake_policy(POP)
  return()
endif()
if(NOT "${_targetsDefined}" STREQUAL "")
  message(FATAL_ERROR "Some (but not all) targets in this export set were already defined.\nTargets Defined: ${_targetsDefined}\nTargets not yet defined: ${_targetsNotDefined}\n")
endif()
unset(_targetsDefined)
unset(_targetsNotDefined)
unset(_expectedTargets)


# Create imported target CGAL::CGAL
add_library(CGAL::CGAL SHARED IMPORTED)

set_target_properties(CGAL::CGAL PROPERTIES
  INTERFACE_COMPILE_OPTIONS "\$<\$<COMPILE_LANGUAGE:CXX>:-frounding-math>"
  INTERFACE_INCLUDE_DIRECTORIES "/usr/include/x86_64-linux-gnu;/usr/include;/usr/include;/home/koelsboe/CGAL-4.12/include;/home/koelsboe/CGAL-4.12/include"
  INTERFACE_LINK_LIBRARIES "/usr/lib/x86_64-linux-gnu/libgmp.so;/usr/lib/x86_64-linux-gnu/libmpfr.so;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_chrono.so;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;/usr/lib/x86_64-linux-gnu/libboost_atomic.so;/usr/lib/x86_64-linux-gnu/libpthread.so"
)

# Create imported target CGAL::CGAL_Core
add_library(CGAL::CGAL_Core SHARED IMPORTED)

set_target_properties(CGAL::CGAL_Core PROPERTIES
  INTERFACE_COMPILE_DEFINITIONS "CGAL_USE_CORE=1"
  INTERFACE_INCLUDE_DIRECTORIES "/usr/include/x86_64-linux-gnu;/usr/include"
  INTERFACE_LINK_LIBRARIES "/usr/lib/x86_64-linux-gnu/libgmp.so;/usr/lib/x86_64-linux-gnu/libmpfr.so;CGAL::CGAL;Boost::thread"
)

# Create imported target CGAL::CGAL_ImageIO
add_library(CGAL::CGAL_ImageIO SHARED IMPORTED)

set_target_properties(CGAL::CGAL_ImageIO PROPERTIES
  INTERFACE_COMPILE_DEFINITIONS "CGAL_USE_ZLIB=1"
  INTERFACE_INCLUDE_DIRECTORIES "/usr/include"
  INTERFACE_LINK_LIBRARIES "CGAL::CGAL;/usr/lib/x86_64-linux-gnu/libz.so"
)

# Create imported target CGAL::CGAL_Qt5
add_library(CGAL::CGAL_Qt5 SHARED IMPORTED)

set_target_properties(CGAL::CGAL_Qt5 PROPERTIES
  INTERFACE_LINK_LIBRARIES "CGAL::CGAL;Qt5::OpenGL;Qt5::Svg"
)

# Import target "CGAL::CGAL" for configuration ""
set_property(TARGET CGAL::CGAL APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(CGAL::CGAL PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "/home/koelsboe/CGAL-4.12/lib/libCGAL.so.13.0.2"
  IMPORTED_SONAME_NOCONFIG "libCGAL.so.13"
  )

# Import target "CGAL::CGAL_Core" for configuration ""
set_property(TARGET CGAL::CGAL_Core APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(CGAL::CGAL_Core PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "/home/koelsboe/CGAL-4.12/lib/libCGAL_Core.so.13.0.2"
  IMPORTED_SONAME_NOCONFIG "libCGAL_Core.so.13"
  )

# Import target "CGAL::CGAL_ImageIO" for configuration ""
set_property(TARGET CGAL::CGAL_ImageIO APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(CGAL::CGAL_ImageIO PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "/home/koelsboe/CGAL-4.12/lib/libCGAL_ImageIO.so.13.0.2"
  IMPORTED_SONAME_NOCONFIG "libCGAL_ImageIO.so.13"
  )

# Import target "CGAL::CGAL_Qt5" for configuration ""
set_property(TARGET CGAL::CGAL_Qt5 APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(CGAL::CGAL_Qt5 PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "/home/koelsboe/CGAL-4.12/lib/libCGAL_Qt5.so.13.0.2"
  IMPORTED_SONAME_NOCONFIG "libCGAL_Qt5.so.13"
  )

# This file does not depend on other imported targets which have
# been exported from the same project but in a separate export set.

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
cmake_policy(POP)
