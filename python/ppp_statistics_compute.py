#libraries
import glob
import re
import os
import csv
import sys
import time
import math
import datetime
import numpy as np #arrays
import numpy.matlib as mat

#+-------------------------+
#|   AUXILIARY FUNCTIONS   |
#+-------------------------+

#LOAD DATA
def get_current_header_index(old_index,firststring,headers):
    """
    starting from 'old_index'+1, go through the list of string-arrays 'headers' 
    and find the next array starting with 'firststring', 
    output None if not found
    """
    new_index = old_index
    rowfound = False
    while not rowfound:
        new_index += 1
        if new_index>=len(headers):
            #print firststring, 'header line was not found!'
            return None
        if headers[new_index][1][0].lower().startswith(firststring):
            rowfound = True
    return new_index


def load_data(filenames,data,headers,param,param_twolines = True):
    """
    load data and headers stored in the file of given name, optionaly read parameters in first two lines
    input: * filename
           * data (modified for output): 2-dim array containing all the data in the file
           * headers (modified for output): list of the data rows containing strings in the beginning
               together with their indices in the data
           * param (modified for output): dictionary containing parameter values, stored in the first two lines of file,
               if None nothing is done
    """
    #possibly read data from multiple files
    if not type(filenames)==list:
        filename_out = filenames
        filenames = [filename_out]
    else:
        filename_out = filenames[0]
        
    ntrials = 0
    
    for filename in filenames:
        print filename
        #open file, read as csv
        datafile = open(filename, 'r')
        datareader = csv.reader(datafile)

        #optionally read general parameters stored in first two lines
        if param_twolines:
            param_titles = next(datareader)
            param_values = next(datareader)
            for i,title in enumerate(param_titles):
                param[title] = int(param_values[i])

        #read data line by line from file
        for row in datareader:
            datarow = []
            #for each data element, try to cast to int, else try for float, else cast to string
            for elem in row:       
                try:
                    datarow.append(int(elem))
                except ValueError:
                    try: 
                        datarow.append(float(elem))
                    except ValueError:
                        if len(str(elem).strip())>0: #only non-empty elements
                            datarow.append(str(elem).strip()) #remove whitespaces from beginning and end     
            if len(datarow)>0:
                data.append(datarow)
                if type(datarow[0])==str:
                    headers.append([len(data)-1,datarow])
                    #get parameters (raw output file)
                    if not param_twolines and datarow[0]=='# trials':
                        param_rowindex = len(data)-1
        
        #get parameters (raw output file)
        if not param_twolines:
            # read parameters
            for i,title in enumerate(data[param_rowindex]):
                param[title]=data[param_rowindex+1][i]
            ntrials += param['# trials']
        datafile.close()
    if len(filenames)>1:
        now = datetime.datetime.now()
        psuffix = ''
        if param['periodic']==1:
            psuffix = '_p'
        
        filename_out = '../output/output'+str(param['dim'])+'_%d-%d-%d_%d_%d'\
        %(now.year,now.month,now.day,param['window size'],ntrials)+psuffix+'_multi.txt'\
    
    if not param_twolines:
        param['# trials']=ntrials
    
    return filename_out

#+-------------------+
#|   LOAD RAW DATA   |
#+-------------------+

#load and process raw data

#load and process raw data

#load and process raw data

def load_output_data(filename):
    """
    load all data from output file of wrap_2_3
     input:  * filename
     output: * param: dictionary containing parameter values 'dim', '# trials', 'window size', 'periodic', 'lambda'
             * [full_header,full_data]: data about full Alpha and Wrap complex (#trials, len(header))-array, 
                 header contains column titles
             * [interval_header, interval_data]: data about each interval, more for singular intervals, header contains titles
             * [overlapping_data, overlapping_header]: data about intervals in overlapping lower sets
    """
    t0 = time.clock()
    
    param = {} #dictionary for parameters
    data = [] #all data will be stored in this
    headers = [] #this array will store rows containing strings together with their row index
    
    filename = load_data(filename,data,headers,param,False)
    print filename 
    
    #initialize arrays
    full_data = []
    alpha_data = []
    interval_data = [] 
    overlapping_data = []
    
    #for every trial
    current_header_index = 0
    for i in range(param['# trials']):
        
        #get data independent of alpha: # points, # intervals
        # get row index
        #if current_header_index+6 < len(headers): #stop if any trial info not complete
        current_header_index = get_current_header_index(current_header_index,'# points',headers)
        header = headers[current_header_index][1]
        rowindex = headers[current_header_index][0]+1
        # save header
        if i==0:
            full_header = header
        # get data
        full_data.append(data[rowindex])

        #get data for every interval: r_alpha, r_wrap, lower dimension, upper dimension
        # for singular intervals also: positive?, persistence, # simplices in lower set, # nested lower sets, diameter of simplex, diameter of lower set
        # get first row index
        current_header_index = get_current_header_index(current_header_index,'r_alpha',headers)
        header = headers[current_header_index][1]
        rowindex = headers[current_header_index][0]+1    
        # save header
        if i==0:
            interval_header = header
        # get data
        if current_header_index+1<len(headers):
            interval_data.append(data[rowindex:headers[current_header_index+1][0]])
        else: #last trial
            interval_data.append(data[rowindex:])        

        #get data about overlapping of lower sets
        next_index = get_current_header_index(current_header_index,'overlapping lower sets',headers)
        if not next_index is None:
            current_header_index = next_index+1
            header = headers[current_header_index][1]
            rowindex = headers[current_header_index][0]+1    
            # save header
            if i==0:
                overlapping_header = header
            # get data
            if current_header_index+1<len(headers):
                overlapping_data.append(data[rowindex:headers[current_header_index+1][0]])
            else: #last trial
                overlapping_data.append(data[rowindex:])
    
    # compute # intervals and #simplices of various types in full complex (constants 'c','d') and add to full_data
    full_header.extend(['c_11','c_22','c_33','c_12','c_23','c_13','d_1','d_2','d_3','d_sum'])
    expected_num_points = param['lambda']*param['window size']**param['dim']
    for trial in range(len(interval_data)):
        #extract columns of dimensions
        lower_upper_dims = [interval_data_row[2:4] for interval_data_row in interval_data[trial]]
        c00 = full_data[trial][0]
        c11 = lower_upper_dims.count([1,1])
        c22 = lower_upper_dims.count([2,2])
        c33 = lower_upper_dims.count([3,3])
        c12 = lower_upper_dims.count([1,2])
        c23 = lower_upper_dims.count([2,3])
        c13 = lower_upper_dims.count([1,3])
        d1 = c11 + c12 + c13
        d2 = c22 + c12 + c23 + 2*c13
        d3 = c33 + c23 + c13
        dsum = c00 + d1 + d2 + d3
        full_data[trial].extend([c11,c22,c33,c12,c23,c13,d1,d2,d3,dsum])
        
    # convert arrays to np
    full_data = np.array(full_data,dtype=object)
    interval_data = np.array(interval_data,dtype=object)
    overlapping_data = np.array(overlapping_data,dtype=object)

    duration = time.clock() - t0; print 'time elapsed: %.4f' % duration
    
    return filename, param, [full_header,full_data], [interval_header,interval_data], [overlapping_header,overlapping_data] 

#+--------------------+
#|   FULL COMPLEXES   |
#+--------------------+

# write info about full complex (# intervals of various types) for every trial to file
def save_full_data(filename, param, full_header, full_data):
    """
    write info about full complex (# intervals of various types) for every trial to file
     input: * filename (of input data)
            * param: dictionary containing parameter values 'dim', '# trials', 'window size', 'periodic', 'lambda'
            * full_header,full_data: data about full complex (#trials, len(header))-array, 
                 header contains column titles
     output file: filename + '_full.txt'
    """
    output_filename = os.path.splitext(filename)[0]+'_full.txt' 
    with open(output_filename, 'wb') as csvfile: #'wb' instead of 'w' to avoid additional line breaks
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow(param.keys())
        writer.writerow(param.values())
        writer.writerow(full_header)
        for i,trial_data in enumerate(full_data):
            writer.writerow(trial_data.tolist())
    print output_filename
    
#some data is not saved since it can be computed from other:
# Betti_0 = 1 -> # neg crit edges = # points - 1
# Betti_1 = 0 -> # pos crit edges = # neg crit triangles
# Betti_2 = 0 -> # pos crit triangles = # neg crit tetrahedra

#+----------------------------+
#|   CURVES OVER FILTRATION   |
#+----------------------------+

def compute_densities(filename, interval_header, interval_data, full_header, full_data, param, r_min=0, r_max=2.5, r_num=250):
    """
    compute discretized densities of desired data over alpha and wrap filtration
     input: * interval_header, interval_data: extracted from output data
            * param: dictionary containing parameter values 'dim', '# trials', 'window size', 'periodic', 'lambda'
            * r_min, r_max, r_num => compute bins np.linspace(r_min,r_max,r_num+1)
     output: densities written to file
    """
    #compute bins
    r_bins = np.linspace(r_min,r_max,r_num+1)
    r_bincenters = (r_bins[1:]+r_bins[:-1])*0.5
    
    expected_num_points = param['lambda']*param['window size']**param['dim']
    dim = param['dim']
    
    #initialize arrays of desired data
    #*intervals of different types
    densities_intervals_alpha = []
    densities_intervals_wrap = []
    if dim==3:
        types_intervals = [[1,1],[2,2],[3,3],[1,2],[2,3],[1,3]]
    else:
        types_intervals = [[1,1],[2,2],[1,2]]
    #*critical simplices, type = (dim, dim, pos?)
    densities_critical = []
    if dim==3:
        types_critical = [[1,1,0],[1,1,1],[2,2,0],[2,2,1],[3,3,0]]
    else:
        types_critical = [[1,1,0],[1,1,1],[2,2,0]]
    #*simplices of different dim
    densities_simplices_alpha = []
    densities_simplices_wrap = []
    if dim==3:
        dims = [1,2,3]
    else:
        dims = [1,2]
    #*Betti numbers
    curves_betti = []
    
    #compute densities over binned filtration values from interval data
    for trial in range(len(interval_data)):
        densities_intervals_alpha_trial = []
        densities_intervals_wrap_trial = []
        densities_critical_trial = []
        densities_simplices_alpha_trial = []
        densities_simplices_wrap_trial = []
        curves_betti_trial = []
        
        #directly from data
        #*intervals of different types
        for typ in types_intervals:
            #get filtration values of desired data
            r_alpha = np.array([interval_data_row[0] for interval_data_row in interval_data[trial] if interval_data_row[2:4]==typ])
            r_wrap = np.array([interval_data_row[1] for interval_data_row in interval_data[trial] if interval_data_row[2:4]==typ])
            #compute density = histogram
            density = np.histogram(r_alpha,bins = r_bins)[0]
            densities_intervals_alpha_trial.append(density)
            density = np.histogram(r_wrap,bins = r_bins)[0]
            densities_intervals_wrap_trial.append(density)
        #*critical simplices, type = (dim, dim, pos?)
        for typ in types_critical:
            #get filtration values of desired data
            r_alphawrap = np.array([interval_data_row[0] for interval_data_row in interval_data[trial] if interval_data_row[2:5]==typ])
            #compute density = histogram
            density = np.histogram(r_alphawrap,bins = r_bins)[0]
            densities_critical_trial.append(density)
        
        #from already computed densities
        #*simplices of different dim
        for di in dims:
            if di==1:
                density_alpha = densities_intervals_alpha_trial[types_intervals.index([1,1])]+\
                          densities_intervals_alpha_trial[types_intervals.index([1,2])]
                if dim==3:
                    density_alpha += densities_intervals_alpha_trial[types_intervals.index([1,3])]
                
                density_wrap = densities_intervals_wrap_trial[types_intervals.index([1,1])]+\
                          densities_intervals_wrap_trial[types_intervals.index([1,2])]
                if dim==3:
                    density_wrap += densities_intervals_wrap_trial[types_intervals.index([1,3])]
                
            elif di==2:
                density_alpha = densities_intervals_alpha_trial[types_intervals.index([2,2])]+\
                          densities_intervals_alpha_trial[types_intervals.index([1,2])]
                if dim==3:
                    density_alpha += densities_intervals_alpha_trial[types_intervals.index([2,3])]+\
                          densities_intervals_alpha_trial[types_intervals.index([1,3])]*2
                density_wrap = densities_intervals_wrap_trial[types_intervals.index([2,2])]+\
                          densities_intervals_wrap_trial[types_intervals.index([1,2])]
                if dim==3:
                    density_wrap += densities_intervals_wrap_trial[types_intervals.index([2,3])]+\
                          densities_intervals_wrap_trial[types_intervals.index([1,3])]*2
            elif di==3:
                density_alpha = densities_intervals_alpha_trial[types_intervals.index([3,3])]+\
                          densities_intervals_alpha_trial[types_intervals.index([2,3])]+\
                          densities_intervals_alpha_trial[types_intervals.index([1,3])]
                density_wrap = densities_intervals_wrap_trial[types_intervals.index([3,3])]+\
                          densities_intervals_wrap_trial[types_intervals.index([2,3])]+\
                          densities_intervals_wrap_trial[types_intervals.index([1,3])]
            densities_simplices_alpha_trial.append(density_alpha)
            densities_simplices_wrap_trial.append(density_wrap)
        #*Betti numbers
        for di in dims:
            d=di-1
            if d==0:
                num_points = full_data[trial][full_header.index('# points')]
                curve = num_points - np.cumsum(densities_critical_trial[types_critical.index([1,1,0])])
            elif d==1:
                curve = np.cumsum(densities_critical_trial[types_critical.index([1,1,1])])\
                        -np.cumsum(densities_critical_trial[types_critical.index([2,2,0])])
            elif d==2:
                curve = np.cumsum(densities_critical_trial[types_critical.index([2,2,1])])\
                        -np.cumsum(densities_critical_trial[types_critical.index([3,3,0])])
            curves_betti_trial.append(curve)
       
        densities_intervals_alpha.append(densities_intervals_alpha_trial)
        densities_intervals_wrap.append(densities_intervals_wrap_trial)
        densities_critical.append(densities_critical_trial)
        densities_simplices_alpha.append(densities_simplices_alpha_trial)
        densities_simplices_wrap.append(densities_simplices_wrap_trial)
        curves_betti.append(curves_betti_trial)
    
    #compute mean and variance over trials
    densities_intervals_alpha_mean = np.mean(np.array(densities_intervals_alpha),axis=0)
    densities_intervals_alpha_var = np.var(np.array(densities_intervals_alpha),axis=0)
    densities_intervals_wrap_mean = np.mean(np.array(densities_intervals_wrap),axis=0)
    densities_intervals_wrap_var = np.var(np.array(densities_intervals_wrap),axis=0)
    densities_critical_mean = np.mean(np.array(densities_critical),axis=0)
    densities_critical_var = np.var(np.array(densities_critical),axis=0)
    densities_simplices_alpha_mean = np.mean(np.array(densities_simplices_alpha),axis=0)
    densities_simplices_alpha_var = np.var(np.array(densities_simplices_alpha),axis=0)
    densities_simplices_wrap_mean = np.mean(np.array(densities_simplices_wrap),axis=0)
    densities_simplices_wrap_var = np.var(np.array(densities_simplices_wrap),axis=0)
    curves_betti_mean = np.mean(np.array(curves_betti),axis=0)
    curves_betti_var = np.var(np.array(curves_betti),axis=0)
    
    densities_intervals_alpha_cumvar = np.var(np.cumsum(np.array(densities_intervals_alpha),axis=2),axis=0)
    densities_intervals_wrap_cumvar = np.var(np.cumsum(np.array(densities_intervals_wrap),axis=2),axis=0)
    densities_critical_cumvar = np.var(np.cumsum(np.array(densities_critical),axis=2),axis=0)
    densities_simplices_alpha_cumvar = np.var(np.cumsum(np.array(densities_simplices_alpha),axis=2),axis=0)
    densities_simplices_wrap_cumvar = np.var(np.cumsum(np.array(densities_simplices_wrap),axis=2),axis=0)
    
    #store to file
    output_filename = os.path.splitext(filename)[0]+'_densities.txt' 
    with open(output_filename, 'wb') as csvfile: #'wb' instead of 'w' to avoid additional line breaks
        writer = csv.writer(csvfile, delimiter=',')
        #parameters
        writer.writerow(param.keys())
        writer.writerow(param.values())
        #bins of filtration values
        writer.writerow(['r_bins:', ' min',' max',' num'])
        writer.writerow([r_min,r_max,r_num])
        
        #data curves: mean and variances
        #*intervals of different types
        writer.writerow([])
        writer.writerow(['# intervals (Delaunay complex)'])
        for i,typ in enumerate(types_intervals):
            writer.writerow(['type'])
            writer.writerow(typ)
            writer.writerow(['mean over trials'])
            writer.writerow(densities_intervals_alpha_mean[i])
            writer.writerow(['variance of pdf'])
            writer.writerow(densities_intervals_alpha_var[i])
            writer.writerow(['variance of cdf'])
            writer.writerow(densities_intervals_alpha_cumvar[i])
            writer.writerow([])
        writer.writerow(['# intervals (Wrap complex)'])
        for i,typ in enumerate(types_intervals):
            writer.writerow(['type'])
            writer.writerow(typ)
            writer.writerow(['mean over trials'])
            writer.writerow(densities_intervals_wrap_mean[i])
            writer.writerow(['variance of pdf'])
            writer.writerow(densities_intervals_wrap_var[i])
            writer.writerow(['variance of cdf'])
            writer.writerow(densities_intervals_wrap_cumvar[i])
        #*critical simplices, type = (dim, dim, pos?)
        writer.writerow([])
        writer.writerow(['# critical simplices'])
        for i,typ in enumerate(types_critical):
            writer.writerow(['type'])
            writer.writerow(typ)
            writer.writerow(['mean over trials'])
            writer.writerow(densities_critical_mean[i])
            writer.writerow(['variance of pdf'])
            writer.writerow(densities_critical_var[i])
            writer.writerow(['variance of cdf'])
            writer.writerow(densities_critical_cumvar[i])
        #*simplices of different dim    
        writer.writerow([])
        writer.writerow(['# simplices (Delaunay complex)'])
        for i,di in enumerate(dims):
            writer.writerow(['dim'])
            writer.writerow([di])
            writer.writerow(['mean over trials'])
            writer.writerow(densities_simplices_alpha_mean[i])
            writer.writerow(['variance of pdf'])
            writer.writerow(densities_simplices_alpha_var[i])
            writer.writerow(['variance of cdf'])
            writer.writerow(densities_simplices_alpha_cumvar[i])
        writer.writerow([])
        writer.writerow(['# simplices (Wrap complex)'])
        for i,di in enumerate(dims):
            writer.writerow(['dim'])
            writer.writerow([di])
            writer.writerow(['mean over trials'])
            writer.writerow(densities_simplices_wrap_mean[i])
            writer.writerow(['variance of pdf'])
            writer.writerow(densities_simplices_wrap_var[i])
            writer.writerow(['variance of cdf'])
            writer.writerow(densities_simplices_wrap_cumvar[i])
        #*Betti numbers
        writer.writerow([])
        writer.writerow(['Betti numbers'])
        for i,di in enumerate(dims):
            writer.writerow(['dim'])
            writer.writerow([di-1])
            writer.writerow(['mean over trials'])
            writer.writerow(curves_betti_mean[i])
            writer.writerow(['variance'])
            writer.writerow(curves_betti_var[i])
        
    print output_filename   
    
#+--------------------------+
#|   LOWER SET STATISTICS   |
#+--------------------------+

def compute_lowersets(filename, interval_header, interval_data, param, numsimp_max = 1000, diam_max = 50., diam_nbins = 250):
    """
    compute distribution of lower set size
     input: * interval_header, interval_data: extracted from output data
            * param: dictionary containing parameter values 'dim', '# trials', 'window size', 'periodic', 'lambda'
     output: distributions written to file
    """
    dim = param['dim']
    #types of singular intervals (dim, dim, pos?), disregarding pos/neg if len==2, all if == [0]
    if dim==3:
        types_lowersets = [[0],[1,1],[2,2],[3,3],[1,1,0],[1,1,1],[2,2,0],[2,2,1],[3,3,0]]
    else:
        types_lowersets = [[0],[1,1],[2,2],[1,1,0],[1,1,1],[2,2,0]]
    
    percentiles = [75,50,25,10,5,1]
    
    #initialize data arrays
    distr_numsimp = []
    distr_diam = []
    max_numsimp = []
    max_diam = []
    corr_numsimp_r = []
    corr_diam_r = []
    percentiles_numsimp = []
    percentiles_diam = []
    
    diam_bins = np.linspace(0,diam_max,diam_nbins+1)
        
    #get data for every trial
    for trial in range(len(interval_data)):
        distr_numsimp_trial = []
        distr_diam_trial = []
        max_numsimp_trial = []
        max_diam_trial = []
        corr_numsimp_r_trial = []
        corr_diam_r_trial = []
        percentiles_numsimp_trial = []
        percentiles_diam_trial = []
        
        for typ in types_lowersets:
            #extract lower set size (# simplices and diamter) for all singular intervals of this type
            #print [row[2:5] for row in singular_intervals]
            if len(typ)==3:
                singular_intervals_type = np.array([row for row in interval_data[trial] if len(row)>4 and row[2:5]==typ])
            elif len(typ)==2:
                singular_intervals_type = np.array([row for row in interval_data[trial] if len(row)>4 and row[2:4]==typ])
            elif len(typ)==1:
                singular_intervals_type = np.array([row for row in interval_data[trial] if len(row)>4])
            rs = singular_intervals_type[:,0]
            numsimp_trial = singular_intervals_type[:,6]
            diam_trial = singular_intervals_type[:,8]
            
            #compute histogram, maximum, correlations and percentiles
            hist = np.histogram(numsimp_trial,bins=range(numsimp_max+1))[0]
            distr_numsimp_trial.append(hist)
            hist = np.histogram(diam_trial,bins=diam_bins)[0]
            distr_diam_trial.append(hist)
            
            max_numsimp_trial.append(np.max(numsimp_trial))
            max_diam_trial.append(np.max(diam_trial))
            
            corr = np.corrcoef(rs,numsimp_trial)[0,1]
            corr_numsimp_r_trial.append(corr)
            corr = np.corrcoef(rs,diam_trial)[0,1]
            corr_diam_r_trial.append(corr)
            
            perc = np.array([np.percentile(np.array(numsimp_trial),100-p) for p in percentiles])
            percentiles_numsimp_trial.append(perc)
            perc = np.array([np.percentile(np.array(diam_trial),100-p) for p in percentiles])
            percentiles_diam_trial.append(perc)
         
        distr_numsimp.append(distr_numsimp_trial)
        distr_diam.append(distr_diam_trial)
        max_numsimp.append(max_numsimp_trial)
        max_diam.append(max_diam_trial)
        corr_numsimp_r.append(corr_numsimp_r_trial)
        corr_diam_r.append(corr_diam_r_trial)
        percentiles_numsimp.append(percentiles_numsimp_trial)
        percentiles_diam.append(percentiles_diam_trial)
      
    #percentiles over full data (not mean over trials)
    percentiles_numsimp_all = []
    percentiles_diam_all = []
    for typ in types_lowersets:
        numsimp_all = []
        diam_all = []
        
        for trial in range(len(interval_data)):
            if len(typ)==3:
                singular_intervals_type = np.array([row for row in interval_data[trial] if len(row)>4 and row[2:5]==typ])
            elif len(typ)==2:
                singular_intervals_type = np.array([row for row in interval_data[trial] if len(row)>4 and row[2:4]==typ])
            elif len(typ)==1:
                singular_intervals_type = np.array([row for row in interval_data[trial] if len(row)>4])
            rs = singular_intervals_type[:,0]
            numsimp_trial = singular_intervals_type[:,6]
            diam_trial = singular_intervals_type[:,8]
            numsimp_all.extend(numsimp_trial)
            diam_all.extend(diam_trial)
            
        perc = np.array([np.percentile(np.array(numsimp_all),100-p) for p in percentiles])
        percentiles_numsimp_all.append(perc)
            perc = np.array([np.percentile(np.array(diam_all),100-p) for p in percentiles])
        percentiles_diam_all.append(perc)
        
    #compute mean over trials
    distr_numsimp_mean = np.mean(np.array(distr_numsimp),axis=0)
    distr_diam_mean = np.mean(np.array(distr_diam),axis=0)
    max_numsimp_mean = np.mean(np.array(max_numsimp),axis=0)
    max_diam_mean = np.mean(np.array(max_diam),axis=0)
    max_numsimp_max = np.max(np.array(max_numsimp),axis=0)
    max_diam_max = np.max(np.array(max_diam),axis=0)
    corr_numsimp_r_mean = np.mean(corr_numsimp_r, axis=0)
    corr_diam_r_mean = np.mean(corr_diam_r, axis=0)
    percentiles_numsimp_mean = np.mean(percentiles_numsimp, axis=0)
    percentiles_diam_mean = np.mean(percentiles_diam, axis=0)
    
    #store to file
    output_filename = os.path.splitext(filename)[0]+'_lowerset.txt' 
    with open(output_filename, 'wb') as csvfile: #'wb' instead of 'w' to avoid additional line breaks
        writer = csv.writer(csvfile, delimiter=',')
        #parameters
        writer.writerow(param.keys())
        writer.writerow(param.values())
        writer.writerow([])
        writer.writerow(['percentiles'])
        writer.writerow(percentiles)
        
        #distribution of lower set sizes
        #*# simplices
        writer.writerow([])
        writer.writerow(['# simplices'])
        writer.writerow(['max, nbins'])
        writer.writerow([numsimp_max, numsimp_max])
        for i,typ in enumerate(types_lowersets):
            writer.writerow(['type'])
            writer.writerow(typ)
            writer.writerow(['mean distribution over trials'])
            writer.writerow(distr_numsimp_mean[i])
            writer.writerow(['correlation with r'])
            writer.writerow([corr_numsimp_r_mean[i]])
            writer.writerow(['mean percentiles'])
            writer.writerow(percentiles_numsimp_mean[i])
            writer.writerow(['percentiles over all trials'])
            writer.writerow(percentiles_numsimp_all[i])
            writer.writerow(['max: avg','max over trials'])
            writer.writerow([max_numsimp_mean[i],max_numsimp_max[i]])
        #*diameter
        writer.writerow([])
        writer.writerow(['diameter'])
        writer.writerow(['max, nbins'])
        writer.writerow([diam_max, diam_nbins])
        for i,typ in enumerate(types_lowersets):
            writer.writerow(['type'])
            writer.writerow(typ)
            writer.writerow(['mean distribution over trials'])
            writer.writerow(distr_diam_mean[i])
            writer.writerow(['correlation with r'])
            writer.writerow([corr_diam_r_mean[i]])
            writer.writerow(['mean percentiles'])
            writer.writerow(percentiles_diam_mean[i])
            writer.writerow(['percentiles over all trials'])
            writer.writerow(percentiles_diam_all[i])
            writer.writerow(['max: avg','max over trials'])
            writer.writerow([max_diam_mean[i],max_diam_max[i]])


# write info about overlapping lower sets for every trial to file
def save_overlap_data(filename, param, overlapping_header, overlapping_data):
    """
    write info about overlapping lower sets for every trial to file
     input: * filename (of input data)
            * param: dictionary containing parameter values 'dim', '# trials', 'window size', 'periodic', 'lambda'
            * overlapping_header,overlapping_data: data about # intervals in overlapping lower sets, 
                 header contains column titles
             * interval_header,interval_data: data about singular intervals, header contains column titles
     output file: filename + '_overlap.txt'
    """
    output_filename = os.path.splitext(filename)[0]+'_overlap.txt' 
    
    with open(output_filename, 'wb') as csvfile: #'wb' instead of 'w' to avoid additional line breaks
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow(param.keys())
        writer.writerow(param.values())
        writer.writerow(overlapping_header)
        for i,trial_data in enumerate(overlapping_data):
            writer.writerow(trial_data[0].tolist())
    print output_filename

    
#+----------+
#|   MAIN   |
#+----------+
                
def main():
    #INPUT
    if len(sys.argv)!= 2:
        print "1 argument expected: filename"
        return
    else:
        filename=str(sys.argv[1])
        
    filename = filename.replace("'",'').replace('[', '').replace(']','').replace(' ','').split(',')
    print len(filename), filename

                   
    #load raw data               
    filename, param, [full_header,full_data], [interval_header,interval_data], [overlapping_header,overlapping_data]  \
    = load_output_data(filename)
    dim = param['dim']
    expected_num_points = param['lambda']*param['window size']**param['dim']
    
    #store data about full complexes
    save_full_data(filename, param, full_header, full_data)
    
    #statistics over filtration
    t0 = time.clock()
    compute_densities(filename, interval_header, interval_data, full_header, full_data, param, r_min=0, r_max=2.5, r_num=250)
    duration = time.clock() - t0; print 'time elapsed: %.4f' % duration
    
    #release memory
    #del interval_data
    
    if dim==2:
        numsimp_max = 100
        diam_max = 6.
        diam_nbins = 300
    else:
        numsimp_max = 1000
        diam_max = 6.
        diam_nbins = 300
    
    compute_lowersets(filename, interval_header, interval_data, param, numsimp_max = numsimp_max, diam_max = diam_max, diam_nbins = diam_nbins)
    
    t0 = time.clock()
    save_overlap_data(filename, param, overlapping_header, overlapping_data)
    duration = time.clock() - t0; print 'time elapsed: %.4f' % duration


main()